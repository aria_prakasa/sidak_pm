<?php
define('ASSETS_PATH', FCPATH . "assets/");
define('ASSETS_URL', "/assets/");
define('SITE_UPLOAD_PATH', ASSETS_PATH . "images/");
define('SITE_UPLOAD_URL', ASSETS_URL . "images/");
define('SITE_UPLOAD_KCFINDER_PATH', ASSETS_PATH . "uploads/");
define('SITE_UPLOAD_KCFINDER_URL', ASSETS_URL . "uploads/");
