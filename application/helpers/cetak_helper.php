<?php

function gen_option_list($arr,$row_length=2)
{
  $arr_reset = $tmp_val = NULL;
  $i=1;
  foreach($arr as $val)
  {
    $tmp_val[] = $val;
    if($i % $row_length == 0)
    {
      $arr_reset[] = implode("<br />", $tmp_val);
      $tmp_val = NULL;
    }
    if($i == count($arr))
    {
      $arr_reset[] = implode("<br />", $tmp_val);
    }
    $i++;
  }
  $tmp = '<table width="100%" class="nomargin">
      <tr valign="top">
      <td valign="top">
      '.implode("</td><td>", $arr_reset).'
      </td>
      </tr>
      </table>';
  return $tmp;
}

function gen_option_value($arr,$value,$length=2)
{
  $tmp_arr = NULL;
  foreach($arr as $key => $val)
  {
    /*if ($key == '' && $key != null) {
      continue;
    }*/
    if ($val == '- Pilih Opsi -') {
      continue;
    }
    $tmp_arr[] = $val;
    // $tmp_arr[] = $key.". ".$val;
  }
  // $tmp = '<div style="float:left;width:40px;">
  // <div class="border" style="margin-right:10px;">'.$value.'</div>
  // </div>
  // <div style="overflow:hidden;">
  // <div class="border border-white">'.gen_option_list($tmp_arr,$length).'</div>
  // </div>
  // ';
  $tmp = '<table width="100%" cellpadding=0 cellspacing=0 border=0 style="margin-bottom: 2px;">
  <tr class="vtop">
    <td width="30">
      <table width="100%" cellpadding=0 cellspacing=0 border=0 class="border" style="margin-bottom: 2px;">
        <tr class="vtop">
          <td>'.$value.'</td>
        </tr>
      </table>
    </td>
    <td>'.gen_option_list($tmp_arr,$length).'</td>
  </tr>
  </table>
  ';
  return $tmp;
}

function gen_option_single_value($value,$suffix)
{
  // $tmp = '<div style="float:left;width:40px;">
  // <div class="border" style="margin-right:10px;">'.$value.'</div>
  // </div>
  // <div style="overflow:hidden">
  // <div class="pull-left">'.$suffix.'</div>
  // </div>
  // ';
  $tmp = '<table width="100%" cellpadding=0 cellspacing=0 border=0 style="margin-bottom: 2px;">
  <tr class="vtop">
    <td width="40">
      <table width="100%" cellpadding=0 cellspacing=0 border=0 style="margin-bottom: 2px;">
        <tr class="vtop">
          <td>'.$value.'</td>
        </tr>
      </table>
    </td>
    <td>'.$suffix.'</td>
  </tr>
  </table>
  ';
  return $tmp;
}

function gen_option_single_value_long($value,$suffix)
{
  // $tmp = '<div style="float:left;width:250px;">
  // <div class="border">'.$value.'</div>
  // </div>
  // <div style="overflow:hidden">
  // <div class="pull-left">'.$suffix.'</div>
  // </div>
  // ';
  $tmp = '<table width="100%" cellpadding=0 cellspacing=0 border=0 style="margin-bottom: 2px;">
  <tr class="vtop">
    <td width="500">
      <table width="100%" cellpadding=0 cellspacing=0 border=0 style="margin-bottom: 2px;">
        <tr class="vtop">
          <td>'.$value.'</td>
        </tr>
      </table>
    </td>
    <td>'.$suffix.'</td>
  </tr>
  </table>
  ';
  return $tmp;
}

function gen_option_double_value($value,$subvalue)
{
  $tmp = '<table width="100%" cellpadding=0 cellspacing=0 border=0>
  <tr class="vtop">
    <td width="40">
      <table width="100%" cellpadding=0 cellspacing=0 border=0 class="border">
        <tr class="vtop">
          <td>'.$value.'</td>
        </tr>
      </table>
    </td>
    <td width="50"> tahun </td>
    <td width="40">
      <table width="100%" cellpadding=0 cellspacing=0 border=0 class="border">
        <tr class="vtop">
          <td>'.$subvalue.'</td>
        </tr>
      </table>
    </td>
    <td> bulan </td>
  </tr>
  </table>
  ';
  return $tmp;
}

function gen_option_dropdown($i,$key,$post,$opsi,$value)
{
  echo "<tr>  ";
  echo "<td><b>".$i.".</b></td>  ";
  echo "<td width='60%'><b>".$key."</b></td>  ";
  echo "<td colspan='2' width='40%'> ";
  $style='class="form-control input-sm" required';
  echo form_dropdown($post,$opsi,$value,$style);
  echo "</td></tr>  ";

}

function print_box($str, $max=3) {
  $str = trim($str);
  if(empty($str)) return;
  if($max > 0 && strlen($str) != $max) {
    $str = str_pad($str, $max, "$", STR_PAD_LEFT);
  }
  // var_dump($str); die;
  $str_arr = str_split($str);
  $ret = '<table class="boxes full"><tr align="right">';
  foreach ($str_arr as $str) {
    if($str == "$") $ret .= '<td>&nbsp;&nbsp;&nbsp;</td>';
    else if($str == ".") $ret .= '<td class="border">&nbsp;&nbsp;&nbsp;</td>';
    else $ret .= '<td class="border">'.$str.'</td>';
  }
  $ret .= '</tr></table>';
  return $ret;
}

function print_box_original($str, $max=3) {
  if(empty($str)) return;
  if($max > 0 && strlen($str) != $max) {
    $str = str_pad($str, $max, "$", STR_PAD_LEFT);
  }
  // var_dump($str); die;
  $str_arr = str_split($str);
  $ret = '<table class="boxes full"><tr align="right">';
  foreach ($str_arr as $str) {
    if($str == "$") $ret .= '<td>&nbsp;&nbsp;&nbsp;</td>';
    elseif($str == "/") $ret .= '<td>&nbsp;/&nbsp;</td>';
    else if($str == ".") $ret .= '<td class="border">&nbsp;&nbsp;&nbsp;</td>';
    else $ret .= '<td class="border">'.$str.'</td>';
  }
  $ret .= '</tr></table>';
  return $ret;
}

function print_box_single($str) {
  $str = trim($str);
  if(empty($str)) return;
  $ret = '<table class="boxes full"><tr align="right">';
  $ret .= '<td class="border">'.$str.'</td>';
  $ret .= '</tr></table>';
  return $ret;
}

function print_box_double($str, $max=2) {
  $str = trim($str);
  if(empty($str)) return;
  if($max > 0 && strlen($str) != $max) {
    $str = str_pad($str, $max, "$", STR_PAD_LEFT);
  }
  // var_dump($str); die;
  $str_arr = str_split($str);
  $ret = '<table class="boxes full"><tr align="right">';
  foreach ($str_arr as $str) {
    if($str == "$") $ret .= '<td>&nbsp;&nbsp;&nbsp;</td>';
    else if($str == ".") $ret .= '<td class="border">&nbsp;&nbsp;&nbsp;</td>';
    else $ret .= '<td class="border">'.$str.'</td>';
  }
  $ret .= '</tr></table>';
  return $ret;
}
