<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* NOTIFICATION HELPER SECTION */

function warning($a, $b="success")
{
	$CI =& get_instance();
	switch ($a)
	{
		case "add":
			$str = "added";
			break;
		case "edit":
			$str = "updated";
			break;
		case "delete":
			$str = "deleted";
			break;
		case "cancel":
			$str = "canceled";
			break;
		case "active":
			$str = "activated";
			break;
		case "inactive":
			$str = "inactivated";
			break;
		default:
			$str = "";
			break;
	}
	if($str != "")
	{
		if($b == "success") $warning_string = "The data has been $str";
		else if($b == "fail") $warning_string = "The data can not be $str";
		else $warning_string = "";
	}
	if($warning_string == "") return "";
	else {
		if($b == "fail") return $CI->session->set_flashdata('warning', $warning_string);
		if($b == "success") return $CI->session->set_flashdata('success', $warning_string);
	}
}

function print_error($str)
{
	if(empty($str)) return;
	return '<div class="alert alert-danger alert-dismissable">
	  <i class="fa fa-ban"></i>
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	  <b>Alert!</b><br>'.$str.'
	</div>';
}

function print_success($str)
{
	if(empty($str)) return;
	return '<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Success!</b><br>'.$str.'
  </div>';
}

function print_warning($str)
{
	if(empty($str)) return;
	return '<div class="alert alert-warning alert-dismissable">
    <i class="fa fa-warning"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Warning!</b><br>'.$str.'
  </div>';
}

function print_info($str)
{
	if(empty($str)) return;
	return '<div class="alert alert-info alert-dismissable">
	  <i class="fa fa-info"></i>
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	  <b>Info!</b><br>'.$str.'
	</div>';
}

function print_status()
{
	$CI =& get_instance();
	if($CI->session->flashdata('success') != "") echo print_success($CI->session->flashdata('success'));
	if($CI->session->flashdata('error') != "") echo print_error($CI->session->flashdata('error'));
	if($CI->session->flashdata('warning') != "") echo print_warning($CI->session->flashdata('warning'));
	if($CI->session->flashdata('info') != "") echo print_info($CI->session->flashdata('info'));
}
