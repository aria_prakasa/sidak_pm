<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* FORM HELPER SECTION */

function radios($name,$arr=array(),$selected_value="",$extraparam="",$separator="<br />")
{
	foreach($arr as $val => $display)
	{
		if($val == $selected_value) $selected = 'checked="checked"'; else $selected = "";
		$ret .= "<label for='{$name}_{$val}'><input type='radio' id='{$name}_{$val}' name='$name' value='$val' $selected $extraparam /> ".$display."</label>".$separator;
	}
	return $ret;
}

function checkboxes($name,$arr,$selected_values = array(), $optional = "", $separator = "<br />")
{
	$id = str_replace("[]","",$name);
	foreach($arr as $val => $display)
	{
		if(in_array($val,$selected_values)) { $checked = 'checked="checked"'; } else $checked = '';
		$ret .= "<label for='{$id}_".url_title($val)."'><input type='checkbox' value='{$val}' name='{$name}' id='{$id}_".url_title($val)."' $checked> $display</label>".$separator;
	}
	return $ret;
}

function dropdown($name,$arr,$selected_value = "", $optional = "", $default_value="",$readonly = FALSE)
{
	$ret = "<select ".($readonly ? "" : "name='{$name}'")." id='{$name}' $optional ".($readonly ? "disabled='disabled'" : "").">";
	if(trim($default_value) != "") $ret .= "<option value=''>$default_value</option>";
	foreach($arr as $val => $display)
	{
		$selected = '';
		if(is_array($selected_value))
		{
			if(in_array($val, $selected_value)) $selected = 'selected="selected"';
		}
		else
		{
			if($val == $selected_value) $selected = 'selected="selected"';
		}
		$ret .= "<option value='{$val}' $selected>$display</option>";
	}
	$ret .= "</select>";
	if($readonly) $ret .= "<input type='hidden' name='{$name}' value='{$selected_value}' />";
	return $ret;
}

function password_strength_check($pwd)
{
	$error = NULL;
	if( strlen($pwd) < 8 ) {
		$error[] = "Password too short!";
	}
	
	if( strlen($pwd) > 20 ) {
		$error[] = "Password too long!";
	}
	
	if( !preg_match("#[0-9]+#", $pwd) ) {
		$error[] = "Password must include at least one number!";
	}
	
	
	if( !preg_match("#[a-z]+#", $pwd) ) {
		$error[] = "Password must include at least one letter!";
	}
	
	
	if( !preg_match("#[A-Z]+#", $pwd) ) {
		$error[] = "Password must include at least one CAPS!";
	}
	
	
	
	if( !preg_match("#\W+#", $pwd) ) {
		$error[] = "Password must include at least one symbol!";
	}
	
	
	if(count($error) > 0){
		return $error;
	} else {
		return TRUE;
	}	
}

function password_check($pwd)
{
	$error = NULL;

	if (preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$#", $pwd)){
		return TRUE;
	} else {
		return FALSE;
	}

}

function get_batch_buttons($button_arr = array("delete"))
{
	$tmp = '<p>Checked Action: '; 
	if(in_array("delete", $button_arr))
	{
		$tmp .= ' <button type="submit" class="btn btn-danger btn-xs" name="batch_submit" value="delete">Delete</button> ';
	}
	if(in_array("active", $button_arr))
	{
		$tmp .= ' <button type="submit" class="btn btn-success btn-xs" name="batch_submit" value="active">Make Active</button> ';
	}
	if(in_array("inactive", $button_arr))
	{
		$tmp .= ' <button type="submit" class="btn btn-warning btn-xs" name="batch_submit" value="inactive">Make Inactive</button> ';
	}
	if(in_array("new", $button_arr))
	{
		$tmp .= ' <button type="submit" class="btn btn-success btn-xs" name="batch_submit" value="new">Mark as New</button>';
	}
	if(in_array("not_new", $button_arr))
	{
		$tmp .= ' <button type="submit" class="btn btn-warning btn-xs" name="batch_submit" value="not_new">Un-Mark as New</button>';
	}
	if(in_array("sale", $button_arr))
	{
		$tmp .= ' <button type="submit" class="btn btn-success btn-xs" name="batch_submit" value="sale">Mark on Sale</button> ';
	}
	if(in_array("not_sale", $button_arr))
	{
		$tmp .= ' <button type="submit" class="btn btn-warning btn-xs" name="batch_submit" value="not_sale">Un-Mark on Sale</button> ';
	}
	if(in_array("featured", $button_arr))
	{
		$tmp .= ' <button type="submit" class="btn btn-success btn-xs" name="batch_submit" value="featured">Mark as Featured</button> ';
	}
	if(in_array("not_featured", $button_arr))
	{
		$tmp .= ' <button type="submit" class="btn btn-warning btn-xs" name="batch_submit" value="not_featured">Un-Mark as Featured</button> ';
	}
	$tmp .= '</p>';
	return $tmp;
}

function get_batch_msg($action="delete")
{
	switch($action)
	{
		case "delete":
			$tmp = "The items deleted successfully";
		break;
		case "active":
			$tmp = "The items made Active successfully";
		break;
		case "inactive":
			$tmp = "The items made Inactive successfully";
		break;
		case "new":
			$tmp = "The items made New successfully";
		break;
		case "sale":
			$tmp = "The items made on Sale successfully";
		break;
		case "featured":
			$tmp = "The items made Featured successfully";
		break;
		default:
			$tmp = "The items changed successfully";
		break;
	}
	return $tmp;
}
