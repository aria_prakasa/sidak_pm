<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function array_to_excel($filename, $header, $body_arr, $output_type = "xls")
{
    $CI = &get_instance();

    //check valid output type
    $valid_ext = array("xls", "xlsx");
    if (!in_array($output_type, $valid_ext)) {
        $output_type = "xls";
    }

    /* USING: PHP EXCEL CLASS */
    //load our new PHPExcel library
    $CI->load->library('excel');

    //activate worksheet number 1
    $CI->excel->setActiveSheetIndex(0);
    //name the worksheet
    $CI->excel->getActiveSheet()->setTitle('Report');
    /*
    //set cell A1 content with some text
    $this->excel->getActiveSheet()->setCellValue('A1', 'Order Report');
    //change the font size
    $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
    //make the font become bold
    $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    //merge cell A1 until D1
    $this->excel->getActiveSheet()->mergeCells('A1:D1');
    //set aligment to center for that merged cell (A1 to D1)
    $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    //*/

    $alphabets      = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $alphabet_total = strlen($alphabets);
    $abjads         = null;
    for ($i = 0; $i < $alphabet_total; $i++) {
        $abjads[] = substr($alphabets, $i, 1);
    }
    $col         = 1;
    $abjad_start = 0;

    // LIMIT 25 columns
    if (count($header) > count($abjads)) {
        return;
    }

    // HEADER
    foreach ($header as $val) {
        $CI->excel->setActiveSheetIndex(0)->setCellValue($abjads[$abjad_start] . $col, strtoupper($val));
        $abjad_start++;
    }
    $col++;
    // BODY
    foreach ($body_arr as $row) {
        $abjad_start = 0;
        foreach ($row as $val) {
            $CI->excel->setActiveSheetIndex(0)->setCellValue($abjads[$abjad_start] . $col, $val);
            $abjad_start++;
        }
        $col++;
    }

    //$filename='just_some_random_name.xls'; //save our workbook as this file name
    $filename = str_replace(array(".csv", ".xls", ".xlsx"), "", $filename) . ".{$output_type}";
    if ($output_type == "xls") {
        header('Content-Type: application/vnd.ms-excel'); //mime type for Excel5 format
    } elseif ($output_type == "xlsx") {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type for Excel2007 format
    }
    header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache

    //save it to 'Excel5' format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
    //if you want to save it as .XLSX Excel 2007 format
    if ($output_type == "xls") {
        $objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');
    } elseif ($output_type == "xlsx") {
        $objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel2007');
    }
    //var_dump($objWriter);
    //force user to download the Excel file without writing it to server's HD
    $objWriter->save('php://output');
    exit;
    //return $objWriter;
}

function excel_to_array($file_path)
{
    $CI = &get_instance();

    //load the excel library
    $CI->load->library('excel');
    //read file from path
    $objPHPExcel = PHPExcel_IOFactory::load($file_path);
    //get only the Cell Collection
    $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
    //extract to a PHP readable array format
    foreach ($cell_collection as $cell) {
        $column     = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
        $row        = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
        $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
        //header will/should be in row 1 only. of course this can be modified to suit your need.
        if ($row == 1) {
            $header[$row][$column] = $data_value;
        } else {
            $arr_data[$row][$column] = $data_value;
        }
    }
    //send the data in an array format
    $data['header'] = $header;
    $data['values'] = $arr_data;
    return $data;
    // - See more at: http://arjunphp.com/how-to-use-phpexcel-with-codeigniter/
}
