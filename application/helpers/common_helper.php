<?php
function &keysToLower(&$obj) {
	$type = (int) is_object($obj) - (int) is_array($obj);
	if ($type === 0) {
		return $obj;
	}

	foreach ($obj as $key => &$val) {
		$element = keysToLower($val);
		switch ($type) {
		case 1:
			if (!is_int($key) && $key !== ($keyLowercase = strtolower($key))) {
				unset($obj->{$key});
				$key = $keyLowercase;
			}
			$obj->{$key} = $element;
			break;
		case -1:
			if (!is_int($key) && $key !== ($keyLowercase = strtolower($key))) {
				unset($obj[$key]);
				$key = $keyLowercase;
			}
			$obj[$key] = $element;
			break;
		}
	}
	return $obj;
}

// Konversi waktu ke : Senin, 4 Januari 2014
function format_hari_tanggal($waktu) {
	// Senin, Selasa dst.
	$hari_array = array(
		'Minggu',
		'Senin',
		'Selasa',
		'Rabu',
		'Kamis',
		'Jumat',
		'Sabtu',
	);
	$hr = date('w', strtotime($waktu));
	$hari = $hari_array[$hr];

	// Tanggal: 1-31 dst, tanpa leading zero.
	$tanggal = date('j', strtotime($waktu));

	// Bulan: Januari, Maret dst.
	$bulan_array = array(
		1 => 'Januari',
		2 => 'Februari',
		3 => 'Maret',
		4 => 'April',
		5 => 'Mei',
		6 => 'Juni',
		7 => 'Juli',
		8 => 'Agustus',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Desember',
	);
	$bl = date('n', strtotime($waktu));
	$bulan = $bulan_array[$bl];

	// Tahun, 4 digit.
	$tahun = date('Y', strtotime($waktu));

	// Hasil akhir: Senin, 1 Oktober 2014
	return "$hari, $tanggal $bulan $tahun";
}

// Format tangal ke 1 Januari 1990
function format_tanggal($waktu) {
	// Tanggal, 1-31 dst, tanpa leading zero.
	$tanggal = date('j', strtotime($waktu));

	// Bulan, Januari, Maret dst
	$bulan_array = array(
		1 => 'Januari',
		2 => 'Februari',
		3 => 'Maret',
		4 => 'April',
		5 => 'Mei',
		6 => 'Juni',
		7 => 'Juli',
		8 => 'Agustus',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Desember',
	);
	$bl = date('n', strtotime($waktu));
	$bulan = $bulan_array[$bl];

	// Tahun
	$tahun = date('Y', strtotime($waktu));

	// Senin, 12 Oktober 2014
	return "$tanggal $bulan $tahun";
}

// Format tangal ke yyyy-mm-dd
function date_to_en($tanggal) {
	$tgl = date('Y-m-d', strtotime($tanggal));
	if ($tgl == '1970-01-01') {
		return '';
	} else {
		return $tgl;
	}
}

// Format tangal ke dd-mm-yyyy
function date_to_id($tanggal) {
	$tgl = date('d-m-Y', strtotime($tanggal));
	if ($tgl == '01-01-1970') {
		return '';
	} else {
		return $tgl;
	}
}

// Status user
function user_desc($user_level) {
	if ($user_level == '0') {
		return 'Administrator';
	} elseif ($user_level == '1') {
		return 'Operator Kota';
	} elseif ($user_level == '2') {
		return 'Operator Kecamatan';
	} elseif ($user_level == '3') {
		return 'Operator Kelurahan';
	} elseif ($user_level == '4') {
		return 'Supervisor';
	} else {
		return ucwords($user_level);
	}
}

// Buat setiap awal kata huruf besar
function format_title_case($string) {
	return ucwords($string);
}

function format_agama($agama) {
	if ($agama == '7') {
		return 'KEPERCAYAAN LAINNYA';
	} elseif ($agama == '1') {
		return 'ISLAM';
	} elseif ($agama == '2') {
		return 'KRISTEN';
	} elseif ($agama == '3') {
		return 'KATHOLIK';
	} elseif ($agama == '4') {
		return 'HINDU';
	} elseif ($agama == '5') {
		return 'BUDHA';
	} elseif ($agama == '6') {
		return 'KONGHUCU';
	} else {
		return ucwords($agama);
	}
}

function format_goldarah($gol_drh) {
	if ($gol_drh == '1') {
		return 'A';
	} elseif ($gol_drh == '2') {
		return 'B';
	} elseif ($gol_drh == '3') {
		return 'AB';
	} elseif ($gol_drh == '4') {
		return 'O';
	} elseif ($gol_drh == '5') {
		return 'A+';
	} elseif ($gol_drh == '6') {
		return 'A-';
	} elseif ($gol_drh == '7') {
		return 'B+';
	} elseif ($gol_drh == '8') {
		return 'B-';
	} elseif ($gol_drh == '9') {
		return 'AB+';
	} elseif ($gol_drh == '10') {
		return 'AB-';
	} elseif ($gol_drh == '11') {
		return 'O+';
	} elseif ($gol_drh == '12') {
		return 'O-';
	} elseif ($gol_drh == '13') {
		return 'TIDAK TAHU';
	} else {
		return ucwords($gol_drh);
	}
}

function format_pendidikan($pddk_akh) {
	if ($pddk_akh == '1') {
		return 'TIDAK/BELUM SEKOLAH';
	} elseif ($pddk_akh == '2') {
		return 'BELUM TAMAT SD/SEDERAJAT';
	} elseif ($pddk_akh == '3') {
		return 'TAMAT SD/SEDERAJAT';
	} elseif ($pddk_akh == '4') {
		return 'SLTP/SEDERAJAT';
	} elseif ($pddk_akh == '5') {
		return 'SLTA/SEDERAJAT';
	} elseif ($pddk_akh == '6') {
		return 'DIPLOMA I/II';
	} elseif ($pddk_akh == '7') {
		return 'AKADEMI/DIPLOMA III/S. MUDA';
	} elseif ($pddk_akh == '8') {
		return 'DIPLOMA IV/STRATA I';
	} elseif ($pddk_akh == '9') {
		return 'STRATA II';
	} elseif ($pddk_akh == '10') {
		return 'STRATA III';
	} else {
		return ucwords($pddk_akh);
	}
}

function format_shdk($stat_hbkel) {
	if ($stat_hbkel == '1') {
		return 'KEPALA KELUARGA';
	} elseif ($stat_hbkel == '2') {
		return 'SUAMI';
	} elseif ($stat_hbkel == '3') {
		return 'ISTRI';
	} elseif ($stat_hbkel == '4') {
		return 'ANAK';
	} elseif ($stat_hbkel == '5') {
		return 'MENANTU';
	} elseif ($stat_hbkel == '6') {
		return 'CUCU';
	} elseif ($stat_hbkel == '7') {
		return 'ORANG TUA';
	} elseif ($stat_hbkel == '8') {
		return 'MERTUA';
	} elseif ($stat_hbkel == '9') {
		return 'FAMILI LAIN';
	} elseif ($stat_hbkel == '10') {
		return 'PEMBANTU';
	} elseif ($stat_hbkel == '11') {
		return 'LAINNYA';
	} else {
		return ucwords($stat_hbkel);
	}
}

function format_stat_hbkel($stat_hbkel) {
	if ($stat_hbkel == '1') {
		return 'KEPALA KELUARGA';
	} elseif ($stat_hbkel == '2') {
		return 'SUAMI / ISTRI';
	} elseif ($stat_hbkel == '3') {
		return 'ANAK';
	} elseif ($stat_hbkel == '4') {
		return 'MENANTU';
	} elseif ($stat_hbkel == '5') {
		return 'CUCU';
	} elseif ($stat_hbkel == '6') {
		return 'ORANG TUA / MERTUA';
	} elseif ($stat_hbkel == '7') {
		return 'PEMBANTU RUTA';
	} elseif ($stat_hbkel == '8') {
		return 'LAINNYA';
	} else {
		return ucwords($stat_hbkel);
	}
}

function format_stat_rtangga($stat_hbkel) {
	if ($stat_hbkel == '1') {
		return 'KEPALA RUMAH TANGGA';
	} elseif ($stat_hbkel == '2') {
		return 'SUAMI / ISTRI';
	} elseif ($stat_hbkel == '3') {
		return 'ANAK';
	} elseif ($stat_hbkel == '4') {
		return 'MENANTU';
	} elseif ($stat_hbkel == '5') {
		return 'CUCU';
	} elseif ($stat_hbkel == '6') {
		return 'ORANG TUA / MERTUA';
	} elseif ($stat_hbkel == '7') {
		return 'PEMBANTU RUTA';
	} elseif ($stat_hbkel == '8') {
		return 'LAINNYA';
	} else {
		return ucwords($stat_hbkel);
	}
}

function format_angka($angka) {
	return number_format($angka, 0);
	// return number_format($angka, 0, ',', '.' );
	// return number_format($number);
}

function get_filter_location_where($params) {
	// filter current user location
	$where_arr = [];
	$where_arr[] = "NO_PROP = " . NO_PROP;
	$where_arr[] = "NO_KAB = " . NO_KAB;
	if (is_array($params) && count($params) > 0) {
		foreach ($params as $key => $value) {
			$key = strtolower($key);
			if (stristr($key, "kec")) {
				$where_arr[] = "NO_KEC = " . $value;
			}
			if (stristr($key, "kel")) {
				$kel_arr = explode('-', $value);
				$no_kec = $kel_arr[0];
				$value = end($kel_arr);
				$where_arr[] = "NO_KEC = " . $no_kec;
				$where_arr[] = "NO_KEL = " . $value;
			}
		}
	}
	$where = implode(" AND ", $where_arr);
	return $where;
}

function kepemilikan($status) {
	if ($status == '1') {
		return 'ADA';
	} elseif ($status == '0') {
		return 'TIDAK ADA';
	}else {
		return ucwords($status);
	}
}

function bantuan($status) {
	if ($status == '1') {
		return 'PUSAT';
	} elseif ($status == '2') {
		return 'DAERAH';
	}else {
		return ucwords($status);
	}
}

function formatkelamin($status) {
	if ($status == '1') {
		return 'L';
	} elseif ($status == '2') {
		return 'P';
	}else {
		return ucwords($status);
	}
}

function formatpekerjaan($status) {
	if ($status == '1') {
		return 'BEKERJA';
	} elseif ($status == '2') {
		return 'TIDAK BEKERJA';
	}else {
		return ucwords($status);
	}
}

function format_kdkel($desa) {
	if ($desa == 'KRAPYAKREJO') {
		return '0001';
	} elseif ($desa == 'BUKIR') {
  		return '0002';
	} elseif ($desa == 'SEBANI') {
  		return '0003';
	} elseif ($desa == 'GENTONG') {
  		return '0004';
	} elseif ($desa == 'GADINGREJO') {
  		return '0008';
	} elseif ($desa == 'PETAHUNAN') {
  		return '0009';
	} elseif ($desa == 'RANDUSARI') {
  		return '0010';
	} elseif ($desa == 'KARANGKETUG') {
  		return '0011';
	} elseif ($desa == 'POHJENTREK') {
  		return '0001';
	} elseif ($desa == 'WIROGUNAN') {
  		return '0002';
	} elseif ($desa == 'TEMBOKREJO') {
  		return '0003';
	} elseif ($desa == 'PURUTREJO') {
  		return '0004';
	} elseif ($desa == 'KEBONAGUNG') {
  		return '0005';
	} elseif ($desa == 'PURWOREJO') {
  		return '0006';
	} elseif ($desa == 'SEKARGADUNG') {
  		return '0007';
	} elseif ($desa == 'BAKALAN') {
  		return '0002';
	} elseif ($desa == 'KRAMPYANGAN') {
  		return '0003';
	} elseif ($desa == 'BLANDONGAN') {
  		return '0004';
	} elseif ($desa == 'KEPEL') {
  		return '0005';
	} elseif ($desa == 'BUGUL KIDUL') {
  		return '0006';
	} elseif ($desa == "TAPA'AN") {
  		return '0011';
	} elseif ($desa == 'PETAMANAN') {
  		return '1001';
	} elseif ($desa == 'PEKUNCEN') {
  		return '1002';
	} elseif ($desa == 'BUGUL LOR') {
  		return '1003';
	} elseif ($desa == 'KANDANGSAPI') {
  		return '1004';
	} elseif ($desa == 'BANGILAN') {
  		return '1005';
	} elseif ($desa == 'KEBONSARI') {
  		return '1006';
	} elseif ($desa == 'KARANGANYAR') {
  		return '1007';
	} elseif ($desa == 'TRAJENG') {
  		return '1008';
	} elseif ($desa == 'MAYANGAN') {
  		return '1009';
	} elseif ($desa == 'MANDARANREJO') {
  		return '1010';
	} elseif ($desa == 'PANGGUNGREJO') {
  		return '1011';
	} elseif ($desa == 'NGEMPLAKREJO') {
  		return '1012';
	} elseif ($desa == "TAMBA'AN") {
  		return '1013';
	}
}

function format_kdkec($kecamatan) {
	if ($kecamatan == 'GADINGREJO') {
		return '01';
	} elseif ($kecamatan == 'PURWOREJO') {
  		return '02';
	} elseif ($kecamatan == 'BUGUL KIDUL') {
  		return '03';
	} elseif ($kecamatan == 'PANGGUNGREJO') {
  		return '03';
	}
}