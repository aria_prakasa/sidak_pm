<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* PAGINATION HELPER */

function get_pagination_template($type="admin")
{
	if($type == "admin")
	{
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
	}
	else
	{
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
	}
	return $config;
}
function getPagination($total, $perpage, $url, $num_links = 5, $template = TRUE, $set_template=array())
{
	$CI =& get_instance();
	$CI->load->library('pagination');
	
	if(stristr($url, $_SERVER['HTTP_HOST']))
	{
		$base_url = $url;
	}
	else
	{
		$base_url = site_url($url);
	}
	$config['base_url'] 	= $base_url;
	$config['total_rows'] 	= $total;
	$config['per_page'] 	= $perpage; 
	//$config['uri_segment'] 	= $uri_segment;
	$config['num_links'] 	= $num_links;
	if($template)
	{
		if(count($set_template) <= 0) $set_template = get_pagination_template();
		foreach($set_template as $key => $val)
		{
			$config[$key] = $val;
		}
	}
	
	$config['page_query_string'] = TRUE;
	$config['query_string_segment'] = 'page';
	
	$CI->pagination->initialize($config);
	return $CI->pagination->create_links();
}
function genPagination($total, $perpage, $url, $uri_segment, $num_links = 5, $template = TRUE, $set_template=array())
{
	$CI =& get_instance();
	$CI->load->library('pagination');
	
	if(stristr($url, $_SERVER['HTTP_HOST']))
	{
		$base_url = $url;
	}
	else
	{
		$base_url = site_url($url);
	}
	$config['base_url'] 	= $base_url;
	$config['total_rows'] 	= $total;
	$config['per_page'] 	= $perpage; 
	$config['uri_segment'] 	= $uri_segment;
	$config['num_links'] 	= $num_links;
	if($template)
	{
		if(count($set_template) <= 0) $set_template = get_pagination_template();
		foreach($set_template as $key => $val)
		{
			$config[$key] = $val;
		}
	}
	$CI->pagination->initialize($config);
	return $CI->pagination->create_links();
}
