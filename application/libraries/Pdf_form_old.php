<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Pdf_form_old {
    
    function __construct()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load()
    {
        include_once APPPATH.'/third_party/mpdf60/mpdf.php';
         
        return new mPDF("en-GB-x","Folio","","",15,15,10,38,5,5);

         /*$mpdf = new mPDF('',    // mode - default ''
 '',    // format - A4, for example, default ''
 0,     // font size - default 0
 '',    // default font family
 15,    // margin_left
 15,    // margin right
 16,     // margin top
 16,    // margin bottom
 9,     // margin header
 9,     // margin footer
 'L');  // L - landscape, P - portrait*/

    }
}