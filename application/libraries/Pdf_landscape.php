<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Pdf_landscape {
    
    function __construct()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load()
    {
        include_once APPPATH.'/third_party/mpdf60/mpdf.php';
         
        return new mPDF("utf-8","Folio-L",0,"",15,15,10,10,5,5);

    }
}