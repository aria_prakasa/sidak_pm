<div class="box-body">

	<div class="row">	
		<div class="box-body">
			<div class="col-sm-12">
			    <div style="text-align: center;" class="alert alert-warning" role="alert">
			      <b>I. PENGENALAN TEMPAT</b>
			    </div>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>1.</b></td>
			    		<td width="40%"><b>Provinsi</b></td>
			    		<td><?php echo form_input('provinsi', $provinsi, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    		<td width="20%"><?php echo form_input('no_prop', $no_prop, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="2" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>2.</b></td>
			    		<td width="40%"><b>Kabupaten/Kota</b></td>
			    		<td><?php echo form_input('kabupaten', $kabupaten, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    		<td width="20%"><?php echo form_input('no_kab', $no_kab, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="2" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>3.</b></td>
			    		<td width="40%"><b>Kecamatan</b></td>
			    		<td><?php echo form_input('kecamatan', $kecamatan, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    		<td width="20%"><?php echo form_input('no_kec', '0'.$no_kec, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="2" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>4.</b></td>
			    		<td width="40%"><b>Desa/Kelurahan/Nagari</b></td>
			    		<td><?php echo form_input('desa', $desa, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    		<td width="20%"><?php echo form_input('no_kel', $no_kel, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="4" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>5.</b></td>
			    		<td width="40%"><b>Nama SLS</b></td>
			    		<td colspan="2" width="50%"><?php echo form_input('nama_sls', 'RT. '.$no_rt.' RW. '.$no_rw, 'class="form-control input-sm" style="text-transform:uppercase" maxlength="4" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>6.</b></td>
			    		<td width="30%"><b>Alamat</b></td>
			    		<td colspan="2" width="20%"><?php echo form_input('b1_r6', $b1_r6, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    			<?php echo form_hidden('no_rt', $no_rt); ?>
			    			<?php echo form_hidden('no_rw', $no_rw); ?>
			    			<?php /*echo "RW ".$no_rw." RT ".$no_rt;*/ ?>
			    		</td>
			    	</tr>
		    	</table>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>7.</b></td>
			    		<td colspan="2" width="30%"><b>Nomor Urut Rumah Tangga (dari PBDT2015.FKP Kolom(1))</b></td>
			    		<td width="20%"><?php echo form_input('status_rt', $status_rt, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="4" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>8.</b></td>
			    		<td width="40%"><b>No KK KRT</b></td>
			    		<td><?php echo form_input('no_kk_rumahtangga_bdt', $no_kk, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?> </td>
			    		<td>&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>9.</b></td>
			    		<td><b>NIK KRT</b></td>
			    		<td><?php echo form_input('nik_rumahtangga_bdt', $nik_kep_rumahtangga, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?> </td>
			    		<td>&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>10.</b></td>
			    		<td><b>Nama KRT</b></td>
			    		<td colspan="2" width="20%"><?php echo form_input('b1_r8_bdt', $b1_r8, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>11.</b></td>
			    		<td><b>Jumlah ART</b></td>
			    		<td>&nbsp;</td>
			    		<td width="20%"><?php echo form_input('b1_r9_bdt', $b1_r9, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>12.</b></td>
			    		<td colspan="2"><b>Jumlah Keluarga (KK)</b></td>
			    		<td width="20%"><?php echo form_input('b1_r10_bdt', $b1_r10, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
		    	</table>
		    </div>
	    </div>
	</div>

	<div class="row">	
		<div class="box-body">
			<div class="col-sm-12">
			    <div style="text-align: center;" class="alert alert-warning" role="alert">
			      <b>II. KETERANGAN PETUGAS DAN RESPONDEN</b>
			    </div>
			</div>
		    <div class="col-sm-10">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="3%"><b>1.</b></td>
			    		<td width="30%"><b>Tanggal Pencacahan</b></td>
			    		<td width="20%"><?php $attributes = 'class="form-control input-sm datepicker" placeholder="tanggal cacah"';
							echo form_input('tgl_cacah', date('d-m-Y', strtotime($tgl_cacah)), $attributes); ?></td>
			    		<td colspan="3"><i>harap diisi tanggal pencacahan</i></td>
			    		<?php /* ?>
			    		
			    		<td width="10%"><?php echo form_input('tgl_cacah', $tgl_cacah, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="2"'); ?></td>
			    		<td width="5%">Bulan</td>
			    		<td width="10%"><?php echo form_input('bln_cacah', $bln_cacah, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="2"'); ?></td>
			    		<td width="5%">Tahun</td>
			    		<td width="20%"><?php echo form_input('thn_cacah', $thn_cacah, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="4"'); ?></td>
			    		<?php */ ?>
			    	</tr>
			    	<tr>
			    		<td><b>2.</b></td>
			    		<td><b>Nama Pencacah</b></td>
			    		<td colspan="2">
			    			<?php 
			    			$arr = array($kd_cacah, $petugas_cacah);
							$pencacah = implode("-",$arr); 
			    			echo form_dropdown('pencacah', $opsi_pencacah, $pencacah, 'class="form-control input-sm" required'); ?>
			    			<?php /*echo form_input('nama_cacah', $petugas_cacah, 'class="form-control input-sm" style="text-transform:uppercase" required');*/ ?></td>
			    		<td>Kode</td>
			    		<td><?php echo form_input('kd_cacah', $kd_cacah, 'class="form-control input-sm" style="text-transform:uppercase" maxlength="16" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>3.</b></td>
			    		<td><b>Tanggal Pemeriksaan</b></td>
			    		<td><?php $attributes = 'class="form-control input-sm datepicker" placeholder="tanggal periksa" required';
							echo form_input('tgl_periksa', date('d-m-Y', strtotime($tgl_periksa)), $attributes); ?></td>
						<td colspan="3"><i>harap diisi tanggal pemeriksaan</i></td>
			    	</tr>
			    	<tr>
			    		<td><b>4.</b></td>
			    		<td><b>Nama Pemeriksa</b></td>
			    		<td colspan="2"><?php echo form_input('nama_periksa', $this->cu->NAMA_LENGKAP, 'class="form-control input-sm" style="text-transform:uppercase" readonly required'); ?></td>
			    		<td>Kode</td>
			    		<td><?php echo form_input('kd_periksa', $this->cu->NIP, 'class="form-control input-sm" style="text-transform:uppercase" maxlength="20" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>5.</b></td>
			    		<td><b>Hasil Pencacahan Rumah Tangga</b></td>
			    		<td colspan="4"><?php echo form_dropdown('hasil_cacah', $opsi_hasil_pencacahan, $hasil_cacah, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>6.</b></td>
			    		<td><b>Nama Responden</b></td>
			    		<td colspan="3"><?php echo form_input('nama_responden', $nama_responden, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?></td>
			    		<td width="15%">&nbsp;</td>
			    	</tr>
		    	</table>
		    </div>
		</div>
	</div>

	<div class="row">
		<div class="box-body">
			<div class="col-sm-12">
			    <div style="text-align: center;" class="alert alert-warning" role="alert">
			      <b>III. KETERANGAN PERUMAHAN</b>
			    </div>
			</div>

		    <div class="col-sm-6">	    	
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>1.</b></td>
			    		<td width="60%"><b>a. Status kepemilikan bangunan tempat tinggal </b></td>
			    		<td colspan="2" width="40%"><?php echo form_dropdown('b3_r1a_bdt', $opsi_b3_r1a, $b3_r1a, 'class="form-control input-sm ddl_disabled_toggle_lahan" data-ddl_target=".lahan" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>b. Status kepemilikan lahan tempat tinggal </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r1b_bdt', $opsi_b3_r1b, $b3_r1b, 'class="form-control input-sm lahan" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>2.</b></td>
			    		<td><b>Luas Lantai </b></td>
			    		<td><?php echo form_input('b3_r2_bdt', $b3_r2, 'class="form-control input-sm" onkeyup="validAngka(this)" style="text-transform:uppercase" required'); ?></td><td width="20%">&nbsp;&nbsp;&nbsp;m2</td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>3.</b></td>
			    		<td><b>Jenis lantai terluas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r3_bdt', $opsi_b3_r3, $b3_r3, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>4.</b></td>
			    		<td><b>a. Jenis dinding terluas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r4a_bdt', $opsi_b3_r4a, $b3_r4a, 'class="form-control input-sm ddl_disabled_toggle_dinding" data-ddl_target=".dinding" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>b. Kualitas dinding terluas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r4b_bdt', $opsi_b3_r4b, $b3_r4b, 'class="form-control input-sm dinding" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>5.</b></td>
			    		<td><b>a. Jenis atap terluas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r5a_bdt', $opsi_b3_r5a, $b3_r5a, 'class="form-control input-sm ddl_disabled_toggle_atap kunci" data-ddl_target=".atap" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>b. Kualitas atap terluas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r5b_bdt', $opsi_b3_r5b, $b3_r5b, 'class="form-control input-sm atap" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>6.</b></td>
			    		<td><b>Jumlah kamar tidur </b></td>
			    		<td><?php echo form_input('b3_r6_bdt', $b3_r6, 'class="form-control input-sm" onkeyup="validAngka(this)" style="text-transform:uppercase" required'); ?></td>
			    		<td width="20%">&nbsp;&nbsp;&nbsp;kamar</td>
			    	</tr>
		    	</table>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>7.</b></td>
			    		<td width="60%"><b>Sumber air minum  </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r7_bdt', $opsi_b3_r7, $b3_r7, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>8.</b></td>
			    		<td><b>Cara memperoleh air minum </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r8_bdt', $opsi_b3_r8, $b3_r8, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>9.</b></td>
			    		<td><b>a. Sumber penerangan utama </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r9a_bdt', $opsi_b3_r9a, $b3_r9a, 'class="form-control input-sm ddl_disabled_toggle_pln" data-ddl_target=".pln" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>b. Daya listrik terpasang (PLN) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r9b_bdt', $opsi_b3_r9b, $b3_r9b, 'class="form-control input-sm pln" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>10.</b></td>
			    		<td><b>Bahan bakar/energi utama untuk memasak </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r10_bdt', $opsi_b3_r10, $b3_r10, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>11.</b></td>
			    		<td><b>a. Penggunaan fasilitas buang air besar </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r11a_bdt', $opsi_b3_r11a, $b3_r11a, 'class="form-control input-sm ddl_disabled_toggle_bab" data-ddl_target=".bab" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>b. Jenis kloset </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r11b_bdt', $opsi_b3_r11b, $b3_r11b, 'class="form-control input-sm bab" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>12.</b></td>
			    		<td><b>Tempat pembuangan akhir tinja </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r12_bdt', $opsi_b3_r12, $b3_r12, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
		    	</table>
		    </div>
		</div>		
	</div>

	<div class="row">	
		<div class="box-body">	
			<div class="col-sm-12">
			    <div style="text-align: center;" class="alert alert-warning" role="alert">
			      <b>V. KEPEMILIKAN ASET DAN KEIKUTSERTAAN PROGRAM</b>
			    </div>
		   	</div>

		    <div class="col-sm-6">	    	
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>1.</b></td>
			    		<td colspan="4" width="95%"><b>Rumah tangga memiliki sendiri aset bergerak sebagai berikut : </b></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>a. Tabung gas 5.5 kg atau lebih </b></td>
			    		<td colspan="2" width="30%"><?php echo form_dropdown('b5_r1a_bdt', $opsi_binary, $b5_r1a, 'class="form-control input-sm kunci" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>b. Lemari es/kulkas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1b_bdt', $opsi_binary_ii, $b5_r1b, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>c. AC </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1c_bdt', $opsi_binary, $b5_r1c, 'class="form-control input-sm kunci" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>d. Pemanas air (water heater) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1d_bdt', $opsi_binary_ii, $b5_r1d, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>e. Telepon Rumah (PSTN) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1e_bdt', $opsi_binary, $b5_r1e, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>f. Televisi </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1f_bdt', $opsi_binary_ii, $b5_r1f, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>g. Emas/perhiasan & tabungan (senilai 10 gram emas) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1g_bdt', $opsi_binary, $b5_r1g, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>h. komputer/laptop </b></td>
			    		<td colspan="2" width="30%"><?php echo form_dropdown('b5_r1h_bdt', $opsi_binary_ii, $b5_r1h, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>i. Sepeda </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1i_bdt', $opsi_binary, $b5_r1i, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>j. Sepeda motor </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1j_bdt', $opsi_binary_ii, $b5_r1j, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>k. Mobil </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1k_bdt', $opsi_binary, $b5_r1k, 'class="form-control input-sm kunci" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>l. Perahu </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1l_bdt', $opsi_binary_ii, $b5_r1l, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>m. Motor tempel </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1m_bdt', $opsi_binary, $b5_r1m, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>n. Perahu motor </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1n_bdt', $opsi_binary_ii, $b5_r1n, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>o. Kapal </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1o_bdt', $opsi_binary, $b5_r1o, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>3.</b></td>
			    		<td colspan="4" width="95%"><b>Rumah tangga memiliki aset tidak bergerak sebagai berikut : </b></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>a. Lahan </b></td>
			    		<td colspan="2" width="20%"><?php echo form_dropdown('b5_r3a1_bdt', $opsi_binary, $b5_r3a1, 'class="form-control input-sm ddl_disabled_toggle" data-ddl_target="input[name=b5_r3a2_bdt]" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"	align="right"><i>luas lahan</i></td>
			    		<!-- Luas lahan -->
			    		<td><?php echo form_input('b5_r3a2_bdt', $b5_r3a2, 'class="form-control input-sm" onkeyup="validAngka(this)" style="text-transform:uppercase" required'); ?> </td>
			    		<td>&nbsp;&nbsp;&nbsp;m2</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>b. Rumah di tempat lain </b></td>
			    		<td colspan="2" width="20%"><?php echo form_dropdown('b5_r3b_bdt', $opsi_binary_ii, $b5_r3b, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
		    	</table>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>4.</b></td>
			    		<td colspan="4" width="95%"><b>Jumlah ternak yang dimiliki : </b></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>a. Sapi </b></td>
			    		<td width="20%"><?php echo form_dropdown('b5_r4a_bdt', $opsi_no_urut, $b5_r4a, 'class="form-control input-sm"'); ?>
			    			<?php /*echo form_input('b5_r4a_bdt', $b5_r4a, 'class="form-control input-sm" style="text-transform:uppercase"');*/ ?> </td>
			    		<td width="10%">&nbsp;&nbsp;&nbsp;ekor</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>b. Kerbau </b></td>
			    		<td><?php echo form_dropdown('b5_r4b_bdt', $opsi_no_urut, $b5_r4b, 'class="form-control input-sm"'); ?>
			    			<?php /*echo form_input('b5_r4b_bdt', $b5_r4b, 'class="form-control input-sm" style="text-transform:uppercase"');*/ ?> </td>
			    		<td width="10%">&nbsp;&nbsp;&nbsp;ekor</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>c. Kuda </b></td>
			    		<td><?php echo form_dropdown('b5_r4c_bdt', $opsi_no_urut, $b5_r4c, 'class="form-control input-sm"'); ?>
			    			<?php /*echo form_input('b5_r4c_bdt', $b5_r4c, 'class="form-control input-sm" style="text-transform:uppercase"');*/ ?> </td>
			    		<td width="10%">&nbsp;&nbsp;&nbsp;ekor</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>d. Babi </b></td>
			    		<td><?php echo form_dropdown('b5_r4d_bdt', $opsi_no_urut, $b5_r4d, 'class="form-control input-sm"'); ?>
			    			<?php /*echo form_input('b5_r4d_bdt', $b5_r4d, 'class="form-control input-sm" style="text-transform:uppercase"');*/ ?> </td>
			    		<td width="10%">&nbsp;&nbsp;&nbsp;ekor</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>e. Kambing/Domba </b></td>
			    		<td><?php echo form_dropdown('b5_r4e_bdt', $opsi_no_urut, $b5_r4e, 'class="form-control input-sm"'); ?>
			    		<?php /*echo form_input('b5_r4e_bdt', $b5_r4e, 'class="form-control input-sm" style="text-transform:uppercase"');*/ ?> </td>
			    		<td width="10%">&nbsp;&nbsp;&nbsp;ekor</td>
			    	</tr>
			    	<tr>
			    		<td><b>5.</b></td>
			    		<td colspan="2"><b>a. Apakah ada ART yang memiliki usaha sendiri/bersama? </b></td>
			    		<td colspan="2" width="20%"><?php echo form_dropdown('b5_r5a_bdt', $opsi_binary, $b5_r5a, 'class="form-control input-sm" disabled'); ?>
			    	</tr>
			    	<?php /* ?>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"	align="right"><i>Isikan</i></td>
			    		<!-- Jika Ya, isikan -->
			    		<td colspan="2" width="30%"><?php echo form_input('b5_r5a_isian_bdt', '', 'class="form-control input-sm" disabled style="text-transform:uppercase" required'); ?> </td>
			    	</tr>
			    	<?php */ ?>
			    	<tr>
			    		<td width="5%"><b>6.</b></td>
			    		<td colspan="4" width="95%"><b>Rumah tangga menjadi peserta program/memiliki kartu program berikut : </b></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>a. Kartu Keluarga Sejahtera (KKS)/Kartu Perlindungan Sosial(KPS) </b></td>
			    		<td colspan="2" width="30%"><?php echo form_dropdown('b5_r6a_bdt', $opsi_binary, $b5_r6a, 'class="form-control input-sm" required'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>b. Kartu Indonesia Pintar (KIP)/Bantuan Siswa Miskin(BSM) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6b_bdt', $opsi_binary_ii, $b5_r6b, 'class="form-control input-sm" required'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>c. Kartu Indonesia Sehat (KIS)/BPJS Kesehatan/Jamkesmas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6c_bdt', $opsi_binary, $b5_r6c, 'class="form-control input-sm" required'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>d. BPJS Kesehatan peserta mandiri </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6d_bdt', $opsi_binary_ii, $b5_r6d, 'class="form-control input-sm" required'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>e. Jaminan Sosial Tenaga Kerja (Jamsostek)/BPJS Ketenagakerjaan</b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6e_bdt', $opsi_binary, $b5_r6e, 'class="form-control input-sm" required'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>f. Asuransi kesehatan lainnya</b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6f_bdt', $opsi_binary_ii, $b5_r6f, 'class="form-control input-sm" required'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>g. Program Keluarga Harapan (PKH)</b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6g_bdt', $opsi_binary, $b5_r6g, 'class="form-control input-sm" required'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>h. Beras untuk orang miskin (Raskin)</b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6h_bdt', $opsi_binary_ii, $b5_r6h, 'class="form-control input-sm" required'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>i. Kredit Usaha Rakyat (KUR)</b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6i_bdt', $opsi_binary, $b5_r6i, 'class="form-control input-sm" required'); ?> </td>
			    	</tr>
		    	</table>
		    </div>
		</div>
	</div>

</div><!-- /.box-body -->

<script type="text/javascript">
	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle_lahan');
		$(document).on('change', '.ddl_disabled_toggle_lahan', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}
	});

	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle');
		$(document).on('change', '.ddl_disabled_toggle', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}

		check_kunci('.kunci');
		$(document).on('change', '.kunci', function() {
			// alert('masuk gak c?');
			check_kunci('.kunci');
		});
		function check_kunci(target_selector) {
			var total_selector = $(target_selector).length;
			var count = 0;
			var max = 3;
			$(target_selector).each(function() {
				var name = $(this).attr('name');
				var val = $(this).val();
				val = parseInt(val);
				if(name == "b3_r5a_bdt") {
					if(val == 1 || val == 2) {
						count = count+1;
					}
				} else {
					if(val == 1 || val == 3) {
						 count = count+1;
					}
				}
				// console.log('name: '+name+ ' | val: '+val);
			});
			// console.log('count: '+count+' | max: '+max+' | total: '+total_selector);
			if(count >= max) {
				$('form [type=submit]').attr('disabled', 'disabled');
			} else {
				$('form [type=submit]').removeAttr('disabled');
			}
		}

		// check_required_form();
		// $(document).on('change blur', 'form [required]', function() {
		// 	check_required_form();
		// });
		function check_required_form() {
			var target_selector = 'form [required]';
			var total_selector = $(target_selector).length;
			var count = 0;
			$(target_selector).each(function(){
				if($(this).val() != "") {
					count++;
				}
			});
			if(count != total_selector) {
				$('form [type=submit]').attr('disabled', 'disabled');
			} else {
				$('form [type=submit]').removeAttr('disabled');
			}
		}
 	});

 	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle_pln');
		$(document).on('change', '.ddl_disabled_toggle_pln', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}
	});

 	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle_bab');
		$(document).on('change', '.ddl_disabled_toggle_bab', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1 || $(sel).val() == 2 || $(sel).val() == 3) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}
	});

	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle_dinding');
		$(document).on('change', '.ddl_disabled_toggle_dinding', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1 || $(sel).val() == 2 || $(sel).val() == 3) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}
	});

	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle_atap');
		$(document).on('change', '.ddl_disabled_toggle_atap', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1 || $(sel).val() == 2 || $(sel).val() == 3 || $(sel).val() == 4 || $(sel).val() == 5 || $(sel).val() == 6 || $(sel).val() == 7) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}
	});

	function validAngka(a)
	{
		if(!/^[0-9.]+$/.test(a.value))
		{
		a.value = a.value.substring(0,a.value.length-1000);
		}
	}
</script>