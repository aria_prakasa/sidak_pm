<?php 
$attributes = array('novalidate' => '', 'class' => 'form-validate form-horizontal form-label-left', 'id' => 'feedback_form', 'data-parsley-validate' => '');
echo form_open('mohon_rumahtangga/submit', $attributes);
?>

<?php echo form_hidden('get_params', get_params($this->input->get())); ?>
<?php echo form_hidden('no_kk', $no_kk); ?>

<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Data Rumah Tangga: </h3>
	</div>

	<div class="form-horizontal">
	<!-- content -->
		<div class="box-body">
			<?php /* ?>
			<div class="row form-group">
				<div class="col-sm-12">					
					<label for="id_rt" class="control-label col-sm-2">No. Urut Rumah Tangga</label>  
					<div class="col-sm-2">
					  <?php echo form_input('id', $nomor_urut_rumah_tangga, 'class="form-control input-sm _edit" readonly'); ?>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-12">					
					<label for="nik" class="control-label col-sm-2">No. Kartu Keluarga Kepala Rumah Tangga</label>  
					<div class="col-sm-2">
					  <?php echo form_input('kk', $kk, 'maxlength="16" class="form-control input-sm _mhn" readonly'); ?>
					</div>
				</div>
			</div>
			<?php */ ?>
			<div class="row form-group">
				<div class="col-sm-6">					
					<label class="col-sm-4 control-label">Kode Wilayah </label>
					<div class="col-sm-4">
						<?php /*$kode = substr($kodewilayah,6);*/ ?>
						<?php echo form_input('kodewilayah_edit', $kodewilayah, 'class="form-control input-sm" style="text-transform:uppercase" maxlength="10" readonly'); ?> 
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Nama Propinsi </label>
					<div class="col-sm-4">
						<?php echo form_input('provinsi_edit', $provinsi, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
					</div>					
				</div>
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Nama Kabupaten </label>
					<div class="col-sm-4">
						<?php echo form_input('kabupaten_edit', $kabupaten, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
					</div>					
				</div>
			</div>			
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Nama Kecamatan </label>
					<div class="col-sm-4">
						<?php echo form_input('kecamatan_edit', $kecamatan, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
					</div>					
				</div>
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Nama Kelurahan </label>
					<div class="col-sm-4">
						<?php echo form_input('desa_edit', $desa, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
					</div>					
				</div>
			</div>	
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Alamat tempat tinggal </label>
					<div class="col-sm-6">
						<?php echo form_input('b1_r6_edit', $b1_r6, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
						<?php /* ?>
						<textarea class="form-control input-sm" name = "b1_r6_edit" readonly="" rows="3" style="text-transform:uppercase"><?php echo "$alamat, No.RT/No.RW. $no_rt/$no_rw"; ?></textarea>  
						<?php */ ?>
					</div>
				</div>
				<div class="col-sm-6">	
					<label class="col-sm-4 control-label">Nama Kepala Rumah Tangga </label>
					<div class="col-sm-4">
						<?php echo form_input('b1_r8_edit', $b1_r8, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?> 
					</div> 
				</div>
			</div>	
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jumlah Anggota Keluarga </label>
					<div class="col-sm-2">
						<?php echo form_input('b1_r9_edit', $b1_r9, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div> 
				</div>
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Status Kesejahteraan (Desil) </label>
					<div class="col-sm-8"> 
						<?php echo form_dropdown('status_kesejahteraan_edit', $opsi_status_kesejahteraan, $status_kesejahteraan, 'class="form-control input-sm" disabled'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Status kepemilikan bangunan tempat tinggal </label>
					<div class="col-sm-4">
						<?php echo form_dropdown('b3_r1a_edit', $opsi_b3_r1a, $b3_r1a, 'class="form-control input-sm" required'); ?>
					</div> 
				</div>
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Status kepemilikan lahan tempat tinggal </label>
					<div class="col-sm-4">
						<?php echo form_dropdown('b3_r1b_edit', $opsi_b3_r1b, $b3_r1b, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>	
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Luas lantai </label>
					<div class="col-sm-2">
						<?php echo form_input('b3_r2_edit', $b3_r2, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div> 
					<span class="col-sm-4 control-label" style="text-align: left;"> m2 </span>
				</div>
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jenis lantai terluas </label>
					<div class="col-sm-6">
						<?php echo form_dropdown('b3_r3_edit', $opsi_b3_r3, $b3_r3, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>	
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jenis dinding terluas </label>
					<div class="col-sm-6">
						<?php echo form_dropdown('b3_r4a_edit', $opsi_b3_r4a, $b3_r4a, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jenis atap terluas </label>
					<div class="col-sm-6">
						<?php echo form_dropdown('b3_r5a_edit', $opsi_b3_r5a, $b3_r5a, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>	
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kualitas dinding terluas </label>
					<div class="col-sm-5">
						<?php echo form_dropdown('b3_r4b_edit', $opsi_b3_r4b, $b3_r4b, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kualitas atap terluas </label>
					<div class="col-sm-5">
						<?php echo form_dropdown('b3_r5b_edit', $opsi_b3_r5b, $b3_r5b, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jumlah kamar tidur </label>
					<div class="col-sm-2">
						<?php echo form_input('b3_r6_edit', $b3_r6, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Sumber air minum </label>
					<div class="col-sm-5">
						<?php echo form_dropdown('b3_r7_edit', $opsi_b3_r7, $b3_r7, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Cara memperoleh air minum </label>
					<div class="col-sm-5">
						<?php echo form_dropdown('b3_r8_edit', $opsi_b3_r8, $b3_r8, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Penggunaan fasilitas buang air besar </label>
					<div class="col-sm-4">
						<?php echo form_dropdown('b3_r11a_edit', $opsi_b3_r11a, $b3_r11a, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jenis kloset </label>
					<div class="col-sm-5">
						<?php echo form_dropdown('b3_r11b_edit', $opsi_b3_r11b, $b3_r11b, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Tempat pembuangan akhir tinja </label>
					<div class="col-sm-6">
						<?php echo form_dropdown('b3_r12_edit', $opsi_b3_r12, $b3_r12, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Sumber penerangan utama </label>
					<div class="col-sm-4">
						<?php echo form_dropdown('b3_r9a_edit', $opsi_b3_r9a, $b3_r9a, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Daya listrik terpasang (PLN) </label>
					<div class="col-sm-4">
						<?php echo form_dropdown('b3_r9b_edit', $opsi_b3_r9b, $b3_r9b, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Bahan bakar untuk memasak </label>
					<div class="col-sm-5">
						<?php echo form_dropdown('b3_r10_edit', $opsi_b3_r10, $b3_r10, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan tabung gas 5.5 kg atau lebih </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1a_edit', $opsi_binary, $b5_r1a, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan sambungan telepon (PSTN) </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1e_edit', $opsi_binary, $b5_r1e, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan komputer/laptop </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1h_edit', $opsi_binary_ii, $b5_r1h, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Sepeda </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1i_edit', $opsi_binary, $b5_r1i, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Sepeda motor </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1j_edit', $opsi_binary_ii, $b5_r1j, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Mobil </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1k_edit', $opsi_binary, $b5_r1k, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Perahu </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1l_edit', $opsi_binary_ii, $b5_r1l, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Motor tempel </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1m_edit', $opsi_binary, $b5_r1m, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Perahu motor </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1n_edit', $opsi_binary_ii, $b5_r1n, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Kapal </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1o_edit', $opsi_binary, $b5_r1o, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Lemari es/kulkas </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1b_edit', $opsi_binary_ii, $b5_r1b, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan AC (penyejuk udara) </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1c_edit', $opsi_binary, $b5_r1c, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Pemanas air (water heater) </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1d_edit', $opsi_binary_ii, $b5_r1d, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Televisi </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1f_edit', $opsi_binary_ii, $b5_r1f, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan Emas/perhiasan/tabungan senilai 10 gram emas </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r1g_edit', $opsi_binary, $b5_r1g, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jumlah nomor HP aktif </label>
					<div class="col-sm-2">
						<?php echo form_input('b5_r2a_edit', $b5_r2a, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jumlah TV layar datar minimal 30inchi </label>
					<div class="col-sm-2">
						<?php echo form_input('b5_r2b_edit', $b5_r2b, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan lahan </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r3a1_edit', $opsi_binary, $b5_r3a1, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Luas lahan yang dimiliki </label>
					<div class="col-sm-2">
						<?php echo form_input('b5_r3a2_edit', $b5_r3a2, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
					<span class="col-sm-4 control-label" style="text-align: left;"> m2 </span>
				</div> 
			</div> 
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepemilikan rumah di lokasi lain </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r3b_edit', $opsi_binary_ii, $b5_r3b, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Ada anggota rumah tangga yang memiliki usaha sendiri/bersama </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r5a_edit', $opsi_binary, $b5_r5a, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div> 
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jumlah ternak sapi yang dimilki </label>
					<div class="col-sm-3">
						<?php echo form_input('b5_r4a_edit', $b5_r4a, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jumlah ternak kerbau yang dimilki </label>
					<div class="col-sm-3">
						<?php echo form_input('b5_r4b_edit', $b5_r4b, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jumlah ternak kuda yang dimilki </label>
					<div class="col-sm-3">
						<?php echo form_input('b5_r4c_edit', $b5_r4c, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jumlah ternak babi yang dimilki </label>
					<div class="col-sm-3">
						<?php echo form_input('b5_r4d_edit', $b5_r4d, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Jumlah ternak kambing/domba yang dimilki </label>
					<div class="col-sm-3">
						<?php echo form_input('b5_r4e_edit', $b5_r4e, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Memiliki KKS/KPS </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r6a_edit', $opsi_binary, $b5_r6a, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Peserta program PKH </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r6g_edit', $opsi_binary, $b5_r6g, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Peserta program Raskin </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r6h_edit', $opsi_binary_ii, $b5_r6h, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Peserta program KUR </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r6i_edit', $opsi_binary, $b5_r6i, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Kepesertaan KB </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r9_edit', $opsi_binary, $b5_r9, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Usia kawin pertama suami </label>
					<div class="col-sm-2">
						<?php echo form_input('b5_r8a_edit', $b5_r8a, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
					<span class="col-sm-4 control-label" style="text-align: left;"> tahun </span>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Usia kawin pertama istri </label>
					<div class="col-sm-2">
						<?php echo form_input('b5_r8b_edit', $b5_r8b, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
					<span class="col-sm-4 control-label" style="text-align: left;"> tahun </span>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Metode/jenis kontrasepsi yang digunakan </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r10_edit', $opsi_b5_r10, $b5_r10, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Lama menggunakan kontrasepsi </label>
					<div class="col-sm-2">
						<?php echo form_input('b5_r11a_edit', $b5_r11a, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
					<span class="col-sm-2 control-label" style="text-align: left;"> tahun </span> 
					<div class="col-sm-2">
						<?php echo form_input('b5_r11b_edit', $b5_r11b, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
					</div>
					<span class="col-sm-2 control-label" style="text-align: left;"> bulan </span>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Tempat pelayanan KB yang sering digunakan </label>
					<div class="col-sm-6">
						<?php echo form_dropdown('b5_r12_edit', $opsi_b5_r12, $b5_r12, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Keinginan punya anak lagi </label>
					<div class="col-sm-6">
						<?php echo form_dropdown('b5_r13_edit', $opsi_b5_r13, $b5_r13, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Alasan tidak mengikuti KB </label>
					<div class="col-sm-5">
						<?php echo form_dropdown('b5_r14_edit', $opsi_b5_r14, $b5_r14, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Memiliki KIP/BSM </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r6b_edit', $opsi_binary_ii, $b5_r6b, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Memiliki KIS/BPJS Kesehatan/Jamkesmas </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r6c_edit', $opsi_binary, $b5_r6c, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Memiliki BPJS Kesehatan peserta mandiri </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r6d_edit', $opsi_binary_ii, $b5_r6d, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>
			<div class="row form-group">
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Memiliki Jamsostek/BPJS ketenagakerjaan </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r6e_edit', $opsi_binary, $b5_r6e, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
				<div class="col-sm-6">
					<label class="col-sm-4 control-label">Memiliki Asuransi kesehatan lainnya </label>
					<div class="col-sm-3">
						<?php echo form_dropdown('b5_r6f_edit', $opsi_binary_ii, $b5_r6f, 'class="form-control input-sm" required'); ?>
					</div>
				</div> 
			</div>

		</div><!-- /.box-body -->
	<!-- /content -->	
	</div>
	
</div><!-- /.box -->	

<div class="row form-group">
	<div class="col-md-2">
		<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/mohon_rumahtangga/index?<?php echo get_params($this->input->get(), array('no_kk','id_rt')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		<button type="submit" name="proses" value="update" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
		<button type="button" class="btn btn-danger" name="reset_form" onclick="this.form.reset();"><i class="fa fa-times"></i> Kembalikan Awal</button>
		<!-- <input type="button" class="btn btn-danger" name="reset_form" value="Hapus" onclick="this.form.reset();"> -->
	</div>
</div>	

<?php echo form_close(); ?>

<script>
function clearForm(oForm) {
    
  var elements = oForm.elements; 
    
  oForm.reset();

  for(i=0; i<elements.length; i++) {
      
    field_type = elements[i].type.toLowerCase();
    
    switch(field_type) {
    
      case "text": 
      case "password": 
      case "textarea":
            case "hidden":  
        
        elements[i].value = ""; 
        break;
          
      case "radio":
      case "checkbox":
          if (elements[i].checked) {
            elements[i].checked = false; 
        }
        break;

      case "select-one":
      case "select-multi":
                  elements[i].selectedIndex = -1;
        break;

      default: 
        break;
    }
  }
}

</script>