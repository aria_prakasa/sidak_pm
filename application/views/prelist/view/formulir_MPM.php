	<div class="box-body">
	<div class="row">	
		<div class="box-body">
		    <div style="text-align: center;" class="alert alert-warning" role="alert">
		      <b>MEKANISME PEMUTAKHIRAN MANDIRI<br />
		      Formulir Pendaftaran Rumah Tangga Miskin dan Kurang Mampu</b>
		    </div>
		    <div class="col-sm-8">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<th colspan="4">INFORMASI PENDAFTAR</th>
			    	</tr>
			    	<tr>
			    		<th width="5%">1.</th>
			    		<th width="40%">NIK</th>
			    		<td width="5%">:</td>
			    		<td>
			    		<?php echo form_input('nik', $nik, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>2.</th>
			    		<th>Nama Lengkap sesuai KTP</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_input('nama', $nama, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>3.</th>
			    		<th>Jenis Kelamin</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_dropdown('b4_k6', $opsi_b4_k6, $b4_k6, 'class="form-control input-sm" disabled'); ?>
						<?php echo form_hidden('b4_k6', $b4_k6); ?></td>
			    	</tr>
			    	<tr>
			    		<th>4.</th>
			    		<th>Nomor KK</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_input('no_kk', $no_kk, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>5.</th>
			    		<th>Hubungan dengan Kepala Keluarga</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_dropdown('b4_k5_edit', $opsi_b4_k5, $b4_k5, 'class="form-control input-sm"'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>6.</th>
			    		<th>Hubungan dengan Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_dropdown('b4_k3_edit', $opsi_b4_k3, $b4_k3, 'class="form-control input-sm"'); ?>
			    		</td>
			    	</tr>
			    </table>
		    </div>

		    <div class="col-sm-8">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<th colspan="5">INFORMASI RUMAH TANGGA</th>
			    	</tr>
			    	<tr>
			    		<th colspan="2" width="30%">Apakah anggota rumah tangga lain ?</th>
						<td width="5%">:</td>
						<td width="15%"><?php echo form_dropdown('anggota_rt', $opsi_binary, '', 'class="form-control input-sm ddl_disabled_toggle" data-ddl_target="._kk"'); ?></td>
						<td><?php echo form_input('kk_rt', $no_kk_bdt, 'placeholder="Masukkan NO. KK Rumah Tangga" data-suffix="_bdt" maxlength="16" class="form-control _kk input-sm"'); ?>
						</td>
						<td><button id="Cari" class="btn btn-primary btn-sm submit_kk" data-url="<?php echo site_url('skrining/ajax/get_rumahtangga'); ?>" data-target="kk_rt">Cari</button></td>
			    	</tr>
			    	<tr>
			    		<th width="5%">1.</th>
			    		<th width="40%">NIK Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php echo form_input('nik_rumahtangga_bdt', $nik_rumahtangga, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>2.</th>
			    		<th>Nomor KK Rumah Tangga</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php echo form_input('no_kk_rumahtangga_bdt', $no_kk_rumahtangga, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>3.</th>
			    		<th>Nama Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php echo form_input('nama_kep_rumahtangga_bdt', $nama_kep_rumahtangga, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>4.</th>
			    		<th>Jenis Kelamin Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php echo form_dropdown('b4_k6_bdt', $opsi_b4_k6, $b4_k6_rumahtangga, 'class="form-control input-sm" disabled'); ?>
						<?php echo form_hidden('b4_k6_bdt', $b4_k6_rumahtangga); ?></td>
			    	</tr>
			    	<tr>
			    		<th>5.</th>
			    		<th>Bulan Lahir Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td width="15%">
			    		<?php 
			    		$bulan_arr = explode("-", $tgl_lhr);
			    		$bulan = $bulan_arr[1];
			    		?>
			    		<?php echo form_input('bulan_lhr_bdt', str_pad($bulan_lhr, 2, "0", STR_PAD_LEFT), 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    		<td colspan="2">MM</td>
			    	</tr>
			    	<tr>
			    		<th>6.</th>
			    		<th>Tahun Lahir Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td width="10%">
			    		<?php 
			    		$tahun_arr = explode("-", $tgl_lhr);
			    		$tahun = $bulan_arr[2];
			    		?>
			    		<?php echo form_input('tahun_lhr_bdt', $tahun_lhr, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    		<td colspan="2">YYYY</td>
			    	</tr>
			    	<tr>
			    		<th>7.</th>
			    		<th>Status Pekerjaan Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php
						$options = array('' => ' - Pilih Opsi - ',
						'1' => '1. Bekerja',
						'2' => '2. Tidak Bekerja',
						);
						echo form_dropdown('bekerja_rumahtangga_bdt', $options, $bekerja_rumahtangga, 'class="form-control input-sm"');
						?>
						</td>
			    	</tr>
			    	<tr>
			    		<th>8.</th>
			    		<th>Jumlah Anggota Rumah Tangga</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_input('b1_r9_bdt', $b1_r9, 'class="form-control input-sm" style="text-transform:uppercase"'); ?>
			    		</td>
			    		<td colspan="2">Orang</td>
			    	</tr>
			    </table>
		    </div>
		</div>
	</div>
	<div class="row">	
		<div class="box-body">
		    <div class="col-sm-8">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<th colspan="4">INFORMASI WILAYAH</th>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>1.</b></td>
			    		<td width="40%"><b>Provinsi</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_input('', $provinsi, 'placeholder="Propinsi" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>2.</b></td>
			    		<td><b>Kabupaten/Kota</b></td>
			    		<td>:</td>
			    		<td><?php echo form_input('', $kabupaten, 'placeholder="Kabupaten" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>3.</b></td>
			    		<td><b>Kecamatan</b></td>
			    		<td>:</td>
			    		<td><?php echo form_input('', $kecamatan, 'placeholder="Kecamatan" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>4.</b></td>
			    		<td><b>Kelurahan/Desa</b></td>
			    		<td>:</td>
			    		<td><?php echo form_input('', $desa, 'placeholder="Kelurahan/Desa" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>5.</b></td>
			    		<td><b>Alamat</b></td>
			    		<td>:</td>
			    		<td><?php echo form_input('', $alamat.' RT/RW '.$no_rt.'/'.$no_rw, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    </table>
		    </div>
		</div>
	</div>
	<div class="row">	
		<div class="box-body">	    
		    <div class="col-sm-8">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<th colspan="4">DATA KUESIONER</th>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>1.</b></td>
			    		<td width="50%"><b>Status Kepemilikan Bangunan Tempat Tinggal</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b3_r1a_bdt', $opsi_b3_r1a, $b3_r1a, 'class="form-control input-sm"'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>2.</b></td>
			    		<td width="50%"><b>Bahan Bangunan Utama Atap Rumah Terluas</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b3_r5a_bdt', $opsi_b3_r5a, $b3_r5a, 'class="form-control input-sm kunci"'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>3.</b></td>
			    		<td width="50%"><b>Bahan Bangunan Utama Dinding Rumah Terluas</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b3_r4a_bdt', $opsi_b3_r4a, $b3_r4a, 'class="form-control input-sm"'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>4.</b></td>
			    		<td width="50%"><b>Penggunaan Fasilitas Tempat Buang Air Besar</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b3_r11a_bdt', $opsi_b3_r11a, $b3_r11a, 'class="form-control input-sm"'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>5.</b></td>
			    		<td width="50%"><b>Apakah Rumah Tangga Memiliki Mobil?</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b5_r1k_bdt', $opsi_binary, $b5_r1k, 'class="form-control input-sm kunci" '); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>6.</b></td>
			    		<td width="50%"><b>Apakah Rumah Tangga Memiliki AC?</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b5_r1c_bdt', $opsi_binary, $b5_r1c, 'class="form-control input-sm kunci" '); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>7.</b></td>
			    		<td width="50%"><b>Apakah Rumah Tangga Memiliki Tabung Gas 5,5 Kg atau lebih?</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b5_r1a_bdt', $opsi_binary, $b5_r1a, 'class="form-control input-sm kunci"'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>8.</b></td>
			    		<td width="50%"><b>Pendidikan Tertinggi Anggota Rumah Tangga Yang Sudah Tidak Bersekolah?</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b4_k18_bdt', $opsi_b4_k18, $b4_k18, 'class="form-control input-sm pendidikan kunci"'); ?></td>
			    	</tr>
			    </table>
		    </div>	
		</div>
	</div>

</div><!-- /.box-body -->

<script type="text/javascript">
	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle');
		$(document).on('change', '.ddl_disabled_toggle', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}

		check_kunci('.kunci');
		$(document).on('change', '.kunci', function() {
			// alert('masuk gak c?');
			check_kunci('.kunci');
		});
		function check_kunci(target_selector) {
			var total_selector = $(target_selector).length;
			var count = 0;
			var max = 2;
			$(target_selector).each(function() {
				var name = $(this).attr('name');
				var val = $(this).val();
				val = parseInt(val);
				if(name == "b3_r5a_bdt") {
					if(val == 1 || val == 2 || val == 3 || val == 4) {
						count = count+1;
					}
				}
				if(name == "b4_k18_bdt") {
					if(val == 5 || val == 6) {
						count = count+1;
					}
				}
				else {
					if(val == 1 || val == 3) {
						 count = count+1;
					}
				}
				// console.log('name: '+name+ ' | val: '+val);
			});
			// console.log('count: '+count+' | max: '+max+' | total: '+total_selector);
			if(count >= max) {
				$('form [type=submit]').attr('disabled', 'disabled');
			} else {
				$('form [type=submit]').removeAttr('disabled');
			}
		}

		// check_required_form();
		// $(document).on('change blur', 'form [required]', function() {
		// 	check_required_form();
		// });
		function check_required_form() {
			var target_selector = 'form [required]';
			var total_selector = $(target_selector).length;
			var count = 0;
			$(target_selector).each(function(){
				if($(this).val() != "") {
					count++;
				}
			});
			if(count != total_selector) {
				$('form [type=submit]').attr('disabled', 'disabled');
			} else {
				$('form [type=submit]').removeAttr('disabled');
			}
		}
 	});
</script>