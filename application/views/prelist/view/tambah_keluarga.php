<?php if ($this->session->flashdata('info')) { ?>
<div class="alert alert-success alert-dismissible" role="alert" style="text-align: center">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<b>SUKSES ... !!<br />Data Keluarga a.n&nbsp;<?php echo $this->session->flashdata('info'); ?> Berhasil ditambahkan</b>
</div>
<?php } ?>
<div class="box box-info">	
	<!-- content -->
	<div class="box-body">
		<div class="row">	
			<div class="box-body">
				<div class="col-sm-12">
					<?php 
					/*$attributes = array('novalidate' => '', 'class' => 'form-validate form-horizontal form-label-left pullout', 'id' => 'feedback_form', 'data-parsley-validate' => '');
					echo form_open('prelist/tambahkk', $attributes);*/
					echo form_open('prelist/cariKK', 'id="check" class="form-validate form-horizontal"') ;
					?>
					<?php echo form_hidden('get_params', get_params($this->input->get())); ?>
					<?php echo form_hidden('no_kk', $no_kk); ?>
					<div class="form-group ">
                  		<label for="nik" class="control-label col-lg-3 pull-left">Masukkan Nomor KK yang akan ditambahkan </label>
                  		<div class="col-lg-3">
                          <?php echo form_input('no_kk_tambah', $no_kk_tambah, 'placeholder="Masukkan Nomor KK" maxlength="16" class="form-control _nik" required'); ?>
                  		</div>
						<div class="col-lg-1">
						  <button id="Cari" class="btn btn-primary">Cari</button>
						</div>
              		</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
		<?php if ($data): ?>

		<div class="row">	
	
			<div class="box-body">		
				<div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-info" role="alert">
				      <b>DATA KELUARGA YANG AKAN TAMBAHKAN</b>
				    </div>
			    </div>
			    
			    <?php if ($nik_pendaftar): ?>

				<?php echo form_open('prelist/proses_tambahKK', 'id="check" class="form-validate form-horizontal"'); ?>	
				<?php echo form_hidden('get_params', get_params($this->input->get())); ?>
				<?php echo form_hidden('no_kk_induk', $no_kk_ruta); ?>
				<?php echo form_hidden('nama_kep_rumahtangga', $nama_kep_rumahtangga); ?>		
			    <div class="col-sm-8">
			    	<table width="100%" class="table responsive" class="nobotmargin" style="font-size: 0.9em">
				    	<tr>
				    		<td width="5%"><b>1.</b></td>
				    		<td width="30%"><b>Nomor Kartu Keluarga</b></td>
				    		<td width="5%">:</td>
				    		<td width="60%"><?php echo form_input('no_kk_tambah', $no_kk, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td><b>2.</b></td>
				    		<td><b>NIK Pendaftar</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $nik_pendaftar, 'placeholder="Nama Kepala Keluarga" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td><b>3.</b></td>
				    		<td><b>Nama Pendaftar</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $nama_pendaftar, 'placeholder="Nama Pendaftar" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td><b>4.</b></td>
				    		<td><b>Alamat</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $alamat.' RT/RW '.$no_rt.'/'.$no_rw.', KEL. '.$desa.', KEC. '.$kecamatan, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
					    <tr>
				    		<td><b>5.</b></td>
				    		<td><b>Hubungan dengan Kepala Rumah Tangga Induk (si pemilik rumah tinggal)</b></td>
				    		<td>:</td>
				    		<td>
				    		<?php echo form_dropdown('hubungan_rumahtangga_induk', $opsi_stat_rtangga, '', 'class="form-control input-sm" required'); ?>
				    		</td>
				    	</tr>
				    </table>
			    </div>
			    
				<?php else: ?>
					<div class="col-sm-12" style="text-align: center;">
				    	<div class="col-sm-6 col-sm-offset-3 alert alert-warning" style="text-align: center;vertical-align: middle;"><span style="font-size: 77px" class="icon fa fa-exclamation-triangle"></span><br /><h4>Data Keluarga belum di daftarkan PRELIST... !!</h4></div>
				    </div>
		    	<?php endif ?>	
			    
				<div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-info" role="alert">
				      <b>DATA ANGGOTA KELUARGA</b>
				    </div>
				</div>

			    <div class="col-sm-12">
			    	<table width="100%" class="table table-bordered table-striped responsive" style="font-size: 0.9em">
						<thead>
							<tr>
								<th style="text-align:center">NO</th>
								<th style="text-align:center">NO. KK</th>
								<th style="text-align:center">NIK</th>
								<th style="text-align:center">NAMA LENGKAP</th>
								<th style="text-align:center">L/P</th>
								<th style="text-align:center">TGL LAHIR</th>
								<th style="text-align:center">HUB. KELUARGA</th>
								<th style="text-align:center">AGAMA</th>
								<th style="text-align:center">PENDIDIKAN</th>
								<th style="text-align:center">PEKERJAAN</th>
								<th style="text-align:center">KATEGORI</th>
								<th style="text-align:center">STATUS</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$no = 1;
						$id_rt = $nomor_urut_rumah_tangga;
						foreach ($data as $row) {
							extract((array) $row);
							?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo "$no_kk"; ?></td>
								<td><?php echo "$nik"; ?></td>
								<td><?php echo "$nama_lgkp"; ?></td>
								<td><?php echo "$kelamin"; ?></td>
								<td><?php echo format_tanggal($tgl_lhr); ?></td>
								<td><?php echo "$shdk"; ?></td>
								<td><?php echo "$agama_desc"; ?></td>
								<td><?php echo "$pendidikan"; ?></td>
								<td><?php echo "$pekerjaan_siak"; ?></td>
								<td align="center">
					              <?php if ($kategori == 3): ?>
					                <span class="label label-primary" data-original-title="Pendaftar Baru (Tidak Terdaftar di dalam PPFM/BDT.2015)" data-toggle="tooltip" data-placement="bottom">
					              <?php else: ?> 
					                <span class="label label-success" data-original-title="Pendaftar Lama (Terdaftar di dalam PPFM/BDT.2015)" data-toggle="tooltip" data-placement="bottom">
					              <?php endif ?>
					              <?php echo $kategori; ?> </span>
					            </td>
								<td align="center">
									<?php if ($proses_ind == 1): ?>
										<span class="label label-default" data-original-title="Data belum dimasukkan" data-toggle="tooltip" data-placement="bottom">
									<?php else: ?> 
										<span class="label label-success" data-original-title="Data sudah dimasukkan" data-toggle="tooltip" data-placement="bottom">
									<?php endif ?>
									<?php echo $proses_ind; ?> </span>
								</td>						
							</tr>
						<?php 
						}
						?>
		                </tbody>
					</table>
					
				</div><!-- /.box-body -->
				<?php if ($nik_pendaftar): ?>
					
				<div class="col-sm-12">
				    <div style="text-align: center;">
				      <button id="Cari" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;Tambahkan Keluarga</button>
				      <?php /* ?><a class="btn btn-success" href="<?php echo base_url(); ?>index.php/prelist/proses_tambahKK?no_kk=<?php echo get_params($this->input->get('no_kk')); ?>&amp;no_kk_tambah=<?php echo $no_kk; ?>&amp;<?php echo get_params($this->input->get(), array('no_kk_tambah')) ?>" > <i class="fa fa-save"></i>&nbsp;&nbsp;Tambahkan Keluarga</a>	<?php */ ?>
				    </div>
			    </div>
			    <?php echo form_close(); ?>
				<?php endif ?>
				<br/>
				
			</div>
		</div>
		
		<?php endif ?>

	</div>
</div>

<div class="box box-danger">	
	<!-- content -->
	<div class="box-body">
		<div class="row">	
			<div class="box-body">
			    <div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-danger" role="alert">
				      <b>DATA RUMAH TANGGA INDUK</b>
				    </div>
				</div>
			    <div class="col-sm-12">
			    	<table width="100%" class="table table-bordered table-striped responsive" style="font-size: 0.9em">
						<thead>
							<tr>
								<th style="text-align:center">NO</th>
								<th style="text-align:center">NO. KK</th>
								<th style="text-align:center">NIK</th>
								<th style="text-align:center">NAMA LENGKAP</th>
								<th style="text-align:center">L/P</th>
								<th style="text-align:center">TGL LAHIR</th>
								<th style="text-align:center">HUB. KELUARGA</th>
								<th style="text-align:center">AGAMA</th>
								<th style="text-align:center">PENDIDIKAN</th>
								<th style="text-align:center">PEKERJAAN</th>
								<th style="text-align:center">KATEGORI</th>
								<th style="text-align:center">STATUS</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$no = 1;
						$id_rt = $nomor_urut_rumah_tangga;
						foreach ($data_ruta as $row) {
							extract((array) $row);
							?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo "$no_kk"; ?></td>
								<td><?php echo "$nik"; ?></td>
								<td><?php echo "$nama_lgkp"; ?></td>
								<td><?php echo "$kelamin"; ?></td>
								<td><?php echo format_tanggal($tgl_lhr); ?></td>
								<td><?php echo "$shdk"; ?></td>
								<td><?php echo "$agama_desc"; ?></td>
								<td><?php echo "$pendidikan"; ?></td>
								<td><?php echo "$pekerjaan_siak"; ?></td>
								<td align="center">
					              <?php if ($kategori == 3): ?>
					                <span class="label label-primary" data-original-title="Pendaftar Baru (Tidak Terdaftar di dalam PPFM/BDT.2015)" data-toggle="tooltip" data-placement="bottom">
					              <?php else: ?> 
					                <span class="label label-success" data-original-title="Pendaftar Lama (Terdaftar di dalam PPFM/BDT.2015)" data-toggle="tooltip" data-placement="bottom">
					              <?php endif ?>
					              <?php echo $kategori; ?> </span>
					            </td>
								<td align="center">
									<?php if ($proses_ind == 1): ?>
										<span class="label label-default" data-original-title="Data belum dimasukkan" data-toggle="tooltip" data-placement="bottom">
									<?php else: ?> 
										<span class="label label-success" data-original-title="Data sudah dimasukkan" data-toggle="tooltip" data-placement="bottom">
									<?php endif ?>
									<?php echo $proses_ind; ?> </span>
								</td>
							</tr>
						<?php 
						}
						?>
		                </tbody>
					</table>
					
				</div><!-- /.box-body -->
				<br>
				<div class="col-md-2">
					<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/prelist/lihat?<?php echo get_params($this->input->get(), array('no_kk_tambah')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
				</div>
			</div>
		</div>

	</div><!-- /.box-body -->


	<!-- /content -->	
	
</div><!-- /.box -->
