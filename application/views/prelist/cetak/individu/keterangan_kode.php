<?php
function print_arr_as_list($arr, $column = 1) {
  $ret = "";
  if(is_array($arr)) {
    // $ret = "<ol style='list-style:none;margin:0;padding-left:15px;'>";
    $ret = "<table>
      <tbody>
        <tr>
          <td>
          ";
    if($column > 1) {
      $divider = ceil(count($arr) / $column);
    }
    $i = 1;
    foreach ($arr as $key => $value) {
      if(empty($key)) continue;
      // $value = end(explode(". ", $value, 2));
      // $ret .= "<li>".$value."</li>";
      $ret .= $value."<br>";
      if($column > 1) {
        if($i > 0 && $i % $divider == 0) {
          $ret .= "</td><td>";
        }
      }
      $i++;
    }
    // $ret .= "</ol>";
    $ret .= "</td>
        </tr>
      </tbody>
    </table>";
  }
  return $ret;
}
?>
<table class="table-content-wrapper full">
  <tbody>
    <tr>
      <td>
        <table class="full table table-bordered petugas2" cellspacing="0">
          <tr>
            <td rowspan="2">
              <b><u>Kode Kolom 3</u><br/>
              Hubungan dengan kepala rumah tangga :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k3);?>
            </td>
            <td rowspan="2">
              <b><u>Kode Kolom 5</u><br/>
              Hubungan dengan kepala keluarga :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k5); ?>
            </td>
            <td rowspan="2">
              <b><u>Kode Kolom 8</u><br/>
              Status perkawinan :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k8); ?>
            </td>
            <td rowspan="2">
              <b><u>Kode Kolom 9</u><br/>
              Kepemilikan akta/buku nikah atau akta cerai :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k9); ?>
            </td>
            <td rowspan="2">
              <b><u>Kode Kolom 13</u><br/>
              Jenis cacat :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k13, 2); ?>
            </td>
            <td rowspan="2">
              <b><u>Kode Kolom 14</u><br/>
              Penyakit kronis/menahun :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k14); ?>
            </td>
            <td rowspan="2">
              <b><u>Kode Kolom 15</u><br/>
              Partisipasi sekolah :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k15); ?>
            </td>
            <td rowspan="2">
              <b><u>Kode Kolom 16</u><br/>
              Jenjang pendidikan tertinggi yang pernah/sedang diduduki :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k16); ?>
            </td>
            <td>
              <b><u>Kode Kolom 17</u><br/>
              Kelas tertinggi yang pernah/sedang diduduki :</b><br/><br/>
              <?php
              $opsi_b4_k17 = array(
                1,2,3,4,5,6,7,8,'(Tamat)'
                );
              echo implode(" &nbsp; ", $opsi_b4_k17); ?>
            </td>
            <td rowspan="2">
              <b><u>Kode Kolom 20</u><br/>
              Lapangan usaha dari pekerjaan utama :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k20, 2); ?>
            </td>
            <td rowspan="2">
              <b><u>Kode Kolom 21</u><br/>
              Status kedudukan dari pekerjaan utama :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k21); ?>
            </td>
          </tr>
          <tr>
            <td>
              <b><u>Kode Kolom 18</u><br/>
              Izasah tertinggi :</b><br/><br/>
              <?php echo print_arr_as_list($opsi_b4_k18); ?>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </tbody>
</table>
