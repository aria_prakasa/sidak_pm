<table class="table-content-wrapper petugas2 full">
  <tbody>
    <tr>
      <td align="center" class="title">
        IV. KETERANGAN SOSIAL EKONOMI ANGGOTA RUMAH TANGGA
      </td>
    </tr>
    <tr>
      <td>
        <table class="full table table-bordered petugas2" cellspacing="0">
          <thead>
            <tr>
              <th rowspan="3">No Urut</th>
              <th rowspan="3">NAMA ANGGOTA RUMAH TANGGA</th>
              <th rowspan="3">Hubungan denga kepala rumah tangga<br/>
                  (Isikan KODE)</th>
              <th rowspan="3">Nomor urut keluarga</th>
              <th rowspan="3">Hubungan dengan kepala keluarga<br/>
                  (Isikan KODE)</th>
              <th rowspan="3">Jenis Kelamin<br/>
                  1. Laki-laki<br/>
                  2. Perempuan</th>
              <th rowspan="3">Umur<br/>
                  (Tahun)</th>
              <th rowspan="3">Status perkawinan<br/>
                  (Isikan KODE)</th>
              <th rowspan="3">JIKA KOL (8) = 2 atau 3,<br/>
                  Kepemilikan akta/buku nikah atau akta cerai<br/>
                  (Isikan KODE)</th>
              <th rowspan="3">Tercantum dalam Kartu Keluarga (KK) dirumah tangga ini<br/>
                  1. Ya<br/>
                  2. Tidak</th>
              <th rowspan="3">Kepemilikan kartu Identitas<br/>
                  0. Tidak memiliki<br/>
                  1. Akta kelahiran<br/>
                  2. Kartu pelajar<br/>
                  4. KTP<br/>
                  8. SIM<br/>
                  (jumlahkan KODE yang sesuai)</th>
              <th rowspan="3">UNTUK WANITA USIA 10-49 TAHUN DAN KOL (8) = 2<br/>
                  Status kehamilan<br/>
                  1. Ya<br/>
                  2. Tidak</th>
              <th rowspan="3">Jenis cacat<br/>
                  (Isikan KODE)</th>
              <th rowspan="3">Penyakit kronis/ menahun<br/>
                  (Isikan KODE)</th>
              <th colspan="7">>UNTUK ART 5 TAHUN KEATAS</th>
            </tr>
            <tr>
              <th rowspan="2">Partisipasi sekolah<br/>
                  (Isikan KODE)</th>
              <th colspan="3">JIKA KOLOM (15) = 1 atau 2</th>
              <th rowspan="2">Bekerja/ membantu bekerja selama seminggu yang lalu<br/>
                  1. Ya ... jam<br/>
                  2. Tidak -> <b>Stop</b></th>
              <th rowspan="2">Lapangan usaha dari pekerjaan utama<br/>
                  (Isikan KODE)</th>
              <th rowspan="2">Status kedudukan dalam pekerjaan utama<br/>
                  (Isikan KODE)</th>
            </tr>
            <tr>
              <th>Jenjang dan jenis pendidikan tertinggi yang pernah/ sedang diduduki<br/>
                  (Isikan KODE)</th>
              <th>Kelas tertinggi yang pernah/ sedang diduduki<br/>
                  (Isikan KODE)</th>
              <th>Ijazah tertinggi yang yang dimiliki<br/>
                  (Isikan KODE)</th>
            </tr>
          </thead>
          <tbody>
          <?php
          $no = 1+$page;
          foreach ($anggota as $row) {
            $row = keysToLower($row);
            extract((array) $row);
            ?>
          <tr>
            <td align="center"><?php echo $no++; ?></td>
            <td><?php echo $nama_lgkp."<br/>".$nik ; ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$..$", 4); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$..$", 4); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$..$", 4); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$..$", 4); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("./..", 4); ?></td>
            <td><?php echo print_box_original("$..$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
          </tr>
          <?php
          }
          ?>
          <?php
          $limit = 10;
          $no = $no;
          while ($no <= $limit) {
            ?>
          <tr>
            <td align="center"><?php echo $no++; ?></td>
            <td><?php echo "&nbsp;<br/>&nbsp;"; ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$..$", 4); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$..$", 4); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$..$", 4); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$..$", 4); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
            <td><?php echo print_box_original("./..", 4); ?></td>
            <td><?php echo print_box_original("$..$", 3); ?></td>
            <td><?php echo print_box_original("$.$", 3); ?></td>
          </tr>
          <?php
          }
          ?>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
