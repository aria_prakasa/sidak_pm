<table class="table-content-wrapper petugas2 full">
  <tbody>
    <tr>
      <td colspan="2">
        Apakah ada anak dari Kepala Keluarga yang masih menjadi tanggungan tetapi sedang sekolah/kuliah dan tidak tinggal di dalam ruta ini? Jika ada, sebutkan; Jika tidak ada -> BLOK V
      </td>
    </tr>
    <tr>
      <td style="padding-right:2px;">
        <table class="full table table-condensed petugas2" cellspacing="0">
          <tr>
            <td width="3%">1.</td>
            <td width="22%">Nama</td>
            <td>:</td>
            <td>&nbsp;</td>
            <td width="25%">NISN/ NO.KTM:</td>
          </tr>
          <tr>
            <td>2.</td>
            <td>Alamat tepat tinggal</td>
            <td>:</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td>3.</td>
            <td>Nomor Induk Kependudukan (NIK)</td>
            <td>:</td>
            <td>&nbsp;</td>
            <td></td>
          </tr>
          <tr>
            <td>4.</td>
            <td>Nama Sekolah</td>
            <td>:</td>
            <td colspan="2">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td style="padding-left:2px;">
        <table class="full table table-condensed petugas2" cellspacing="0">
          <tr>
            <td width="3%">1.</td>
            <td width="22%">Nama</td>
            <td>:</td>
            <td>&nbsp;</td>
            <td width="25%">NISN/ NO.KTM:</td>
          </tr>
          <tr>
            <td>2.</td>
            <td>Alamat tepat tinggal</td>
            <td>:</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td>3.</td>
            <td>Nomor Induk Kependudukan (NIK)</td>
            <td>:</td>
            <td>&nbsp;</td>
            <td></td>
          </tr>
          <tr>
            <td>4.</td>
            <td>Nama Sekolah</td>
            <td>:</td>
            <td colspan="2">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </tbody>
</table>
