<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php  //*  ?>
<link rel="stylesheet" href="<?php echo base_url("_assets/css/screen.css?v".date('YmdHi')); ?>">
<link rel="stylesheet" href="<?php echo base_url("_assets/css/rumahtangga.css?v".date('YmdHi')); ?>">
<?php  //*/  ?>
</head>

<body>
<table width="100%" class="list withnopadding responsive">
  <tr>
    <td width="50%">&nbsp;</td>
    <td align="center"><div class="nomargin"></div><h1 style="font-size: 16px">FORMULIR PERUBAHAN / PENDAFTARAN <br/> DATA TERPADU PROGRAM PENANGANAN FAKIR MISKIN</h1></td>
  </tr>
</table>
<table width="100%">
  <tr>
    <td valign="top" width="50%">
      <div class="border_body"> 
      <table class="table-content-wrapper full">
  <caption class="title text-center">II. KETERANGAN PETUGAS DAN RESPONDEN</caption>
  <tbody>
    <tr>
      <td>
        <table class="full table-bordered petugas" cellspacing="0">
          <tbody>

    <tr>
      <td width="20%">1. Tanggal pencacahan</td>
      <td colspan="2">
        <table class="full">
          <tbody>
            <tr class="text-center">
              <td>Tanggal</td>
              <td>Bulan</td>
              <td>Tahun</td>
            </tr>
            <tr class="text-center">
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
            </tr>
          </tbody>
        </table>
      </td>
      <td rowspan="2" class="text-center">Saya menyatakan telah melaksanakan pencacahan sesuai dengan prosedur<br>
	      <br>
	      <br>
	      ( ............................... )<br>
	      Tanda Tangan Pencacah
      </td>
    </tr>
    <tr>
      <td>2. Nama pencacah, kode</td>
      <td colspan="2">
        <table class="full">
          <tbody>
            <tr>
              <td>............................</td>
              <td>Kode</td>
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td>3. Tanggal pemeriksaan</td>
      <td colspan="2">
        <table class="full">
          <tbody>
            <tr class="text-center">
              <td>Tanggal</td>
              <td>Bulan</td>
              <td>Tahun</td>
            </tr>
            <tr class="text-center">
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
            </tr>
          </tbody>
        </table>
      </td>
      <td rowspan="2" class="text-center">
        Saya menyatakan telah melaksanakan pemeriksaan sesuai dengan prosedur<br>
        <br>
        <br>
        ( ............................... )<br>
        Tanda Tangan Pemeriksa
      </td>
    </tr>
    <tr>
      <td>4. Nama pemeriksa, kode</td>
      <td colspan="2">
        <table class="full">
          <tbody>
            <tr>
              <td>............................</td>
              <td>Kode</td>
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="3">

        <table class="full">
          <tbody>
            <tr>
              <td>

        5. Hasil pencacahan rumah tangga:
        <ol>
          <li>Hasil dicacah</li>
          <li>Rumah tangga tidak ditemukan</li>
          <li>Rumah tangga pindah/bangunan sensus sudah tidak ada</li>
          <li>Bagian dari rumah tangga di dokumen PBDT2015.FKP</li>
        </ol>
                
              </td>
              <td class="text-right">
                <font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font>
              </td>
            </tr>
          </tbody>
        </table>

      </td>
      <td class="text-center">
        Saya menyatakan bahwa informasi yang saya berikan adalah benar,<br>
        dan boleh dipergunakan untuk keperluan pemerintah<br>
        <br>
        <br>
        ( ............................... )<br>
        Nama Lengkap &amp; Tanda Tangan Responden
      </td>
    </tr>

          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
      </div>
    </td> 
    <td valign="top" width="50%">
      <div class="border_body"> 
      <table class="table-content-wrapper full">
  <caption class="title text-center">I. PENGENALAN TEMPAT</caption>
  <tbody>
    <tr>
      <td>
        <table class="full table-bordered tempat" cellspacing="0">
          <tbody>
            <tr>
              <td width="20%">1. Provinsi</td>
              <td width="20%">JAWA TIMUR</td>
              <td class="text-right" width="10%"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td class="border">3</td><td class="border">5</td></tr></table></td>
              <td width="15%">6. Alamat</td>
              <td colspan="2">JL KH MANSUR NO 72 RT.1 RW.1</td>
              
            </tr>
            <tr>
              <td>2. Kabupaten/Kota</td>
              <td>KOTA PASURUAN</td>
              <td class="text-right"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td class="border">7</td><td class="border">5</td></tr></table></td>
              <td colspan="2">7. Nomor urut rumah tangga (dari PBDT2015.FKP kolom (1))</td>
              <td class="text-right">
                                  <font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font>
                                            </td>
            </tr>
            <tr>
              <td>3. Kecamatan</td>
              <td>PURWOREJO</td>
              <td class="text-right"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">2</td></tr></table></td>
              <td>8. Nama KRT</td>
              <td colspan="2">PARLI</td>
  
            </tr>
            <tr>
              <td>4. Desa/Kelurahan/Nagari</td>
              <td>TEMBOKREJO</td>
              <td class="text-right"><table class="boxes full"><tr align="right"><td class="border">1</td><td class="border">0</td><td class="border">0</td><td class="border">3</td></tr></table></td>
              <td>9. Jumlah ART</td>
              <td></td>
              <td class="text-right"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">3</td></tr></table></td>
            </tr>
            <tr>
              <td>5. Nama SLS</td>
              <td colspan="2">&nbsp;</td>
              <td>10. Jumlah Keluarga</td>
              <td width="25%"></td>
              <td class="text-right" width="10%"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">1</td></tr></table></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
      </div>
    </td>
  </tr>
  <tr>
    <td valign="top">
    <div class="border_body">   
      <table class="table-content-wrapper full" style="font-size: 14px">
  <caption class="title text-center">V. KEPEMILIKAN ASET DAN KEIKUTSERTAAN PROGRAM</caption>
  <tbody>
    <tr>
      <td>
        <table class="full table-bordered" cellspacing="0">
          <tbody>

    <tr>
      <td width="3%">1.</td>
      <td class="half">
        Rumah tangga memiliki sendiri aset bergerak sebagai berikut:
        <table class="full">
          <tbody>
                                    <tr>
              <td width="3%">a. </td>
              <td width="50%">Tabung gas 5,5 kg atau lebih</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">a. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">b. </td>
              <td width="50%">Lemari es/kulkas</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">b. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">c. </td>
              <td width="50%">AC</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">c. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">d. </td>
              <td width="50%">Pemanas air (water heater)</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">d. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">e. </td>
              <td width="50%">Telepon Rumah (PSTN)</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">e. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">f. </td>
              <td width="50%">Televisi</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">f. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">g. </td>
              <td width="50%">Emas/perhiasan & tabungan (senilai 10 gram emas)</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">g. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">h. </td>
              <td width="50%">Komputer/laptop</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">h. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                          </tbody>
          </table>
        </td>
        <td class="half">
          <br/>
          <table class="full">
            <tbody>
                                      <tr>
              <td width="3%">i. </td>
              <td width="50%">Sepeda</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">i. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">j. </td>
              <td width="50%">Sepeda Motor</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">j. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">k. </td>
              <td width="50%">Mobil</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">k. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">l. </td>
              <td width="50%">Perahu</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">l. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">m. </td>
              <td width="50%">Motor tempel</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">m. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">n. </td>
              <td width="50%">Perahu motor</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">n. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">o. </td>
              <td width="50%">Kapal</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">o. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                    </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td>2.</td>
      <td colspan="2">
        Rumah tangga memiliki aset tidak bergerak sebagai berikut:
        <table class="full">
          <tbody>
            <tr>
              <td>a. Lahan</td>
              <td>1. Ya, ...... m<sup>2</sup></td>
              <td>2. Tidak</td>
              <td>a.</td>
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
              <td>/</td>
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
               , 1        <tr>
              <td>b. Rumah di tempat lain</td>
              <td>3. Ya</td>
              <td>4. Tidak</td>
              <td>b.</td>
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
              <td colspan="2"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td>3.</td>
      <td class="half">
        Jumlah ternak yang dimiliki (ekor):
        <table class="full">
          <tbody>
                                    <tr>
              <td width="3%">a. </td>
              <td>Sapi</td>
              <td class="text-right" width="5%">a.</td>
              <td width="17%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">b. </td>
              <td>Kerbau</td>
              <td class="text-right" width="5%">b.</td>
              <td width="17%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">c. </td>
              <td>Kuda</td>
              <td class="text-right" width="5%">c.</td>
              <td width="17%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                          </tbody>
          </table>
        </td>
        <td class="half">
          <br/>
          <table class="full">
            <tbody>
                                      <tr>
              <td width="3%">d. </td>
              <td>Babi</td>
              <td class="text-right" width="5%">d.</td>
              <td width="17%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">e. </td>
              <td>Kambing/Domba</td>
              <td class="text-right" width="5%">e.</td>
              <td width="17%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                    </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td>4.</td>
      <td colspan="2">
        <table class="full">
          <tbody>
            <tr>
              <td>a. Apakah ada ART yang memiliki usaha sendiri/bersama?</td>
              <td>1. Ya</td>
              <td>2. Tidak</td>
              <td><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
            <tr>
              <td>b. Jika "Ya", isikan: </td>
              <td colspan="3">
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td>5.</td>
      <td class="half">
        Rumah tangga memiliki sendiri aset bergerak sebagai berikut:
        <table class="full">
          <tbody>
                                    <tr>
              <td width="3%">a. </td>
              <td width="50%">Kartu Keluarga Sejahtera (KKS)/ Kartu Perlindungan Sosial</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">a. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">b. </td>
              <td width="50%">Kartu Indonesia Pintar (KIP)/ Bantuan Siswa Miskin (BSM)</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">b. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">c. </td>
              <td width="50%">Kartu Indonesia Sehat (KIS)/ BPJS Kesehatan/Jamkesmas</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">c. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">d. </td>
              <td width="50%">BPJS Kesehatan peserta mandiri</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">d. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">e. </td>
              <td width="50%">Jaminan Sosial Tenaga Kerja (BPJS)/ BPJS Ketenagakerjaan</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">e. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                          </tbody>
          </table>
        </td>
        <td class="half">
          <br/>
          <table class="full">
            <tbody>
                                      <tr>
              <td width="3%">f. </td>
              <td width="50%">Asuransi Kesehatan Lainnya</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">f. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">g. </td>
              <td width="50%">Program Keluarga Harapan (PKH)</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">g. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">h. </td>
              <td width="50%">Beras untuk orang miskin (Raskin)</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">h. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                      <tr>
              <td width="3%">i. </td>
              <td width="50%">Kredit Usaha Rakyat (KUR)</td>
              <td width="10%">1. Ya</td>
              <td width="20%">2. Tidak</td>
              <td class="text-right" width="5%">i. </td>
              <td width="7%"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></td>
            </tr>
                                    </tbody>
        </table>
      </td>
    </tr>

          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
    </div>
    </td>
    <td valign="top" rowspan="2">
    <div class="border_body"> 
      <table class="table-content-wrapper full" style="font-size: 14px">
  <caption class="title text-center">III. KETERANGAN PERUMAHAN</caption>
  <tbody>
    <tr>
      <td>
        <table class="full table-bordered" cellspacing="0">
          <tbody>

    <tr>
      <td width="3%">1.</td>
      <td width="40%">a. Status penguasaan bangunan tempat tinggal yang ditempati</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Milik Sendiri<br>2. Kontrak/sewa<br>
                  </td>
                  <td>
                  3. Bebas Sewa<br>4. Dinas<br>
                  </td>
                  <td>
                  5. Lainnya<br>              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>
    <tr>
      <td></td>
      <td>b. Status lahan tempat tinggal yang ditempati</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Milik Sendiri<br>2. Milik Orang Lain<br>
                  </td>
                  <td>
                  3. Tanah Negara<br>4. Lainnya<br>
                  </td>
                  <td>
                                </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>

    <tr>
      <td>2.</td>
      <td>Luas Lantai</td>
      <td>........... m<sup>2</sup></td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>

    <tr>
      <td>3.</td>
      <td>Jenis lantai terluas</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Marmer/granit<br>2. Keramik<br>3. Parket/vinil/permadani<br>4. Ubin/tegel/teraso<br>
                  </td>
                  <td>
                  5. Kayu/papan Kualitas Tinggi<br>6. Semen/bata Merah<br>7. Bambu<br>8. Kayu/papan Kualitas Rendah<br>
                  </td>
                  <td>
                  9. Tanah<br>10. Lainnya<br>              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>

    <tr>
      <td>4.</td>
      <td>a. Jenis dinding terluas</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Tembok<br>2. Plesteran Anyaman Bambu/kawat<br>3. Kayu<br>
                  </td>
                  <td>
                  4. Anyaman Bambu<br>5. Batang Kayu<br>6. Bambu<br>
                  </td>
                  <td>
                  7. Lainnya<br>              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>
    <tr>
      <td></td>
      <td>b. Jika R.4a berkode 1, 2 atau 3, kondisi dinding</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. 1. Bagus/kualitas Tinggi<br>
                  </td>
                  <td>
                  2. 2. Jelek/kualitas Rendah<br>
                  </td>
                  <td>
                                </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>

    <tr>
      <td>5.</td>
      <td>a. Jenis atap terluas</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Beton/genteng Beton<br>2. Genteng Keramin<br>3. Genteng Metal<br>4. Genteng Tanah Liat<br>
                  </td>
                  <td>
                  5. Asbes<br>6. Seng<br>7. Sirap<br>8. Bambu<br>
                  </td>
                  <td>
                  9. Jerami/ijuk/daundaunan/rumbia<br>10. Lainnya<br>              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>
    <tr>
      <td></td>
      <td>b. Jika R.5a berkode 1, 2, 3, 4, 5, 6 atau 7, kondisi atap</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Bagus/kualitas Tinggi<br>
                  </td>
                  <td>
                  2. Jelek/kualitas Rendah<br>
                  </td>
                  <td>
                                </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>

    <tr>
      <td>6.</td>
      <td>Jumlah kamar tidur</td>
      <td>........... kamar</td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>

    <tr>
      <td>7.</td>
      <td>Sumber air minum</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Air Kemasan Bermerk<br>2. Air Isi Ulang<br>3. Leding Meteran<br>4. Leding Eceran<br>
                  </td>
                  <td>
                  5. Sumur Bor/pompa<br>6. Sumur Terlindung<br>7. Sumur Tak Terlindung<br>8. Mata Air Terlindung<br>
                  </td>
                  <td>
                  9. Mata Air Tak Terlindung<br>10. Air Sungai/danau/waduk<br>11. Air Hujan<br>12. Lainnya<br>
                  </td>
                  <td>
                                </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>

    <tr>
      <td>8.</td>
      <td>Cara memperoleh air minum</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Membeli Eceran<br>2. Langganan<br>
                  </td>
                  <td>
                  3. Tidak Membeli<br>              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>

    <tr>
      <td>9.</td>
      <td>a. Sumber penerangan utama</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Listrik PLN<br>2. Listrik Non PLN<br>
                  </td>
                  <td>
                  3. Bukan Listrik<br>              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>
    <tr>
      <td></td>
      <td>b. Jika R.9a berkode 1, daya terpasang</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. 450 Watt<br>2. 900 Watt<br>3. 1300 Watt<br>
                  </td>
                  <td>
                  4. 2200 Watt<br>5. >2200 Watt<br>6. Tanpa Meteran<br>
                  </td>
                  <td>
                                </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>
    <tr>
      <td>10.</td>
      <td>Bahan bakar/energi utama untuk memasak</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Listrik<br>2. Gas >3kg<br>3. Gas 3kg<br>
                  </td>
                  <td>
                  4. Gas Kota/biogas<br>5. Minyak Tanah<br>6. Briket<br>
                  </td>
                  <td>
                  7. Arang<br>8. Kayu Bakar<br>9. Tidak Memasak Di Rumah<br>
                  </td>
                  <td>
                                </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>
    <tr>
      <td>11.</td>
      <td>a. Penggunaan fasilitas tempat buang air besar</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Sendiri<br>
                  </td>
                  <td>
                  2. Bersama<br>
                  </td>
                  <td>
                  3. Umum<br>
                  </td>
                  <td>
                  4. Tidak Ada<br>
                  </td>
                  <td>
                                </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>
    <tr>
      <td></td>
      <td>b. Jenis kloset</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Leher Angsa<br>2. Plengsengan<br>
                  </td>
                  <td>
                  3. Cemplung/cubluk<br>4. Tidak Pakai<br>
                  </td>
                  <td>
                                </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>
    <tr>
      <td>12.</td>
      <td>Tempat pembuangan akhir tinja</td>
      <td>
                <table class="full">
          <tbody>
            <tr>
              <td>
              1. Tangki<br>2. SPAL<br>3. Lubang Tanah<br>
                  </td>
                  <td>
                  4. Kolam/sawah/sungai/danau/laut<br>5. Pantai/tanah Lapang/kebun<br>6. Lainnya<br>
                  </td>
                  <td>
                                </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right"><font color="white"><table class="boxes full"><tr align="right"><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td class="border">&nbsp;&nbsp;&nbsp;</td></tr></table></font></td>
    </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
    </div>
    </td>
  </tr>
  <tr>
    <td valign="top">
    <div> 
      <table width="100%" class="table-content-wrapper full">
	<tr>
		<td width="50%">
			<table>	
				<tr>	
					<td colspan="3"><b>	Direktorat Jendral Penanganan Fakir Miskin </b></td>
				</tr>
				<tr>	
					<td colspan="3">Kementrian Sosial Republik Indonesia</td>
				</tr>
				<tr>	
					<td colspan="3">Jl. Salemba Raya No.28 Jakarta Pusat 10430</td>
				</tr>
				<tr>	
					<td>Telp/Fax</td>
					<td>:</td>
					<td>(021)3161574</td>	
				</tr>
				<tr>	
					<td>Email</td>
					<td>:</td>
					<td>pokjapdt@kemsos.go.id</td>	
				</tr>
				<tr>	
					<td>Website</td>
					<td>:</td>
					<td>http://www.kemsos.go.id</td>	
				</tr>
			</table>			
		</td>
		<td width="50%">	
			<table>	
				<tr>	
					<td colspan="3"><b>	Sekretariat TNP2K </b></td>
				</tr>
				<tr>	
					<td colspan="3">Jl. Kebon SIrih No.14 Jakarta Pusat 10110</td>
				</tr>
				<tr>	
					<td>Telp.</td>
					<td>:</td>
					<td>(021)3912812</td>	
				</tr>
				<tr>	
					<td>Fax</td>
					<td>:</td>
					<td>(021)3912511</td>	
				</tr>
				<tr>	
					<td>Email</td>
					<td>:</td>
					<td>info@tnp2k.go.id</td>	
				</tr>
				<tr>	
					<td>Website</td>
					<td>:</td>
					<td>http://www.tnp2k.go.id</td>	
				</tr>
			</table>
		</td>
	</tr>
</table>    </div>
    </td>
  </tr>
</table>



</body>

</html>