<table class="table-content-wrapper full">
  <caption class="title text-center">II. KETERANGAN PETUGAS DAN RESPONDEN</caption>
  <tbody>
    <tr>
      <td>1. Tanggal pencacahan</td>
      <td colspan="2">DD-MM-YYYY</td>
    </tr>
    <tr>
      <td>2. Nama pencacah, kode</td>
      <td>&nbsp;</td>
      <td class="text-right"></td>
    </tr>
    <tr rowspan="2">
      <td>Saya menyatakan telah melaksanakan pencacahan sesuai dengan prosedur<br>
      <br>
      <br>
      ( ............................... )<br>
      Tanda Tangan Pencacah
      </td>
    </tr>
    <tr>
      <td>3. Tanggal pemeriksaan</td>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td>4. Nama pemeriksa, kode</td>
      <td>&nbsp;</td>
      <td class="text-right"></td>
    </tr>
    <tr rowspan="2">
      <td>Saya menyatakan telah melaksanakan pemeriksaan sesuai dengan prosedur<br>
      <br>
      <br>
      ( ............................... )<br>
      Tanda Tangan Pemeriksa
      </td>
    </tr>
    <tr>
      <td colspan="2">5. Hasil pencacahan rumah tangga:
        <ol>
          <li>Hasil dicacah</li>
          <li>Rumah tangga tidak ditemukan</li>
          <li>Rumah tangga pindah/bangunan sensus sudah tidak ada</li>
          <li>Bagian dari rumah tangga di dokumen PBDT2015.FKP</li>
        </ol>
      </td>
      <td class="text-right"></td>
    </tr>
    <tr>
      <td>Saya menyatakan bahwa informasi yang saya berikan adalah benar,<br>
      dan boleh dipergunakan untuk keperluan pemerintah<br>
      <br>
      <br>
      ( ............................... )<br>
      Nama Lengkap &amp; Tanda Tangan Responden
      </td>
    </tr>
  </tbody>
</table>
