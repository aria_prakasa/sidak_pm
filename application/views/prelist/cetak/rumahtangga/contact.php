<table width="100%" class="table-content-wrapper full">
	<tr>
		<td width="50%">
			<table>	
				<tr>	
					<td colspan="3"><b>	Direktorat Jendral Penanganan Fakir Miskin </b></td>
				</tr>
				<tr>	
					<td colspan="3">Kementrian Sosial Republik Indonesia</td>
				</tr>
				<tr>	
					<td colspan="3">Jl. Salemba Raya No.28 Jakarta Pusat 10430</td>
				</tr>
				<tr>	
					<td>Telp/Fax</td>
					<td>:</td>
					<td>(021)3161574</td>	
				</tr>
				<tr>	
					<td>Email</td>
					<td>:</td>
					<td>pokjapdt@kemsos.go.id</td>	
				</tr>
				<tr>	
					<td>Website</td>
					<td>:</td>
					<td>http://www.kemsos.go.id</td>	
				</tr>
			</table>			
		</td>
		<td width="50%">	
			<table>	
				<tr>	
					<td colspan="3"><b>	Sekretariat TNP2K </b></td>
				</tr>
				<tr>	
					<td colspan="3">Jl. Kebon SIrih No.14 Jakarta Pusat 10110</td>
				</tr>
				<tr>	
					<td>Telp.</td>
					<td>:</td>
					<td>(021)3912812</td>	
				</tr>
				<tr>	
					<td>Fax</td>
					<td>:</td>
					<td>(021)3912511</td>	
				</tr>
				<tr>	
					<td>Email</td>
					<td>:</td>
					<td>info@tnp2k.go.id</td>	
				</tr>
				<tr>	
					<td>Website</td>
					<td>:</td>
					<td>http://www.tnp2k.go.id</td>	
				</tr>
			</table>
		</td>
	</tr>
</table>