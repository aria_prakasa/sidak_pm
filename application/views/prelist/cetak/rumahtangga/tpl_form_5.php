<table class="table-content-wrapper full">
  <caption class="title text-center">V. KEPEMILIKAN ASET DAN KEIKUTSERTAAN PROGRAM</caption>
  <tbody>
    <tr>
      <td class="half">
        1. Rumah tangga memiliki sendiri aset bergerak sebagai berikut:
        <table class="full">
          <tbody>
            <?php
            $arr = array(
              "a" => array("Tabung gas 5,5 kg atau lebih", ""),
              "b" => array("Lemari es/kulkas", ""),
              "c" => array("AC", ""),
              "d" => array("Pemanas air (water heater)", ""),
              "e" => array("Telepon Rumah (PSTN)", ""),
              "f" => array("Televisi", ""),
              "g" => array("Emas/perhiasan & tabungan (senilai 10 gram emas)", ""),
              "h" => array("Komputer/laptop", ""),
              "i" => array("Sepeda", ""),
              "j" => array("Sepeda Motor", ""),
              "k" => array("Mobil", ""),
              "l" => array("Perahu", ""),
              "m" => array("Motor tempel", ""),
              "n" => array("Perahu motor", ""),
              "o" => array("Kapal", ""),
              );
            ?>
            <?php
            $half_div = ceil(count($arr) / 2);
            $i = 0;
            foreach ($arr as $key => $value) {
              $i++;
            ?>
            <tr>
              <td><?php echo $key; ?>. </td>
              <td><?php echo $value[0]; ?></td>
              <td>1. Ya</td>
              <td>2. Tidak</td>
              <td class="text-right"><?php echo $key; ?>. ...<?php //echo $value[1] ?></td>
            </tr>
              <?php
              if($i == $half_div) {
              ?>
            </tbody>
          </table>
        </td>
        <td class="half">
          <table class="full">
            <tbody>
              <?php
              } ?>
            <?php
            } ?>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        2. Rumah tangga memiliki aset tidak bergerak sebagai berikut:
        <table class="full">
          <tbody>
            <tr>
              <td>a. Lahan</td>
              <td>1. Ya, ...... m<sup>2</sup></td>
              <td>2. Tidak</td>
              <td>a. ... / ...............</td>
            </tr>
            <tr>
              <td>b. Rumah di tempat lain</td>
              <td>3. Ya</td>
              <td>4. Tidak</td>
              <td>b. ...</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td class="half">
        3. Jumlah ternak yang dimiliki (ekor):
        <table class="full">
          <tbody>
            <?php
            $arr = array(
              "a" => array("Sapi", ""),
              "b" => array("Kerbau", ""),
              "c" => array("Kuda", ""),
              "d" => array("Babi", ""),
              "e" => array("Kambing/Domba", ""),
              );
            ?>
            <?php
            $half_div = ceil(count($arr) / 2);
            $i = 0;
            foreach ($arr as $key => $value) {
              $i++;
            ?>
            <tr>
              <td><?php echo $key; ?>. </td>
              <td><?php echo $value[0]; ?></td>
              <td class="text-right"><?php echo $key; ?>. ......<?php //echo $value[1] ?></td>
            </tr>
              <?php
              if($i == $half_div) {
              ?>
            </tbody>
          </table>
        </td>
        <td class="half">
          <table class="full">
            <tbody>
              <?php
              } ?>
            <?php
            } ?>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table class="full">
          <tbody>
            <tr>
              <td>4. a. Apakah ada ART yang memiliki usaha sendiri/bersama?</td>
              <td>1. Ya</td>
              <td>2. Tidak</td>
              <td>...</td>
            </tr>
            <tr>
              <td> &nbsp; &nbsp;b. Jika "Ya", isikan: </td>
              <td colspan="3">
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td class="half">
        1. Rumah tangga memiliki sendiri aset bergerak sebagai berikut:
        <table class="full">
          <tbody>
            <?php
            $arr = array(
              "a" => array("Kartu Keluarga Sejahtera (KKS)/ Kartu Perlindungan Sosial", ""),
              "b" => array("Kartu Indonesia Pintar (KIP)/ Bantuan Siswa Miskin (BSM)", ""),
              "c" => array("Kartu Indonesia Sehat (KIS)/ BPJS Kesehatan/Jamkesmas", ""),
              "d" => array("BPJS Kesehatan peserta mandiri", ""),
              "e" => array("Jaminan Sosial Tenaga Kerja (BPJS)/ BPJS Ketenagakerjaan", ""),
              "f" => array("Asuransi Kesehatan Lainnya", ""),
              "g" => array("Program Keluarga Harapan (PKH)", ""),
              "h" => array("Beras untuk orang miskin (Raskin)", ""),
              "i" => array("Kredit Usaha Rakyat (KUR)", ""),
              );
            ?>
            <?php
            $half_div = ceil(count($arr) / 2);
            $i = 0;
            foreach ($arr as $key => $value) {
              $i++;
            ?>
            <tr>
              <td><?php echo $key; ?>. </td>
              <td><?php echo $value[0]; ?></td>
              <td>1. Ya</td>
              <td>2. Tidak</td>
              <td class="text-right"><?php echo $key; ?>. ...<?php //echo $value[1] ?></td>
            </tr>
              <?php
              if($i == $half_div) {
              ?>
            </tbody>
          </table>
        </td>
        <td class="half">
          <table class="full">
            <tbody>
              <?php
              } ?>
            <?php
            } ?>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
