<style>
 	body {font-family: "Calibri", serif;}
	div.border_body {border: 2px solid #000000; padding: 5}
	table tr td {font-size: 12px;}
	table.tempat tr td {font-size: 14px;
	                  padding-bottom: 15px;}
	table.petugas tr td {font-size: 10px;}
	.table-content-wrapper {
	}
	.table-content-wrapper .title {
		background: #ccc;
		color: #000;
		padding: 5px 10px;
	}
	.table-content-wrapper > tbody > tr > td {
		padding: 0;
	}
	table {
		border-collapse: collapse;
	}
	table tr td,
	table tr th {
	}
	table.full {
		width: 100%;
	}
	.full tr td {
		vertical-align: top;
	}
	table .half{
		width: 50%;
	}
	.text-right {
		text-align: right;
	}
	.text-center {
		text-align: center;
	}
	.table-bordered > tbody > tr > td,
	.table-bordered > tbody > tr > th {
		border: 1px solid #000;
		padding: 2px 4px;
	}
	.boxes tr td {
		padding: 2px 8px;
	}
	.boxes tr td.border {
		border: 1px solid #000;
	}
</style>