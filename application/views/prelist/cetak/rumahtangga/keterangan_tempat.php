<table class="table-content-wrapper full">
  <tbody>
    <tr>
      <td align="center" class="title">
        I. PENGENALAN TEMPAT
      </td>
    </tr>
    <tr>
      <td>
        <table class="full table table-bordered tempat" cellspacing="0">
          <tbody>
            <tr>
              <td width="20%">1. Provinsi</td>
              <td width="20%"><?php echo $provinsi; ?></td>
              <td class="text-right" width="10%"><?php echo print_box($no_prop); ?></td>
              <td width="15%">6. Alamat</td>
              <td colspan="2"><?php echo $alamat; ?><?php echo ($no_rt != "" ? " RT.".$no_rt : ""); ?><?php echo ($no_rw != "" ? " RW.".$no_rw : ""); ?></td>

            </tr>
            <tr>
              <td>2. Kabupaten/Kota</td>
              <td><?php echo $kabupaten; ?></td>
              <td class="text-right"><?php echo print_box($no_kab); ?></td>
              <td colspan="2">7. Nomor urut rumah tangga (dari PBDT2015.FKP kolom (1))</td>
              <td class="text-right">
                <?php if ($nomor_urut_rumah_tangga != ""): ?>
                  <?php echo print_box($nomor_urut_rumah_tangga, 4); ?>
                <?php else: ?>
                  <font color="white"><?php echo print_box("....", 4); ?></font>
                <?php endif ?>
              <?php /*echo ($nomor_urut_rumah_tangga != "" ? print_box($nomor_urut_rumah_tangga, 4) : print_box("....", 4));*/ ?>
              </td>
            </tr>
            <tr>
              <td>3. Kecamatan</td>
              <td><?php echo $kecamatan; ?></td>
              <td class="text-right"><?php echo print_box(format_kdkec($kecamatan)); ?></td>
              <td>8. Nama KRT</td>
              <td colspan="2"><?php echo $nama_kep_rumahtangga; ?></td>

            </tr>
            <tr>
              <td>4. Desa/Kelurahan/Nagari</td>
              <td><?php echo $desa; ?></td>
              <td class="text-right"><?php echo print_box(format_kdkel($desa)); ?></td>
              <td>9. Jumlah ART</td>
              <td></td>
              <td class="text-right"><?php echo print_box($b1_r9); ?></td>
            </tr>
            <tr>
              <td>5. Nama SLS</td>
              <td colspan="2">&nbsp;</td>
              <td>10. Jumlah Keluarga</td>
              <td width="25%"></td>
              <td class="text-right" width="10%"><?php echo print_box(1); ?></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
