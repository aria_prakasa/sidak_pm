<table class="table-content-wrapper full">
  <tbody>
    <tr>
      <td align="center" class="title">
        III. KETERANGAN PERUMAHAN
      </td>
    </tr>
    <tr>
      <td>
        <table class="full table table-bordered" cellspacing="0">
          <tbody>

            <tr>
              <td width="3%">1.</td>
              <td width="40%">a. Status penguasaan bangunan tempat tinggal yang ditempati</td>
              <td>
                <?php
                // var_dump($opsi_b3_r1a);
                $arr = $opsi_b3_r1a;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>
            <tr>
              <td></td>
              <td>b. Status lahan tempat tinggal yang ditempati</td>
              <td>
                <?php
                $arr = $opsi_b3_r1b;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>

            <tr>
              <td>2.</td>
              <td>Luas Lantai</td>
              <td>........... m<sup>2</sup></td>
              <td class="text-right"><font color="white"><?php echo print_box("...", 3); ?></font></td>
            </tr>

            <tr>
              <td>3.</td>
              <td>Jenis lantai terluas</td>
              <td>
                <?php
                $arr = $opsi_b3_r3;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box(".."); ?></font></td>
            </tr>

            <tr>
              <td>4.</td>
              <td>a. Jenis dinding terluas</td>
              <td>
                <?php
                $arr = $opsi_b3_r4a;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>
            <tr>
              <td></td>
              <td>b. Jika R.4a berkode 1, 2 atau 3, kondisi dinding</td>
              <td>
                <?php
                $arr = $opsi_b3_r4b;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo $i.". ".ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>

            <tr>
              <td>5.</td>
              <td>a. Jenis atap terluas</td>
              <td>
                <?php
                $arr = $opsi_b3_r5a;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box(".."); ?></font></td>
            </tr>
            <tr>
              <td></td>
              <td>b. Jika R.5a berkode 1, 2, 3, 4, 5, 6 atau 7, kondisi atap</td>
              <td>
                <?php
                $arr = $opsi_b3_r5b;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>

            <tr>
              <td>6.</td>
              <td>Jumlah kamar tidur</td>
              <td>........... kamar</td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>

            <tr>
              <td>7.</td>
              <td>Sumber air minum</td>
              <td>
                <?php
                $arr = $opsi_b3_r7;
                $div = ceil(count($arr)/4);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box(".."); ?></font></td>
            </tr>

            <tr>
              <td>8.</td>
              <td>Cara memperoleh air minum</td>
              <td>
                <?php
                $arr = $opsi_b3_r8;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>

            <tr>
              <td>9.</td>
              <td>a. Sumber penerangan utama</td>
              <td>
                <?php
                $arr = $opsi_b3_r9a;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>
            <tr>
              <td></td>
              <td>b. Jika R.9a berkode 1, daya terpasang</td>
              <td>
                <?php
                $arr = $opsi_b3_r9b;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>
            <tr>
              <td>10.</td>
              <td>Bahan bakar/energi utama untuk memasak</td>
              <td>
                <?php
                $arr = $opsi_b3_r10;
                $div = ceil(count($arr)/4);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>
            <tr>
              <td>11.</td>
              <td>a. Penggunaan fasilitas tempat buang air besar</td>
              <td>
                <?php
                $arr = $opsi_b3_r11a;
                $div = ceil(count($arr)/5);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>
            <tr>
              <td></td>
              <td>b. Jenis kloset</td>
              <td>
                <?php
                $arr = $opsi_b3_r11b;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>
            <tr>
              <td>12.</td>
              <td>Tempat pembuangan akhir tinja</td>
              <td>
                <?php
                $arr = $opsi_b3_r12;
                $div = ceil(count($arr)/3);
                $i = 0;
                ?>
                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                      <?php
                      foreach ($arr as $key => $value):
                        if ($value == '- Pilih Opsi -') {
                          continue;
                        }
                        $i++;
                        echo ucfirst($value)."<br>";
                        if($i%$div == 0):
                          echo '
                          </td>
                          <td>
                          ';
                        endif;
                      endforeach; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td class="text-right"><font color="white"><?php echo print_box("."); ?></font></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
