<table class="table-content-wrapper full">
  <tbody>
    <tr>
      <td align="center" class="title">
        II. KETERANGAN PETUGAS DAN RESPONDEN
      </td>
    </tr>
    <tr>
      <td>
        <table class="full table table-bordered petugas" cellspacing="0">
          <tbody>

            <tr>
              <td width="20%">1. Tanggal pencacahan</td>
              <td colspan="2">
                <table class="full">
                  <tbody>
                    <tr class="text-center">
                      <td>Tanggal</td>
                      <td>Bulan</td>
                      <td>Tahun</td>
                    </tr>
                    <tr class="text-center">
                      <td><font color="white"><?php echo print_box("..", 2); ?></font></td>
                      <td><font color="white"><?php echo print_box(".. ", 2); ?></font></td>
                      <td><font color="white"><?php echo print_box(".... ", 4); ?></font></td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td rowspan="2" class="text-center">Saya menyatakan telah melaksanakan pencacahan sesuai dengan prosedur<br>
        	      <br>
        	      <br>
        	      ( ............................... )<br>
        	      Tanda Tangan Pencacah
              </td>
            </tr>
            <tr>
              <td>2. Nama pencacah, kode</td>
              <td colspan="2">
                <table class="full">
                  <tbody>
                    <tr>
                      <td>............................</td>
                      <td>Kode</td>
                      <td><font color="white"><?php echo print_box("...", 3); ?></font></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td>3. Tanggal pemeriksaan</td>
              <td colspan="2">
                <table class="full">
                  <tbody>
                    <tr class="text-center">
                      <td>Tanggal</td>
                      <td>Bulan</td>
                      <td>Tahun</td>
                    </tr>
                    <tr class="text-center">
                      <td><font color="white"><?php echo print_box("..", 2); ?></font></td>
                      <td><font color="white"><?php echo print_box("..", 2); ?></font></td>
                      <td><font color="white"><?php echo print_box("....", 4); ?></font></td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td rowspan="2" class="text-center">
                Saya menyatakan telah melaksanakan pemeriksaan sesuai dengan prosedur<br>
                <br>
                <br>
                ( ............................... )<br>
                Tanda Tangan Pemeriksa
              </td>
            </tr>
            <tr>
              <td>4. Nama pemeriksa, kode</td>
              <td colspan="2">
                <table class="full">
                  <tbody>
                    <tr>
                      <td>............................</td>
                      <td>Kode</td>
                      <td><font color="white"><?php echo print_box("...", 3); ?></font></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td colspan="3">

                <table class="full">
                  <tbody>
                    <tr>
                      <td>
                        5. Hasil pencacahan rumah tangga:
                        <table class="full">
                          <tr>
                            <td width="5%">&nbsp;</td>
                            <td>1. Hasil dicacah</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td>2. Rumah tangga tidak ditemukan</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td>3. Rumah tangga pindah/bangunan sensus sudah tidak ada</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td>4. Bagian dari rumah tangga di dokumen PBDT2015.FKP</td>
                        </tr>
                        </table>

                      </td>
                      <td style="text-align: center; vertical-align: middle;">
                        <?php echo print_box(".", 1); ?>
                      </td>
                    </tr>
                  </tbody>
                </table>

              </td>
              <td class="text-center">
                Saya menyatakan bahwa informasi yang saya berikan adalah benar,<br>
                dan boleh dipergunakan untuk keperluan pemerintah<br>
                <br>
                <br>
                ( ............................... )<br>
                Nama Lengkap &amp; Tanda Tangan Responden
              </td>
            </tr>

          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
