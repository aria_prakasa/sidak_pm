<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php  //*  ?>
<link rel="stylesheet" href="<?php echo base_url("_assets/css/screen.css?v".date('ymdHis')); ?>">
<link rel="stylesheet" href="<?php echo base_url("_assets/css/rumahtangga.css?v".date('ymdHis')); ?>">
<?php  //*/  ?>
<?php  /*  ?>
<style type="text/css" media="screen">
  <?php $this->load->view('prelist/cetak/rumahtangga/css'); ?>
</style>
<?php  //*/  ?>
</head>

<body>
<table width="100%" class="list withnopadding responsive">
  <tr>
    <td width="50%">&nbsp;</td>
    <td align="center" class="page_title">
      FORMULIR PERUBAHAN / PENDAFTARAN <br/> DATA TERPADU PROGRAM PENANGANAN FAKIR MISKIN
    </td>
  </tr>
</table>
<?php //* ?>
<table width="100%" style="border-collapse: separate; border-spacing: 2px;">
  <tr>
    <td valign="top" width="50%" class="border_body">
      <?php $this->load->view("prelist/cetak/rumahtangga/keterangan_petugas"); ?>
    </td>
    <td valign="top" class="border_body">
      <?php $this->load->view("prelist/cetak/rumahtangga/keterangan_tempat"); ?>
    </td>
  </tr>
  <tr>
    <td valign="top" class="border_body">
      <?php $this->load->view("prelist/cetak/rumahtangga/keterangan_aset"); ?>
    </td>
    <td valign="top" rowspan="2" class="border_body">
      <?php $this->load->view("prelist/cetak/rumahtangga/keterangan_perumahan"); ?>
    </td>
  </tr>
  <tr>
    <td valign="top">
      <?php $this->load->view("prelist/cetak/rumahtangga/contact"); ?>
    </td>
  </tr>
</table>
<?php //*/ ?>

<?php  ?>
<table width="100%">
  <tr>
    <td valign="top">
      <div class="border_body">
      <?php $this->load->view("prelist/cetak/individu/keterangan_anggota"); ?>
      </div>
    </td>
  </tr>
  <?php /* ?>
  <tr>
    <td valign="top">
    <div class="border_body">
      <?php $this->load->view("prelist/cetak/individu/keterangan_aset"); ?>
    </div>
    </td>
  </tr><?php */ ?>
  <tr>
    <td valign="top">
    <div class="border_body">
      <?php $this->load->view("prelist/cetak/individu/tambahan_anggota"); ?>
    </div>
    </td>
  </tr>
  <tr>
    <td valign="top">
    <div class="border_body">
      <?php $this->load->view("prelist/cetak/individu/keterangan_kode"); ?>
    </div>
    </td>
  </tr>
</table>
<?php // ?>


</body>

</html>
