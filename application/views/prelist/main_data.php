<div class="callout callout-danger">
	<h4>PERHATIAN !</h4>
	<p>Ikuti instruksi pengisian data berikut ini : <br/>
	1. Isikan terlebih dahulu Variabel Kemiskinan Rumah Tangga pada bagian <b>Data Keluarga</b> dengan cara klik tombol Input Data Kemiskinan. <br/>
	2. Lanjut lakukan pengisian Variabel Kemiskinan Individu pada <b>Data Anggota Keluarga</b> dengan cara klik tombol pada kolom paling kanan pada tabel. <br/>
	3. Isikan secara urut sesuai urutan keluarga, <b>Kepala Keluarga </b>untuk didahulukan.<br/>
	Untuk <b>Panduan Lengkap</b> pengoperasian pengajuan data MPM bagi verifikator, silahkan unduh disini &nbsp;&nbsp;>> &nbsp;&nbsp;
	<?php echo anchor(current_url()."?".get_params($this->input->get())."&download=1", '<i>Panduan Verifikator.pdf</i>', 'target="_blank"'); ?>
	</p>
</div>
<div class="box box-info">	
	<!-- content -->
	<div class="box-body">
		<div class="row">	
			<div class="box-body">
				<div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-info" role="alert">
				      <b>DATA KELUARGA</b>
				    </div>
			    </div>
			    <div class="col-sm-8">
			    	<table width="100%" class="table responsive" class="nobotmargin" style="font-size: 0.9em">
				    	<tr>
				    		<td width="5%"><b>1.</b></td>
				    		<td width="40%"><b>Provinsi</b></td>
				    		<td width="5%">:</td>
				    		<td><?php echo form_input('', $provinsi, 'placeholder="Propinsi" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>2.</b></td>
				    		<td><b>Kabupaten/Kota</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $kabupaten, 'placeholder="Kabupaten" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>3.</b></td>
				    		<td><b>Kecamatan</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $kecamatan, 'placeholder="Kecamatan" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>4.</b></td>
				    		<td><b>Kelurahan/Desa</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $desa, 'placeholder="Kelurahan/Desa" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>5.</b></td>
				    		<td><b>Alamat</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $alamat.' RT/RW '.$no_rt.'/'.$no_rw, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td><b>6.</b></td>
				    		<td><b>Nomor Kartu Keluarga</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $no_kk, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td colspan="2">&nbsp;</td>
				    		<td>&nbsp;</td>
				    		<td>
				    		<?php if ($updated == 1): ?>
				    			<a href="<?php echo base_url();?>index.php/prelist/daftar_rtangga?rid_rumahtangga=<?php echo $rid_rumahtangga; ?>&amp;<?php echo get_params($this->input->get(), array('rid_rumahtangga')); ?>" class="ajaxify fa-item tooltips" data-original-title="Isikan Variabel Kemiskinan Rumah Tangga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-info btn-sm"><i class="fa fa-clipboard"></i> + Input Data Kemiskinan</span></a>	&nbsp;&nbsp;
				    			<a href="<?php echo base_url();?>index.php/prelist/reset_rtangga?no_kk=<?php echo $no_kk; ?>&amp;<?php echo get_params($this->input->get(), array('no_kk')); ?>" class="ajaxify fa-item tooltips" data-original-title="Hapus Verifikasi Rumah Tangga" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Verifikasi Rumah Tangga akan dikembalikan. Yakin akan mengembalikan ?')"><span class="btn btn-success btn-sm"> <i class="fa fa-refresh"></i> Reset Rumah Tangga</span></a>
				    		<?php else: ?> 
				    			<a href="<?php echo base_url();?>index.php/prelist/edit_rtangga?<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Edit Data Rumah Tangga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Edit Data Rumah Tangga</span></a>	
				    			&nbsp;&nbsp;
				    			<a href="<?php echo base_url();?>index.php/prelist/delete_rtangga?no_kk=<?php echo $no_kk; ?>&amp;<?php echo get_params($this->input->get(), array('no_kk')); ?>" class="ajaxify fa-item tooltips" data-original-title="Hapus Data Rumah Tangga" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Rumah Tangga akan terhapus beserta Anggota Rumah Tangga. Yakin akan menghapus ?')"><span class="btn btn-danger btn-sm"> <i class="fa fa-times"></i> Delete Rumah Tangga</span></a>
				    		<?php endif ?>				    
				    		</td>
				    	</tr>
				    </table>
			    </div>
			    <?php if ($updated == 2): ?>
				    <div class="col-sm-4">
				    	<div class="alert alert-success" style="text-align: center;vertical-align: middle;"><span style="font-size: 77px" class="icon fa fa-info-circle"></span><br /><h4>Data Rumah Tangga sudah di masukkan ... !!</h4></div>
				    </div>
				<?php endif ?>
		    </div>
	    </div>

		<div class="row">	
			<div class="box-body">
				<div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-info" role="alert">
				      <b>DATA ANGGOTA KELUARGA</b>
				    </div>
				</div>

			    <div class="col-sm-12">
			    	<div class="table-responsive">
			    	<table width="100%" class="table table-bordered table-striped table-responsive" style="font-size: 0.9em">
						<thead>
							<tr>
								<th style="text-align:center">NO</th>
								<th style="text-align:center">NO KK</th>
								<th style="text-align:center">NIK</th>
								<th style="text-align:center">NO. ART</th>
								<th style="text-align:center">NO. URUT KK</th>
								<th style="text-align:center">NAMA LENGKAP</th>
								<th style="text-align:center">L/P</th>
								<th style="text-align:center">TGL LAHIR</th>
								<th style="text-align:center">HUB. RUMAHTANGGA</th>
								<th style="text-align:center">HUB. KELUARGA</th>
								<th style="text-align:center">AGAMA</th>
								<th style="text-align:center">PENDIDIKAN</th>
								<th style="text-align:center">PEKERJAAN</th>
								<th style="text-align:center">KATEGORI</th>
								<th style="text-align:center">STATUS</th>
								<th style="text-align:center"><i class="fa fa-tags"></i></th>
							</tr>
						</thead>
						<tbody>
						<?php
						$no = 1;
						$id_rt = $nomor_urut_rumah_tangga;
						foreach ($data as $row) {
							extract((array) $row);
							?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo "$no_kk"; ?></td>
								<td style="text-align:center"><?php echo "$nik"; ?></br>
									<?php if ($updated == 2): ?>
										<?php if ($proses_ind != 1): ?>
											<a href="<?php echo base_url();?>index.php/prelist/delete_individu?no_urut_art=<?php echo $no_urut_art; ?>&amp;nik=<?php echo $nik; ?>&amp;<?php echo get_params($this->input->get(), array('nik','no_urut_art')); ?>" class="ajaxify fa-item tooltips" data-original-title="Hapus Data Individu" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Individu akan terhapus. Yakin akan menghapus ?')"><span class="btn btn-danger btn-xs"> <i class="fa fa-times"></i></span></a>
										<?php endif ?>
									<?php endif ?>
								</td>
								<td><?php echo "$no_urut_art"; ?></td>
								<td>
									<?php /*echo "$b4_k4";*/ ?>
									<?php echo "$no_urut_kk"; ?>
								</td>
								<td><?php echo "$nama_lgkp"; ?></td>
								<td><?php echo "$kelamin"; ?></td>
								<td><?php echo format_tanggal($tgl_lhr); ?></td>
								<td><?php echo "$hub_ruta"; ?></td>
								<td><?php echo "$shdk"; ?></td>
								<td><?php echo "$agama_desc"; ?></td>
								<td><?php echo "$pendidikan"; ?></td>
								<td><?php echo "$pekerjaan_siak"; ?></td>
								<td align="center">
									<?php if ($kategori == 3): ?>
										<span class="label label-primary" data-original-title="Pendaftar Baru (Tidak Terdaftar di dalam PPFM/BDT.2015)" data-toggle="tooltip" data-placement="bottom">
									<?php else: ?> 
										<span class="label label-success" data-original-title="Pendaftar Lama (Terdaftar di dalam PPFM/BDT.2015)" data-toggle="tooltip" data-placement="bottom">
									<?php endif ?>
									<?php echo $kategori; ?> </span>
					            </td>
								<td align="center">
									<?php if ($proses_ind == 1): ?>
										<span class="label label-default" data-original-title="Data belum dimasukkan" data-toggle="tooltip" data-placement="bottom">
									<?php else: ?> 
										<span class="label label-success" data-original-title="Data sudah dimasukkan" data-toggle="tooltip" data-placement="bottom">
									<?php endif ?>
									<?php echo $proses_ind; ?> </span>
								</td>						
								<td align="center">
								<?php if ($updated == 2): ?>
									<?php if ($proses_ind == 1): ?>
										<a href="<?php echo base_url();?>index.php/prelist/daftar_individu?no_kk_individu=<?php echo $no_kk; ?>&amp;nik=<?php echo $nik; ?>&amp;rid_rumahtangga=<?php echo $rid_rumahtangga; ?>&amp;<?php echo get_params($this->input->get(), array('nik', 'no_kk_individu', 'rid_rumahtangga')); ?>" class="ajaxify fa-item tooltips" data-original-title="Isikan Variabel Kemiskinan Individu" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-info btn-xs">+ <i class="fa fa-clipboard"></i></span></a>
									<?php else: ?> 
										<a href="<?php echo base_url();?>index.php/prelist/edit_individu?nik=<?php echo $nik; ?>&amp;<?php echo get_params($this->input->get(), array('nik', 'no_kk_individu')); ?>" class="ajaxify fa-item tooltips" data-original-title="Edit Variabel Kemiskinan Individu" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-xs"> <i class="fa fa-pencil"></i></span></a>
									<?php endif ?>
								<?php endif ?>
								</td>
							</tr>
						<?php 
						}
						?>
		                </tbody>
					</table>
					</div>
					
				</div><!-- /.box-body -->
				<br/>
				<div class="col-md-2">
					<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/prelist/index?<?php echo get_params($this->input->get(), array('nik','no_kk_individu')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
				</div>
				<div class="col-md-8"></div>
				<div align="right" class="col-md-2">
					<a class="btn btn-success" href="<?php echo base_url(); ?>index.php/prelist/tambahKK?<?php echo get_params($this->input->get()) ?>" > <i class="fa fa-clipboard"></i>&nbsp;&nbsp;+ Tambah Keluarga</a>
				</div>
			</div>
		</div>

	</div><!-- /.box-body -->


	<!-- /content -->	
	
</div><!-- /.box -->
