<?php /*if($error != "") {
	echo validation_errors();
	} else { */

if ($show==0):?>
  <div class="alert alert-danger" style="text-align: center">
    <h4><span style="font-size: 40px" class="icon fa fa-warning"></span> <br /> Peringatan!</h4>
    Maaf, data tidak ditemukan !!.
  </div>
<?php else: ?>

<div class="table-responsive">
  <div class="row">
    <div class="col-sm-10">
      <p style="font-size: 0.9em"><i>Menampilkan <b><?php echo count($data); ?></b> dari <b><?php echo $total_data; ?></b> data.</i></p>
    </div>
    <div class="col-sm-2">
      <p class="text-right">
        <?php echo anchor(current_url()."?".get_params($this->input->get())."&export=1", '<i class="fa fa-download"></i> Download Excel', 'class="btn btn-sm btn-primary" target="_blank"'); ?>
        <?php /*echo '<button type="submit" name="export" value="1" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Download Excel</button>'*/ ?>
      </p>
      </div>
    </div>
<?php /*echo form_hidden('get_params', get_params($this->input->get()));*/ ?>

	<table width="100%" class="table table-bordered table-striped table-desa" style="font-size: 0.9em">
		<thead>
          <tr>
            <th style="text-align:center">NO</th> 
            <th style="text-align:center">NO KK</th>
            <th style="text-align:center">NIK PENDAFTAR</th>
            <th style="text-align:center">NAMA PENDAFTAR</th>
            <th style="text-align:center">NAMA KEPALA KELUARGA</th>
            <th style="text-align:center">ALAMAT</th>
            <?php /* ?><th style="text-align:center">NO RT</th>
            <th style="text-align:center">NO RW</th><?php */ ?>
            <th style="text-align:center">KELURAHAN</th>
            <th style="text-align:center">KECAMATAN</th>
            <th style="text-align:center">KATEGORI</th>
            <th style="text-align:center">PROSES</th>
            <th width="10%" style="text-align:center"><i class="fa fa-tags"></i></th>
            <th style="text-align:center">CETAK</th>
          </tr>
        </thead>
        <tbody>
      	<?php
		$no = 1+$page;
		foreach ($data as $row) {
			$row = keysToLower($row);
			extract((array) $row);
		?>
          <tr>                      
            <td><?php echo $no++; ?></td>
            <td><?php echo "$no_kk"; ?></td>
            <td><?php echo "$nik_pendaftar"; ?></td>
            <td><?php echo "$nama_pendaftar"; ?></td>
            <td><?php echo "$nama_kep_rumahtangga"; ?></td>
            <td><?php echo $alamat." RT. ".$no_rt." / RW. ".$no_rw; ?></td>
            <?php /* ?><td align="center"><?php echo "$no_rt"; ?></td>
            <td align="center"><?php echo "$no_rw"; ?></td><?php */ ?>
            <td><?php echo "$desa"; ?></td>
            <td><?php echo "$kecamatan"; ?></td>
            <td align="center">
              <?php if ($kd_pengajuan == 3): ?>
                <span class="label label-primary" data-original-title="Pendaftar Baru (Tidak Terdaftar di dalam PPFM/BDT.2015)" data-toggle="tooltip" data-placement="bottom">
              <?php else: ?> 
                <span class="label label-success" data-original-title="Pendaftar Lama (Terdaftar di dalam PPFM/BDT.2015)" data-toggle="tooltip" data-placement="bottom">
              <?php endif ?>
              <?php echo $kd_pengajuan; ?> </span>
            </td>
            <td align="center">
              <?php if ($proses == 1): ?>
                <span class="label label-default" data-original-title="Dalam Pengajuan" data-toggle="tooltip" data-placement="bottom">
              <?php elseif ($proses == 2): ?> 
                <span class="label label-warning" data-original-title="Sudah verifikasi, Data belum dimasukkan" data-toggle="tooltip" data-placement="bottom">
              <?php elseif ($proses == 3): ?> 
                <span class="label label-success" data-original-title="Sudah verifikasi, Data sudah dimasukkan" data-toggle="tooltip" data-placement="bottom">  
              <?php elseif ($proses == 4): ?> 
                <span class="label label-danger" data-original-title="Data Tidak Lolos Verifikasi" data-toggle="tooltip" data-placement="bottom">  
              <?php endif ?>
              <?php echo $proses; ?></span>
            </td>
            <td align="center">
              <?php if ($verifikasi == 1): ?>
                <a href="<?php echo base_url();?>index.php/prelist/proses?verifikasi=2&amp;no_kk=<?php echo $row->no_kk_induk; ?>&amp;uri=<?php echo $this->uri->segment(2); ?>&amp;<?php echo get_params($this->input->get(), array('no_kk','verifikasi')); ?>" class="ajaxify fa-item tooltips" data-original-title="Data Lolos Verifikasi" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-info btn-xs"><i class="fa fa-check"></i></span></a>&nbsp;&nbsp;
                <a href="<?php echo base_url();?>index.php/prelist/proses?verifikasi=3&amp;no_kk=<?php echo $row->no_kk_induk; ?>&amp;uri=<?php echo $this->uri->segment(2); ?>&amp;<?php echo get_params($this->input->get(), array('no_kk','verifikasi')); ?>" class="ajaxify fa-item tooltips" data-original-title="Tidak Lolos Verifikasi" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-danger btn-xs"><i class="fa fa-times"></i></span></a>
              <?php elseif ($verifikasi == 2): ?>
                <a href="<?php echo base_url();?>index.php/prelist/lihat?no_kk=<?php echo $row->no_kk_induk; ?>&amp;<?php echo get_params($this->input->get(), array('no_kk')); ?>" class="ajaxify fa-item tooltips" data-original-title="Masukkan Data & Lihat Anggota Keluarga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-xs"> + <i class="fa fa-clipboard"></i></span></a>
              <?php endif ?>
            </td>
            <td align="center">
              <?php echo anchor(base_url()."index.php/prelist/daftar_rtangga?no_kk=".$row->no_kk."&amp;".get_params($this->input->get(), array('no_kk'))."&print=1", '<i class="fa fa-print"></i>', 'class="btn btn-xs btn-success" target="_blank"'); ?>
            </td>
          </tr>
          <?php
          }
          ?>

        </tbody>
  </table>


  <?php echo $pagination; ?>
</div>
<?php endif ?>
<?php /*}*/ ?>