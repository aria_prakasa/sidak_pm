<?php
// var_dump($biodata_list);die;
if (!$data):
	die('DATA TIDAK ADA');
else:
?>
<table class="table table-bordered table-striped">
<thead>
  	<tr>
	    <th>NO</th> 
	    <th>NIK</th> 
	    <th>NAMA LENGKAP</th>
	    <th>ALAMAT</th>
	    <th>KELURAHAN</th>
	    <th>KECAMATAN</th>
  	</tr>
</thead>
<tbody>
<?php
$no = 1;
foreach ($data as $row) {
	$row = keysToLower($row);
	extract((array) $row);
	?>
    <tr>
		<td><?php echo $no++; ?></td>
		<td><?php echo "'$nik"; ?></td>
        <td><?php echo "$nama"; ?></td>
        <td><?php echo "$alamat"; ?></td>
        <td><?php echo "$desa"; ?></td>
        <td><?php echo "$kecamatan"; ?></td>
    </tr>
<?php
}
?>

</tbody>
</table>

<?php endif;?>