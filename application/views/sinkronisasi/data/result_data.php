<?php 
// var_dump('masuk');die;
 // var_dump($show); 

if($error != "") {
	echo validation_errors();
	} else { 

if ($show==0):?>
  <div class="alert alert-danger" style="text-align: center">
    <h4><span style="font-size: 40px" class="icon fa fa-warning"></span> <br /> Peringatan!</h4>
    Maaf, data tidak ditemukan !!.
  </div>
<?php else: ?>
<div class="table-responsive">
  <div class="row">
    <div class="col-sm-10">
      <p style="font-size: 0.9em"><i>Menampilkan <b><?php echo count($data); ?></b> dari <b><?php echo format_angka($total_data); ?></b> data.</i></p>
    </div>
    
  </div>
<?php echo form_open('sink_data/update', 'method="post" class="form-validate form-horizontal"');
echo form_hidden('get_params', get_params($this->input->get()));
?>
	<table width="100%" class="table table-bordered table-striped table-desa" style="font-size: 0.9em">
		<thead>
          <tr>
            <th>NO</th> 
            <th>NO. KK</th> 
            <th>UPDATE NIK</th> 
            <th>&nbsp;</th> 
            <th>NIK DATA PUSAT</th> 
            <th>NAMA DATA PUSAT</th>
            <th>NAMA DATA SIAK</th>
            <th>NIK SIAK</th>
            <th><i class="fa fa-tags"></i></th>
          </tr>
        </thead>
        <tbody>
      	<?php
		$no = 1+$page;
		foreach ($data as $row) {
			$row = keysToLower($row);
			extract((array) $row);
		?>
          <tr>                      
            <td><?php echo $no++; ?></td>
            <td><?php echo "$no_kk"; ?></td>
            <td><?php echo form_input('new_nik['.$nomor_urut_rumah_tangga.'|'.$no_art.']', '', 'maxlength="16" class="form-control input-sm"'); ?>&nbsp;</td>
            <td>
              <button type="submit" class="btn btn-block btn-default btn-xs"><i class="glyphicon glyphicon-save"></i></button>
            </td>
            <td><?php echo "$nik_bdt"; ?></td>
            <td><?php echo "$nama_bdt"; ?></td>
            <td><?php echo "$nama_siak"; ?></td>
            <td><?php echo "$nik_siak"; ?></td>
            <td align="center">
              <a href="<?php echo base_url();?>index.php/sink_data/lihat?id_rt=<?php echo $nomor_urut_rumah_tangga; ?>&amp;no_keluarga=<?php echo $b4_k4; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Lihat daftar keluarga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-xs"><i class="fa fa-search"></i></span></a>
            </td>            
          </tr>
          <?php
          }
          ?>

        </tbody>
	</table>

  <?php echo form_close(); ?>

	<?php echo $pagination; ?>
</div>
<?php endif ?>
<?php } ?>