<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><strong>Data Keluarga :</strong></h3>
	</div>
	<div classs="row" style="margin: 15px">
		<div class="box-body table-responsive no-padding">
		<?php echo form_open('sink_kosong/update', 'method="post" class="form-validate form-horizontal"');
		echo form_hidden('get_params', get_params($this->input->get()));
		echo form_hidden('direct', 'lihat');
		?>
		<table width="100%" class="table table-bordered table-striped" style="font-size: 0.9em">
				<thead>
					<tr>
						<th>NO</th> 
			            <th>UPDATE NIK</th> 
			            <th>&nbsp;</th> 
			            <th>NIK</th> 
			            <th>NAMA LENGKAP</th>
			            <th>KELAMIN</th>
			            <th>UMUR</th>
			            <th>ALAMAT</th>
			            <th>KELURAHAN</th>
			            <th>KECAMATAN</th>
					</tr>
				</thead>
				<tbody>
<?php
$no = 1;
// $id_rt = $nomor_urut_rumah_tangga;
foreach ($data as $row) {
	extract((array) $row);
	?>
					<tr>
						<td><?php echo $no++; ?></td>
						<?php if ($status_nik < 2): ?>
							<td><?php echo form_input('new_nik['.$nomor_urut_rumah_tangga.'|'.$no_art.']', '', 'maxlength="16" class="form-control input-sm"'); ?>&nbsp;</td>
				            <td>
				              <button type="submit" class="btn btn-block btn-default btn-xs ajaxify fa-item tooltips" data-original-title="Update NIK" data-toggle="tooltip" data-placement="bottom"><i class="glyphicon glyphicon-save"></i></button>
				            </td>
				            <?php /* ?>
							<td><?php echo form_input('new_nik', '', 'maxlength="16" class="form-control input-sm _mhn"'); ?>&nbsp;</td>
				            <td><a href="<?php echo base_url();?>index.php/sink_kosong/update?id_rt=<?php echo $nomor_urut_rumah_tangga; ?>&amp;no_art=<?php echo $no_art; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Update NIK" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-block btn-default btn-xs"><i class="glyphicon glyphicon-save"></i></span></a></td>
				            <?php */ ?>
				        <?php else: ?>
				        	<td align="center"><span class="label label-success" style="font-size: 0.9em">NIK sudah ada di SIAK</span></td>
				        	<td><a href="<?php echo base_url();?>index.php/sink_kosong/delete?nik=<?php echo $nik; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Hapus NIK" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-block btn-danger btn-xs"><i class="fa fa-times"></i></span></a></td>							        	
				        <?php endif ?>
			            <td><?php echo $nik; ?></td>
			            <td><?php echo "$nama"; ?></td>
			            <td><?php echo "$b4_k6"; ?></td>
			            <td><?php echo "$b4_k7 Thn"; ?></td>
			            <td><?php echo "$alamat"; ?></td>
			            <td><?php echo "$desa"; ?></td>
			            <td><?php echo "$kecamatan"; ?></td>
					</tr>
<?php 
}
?>
                </tbody>
			</table>
			<?php echo form_close(); ?>
		</div><!-- /.box-body -->
		
  	</div><!-- /.row-->

</div><!-- /.box -->

<div class="row">
	<div class="col-md-2">
		<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/sink_kosong/index?<?php echo get_params($this->input->get(), array('id_rt', 'no_keluarga')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
	</div>
</div><!-- /.row -->