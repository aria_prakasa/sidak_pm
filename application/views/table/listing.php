<?php
// var_dump($data_list);die;
if (!$data_list):
	die('DATA TIDAK ADA');
else:
?>
<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>NO</th>
      <th>NO KK</th>
      <th>NAMA KEP KELUARGA</th>
      <th>ALAMAT</th>
      <th>RT</th>
      <th>RW</th>
      <th>KELURAHAN</th>
      <th>KECAMATAN</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
foreach ($data_list as $row) {
	// $row = keysToLower($row);
	extract((array) $row);
	?>
  	<tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo "'{$no_kk}'"; ?></td>
        <td><?php echo "$nama_kep"; ?></td>
        <td><?php echo "$alamat"; ?></td>
        <td><?php echo "$no_rt"; ?></td>
        <td><?php echo "$no_rw"; ?></td>
        <td><?php echo "$nama_kel"; ?></td>
        <td><?php echo "$nama_kec"; ?></td>
  	</tr>
  <?php
}
?>
  </tbody>
</table>
<?php endif;?>