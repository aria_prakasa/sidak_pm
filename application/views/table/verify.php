<?php
// var_dump($biodata_list);die;
if (!$biodata_list):
	die('DATA TIDAK ADA');
else:
?>
<table class="table table-bordered table-striped">
<thead>
  	<tr>
	    <th>NO</th> 
        <th>NO KK</th>
        <th>NAMA KA.KELUARGA</th>
        <th>NIK</th>
        <th>NAMA LENGKAP</th>
        <th>STATUS KELUARGA</th>
        <th>JENIS KELAMIN</th>	
	    <th>TANGGAL LAHIR</th>
	    <th>TEMPAT LAHIR</th>
	    <th>STATUS KAWIN</th>
	    <th>AGAMA</th>
	    <th>GOL DARAH</th>
	    <th>PENDIDIKAN</th>
	    <th>PEKERJAAN_SIDAK</th>
	    <th>PEKERJAAN_SIAK</th>
        <th>ALAMAT</th>
        <th>NO RT</th>
        <th>NO RW</th>
        <th>KELURAHAN</th>
        <th>KECAMATAN</th>
  	</tr>
</thead>
<tbody>
<?php
$no = 1;
foreach ($biodata_list as $row) {
//$row = keysToLower($row);
	extract((array) $row);
	?>
    <tr>
		<td><?php echo $no++; ?></td>
		<td><?php echo "'{$no_kk}'"; ?></td>
		<td><?php echo "$nama_kep"; ?></td>
		<td><?php echo "'{$nik}'"; ?></td>
		<td><?php echo "$nama_lgkp"; ?></td>
		<td><?php echo "$stat_hbkel"; ?></td>
		<td><?php echo "$jenis_klmin"; ?></td>
		<td><?php echo "'{$tgl_lhr}'"; ?></td>
		<td><?php echo "$tmpt_lhr"; ?></td>
		<td><?php echo "$stat_kwn"; ?></td>
		<td><?php echo "$agama"; ?></td>
		<td><?php echo "$gol_drh"; ?></td>
		<td><?php echo "$pddk_akh"; ?></td>
		<td><?php echo "$jenis_pkrjn_sidak"; ?></td>
		<td><?php echo "$jenis_pkrjn_siak"; ?></td>
		<td><?php echo "$alamat"; ?></td>
		<td><?php echo "$no_rt"; ?></td>
		<td><?php echo "$no_rw"; ?></td>
		<td><?php echo "$nama_kel"; ?></td>
		<td><?php echo "$nama_kec"; ?></td>
<?php
}
?>

</tbody>
</table>

<?php endif;?>