<?php
// var_dump($biodata_list);die;
if (!$biodata_list):
	die('DATA TIDAK ADA');
else:
?>
<table class="table table-bordered table-striped">
<thead>
  	<tr>
	    <th>NO</th>
        <th>NO KK</th>
        <th>NAMA KEP.KELUARGA</th>
        <th>NIK</th>
        <th>NAMA LENGKAP</th>
        <th>JENIS KELAMIN</th>
        <th>JENIS PEKERJAAN</th>
        <th>ALAMAT</th> 
		<th>RT</th>
	    <th>RW</th>
        <th>KELURAHAN</th>
        <th>KECAMATAN</th>
  	</tr>
</thead>
<tbody>
<?php
$no = 1;
foreach ($biodata_list as $row) {
//$row = keysToLower($row);
	extract((array) $row);
	?>
    <tr>
		<td><?php echo $no++; ?></td>
        <td><?php echo "'{$no_kk}'"; ?></td>
        <td><?php echo "$nama_kep"; ?></td>
        <td><?php echo "'{$nik}'"; ?></td>
        <td><?php echo "$nama_lgkp"; ?></td>
        <td><?php echo strtoupper($jenis_klmin); ?></td>
        <td><?php echo "$jenis_pkrjn_siak"; ?></td>
        <td><?php echo "$alamat"; ?></td>
		<td><?php echo "$no_rt"; ?></td>
		<td><?php echo "$no_rw"; ?></td>
        <td><?php echo "$nama_kel"; ?></td>
        <td><?php echo "$nama_kec"; ?></td>
<?php
}
?>

</tbody>
</table>

<?php endif;?>