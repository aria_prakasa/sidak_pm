<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>_assets/dist/img/user.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $this->cu->NAMA_LENGKAP; ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> <?php echo user_desc($this->cu->USER_LEVEL); ?></a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">NAVIGASI UTAMA</li>
            <li class="<?php echo $home_nav; ?> treeview">
              <a href="<?php echo base_url(); ?>index.php/home">
                <i class="glyphicon glyphicon-home"></i> <span> Halaman Utama</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $dash_nav; ?>"><a href="<?php echo base_url(); ?>index.php/home"><i class="fa fa-caret-right"></i> Dashboard </a></li>
                <li class="<?php echo $about_nav; ?>"><a href="<?php echo base_url(); ?>index.php/about"><i class="fa fa-caret-right"></i> Tentang Kami </a></li>
              </ul>
            </li>
            
            <?php if ($this->cu->USER_LEVEL != 4) {?>
            <?php /* ?>
            <li class="<?php echo $data_nav; ?> treeview">
              <a href="<?php echo base_url(); ?>index.php/entry">
                <i class="glyphicon glyphicon-plus"></i> <span> Pemasukan Data</span>
              </a>     
            </li>
            <?php */ ?>
            <li class="<?php echo $daftar_nav; ?> treeview">
              <a href="<?php echo base_url(); ?>index.php/home">
                <i class="glyphicon glyphicon-plus"></i> <span> Pendaftaran</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $entry_nav; ?>"><a href="<?php echo base_url(); ?>index.php/entry"><i class="fa fa-caret-right"></i> Entri Data </a></li>
                <li class="<?php echo $mohon_nav; ?>"><a href="<?php echo base_url(); ?>index.php/mohon_rumahtangga"><i class="fa fa-caret-right"></i> Data Pendaftaran </a></li>
              </ul>
            </li>
            <?php } ?>

            <?php /* ?>
            <li class="<?php echo $mohon_nav; ?> treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span> Daftar Pengajuan</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $mohon_rt_nav; ?>"><a href="<?php echo base_url(); ?>index.php/mohon_rumahtangga"><i class="fa fa-caret-right"></i> Data Rumah Tangga</a></li>
                <li class="<?php echo $mohon_ind_nav; ?>"><a href="<?php echo base_url(); ?>index.php/mohon_individu"><i class="fa fa-caret-right"></i> Data Individu</a></li>
              </ul>
            </li>

            <li class="<?php echo $plus_nav; ?> treeview hide">
              <a href="#">
                <i class="glyphicon glyphicon-plus"></i> <span> Entri Kemiskinan Plus</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $kis_nav; ?> hide"><a href="<?php echo base_url(); ?>index.php/kis"><i class="fa fa-circle-o"></i> KIS (Kartu Indonesia Sehat)</a></li>
                <li class="<?php echo $bpjs_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bpjs"><i class="fa fa-circle-o"></i> BPJS</a></li>
                <li class="<?php echo $raskin_nav; ?>"><a href="<?php echo base_url(); ?>index.php/raskin"><i class="fa fa-circle-o"></i> Raskin</a></li>
                <li class="<?php echo $pkh_nav; ?>"><a href="<?php echo base_url(); ?>index.php/pkh"><i class="fa fa-circle-o"></i> PKH/Rumah Tdk Layak Huni</a></li>
              </ul>
            </li>
            <?php */ ?>

            <?php
            if ($this->cu->USER_LEVEL == 0) {?>
            <?php /* ?>
            <li class="<?php echo $verify_nav; ?> treeview hide">
              <a href="#">
                <i class="glyphicon glyphicon-download-alt"></i> <span> Verifikasi Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $apply_nav; ?>"><a href="<?php echo base_url(); ?>index.php/verify"><i class="fa fa-circle-o"></i> Permohonan Data</a></li>
                <li class="<?php echo $anomali_nav; ?>"><a href="<?php echo base_url(); ?>index.php/anomali"><i class="fa fa-circle-o"></i> Data Anomali</a></li>
                <li class="<?php echo $anggota_nav; ?>"><a href="<?php echo base_url(); ?>index.php/anggota"><i class="fa fa-circle-o"></i> Anggota Baru</a></li>
              </ul>
            </li>
            <?php */ ?>
            <li class="<?php echo $verify_nav; ?> treeview">
              <a href="#">
                <i class="fa fa-check-square-o"></i> <span> Verifikasi Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $proses_nav; ?>"><a href="<?php echo base_url(); ?>index.php/proses"><i class="fa fa-caret-right"></i> Proses Pendaftaran</a></li>
                <li class="<?php echo $rubah_nav; ?>">
                  <a href="#"><i class="fa fa-caret-right"></i> Perubahan Data (lama)<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $data_nav; ?>"><a href="<?php echo base_url(); ?>index.php/rubah_data"><i class="fa fa-caret-right"></i>  Status (Elemen Data)</a>
                    </li>
                    <li class="<?php echo $dalam_nav; ?>"><a href="<?php echo base_url(); ?>index.php/rubah_pindah/dalam"><i class="fa fa-caret-right"></i> Pindah Alamat</a>
                    </li>
                    <?php /* ?><li class="<?php echo $pindah_nav; ?>">
                      <a href="#"><i class="fa fa-caret-right"></i> Rubah Pend. Pindah <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li class="<?php echo $dalam_nav; ?>"><a href="<?php echo base_url(); ?>index.php/rubah_pindah/dalam"><i class="fa fa-caret-right"></i> Dalam Kota</a>
                        </li>
                        <li class="<?php echo $luar_nav; ?>"><a href="<?php echo base_url(); ?>index.php/rubah_pindah/luar"><i class="fa fa-caret-right"></i> Luar Kota</a>
                        </li>
                      </ul>
                    </li><?php */ ?>
                    <?php /* ?><li class="<?php echo $mati_nav; ?>"><a href="<?php echo base_url(); ?>index.php/rubah_mati"><i class="fa fa-caret-right"></i> Rubah Penduduk Mati</a>
                    </li><?php */ ?>
                  </ul>
                </li>
                <li class="<?php echo $edit_nav; ?>">
                <a href="#"><i class="fa fa-caret-right"></i> Verifikasi Pendaftaran<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $edit_baru_nav; ?>"><a href="<?php echo base_url(); ?>index.php/edit_rumahtangga"><i class="fa fa-caret-right"></i> Pengajuan Baru</a>
                    </li>
                    <li class="<?php echo $edit_rubah_nav; ?>"><a href="<?php echo base_url(); ?>index.php/edit_rumahtangga_rubah"><i class="fa fa-caret-right"></i> Pengajuan Perubahan</a>
                    </li>
                  </ul>
                </li>
                <li class="<?php echo $penetapan_nav; ?>">
                  <a href="#"><i class="fa fa-caret-right"></i> Penetapan <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $pengajuan_baru_nav; ?>"><a href="<?php echo base_url(); ?>index.php/penetapan"><i class="fa fa-caret-right"></i> Pengajuan Baru</a>
                    </li>
                    <li class="<?php echo $pengajuan_rubah_nav; ?>"><a href="<?php echo base_url(); ?>index.php/penetapan_rubah"><i class="fa fa-caret-right"></i> Pengajuan Perubahan</a>
                    </li>
                  </ul>
                  <?php /* ?><ul class="treeview-menu">
                    <li class="<?php echo $pengajuan_nav; ?>"><a href="<?php echo base_url(); ?>index.php/penetapan"><i class="fa fa-caret-right"></i> Data Pengajuan</a>
                    </li>
                  </ul><?php */ ?>
                </li>
                
                <li class="<?php echo $hasil_nav; ?>">
                  <a href="#"><i class="fa fa-caret-right"></i> Data Hasil Verifikasi<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $terima_nav; ?>"><a href="<?php echo base_url(); ?>index.php/hasil/terima"><i class="fa fa-caret-right"></i> Diterima</a></li>
                    <li class="<?php echo $tolak_nav; ?>"><a href="<?php echo base_url(); ?>index.php/hasil/tolak"><i class="fa fa-caret-right"></i> Ditolak</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <?php }?>

            <?php /* ?>
            <li class="<?php echo $miskin_nav; ?> treeview">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span> Data Kemiskinan</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $bdt_nav; ?>">
                  <a href="#"><i class="fa fa-caret-right"></i> BDT (Basis Data Terpadu) <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $bdt_rt_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bdt_rumahtangga"><i class="fa fa-caret-right"></i> Data Rumah Tangga</a></li>
                    <li class="<?php echo $bdt_ind_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bdt_individu"><i class="fa fa-caret-right"></i> Data Individu</a></li>
                    <li class="<?php echo $bdt_siak_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bdt_siak"><i class="fa fa-caret-right"></i> BDT Sanding SIAK</a></li>
                    <li class="<?php echo $bdt_mati_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bdt_mati"><i class="fa fa-caret-right"></i> BDT NIK Mati</a></li>
                    <li class="<?php echo $bdt_pindah_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bdt_pindah"><i class="fa fa-caret-right"></i> BDT NIK Pindah</a></li>
                    <li class="<?php echo $bdt_invalid_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bdt_invalid"><i class="fa fa-caret-right"></i> BDT NIK Invalid</a></li>
                    <li class="<?php echo $bdt_kosong_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bdt_kosong"><i class="fa fa-caret-right"></i> BDT NIK Kosong</a></li>
                  </ul>
                </li>
                <li class="<?php echo $bpjs_nav; ?>">
                  <a href="#"><i class="fa fa-caret-right"></i> Data BPJS <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $bpjs_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bpjs"><i class="fa fa-caret-right"></i> Data Peserta BPJS</a></li>
                    <li class="<?php echo $bpjs_siak_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bpjs_siak"><i class="fa fa-caret-right"></i> BPJS Sanding SIAK</a></li>
                    <li class="<?php echo $bpjs_mati_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bpjs_mati"><i class="fa fa-caret-right"></i> BPJS NIK Mati</a></li>
                    <li class="<?php echo $bpjs_pindah_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bpjs_pindah"><i class="fa fa-caret-right"></i> BPJS NIK Pindah</a></li>
                    <li class="<?php echo $bpjs_invalid_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bpjs_invalid"><i class="fa fa-caret-right"></i> BPJS NIK Invalid</a></li>
                    <li class="<?php echo $bpjs_kosong_nav; ?>"><a href="<?php echo base_url(); ?>index.php/bpjs_kosong"><i class="fa fa-caret-right"></i> BPJS NIK Kosong</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <?php */ ?>

            <li class="<?php echo $kemiskinan_nav; ?> treeview">
              <a href="#">
                <i class="fa fa-book"></i><span> Data Induk Kemiskinan</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $induk_rt_nav; ?>">
                  <a href="<?php echo base_url(); ?>index.php/induk"><i class="fa fa-caret-right"></i> Data Rumah Tangga <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $keseluruhan_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk_rt"><i class="fa fa-caret-right"></i>Keseluruhan</a>
                    </li>
                    <li class="<?php echo $pusat_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk_rt/pusat"><i class="fa fa-caret-right"></i>Pusat</a>
                    </li>
                    <li class="<?php echo $daerah_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk_rt/daerah"><i class="fa fa-caret-right"></i>Daerah</a>
                    </li>
                  </ul>
                </li>
                <li class="<?php echo $induk_nav; ?>">
                  <a href="<?php echo base_url(); ?>index.php/induk"><i class="fa fa-caret-right"></i> Data Individu <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $keseluruhan_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk/index"><i class="fa fa-caret-right"></i>Keseluruhan</a>
                    </li>
                    <li class="<?php echo $pusat_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk/pusat"><i class="fa fa-caret-right"></i>Pusat</a>
                    </li>
                    <li class="<?php echo $daerah_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk/daerah"><i class="fa fa-caret-right"></i>Daerah</a>
                    </li>
                  </ul>
                </li>
                <li class="<?php echo $pindah_nav; ?>">
                  <a href="#"><i class="fa fa-caret-right"></i> Data Pend. Pindah <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $dalam_nav; ?>"><a href="<?php echo base_url(); ?>index.php/anomali_pindah/dalam"><i class="fa fa-caret-right"></i> Alamat Baru / Pisah KK</a>
                    </li>
                    <li class="<?php echo $keluarga_nav; ?>"><a href="<?php echo base_url(); ?>index.php/anomali_pindah/keluarga"><i class="fa fa-caret-right"></i> Pindah Keluarga Lain</a>
                    </li>
                    <li class="<?php echo $luar_nav; ?>"><a href="<?php echo base_url(); ?>index.php/anomali_pindah/luar"><i class="fa fa-caret-right"></i> Pindah Keluar Kota</a>
                    </li>
                  </ul>
                </li>
                <li class="<?php echo $mati_nav; ?>"><a href="<?php echo base_url(); ?>index.php/anomali_mati"><i class="fa fa-caret-right"></i> Data Penduduk Mati</a>
                </li>
              </ul>
            </li>

            <?php /* ?>
            <li class="<?php echo $induk_nav; ?> treeview">
              <a href="<?php echo base_url(); ?>index.php/induk">
                <i class="fa fa-book"></i><span> Data Induk Kemiskinan</span>
              </a>            
            </li>
            <?php */ ?>

            <?php /* ?>
            <li class="<?php echo $agr_nav; ?> treeview hide">
              <a href="#">
                <i class="glyphicon glyphicon-stats"></i><span> Agregat dan Statistik</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $agr_miskin_nav; ?>"><a href="<?php echo base_url(); ?>index.php/agr_miskin"><i class="fa fa-caret-right"></i> Penduduk Miskin</a></li>
                <li class="<?php echo $agr_pend_nav; ?>"><a href="<?php echo base_url(); ?>index.php/agr_penduduk"><i class="fa fa-caret-right"></i> Kota Pasuruan</a></li>
              </ul>
            </li>
            <?php */ ?>

            <li class="<?php echo $stat_nav; ?> treeview">
              <a href="<?php echo base_url(); ?>index.php/statistik">
                <i class="glyphicon glyphicon-stats"></i><span> Statistik</span>
              </a>            
            </li>

            <?php /* ?>
            <li class="<?php echo $stat_nav; ?> treeview">
              <a href="#">
                <i class="glyphicon glyphicon-stats"></i><span> Statistik</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $stat_bdt_nav; ?>"><a href="<?php echo base_url(); ?>index.php/stat_bdt"><i class="fa fa-caret-right"></i> Statistik Basis D. Terpadu</a></li>
                <li class="<?php echo $stat_bpjs_nav; ?>"><a href="<?php echo base_url(); ?>index.php/stat_bpjs"><i class="fa fa-caret-right"></i> Statistik BPJS</a></li>
              </ul>
            </li>
            <?php */ ?>

            <li class="<?php echo $sink_nav; ?> treeview">
              <a href="#">
                <i class="glyphicon glyphicon-refresh"></i><span> Sinkronisasi Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $sink_invalid_nav; ?>"><a href="<?php echo base_url(); ?>index.php/sink_invalid"><i class="fa fa-caret-right"></i> Update NIK Invalid</a></li>
                <li class="<?php echo $sink_kosong_nav; ?>"><a href="<?php echo base_url(); ?>index.php/sink_kosong"><i class="fa fa-caret-right"></i> Update NIK Kosong</a></li>
                <li class="<?php echo $sink_data_nav; ?>"><a href="<?php echo base_url(); ?>index.php/sink_data"><i class="fa fa-caret-right"></i> Update Data Pusat(by Name)</a></li>
              </ul>
            </li>

          </ul>
      </section>
        <!-- /.sidebar -->
</aside>