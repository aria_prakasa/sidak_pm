<div class="box box-info">
	
	<div class="box-header with-border">
	<?php /* ?>
		<h3 class="box-title">Data Rumah Tangga: </h3>
	<?php */ ?>
	</div>
	
	<div class="form-horizontal">
	<!-- content -->
		<div class="box-body">
			<table width="100%" class="table table-bordered table-striped table-desa" style="font-size: 0.9em">
				<tbody>
					<tr>
						<td width="25%">No. Peserta BPJS</td>
						<td width="3%">:</td>
						<td><?php echo "$no_bpjs"; ?></td>	
					</tr>
					<tr>
						<td>Jenis Kepesertaan</td>
						<td>:</td>
						<td><?php echo "$jenis"; ?></td>	
					</tr>
					<tr>
						<td>Provinsi</td>
						<td>:</td>
						<td><?php echo "$propinsi"; ?></td>	
					</tr>
					<tr>
						<td>Kabupaten</td>
						<td>:</td>
						<td><?php echo "$kota"; ?></td>	
					</tr>
					<tr>
						<td>Kecamatan</td>
						<td>:</td>
						<td><?php echo "$kecamatan"; ?></td>	
					</tr>
					<tr>
						<td>Kelurahan</td>
						<td>:</td>
						<td><?php echo "$kelurahan"; ?></td>	
					</tr>
					<tr>
						<td>Alamat tempat tinggal</td>
						<td>:</td>
						<td><?php echo "$alamat"; ?></td>	
					</tr>
					<tr>
						<td>N.I.K</td>
						<td>:</td>
						<td><?php echo "$nik"; ?></td>	
					</tr>
					<tr>
						<td>Nama lengkap</td>
						<td>:</td>
						<td><?php echo "$nama"; ?></td>	
					</tr>
					<tr>
						<td>Jenis kelamin</td>
						<td>:</td>
						<td><?php echo "$jenis_klmin"; ?></td>	
					</tr>
					<tr>
						<td>Tanggal lahir</td>
						<td>:</td>
						<td><?php echo strtoupper(format_tanggal($tgl_lhr)); ?></td>	
					</tr>
					<tr>
						<td>Status Hubungan Dalam Keluarga</td>
						<td>:</td>
						<td><?php echo "$shdk"; ?></td>	
					</tr>
				</tbody>
			</table>		

		</div><!-- /.box-body -->		
	<!-- /content -->	
	</div>	
</div><!-- /.box -->	

<div class="row form-group">
	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
		<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/bdt_bpjs/index?<?php echo get_params($this->input->get(), array('no_bpjs')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
		<button type="submit" name="proses" value="kepala" class="btn btn-success"><i class="fa fa-print"></i>&nbsp;&nbsp;Cetak</button>
		<!-- <button type="button" class="btn btn-danger" name="reset_form" onclick="this.form.reset();"><i class="fa fa-times"></i> Kembalikan Awal</button> -->
		<!-- <input type="button" class="btn btn-danger" name="reset_form" value="Hapus" onclick="this.form.reset();"> -->
	</div>
</div>