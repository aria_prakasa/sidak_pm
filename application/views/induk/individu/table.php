<?php
// var_dump($biodata_list);die;
if (!$data):
	die('DATA TIDAK ADA');
else:
?>
<table class="table table-bordered table-striped">
<thead>
  	<tr>
	    <th>NO</th>
      <th>NO KK</th>
      <th>NIK</th>
      <th>NAMA LENGKAP</th>
      <th>KELAMIN</th>
      <th>TGL LAHIR</th>
      <th>STATUS</th>
      <th>HUB. KELUARGA</th>
      <th>AGAMA</th>
      <th>PENDIDIKAN</th>
      <th>PEKERJAAN DI SIAK</th>
      <th>ALAMAT</th>
      <th>NO RT</th>
      <th>NO RW</th>
      <th>KELURAHAN</th>
      <th>KECAMATAN</th>
  	</tr>
</thead>
<tbody>
<?php
$no = 1;
foreach ($data as $row) {
	$row = keysToLower($row);
	extract((array) $row);
	?>
    <tr>
		<td><?php echo $no++; ?></td>
        <td><?php echo "'$no_kk"; ?></td>
        <td><?php echo "'$nik"; ?></td>
        <td><?php echo "$nama_lgkp"; ?></td>
        <td><?php echo "$kelamin"; ?></td>
        <td><?php echo "'$tgl_lhr"; ?></td>
        <td><?php echo "$stat_kwn_desc"; ?></td>
        <td><?php echo "$shdk"; ?></td>
        <td><?php echo "$agama_desc"; ?></td>
        <td><?php echo "$pendidikan"; ?></td>
        <td><?php echo "$pekerjaan_siak"; ?></td>
        <td><?php echo "$alamat"; ?></td>
        <td><?php echo "$no_rt"; ?></td>
        <td><?php echo "$no_rw"; ?></td>
        <td><?php echo "$kelurahan"; ?></td>
        <td><?php echo "$kecamatan"; ?></td>
    </tr>
<?php
}
?>

</tbody>
</table>

<?php endif;?>