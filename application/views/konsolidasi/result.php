<?php /*if($error != "") {
	echo validation_errors();
	} else { */

if ($show==0):?>
  <div class="alert alert-danger" style="text-align: center">
    <h4><span style="font-size: 40px" class="icon fa fa-warning"></span> <br /> Peringatan!</h4>
    Maaf, data tidak ditemukan !!.
  </div>
<?php else: ?>

<div class="table-responsive">
  <div class="row">
    <div class="col-sm-10">
      <p style="font-size: 0.9em"><i>Menampilkan <b><?php echo count($data); ?></b> dari <b><?php echo $total_data; ?></b> data.</i></p>
    </div>
    <div class="col-sm-2"><?php  ?>
      <p class="text-right">
        <?php echo anchor(current_url()."?".get_params($this->input->get())."&konsolidate=1", '<i class="fa fa-link"></i>&nbsp;&nbsp;&nbsp; Konsolidasikan Sekarang', 'class="btn btn-sm btn-success" target="_blank"'); ?>
      </p><?php  ?>
    </div>
  </div>
<?php /*echo form_hidden('get_params', get_params($this->input->get()));*/ ?>

	<table width="100%" class="table table-bordered table-striped table-desa" style="font-size: 0.9em">
		<?php if ($table_id == 1): ?>
    <thead>
      <tr>
        <th style="text-align:center">NO</th> 
        <th style="text-align:center">NO KK RUTA</th>
        <th style="text-align:center">NIK RUTA</th>
        <th style="text-align:center">NAMA KEPALA RUTA</th>
        <th style="text-align:center">ALAMAT</th>
        <?php /* ?><th style="text-align:center">NO RT</th>
        <th style="text-align:center">NO RW</th><?php */ ?>
        <th style="text-align:center">KELURAHAN</th>
        <th style="text-align:center">KECAMATAN</th>
      </tr>
    </thead>
    <tbody>
      	<?php
		$no = 1+$page;
		foreach ($data as $row) {
			$row = keysToLower($row);
			extract((array) $row);
		?>
      <tr>                      
        <td><?php echo $no++; ?></td>
        <td><?php echo "$no_kk"; ?></td>
        <td><?php echo "$nik_kep_rumahtangga"; ?></td>
        <td><?php echo "$b1_r8"; ?></td>
        <td><?php echo "$b1_r6"; ?></td>
        <?php /* ?><td align="center"><?php echo "$no_rt"; ?></td>
        <td align="center"><?php echo "$no_rw"; ?></td><?php */ ?>
        <td><?php echo "$desa"; ?></td>
        <td><?php echo "$kecamatan"; ?></td>
      </tr>
      <?php
      }
      ?>
    </tbody>

    <?php elseif ($table_id == 2): ?> 
    <thead>
      <tr>
        <th style="text-align:center">NO</th> 
        <th style="text-align:center">NO KK RUTA</th>
        <th style="text-align:center">NIK</th>
        <th style="text-align:center">NAMA LENGKAP</th>
        <th style="text-align:center">L/P</th>
        <th style="text-align:center">UMUR</th>
        <th style="text-align:center">STATUS HUB. RUTA</th>
        <th style="text-align:center">KELURAHAN</th>
        <th style="text-align:center">KECAMATAN</th>
      </tr>
    </thead>
    <tbody>
        <?php
    $no = 1+$page;
    foreach ($data as $row) {
      $row = keysToLower($row);
      extract((array) $row);
    ?>
      <tr>                      
        <td><?php echo $no++; ?></td>
        <td><?php echo "$no_kk"; ?></td>
        <td><?php echo "$nik"; ?></td>
        <td><?php echo "$nama"; ?></td>
        <td><?php echo formatkelamin($b4_k6); ?></td>
        <td><?php echo "$b4_k7"; ?> Tahun</td>
        <td><?php echo format_stat_rtangga($b4_k3); ?></td>
        <td><?php echo "$desa"; ?></td>
        <td><?php echo "$kecamatan"; ?></td>
      </tr>
      <?php
      }
      ?>
    </tbody>

    <?php endif ?>
  </table>


  <?php echo $pagination; ?>
</div>
<?php endif ?>
<?php /*}*/ ?>