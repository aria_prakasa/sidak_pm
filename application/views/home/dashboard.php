<?php /*echo $user_level;*/ ?>
<?php if ($user_level == 3) : ?>
<div class="callout callout-info">
  <h4><i class="fa fa-info">&nbsp;</i>Info :</h4>
  <p>Untuk <b>Panduan Lengkap</b> pengoperasian pengajuan data MPM bagi Operator Kelurahan, silahkan unduh disini  &nbsp;>> &nbsp;&nbsp;
      <?php echo anchor(current_url()."?".get_params($this->input->get())."&download=3", '<i>Panduan Operator Kelurahan.pdf</i>', 'target="_blank"'); ?></p>
</div>
<?php elseif ($user_level == 1): ?>
<div class="callout callout-info">
  <h4><i class="fa fa-info">&nbsp;</i>Info :</h4>
  <p>Untuk <b>Panduan Lengkap</b> pengoperasian pengajuan data MPM bagi Verifikator, silahkan unduh disini  &nbsp;>> &nbsp;&nbsp;
      <?php echo anchor(current_url()."?".get_params($this->input->get())."&download=2", '<i>Panduan Verifikator.pdf</i>', 'target="_blank"'); ?></p>
</div>
<?php else: ?>
<div class="callout callout-info">
  <h4><i class="fa fa-info">&nbsp;</i>Info :</h4>
  <p>Untuk <b>Panduan Lengkap</b> pengoperasian pengajuan data MPM, silahkan unduh disini  &nbsp;>> &nbsp;&nbsp;
      <?php echo anchor(current_url()."?".get_params($this->input->get())."&download=1", '<i>Panduan Sidak.pdf</i>', 'target="_blank"'); ?></p>
</div>
<?php endif ?>

<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title"><strong>Data Wilayah :</strong></h3>
  </div>

  <div class="box-body" clearfix>
    <div class="row">
      <div class="col-md-6">
        <table class="responsive" width="100%">
          <tr>
            <td width="40%" padding-right="5">Kode - Nama Propinsi</td>
            <td>:</td>
            <td><b>35 - JAWA TIMUR</b></td>
          </tr>
          <tr>
            <td>Kode - Nama Kota/Kabupaten</td>
            <td>:</td>
            <td><b>75 - KOTA PASURUAN</b></td>
          </tr>
          <?php if ($this->cu->NO_KEC): ?>
            <?php if ($this->cu->NO_KEL): ?>  
              <tr>
                <td>Kode - Nama Kecamatan</td>
                <td>:</td>
                <td><b>0<?php echo $no_kec; ?> - <?php echo $nama_kec; ?></b></td>
              </tr>
              <tr>
                <td>Kode - Nama Kelurahan</td>
                <td>:</td>
                <td><b><?php echo $no_kel; ?> - <?php echo $nama_kel; ?></b></td>
              </tr>
            <?php else: ?>
              <tr>
                <td>Kode - Nama Kecamatan</td>
                <td>:</td>
                <td><b>0<?php echo $no_kec; ?> - <?php echo $nama_kec; ?></b></td>
              </tr>
            <?php endif ?>
          <?php endif ?>
        </table>    
      </div>
    </div>  
    <br/>
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3><?php echo number_format($kpl_jml); ?></h3>
            <p>Jumlah Kepala Keluarga</p>
          </div>
          <div class="icon">
            <i class="ion ion-person"></i>
          </div>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3><?php echo number_format($agr_jml); ?></h3>
            <p>Jumlah Penduduk</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-stalker"></i>
          </div>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3><?php echo number_format($agr_lk); ?></h3>
            <p>Jumlah Penduduk Laki-laki</p>
          </div>
          <div class="icon">
            <i class="ion ion-male"></i>
          </div>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3><?php echo number_format($agr_pr); ?></h3>
            <p>Jumlah Penduduk Perempuan</p>
          </div>
          <div class="icon">
            <i class="ion ion-female"></i>
          </div>
        </div>
      </div><!-- ./col -->
      
    </div><!-- /.row -->
  </div><!-- /.box body-->
</div><!-- /.box -->

<div class="row">
  <div class="col-lg-6">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title"><strong>Data Kemiskinan</strong></h3>
      </div>
      <div class="box-body" clearfix>
        <div class="row">
          <div class="col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="ion ion-person"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Jumlah Kepala Keluarga Miskin</span>
                <span class="info-box-number"><?php echo number_format($miskinkpl_jml); ?>&nbsp;</span><small>Kepala Keluarga</small>
              </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row">
          <div class="col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="ion ion-person-stalker"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Jumlah Penduduk Miskin</span>
                <span class="info-box-number"><?php echo number_format($miskin_jml); ?>&nbsp;</span><small>Penduduk</small>
              </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row">
          <div class="col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-green"><i class="ion ion-male"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Laki-laki</span>
                <span class="info-box-number"><?php echo number_format($miskin_lk); ?>&nbsp;</span><small>Penduduk</small>
              </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row">
          <div class="col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="ion ion-female"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Perempuan</span>
                <span class="info-box-number"><?php echo number_format($miskin_pr); ?>&nbsp;</span><small>Penduduk</small>
              </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        
      </div><!-- /.box body-->
    </div><!-- /.box -->
  </div>
  <div class="col-lg-6">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title"><strong>Data Kemiskinan Chart</strong></h3>
      </div>
      <div class="box-body">
          <canvas id="pieChart" style="height:250px" auto-legend></canvas>
          
      </div><!-- /.box-body -->
    </div><!-- /.box -->  
  </div>
</div>

<div class="row">
  <div class="col-lg-4">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title"><strong>Progress Pengajuan (Kepala Keluarga)</strong></h3>
      </div>
      <div class="box-body" clearfix>
        <!-- Info Boxes Style 2 -->
        <div class="info-box bg-green">
          <span class="info-box-icon"><i class="ion ion-man"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">LAKI - LAKI</span>
            <span class="info-box-number"><?php echo number_format($mohon_lk); ?></span>
            <div class="progress">
              <?php 
                $l = ($mohon_lk / $mohon_jml) * 100;
              ?>
              <div class="progress-bar" style="width: <?php echo "$l"; ?>%"></div>
            </div>
            <span class="progress-description">
              <?php echo number_format($l,2); ?>% dari <?php echo number_format($mohon_jml); ?> pengajuan laki-laki
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
        <div class="info-box bg-yellow">
          <span class="info-box-icon"><i class="ion ion-woman"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">PEREMPUAN</span>
            <span class="info-box-number"><?php echo number_format($mohon_pr); ?></span>
            <div class="progress">
              <?php 
                $p = ($mohon_pr / $mohon_jml) * 100;
              ?>
              <div class="progress-bar" style="width: <?php echo "$p"; ?>%"></div>
            </div>
            <span class="progress-description">
              <?php echo number_format($p,2); ?>% dari <?php echo number_format($mohon_jml); ?> pengajuan perempuan
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
        <div class="info-box bg-red">
          <span class="info-box-icon"><i class="ion ion-android-warning"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">PERMOHONAN BELUM VERIFIKASI</span>
            <span class="info-box-number"><?php echo number_format($mohon_jml-$verifikasi_jml); ?></span>
            <div class="progress">
              <?php 
                $m = ($verifikasi_jml / $mohon_jml) * 100;
              ?>
              <div class="progress-bar" style="width: <?php echo "$m"; ?>%"></div>
            </div>
            <span class="progress-description">
              <?php echo number_format($m,2); ?>% dari total <?php echo number_format($mohon_jml); ?> pengajuan
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
        
      </div><!-- /.box body-->
    </div><!-- /.box -->
  </div>

  <div class="col-lg-4">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title"><strong>Sudah Diverifikasi (Kepala Keluarga)</strong></h3>
      </div>
      <div class="box-body" clearfix>
        <!-- Info Boxes Style 2 -->
        <div class="info-box bg-green">
          <span class="info-box-icon"><i class="ion ion-man"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">LAKI - LAKI</span>
            <span class="info-box-number"><?php echo number_format($verifikasi_lk); ?></span>
            <div class="progress">
              <?php 
                $l = ($verifikasi_lk / $verifikasi_jml) * 100;
              ?>
              <div class="progress-bar" style="width: <?php echo "$l"; ?>%"></div>
            </div>
            <span class="progress-description">
              <?php echo number_format($l,2); ?>% dari <?php echo number_format($verifikasi_jml); ?> jumlah verifikasi
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
        <div class="info-box bg-yellow">
          <span class="info-box-icon"><i class="ion ion-woman"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">PEREMPUAN</span>
            <span class="info-box-number"><?php echo number_format($verifikasi_pr); ?></span>
            <div class="progress">
              <?php 
                $p = ($verifikasi_pr / $verifikasi_jml) * 100;
              ?>
              <div class="progress-bar" style="width: <?php echo "$p"; ?>%"></div>
            </div>
            <span class="progress-description">
              <?php echo number_format($p,2); ?>% dari <?php echo number_format($verifikasi_jml); ?> jumlah verifikasi
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
        <?php /* ?>
        <div class="info-box bg-red">
          <span class="info-box-icon"><i class="ion ion-android-warning"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">VERIFIKASI BELUM DITETAPKAN</span>
            <span class="info-box-number"><?php echo number_format($verifikasi_jml-$penetapan_jml); ?></span>
            <div class="progress">
              <?php 
                $m = ($penetapan_jml / $verifikasi_jml) * 100;
              ?>
              <div class="progress-bar" style="width: <?php echo "$m"; ?>%"></div>
            </div>
            <span class="progress-description">
              <?php echo number_format($m,2); ?>% dari total <?php echo number_format($verifikasi_jml); ?> pengajuan diverifikasi
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
        <?php */ ?>
      </div><!-- /.box body-->
    </div><!-- /.box -->
  </div>

  <div class="col-lg-4">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"><strong>Hasil Verifikasi</strong></h3>
      </div>
      <div class="box-body" clearfix>
        <!-- Info Boxes Style 2 -->
        <div class="info-box bg-blue">
          <span class="info-box-icon"><i class="fa fa-check"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">KELUARGA LOLOS VERIFIKASI</span>
            <span class="info-box-number"><?php echo number_format($penetapan_lolos); ?></span>
            <div class="progress">
              <?php 
                $l = ($penetapan_lolos / $penetapan_jml) * 100;
              ?>
              <div class="progress-bar" style="width: <?php echo "$l"; ?>%"></div>
            </div>
            <span class="progress-description">
              <?php echo number_format($l,2); ?>% dari <?php echo number_format($penetapan_jml); ?> jumlah penetapan
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
        <div class="info-box bg-red">
          <span class="info-box-icon"><i class="fa fa-times"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">KELUARGA TIDAK LOLOS VERIFIKASI</span>
            <span class="info-box-number"><?php echo number_format($penetapan_tidak_lolos); ?></span>
            <div class="progress">
              <?php 
                $p = ($penetapan_tidak_lolos / $penetapan_jml) * 100;
              ?>
              <div class="progress-bar" style="width: <?php echo "$p"; ?>%"></div>
            </div>
            <span class="progress-description">
              <?php echo number_format($p,2); ?>% dari <?php echo number_format($penetapan_jml); ?> jumlah penetapan
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
        
      </div><!-- /.box body-->
    </div><!-- /.box -->
  </div>
</div>

<?php /* ?>
<div class="col-md-6">
  
</div>
<?php */ ?>
<script type="text/javascript">
$(function () {
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = [
    {
      value: <?php echo $miskin_lk; ?>,
      color: "#00a65a",
      highlight: "#00a65a",
      label: "Laki-laki"
    },
    {
      value: <?php echo $miskin_pr; ?>,
      color: "#f39c12",
      highlight: "#f39c12",
      label: "Perempuan"
    }
  ];
  var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    //String - The colour of each segment stroke
    segmentStrokeColor: "#fff",
    //Number - The width of each segment stroke
    segmentStrokeWidth: 2,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps: 100,
    //String - Animation easing effect
    animationEasing: "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);

  document.getElementById('js-legend').innerHTML = pieChart.generateLegend();

});
</script>
