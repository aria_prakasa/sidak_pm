
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title"><b>Pengantar</b></h3>
  </div>
  <div class="box-body">
    <div class="row margin-bottom">
        <div class="col-sm-3" align="center">
          <img class="img-responsive" src="<?php echo base_url(); ?>_assets/dist/img/logo.png" alt="Photo" style="margin-left: 20px;margin-bottom: 10px;margin-right: 20px;">
          <p><b><i> Logo Pemerintah Kota Pasuruan </i></b></p>
        </div><!-- /.col -->
        <div class="col-sm-9">
        	<p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Saat ini Pemerintah telah memiliki Data Terpadu Program Penanganan Fakir Miskin (PPFM) yang bersumber dari data Pemutakhiran Basis Data Terpadu tahun 2015 dan dikelola oleh Kelompok Kerja Pengelola Data Terpadu Program Penanganan Fakir Miskin. Data Terpadu PPFM, yang berisikan nama dan alamat 40 persen penduduk dengan status kesejahteraan terendah di Indonesia, telah digunakan sebagai acuan data tunggal rumah tangga dan individu kurang mampu sasaran penerima bantuan program perlindungan sosial di Indonesia.
        	</p> 

        	<p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Namun pada kenyataan di lapangan, data tersebut dirasakan masih kurang dari segi kevalidan datanya. Dengan bantuan penyandingan Data Kependudukan dari Dinas Kependudukan dan Pencatatan sipil, ditemukan beberapa data dinyatakan sudah tidak aktif dan sebagian lain dinyatakan sudah tidak update. Tidak Aktif disini dalam artian data penduduk tersebut sudah tidak berdomisili di kota pasuruan atau sudah meninggal dunia. Sedangkan untuk data tidak update disini berarti data penduduk tersebut sudah tidak memiliki komposisi keluarga yang sama dengan data PBDT tahun 2015.
          </p>

        	<p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berdasar dari konsep diatas maka dibuatlah sebuah sistem informasi dalam rangka mempermudah proses penyandingan data Terpadu PBDT 2015 dengan Data Kependudukan, sehingga dalam perjalannya dapat diketahui perubahan serta mobilitas data penduduk atau dalam artian bias mendeteksi secara dini perubahan data penduduk dalam kategori miskin. 
          </p> 
        	
        	 
        </div><!-- /.col -->
      </div>
  </div><!-- /.box-body -->
</div>