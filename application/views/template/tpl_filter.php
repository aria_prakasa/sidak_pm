<script type="text/javascript">
function toggleStatusKk() {
    if ($('#toggleElementKk').is(':checked')) {
        $('#toggleKk :input').removeAttr('readonly');
    } else {
        $('#toggleKk :input').attr('readonly', true);
        $('#toggleKk :input').val("");
    }
}
function toggleStatusNik() {
    if ($('#toggleElementNik').is(':checked')) {
        $('#toggleNik :input').removeAttr('readonly');
    } else {
        $('#toggleNik :input').attr('readonly', true);
        $('#toggleNik :input').val("");
    }
}
function toggleStatusNama() {
    if ($('#toggleElementNama').is(':checked')) {
        $('#toggleNama :input').removeAttr('readonly');
    } else {
        $('#toggleNama :input').attr('readonly', true);
        $('#toggleNama :input').val("");
    }
}
function toggleStatusRt() {
    if ($('#toggleElementRt').is(':checked')) {
        $('#toggleRt :input').removeAttr('readonly');
    } else {
        $('#toggleRt :input').attr('readonly', true);
        $('#toggleRt :input').val("");
    }
}
function toggleStatusRw() {
    if ($('#toggleElementRw').is(':checked')) {
        $('#toggleRw :input').removeAttr('readonly');
    } else {
        $('#toggleRw :input').attr('readonly', true);
        $('#toggleRw :input').val("");
    }
}
function toggleStatusProses() {
    if ($('#toggleElementProses').is(':checked')) {
        $('#toggleProses :input').removeAttr('disabled');
    } else {
        $('#toggleProses :input').attr('disabled', true);
        $('#toggleProses :input').val("");
    }
}
function toggleStatusEntri() {
    if ($('#toggleElementEntri').is(':checked')) {
        $('#toggleEntri :input').removeAttr('disabled');
    } else {
        $('#toggleEntri :input').attr('disabled', true);
        $('#toggleEntri :input').val("");
    }
}
function toggleStatusPengajuan() {
    if ($('#toggleElementPengajuan').is(':checked')) {
        $('#togglePengajuan :input').removeAttr('disabled');
    } else {
        $('#togglePengajuan :input').attr('disabled', true);
        $('#togglePengajuan :input').val("");
    }
}

/*jQuery(function($){
    $("#tgl_awal").mask("99-99-9999");
    $("#tgl_akhir").mask("99-99-9999");
    // $("#tgl_lapor").mask("99-99-9999");
    // $("#tgl").mask("99-99-9999");
    // $("#npwp").mask("99-999-999-9-999-999");
});*/

</script>
<?php 
// $attributes = array('class' => 'form-validate form-horizontal', 'id' => 'check');
echo form_open('', 'id="check" method="get" class="form-validate form-horizontal"') 
// echo form_open('', $attributes);
?>
<div class="box-body">
<div class="table-responsive">
  <div class="row table no-border">
  <?php
  $provinsi = $this->model_wilayah->ambil_provinsi();
  $kabupaten = $this->model_wilayah->ambil_kabupaten();
  $in_array = null; //default
  if($this->global_data['no_kec'] != "") {
    $kecamatan_id = $this->global_data['no_kec'];
    $in_array = array($kecamatan_id);
  }
  $kecamatan = $this->model_wilayah->ambil_kecamatan($in_array);

  $kelurahan = array('Pilih Kelurahan' => '- Pilih Kecamatan Terlebih Dahulu -'); // default
  if($this->global_data['no_kec'] != "" && $this->global_data['no_kel'] != "") {
    $kel_id = $this->global_data['no_kel'];
    $kec_id = $this->global_data['no_kec'];
    $in_array = array($kel_id);
    $kelurahan = $this->model_wilayah->ambil_kelurahan($this->global_data['no_kec'], $in_array);
    $kelurahan_id = $kec_id."-".$kel_id;
    // var_dump($kelurahan_id);
  }
  if($this->input->post()) {
    extract($this->input->post());
  }
  ?>

    <div class="col-sm-6">
      <div class="form-group">
      
        <table width="100%" class="table" >
          <tr>
            <td width="30%">
              <label class="col-sm-4 control-label">Propinsi</label>
            </td>
            <td>
              <div class="col-sm-8">
              <?php
                $style_provinsi = 'class="form-control input-sm" id="provinsi_id"';
                echo form_dropdown('provinsi_id', $provinsi, NO_PROP, $style_provinsi);
              ?>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <label class="col-sm-4 control-label">Kota/Kab</label>
            </td>
            <td>
              <div class="col-sm-8">
              <?php
                $style_kabupaten = 'class="form-control input-sm" id="kabupaten_id"';
                echo form_dropdown("kabupaten_id", $kabupaten, NO_KAB, $style_kabupaten);
              ?>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <label class="col-sm-4 control-label">Kecamatan</label>
            </td>
            <td>
              <div class="col-sm-8">
              <?php
              // var_dump($kecamatan, $kecamatan_id);die;
              ?>
              <?php
                $style_kecamatan = 'class="form-control input-sm" id="kecamatan_id" onChange="tampilKelurahan()"';
                echo form_dropdown("kecamatan_id", $kecamatan, $kecamatan_id, $style_kecamatan);
              ?>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <label class="col-sm-4 control-label">Kelurahan</label>
            </td>
            <td>
              <div class="col-sm-8">
              <?php
                $style_kelurahan = 'disabled class="form-control input-sm" id="kelurahan_id"';
                echo form_dropdown("kelurahan_id", $kelurahan, $kelurahan_id, $style_kelurahan);
              ?>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <label class="col-sm-4 control-label">&nbsp;</label>
            </td>
            <td>
              <div class="col-sm-8">
                <button type="submit" name="tampil" value="1" class="btn bg-maroon">
                <span class="fa fa-search"></span>&nbsp;Tampilkan</button>
              </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
    

    <?php  ?>
    <?php if ( stristr($this->uri->segment(1), 'stat') ):
        $this->load->view('template/filter_statistik'); ?>
    <?php elseif ( stristr($this->uri->segment(1), 'hasil') || 
      stristr($this->uri->segment(1), 'rubah_pindah') ||
      stristr($this->uri->segment(1), 'entry')):
          $this->load->view('template/filter_nik'); ?>
    <?php elseif ( stristr($this->uri->segment(1), 'daftar') ):
          $this->load->view('template/filter_daftar'); ?>  
    <?php elseif ( stristr($this->uri->segment(1), 'sink') ):
          $this->load->view('template/filter_bdt'); ?>  
    <?php elseif ( stristr($this->uri->segment(1), 'induk') || 
      stristr($this->uri->segment(1), 'anomali') ||
      stristr($this->uri->segment(1), 'release')):
          $this->load->view('template/filter_alamat'); ?>  
    <?php elseif ( stristr($this->uri->segment(1), 'prelist') ):
          $this->load->view('template/filter_prelist'); ?>  
    <?php else: ?>
      <?php $this->load->view('template/filter_kk'); ?>
    <?php endif;?>
    <?php  ?>
  </div>
</div>
</div>



<?php /* ?>
  <div class="col-sm-6">
    <div class="form-group">
      <label class="col-sm-4 control-label">Propinsi</label>
      <div class="col-sm-8">
      <?php
$style_provinsi = 'class="form-control input-sm" id="provinsi_id"';
echo form_dropdown('provinsi_id', $provinsi, NO_PROP, $style_provinsi);
?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">Kota/Kab</span></label>
      <div class="col-sm-8">
      <?php
$style_kabupaten = 'class="form-control input-sm" id="kabupaten_id"';
echo form_dropdown("kabupaten_id", $kabupaten, NO_KAB, $style_kabupaten);
?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">Kecamatan</label>
      <div class="col-sm-8">
      <?php
// var_dump($kecamatan, $kecamatan_id);die;
?>
<?php
$style_kecamatan = 'class="form-control input-sm" id="kecamatan_id" onChange="tampilKelurahan()"';
echo form_dropdown("kecamatan_id", $kecamatan, $kecamatan_id, $style_kecamatan);
?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">Kelurahan</label>
      <div class="col-sm-8">
        <div id="kelurahan_id_wrap" data-id="<?php echo $kelurahan_id; ?>">
        <?php
        $style_kelurahan = 'disabled class="form-control input-sm" id="kelurahan_id"';
        echo form_dropdown("kelurahan_id", $kelurahan, $kelurahan_id, $style_kelurahan);
        ?>
        </div>
      </div>
    </div>
  </div>
<?php if ( stristr($this->uri->segment(1), 'stat') ):
      $this->load->view('template/filter_statistik'); ?>
<?php elseif ( stristr($this->uri->segment(1), 'hasil') || 
  stristr($this->uri->segment(1), 'rubah_pindah') ||
  stristr($this->uri->segment(1), 'anomali') ||
  stristr($this->uri->segment(1), 'entry') ||
  stristr($this->uri->segment(1), 'daftar')):
      $this->load->view('template/filter_nik'); ?>
<?php elseif ( stristr($this->uri->segment(1), 'sink') ):
      $this->load->view('template/filter_bdt'); ?>  
<?php elseif ( stristr($this->uri->segment(1), 'induk') ):
      $this->load->view('template/filter_alamat'); ?>  
<?php else: ?>
  <?php $this->load->view('template/filter_kk'); ?>
<?php endif;?>

</div>
<?php */ ?>
<?php /* ?>
<div class="row">  
  <div class="col-sm-6">
    <div class="form-group">
      <div class="col-sm-4"></div>
      <div class="col-sm-4">
          <button type="submit" name="tampil" value="1" class="btn bg-maroon"><span class="fa fa-search"></span>&nbsp;Tampilkan</button>
      </div>
    </div>
  </div>
</div>
<?php */ ?>
<?php echo form_close(); ?>

<?php /* ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#check')
        .formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                inputnama: {
                    message: 'The username is not valid',
                    validators: {
                        
                        stringLength: {
                            min: 3,
                            //max: 50,
                            message: 'Nama harus lebih dari 3 karakter'
                        },
                        
                        regexp: {
                            regexp: /^[a-zA-Z \.]+$/,
                            message: 'Nama hanya dapat berisi huruf alphabet'
                        }
                    }
                },
                inputnik: {
                    message: 'The username is not valid',
                    validators: {
                        
                        stringLength: {
                            min: 16,
                            max: 16,
                            message: 'NIK harus berisi 16 digit'
                        },
                        
                        regexp: {
                            regexp: /^[0-9\.]+$/,
                            message: 'NIK hanya dapat berisi angka'
                        }
                    }
                },
                inputkk: {
                    message: 'The username is not valid',
                    validators: {
                        
                        stringLength: {
                            min: 16,
                            max: 16,
                            message: 'KK harus berisi 16 digit'
                        },
                        
                        regexp: {
                            regexp: /^[0-9\.]+$/,
                            message: 'KK hanya dapat berisi angka'
                        }
                    }
                }
            }
        });
});
</script>

<?php */ ?>