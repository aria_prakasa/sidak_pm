<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <table width="100%" class="table responsive" >
        <tr>
          <td width="30%">
            <label class="control-label">Jenis Statistik </label>
          </td>
          <td>
            <div class="col-sm-8">
            <?php
              $options = array('' => ' - Pilih Statistik - ',
              '1' => 'Seluruh Penduduk Berdasarkan Jenis Kelamin',
              '11' => 'Penduduk Miskin (Hasil Sinkronisasi PBDT2015-SIAK)',
              '2' => 'Penduduk Miskin Berdasarkan Agama',
              // '3' => 'Penduduk Berdasarkan Golongan Darah',
              '4' => 'Penduduk Miskin Berdasarkan Pendidikan',
              // '5' => 'Penduduk Berdasarkan Status Hubungan Keluarga',
              // '6' => 'Penduduk Berdasarkan Umur',
              '7' => 'Penduduk Miskin Berdasarkan Umur',
              // '8' => 'Penduduk Wajib KTP',
              '9' => 'Penduduk Miskin Berdasarkan Jenis Pekerjaan',
              // '10' => 'Penduduk Berdasarkan Jenis Pekerjaan di SIAK',
              // '12' => 'Penduduk Miskin Berdasarkan Penerima Bantuan Non Pusat',
              // '13' => 'Penduduk Miskin Berdasarkan Penerima BPJS'
              '14' => 'Penduduk Dalam Prelist (Pengajuan)'
              );
              echo form_dropdown('kd_agr', $options, $kd_agr, 'class="form-control input-sm"');
            ?>
            </div>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
            <div class="col-sm-8">
            <span style="color:red"><i>* Wajib diisi</i></span>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
