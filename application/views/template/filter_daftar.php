<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <table width="100%" class="table responsive" >
        <tr>
          <td width="30%">
            <label class="control-label">
              <input <?php echo $toggleKk ?> id="toggleElementKk" type="checkbox" name="toggle" onchange="toggleStatusKk()"  />&nbsp;&nbsp;&nbsp;&nbsp;Nomor KK 
            </label>
          </td>
          <td>
            <div class="col-md-6" id="toggleKk">
              <input readonly placeholder="masukkan no.KK 16 digit angka" type="text" class="form-control input-sm" name="inputkk" id="inputkk" value="<?php echo "$inputkk" ?>" maxlength="16" />
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <label class="control-label">
              <input <?php echo $toggleNik ?> id="toggleElementNik" type="checkbox" name="toggle" onchange="toggleStatusNik()" />&nbsp;&nbsp;&nbsp;&nbsp;NIK 
            </label>
          </td>
          <td>
            <div class="col-sm-6" id="toggleNik">
              <input readonly placeholder="masukkan NIK 16 digit angka" type="text" class="form-control input-sm" name="inputnik" id="inputnik" value="<?php echo "$inputnik" ?>" maxlength="16" />
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <label class="control-label">
              <input <?php echo $toggleNama ?> id="toggleElementNama" type="checkbox" name="toggle" onchange="toggleStatusNama()" />&nbsp;&nbsp;&nbsp;&nbsp;Nama Lengkap 
            </label>
          </td>
          <td>
            <div class="col-sm-6" id="toggleNama">
              <input readonly placeholder="masukkan Nama" type="text" class="form-control input-sm" name="inputnama" id="inputnama" value="<?php echo "$inputnama" ?>" style="text-transform:uppercase;width: 250px;" />
            </div>
          </td>
        </tr>
        <tr>
          <td width="40%">
            <label class="control-label">
              <input <?php echo $togglePengajuan; ?> id="toggleElementPengajuan" type="checkbox" name="toggle" onchange="toggleStatusPengajuan()" />&nbsp;&nbsp;&nbsp;&nbsp;Status Pengajuan
            </label>
          </td>
          <td>
            <div class="col-sm-3" id="togglePengajuan">
              <?php
                $options = array('' => ' - Pilih Status Pengajuan - ',
                '2' => 'Pengajuan Lama',
                '3' => 'Pengajuan Baru',
                );
                echo form_dropdown('inputpengajuan', $options, $inputpengajuan, 'class="form-control input-sm" style="width: 200px;" disabled');
              ?>
            </div>
          </td>
        </tr>
        <tr>
          <td width="40%">
            <label class="control-label">
              <input <?php echo $toggleProses; ?> id="toggleElementProses" type="checkbox" name="toggle" onchange="toggleStatusProses()" />&nbsp;&nbsp;&nbsp;&nbsp;Status Proses
            </label>
          </td>
          <td>
            <div class="col-sm-3" id="toggleProses">
              <?php
                $options = array('' => ' - Pilih Status Proses - ',
                '1' => 'Dalam Pengajuan',
                '2' => 'Sudah Verifikasi, Data belum entri',
                '3' => 'Verifikasi, Data sudah entri',
                '4' => 'Tidak Lolos Verifikasi',
                );
                echo form_dropdown('inputproses', $options, $inputproses, 'class="form-control input-sm" style="width: 200px;" disabled');
              ?>
              <?php /* ?>
              <input readonly placeholder="Proses" type="text" class="form-control input-sm" name="inputproses" id="inputproses" value="<?php echo "$inputproses" ?>" style="width: 50px;"/>
              <?php */ ?>
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <label class="control-label">
              <input <?php echo $toggleEntri ?> id="toggleElementEntri" type="checkbox" name="toggle" onchange="toggleStatusEntri()" />&nbsp;&nbsp;&nbsp;&nbsp;Tanggal Entri 
            </label>
          </td>
          <td>
            <div class="col-sm-6" id="toggleEntri">
              <table>
                <tr>
                  <td>                  
                    <?php $attributes = 'class="form-control input-sm datepicker" placeholder="tanggal awal" style="width: 100px;" disabled';
                      echo form_input('tgl_awal', '', $attributes); ?>
                  </td>
                  <td>&nbsp;&nbsp;s/d&nbsp;&nbsp;</td>
                  <td>
                    <?php $attributes = 'class="form-control input-sm datepicker" placeholder="tanggal akhir" style="width: 100px;" disabled';
                      echo form_input('tgl_akhir', '', $attributes); ?>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        

        <?php /* ?>
        <tr>
          <td width="40%">
            <label class="control-label">
              <input <?php echo $toggleTriwulan; ?> id="toggleElementTriwulan" type="checkbox" name="toggle" onchange="toggleStatusTriwulan()" />&nbsp;&nbsp;&nbsp;&nbsp;Triwulan
            </label>
          </td>
          <td>
            <div class="col-sm-3" id="toggleTriwulan">
              <?php
                $options = array('' => ' - pilih Triwulan - ',
                '1' => 'Triwulan I',
                '2' => 'Triwulan II',
                '3' => 'Triwulan III',
                '4' => 'Triwulan IV',
                );
                echo form_dropdown('inputtriwulan', $options, $inputtriwulan, 'class="form-control input-sm" style="width: 200px;" disabled');
              ?>
            </div>
          </td>
        </tr>
        <tr>
          <td width="40%">
            <label class="control-label">
              <input <?php echo $toggleTahap; ?> id="toggleElementTahap" type="checkbox" name="toggle" onchange="toggleStatusTahap()" />&nbsp;&nbsp;&nbsp;&nbsp;Tahap
            </label>
          </td>
          <td>
            <div class="col-sm-3" id="toggleTahap">
              <?php
                $options = array('' => ' - pilih Tahap - ',
                '1' => 'Tahap I',
                '2' => 'Tahap II',
                );
                echo form_dropdown('inputtahap', $options, $inputtahap, 'class="form-control input-sm" style="width: 200px;" disabled');
              ?>
            </div>
          </td>
        </tr>
        <?php */ ?>


      </table>
    </div>
  </div>
</div>

