<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <table width="100%" class="table responsive" >
        <tr>
          <td width="30%">
            <label class="control-label">
              <input <?php echo $toggleKk; ?> id="toggleElementKk" type="checkbox" name="toggle" onchange="toggleStatusKk()"  />&nbsp;&nbsp;&nbsp;&nbsp;Nomor KK 
            </label>
          </td>
          <td>
            <div class="col-md-6" id="toggleKk">
              <input readonly placeholder="masukkan no.KK" type="text" class="form-control input-sm" name="inputkk" id="inputkk" value="<?php echo "$inputkk" ?>" maxlength="16" />
            </div>  
          </td>
        </tr>
        <tr>
          <td width="40%">
            <label class="control-label">
              <input <?php echo $toggleNik; ?> id="toggleElementNik" type="checkbox" name="toggle" onchange="toggleStatusNik()" />&nbsp;&nbsp;&nbsp;&nbsp;NIK 
            </label>
          </td>
          <td>
            <div class="col-sm-6" id="toggleNik">
              <input readonly placeholder="masukkan NIK" type="text" class="form-control input-sm" name="inputnik" id="inputnik" value="<?php echo "$inputnik" ?>" maxlength="16" />
            </div> 
          </td>
        </tr>
        <tr>
          <td width="40%">
            <label class="control-label">
              <input <?php echo $toggleNama; ?> id="toggleElementNama" type="checkbox" name="toggle" onchange="toggleStatusNama()" />&nbsp;&nbsp;&nbsp;&nbsp;Nama Lengkap 
            </label>
          </td>
          <td>
            <div class="col-sm-6" id="toggleNama">
              <input readonly placeholder="masukkan Nama" type="text" class="form-control input-sm" name="inputnama" id="inputnama" value="<?php echo "$inputnama" ?>" style="text-transform:uppercase;width: 250px;" />
            </div> 
          </td>
        </tr>
        <tr>
          <td width="40%">
            <label class="control-label">
              <input <?php echo $toggleRw; ?> id="toggleElementRw" type="checkbox" name="toggle" onchange="toggleStatusRw()" />&nbsp;&nbsp;&nbsp;&nbsp;RW 
            </label>
          </td>
          <td>
            <div class="col-sm-3" id="toggleRw">
              <input readonly placeholder="RW" type="text" class="form-control input-sm" name="inputrw" id="inputrw" value="<?php echo "$inputrw" ?>" style="width: 50px;"/>
            </div>
          </td>
        </tr>
        <tr>
          <td width="40%">
            <label class="control-label">
              <input <?php echo $toggleRt; ?> id="toggleElementRt" type="checkbox" name="toggle" onchange="toggleStatusRt()" />&nbsp;&nbsp;&nbsp;&nbsp;RT
            </label>
          </td>
          <td>
            <div class="col-sm-3" id="toggleRt">
              <input readonly placeholder="RT" type="text" class="form-control input-sm" name="inputrt" id="inputrt" value="<?php echo "$inputrt" ?>" style="width: 50px;"/>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>