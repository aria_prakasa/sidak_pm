<script type="text/javascript">
function toggleStatusKk() {
    if ($('#toggleElementKk').is(':checked')) {
        $('#toggleKk :input').removeAttr('readonly');
    } else {
        $('#toggleKk :input').attr('readonly', true);
        $('#toggleKk :input').val("");
    }
}
function toggleStatusNik() {
    if ($('#toggleElementNik').is(':checked')) {
        $('#toggleNik :input').removeAttr('readonly');
    } else {
        $('#toggleNik :input').attr('readonly', true);
        $('#toggleNik :input').val("");
    }
}
function toggleStatusNama() {
    if ($('#toggleElementNama').is(':checked')) {
        $('#toggleNama :input').removeAttr('readonly');
    } else {
        $('#toggleNama :input').attr('readonly', true);
        $('#toggleNama :input').val("");
    }
}
</script>

<?php 
// $attributes = array('class' => 'form-validate form-horizontal', 'id' => 'check');
echo form_open('', 'id="check" method="get" class="form-validate form-horizontal"') 
// echo form_open('', $attributes);
?>
<div class="row">
<?php
$provinsi = $this->model_wilayah->ambil_provinsi();
$kabupaten = $this->model_wilayah->ambil_kabupaten();
$in_array = null; //default
if($this->global_data['no_kec'] != "") {
  $in_array = array($this->global_data['no_kec']);
}
$kecamatan = $this->model_wilayah->ambil_kecamatan($in_array);
$kelurahan_id = empty($kelurahan_id) ? $this->global_data['no_kec']."-".$this->global_data['no_kel'] : $kelurahan_id;
?>
  <div class="col-sm-6">
    <div class="form-group">
      <label class="col-sm-4 control-label">Propinsi</label>
      <div class="col-sm-8">
      <?php
      $style_provinsi = 'class="form-control input-sm" id="provinsi_id"';
      echo form_dropdown('provinsi_id', $provinsi, NO_PROP, $style_provinsi);
      ?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">Kota/Kab</span></label>
      <div class="col-sm-8">
      <?php
      $style_kabupaten = 'class="form-control input-sm" id="kabupaten_id"';
      echo form_dropdown("kabupaten_id", $kabupaten, NO_KAB, $style_kabupaten);
      ?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">Kecamatan</label>
      <div class="col-sm-8">
      <?php
// var_dump($kecamatan, $kecamatan_id);die;
      ?>
      <?php
      $style_kecamatan = 'class="form-control input-sm" id="kecamatan_id" onChange="tampilKelurahan()"';
      echo form_dropdown("kecamatan_id", $kecamatan, $kecamatan_id, $style_kecamatan);
      ?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">Kelurahan</label>
      <div class="col-sm-8">
        <div id="kelurahan_id_wrap" data-id="<?php echo $kelurahan_id; ?>">
        <?php
        $style_kelurahan = 'disabled class="form-control input-sm" id="kelurahan_id"';
        $kelurahan = array('Pilih Kelurahan' => '- Pilih Kecamatan Terlebih Dahulu -'); // default
        if($this->global_data['no_kec'] != "" && $this->global_data['no_kel'] != "") {
          $in_array = array($this->global_data['no_kel']);
          $kelurahan = $this->model_wilayah->ambil_kelurahan($this->global_data['no_kec'], $in_array);
        }
        echo form_dropdown("kelurahan_id", $kelurahan, $kelurahan_id, $style_kelurahan);
        ?>
        </div>
      </div>
    </div>
  </div>
  
<?php if ( stristr($this->uri->segment(1), 'stat') ):
      $this->load->view('template/filter_statistik'); ?>
<?php elseif ( stristr($this->uri->segment(1), 'hasil') || 
  stristr($this->uri->segment(1), 'rubah_pindah') ||
  stristr($this->uri->segment(1), 'anomali') ||
  stristr($this->uri->segment(1), 'entry') ||
  stristr($this->uri->segment(1), 'mohon') ||
  stristr($this->uri->segment(1), 'induk')):
      $this->load->view('template/filter_nik'); ?>
<?php elseif ( stristr($this->uri->segment(1), 'sink') ):
      $this->load->view('template/filter_bdt'); ?>  

<?php /*if ($this->uri->segment(1) == 'agr_penduduk' || $this->uri->segment(1) == 'agr_miskin'): ?>

  <?php $this->load->view('template/filter_agregat'); ?>
   
<?php elseif ($this->uri->segment(1) == 'listing' || $this->uri->segment(1) == 'listing_biodata' || $this->uri->segment(1) == 'listing_bpjs'): ?>
  
  <?php $this->load->view('template/filter_list'); ?>

<?php elseif ($this->uri->segment(1) == 'bpjs'): ?>
  
  <?php $this->load->view('template/filter_plus'); ?>  

<?php elseif ($this->uri->segment(1) == 'anomali'): ?>
  
  <?php $this->load->view('template/filter_anomali'); ?>  

<?php elseif (stristr($this->uri->segment(1), 'bdt_') || stristr($this->uri->segment(1), 'sink_')): ?>
  
  <?php $this->load->view('template/filter_bdt'); ?>  

<?php elseif (stristr($this->uri->segment(1), 'stat')): ?>
  
  <?php $this->load->view('template/filter_statistik');*/ ?>  

<?php else: ?>
  <?php $this->load->view('template/filter_kk'); ?>

<?php endif;?>

</div>

<div class="row">  
  <div class="col-sm-6">
    <div class="form-group">
      <div class="col-sm-4"></div>
      <div class="col-sm-4">
          <button type="submit" name="tampil" value="1" class="btn bg-maroon"><span class="fa fa-search"></span>&nbsp;Tampilkan</button>
      </div>
    </div>
  </div>
</div>

<?php echo form_close(); ?>

<?php /* ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#check')
        .formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                inputnama: {
                    message: 'The username is not valid',
                    validators: {
                        
                        stringLength: {
                            min: 3,
                            //max: 50,
                            message: 'Nama harus lebih dari 3 karakter'
                        },
                        
                        regexp: {
                            regexp: /^[a-zA-Z \.]+$/,
                            message: 'Nama hanya dapat berisi huruf alphabet'
                        }
                    }
                },
                inputnik: {
                    message: 'The username is not valid',
                    validators: {
                        
                        stringLength: {
                            min: 16,
                            max: 16,
                            message: 'NIK harus berisi 16 digit'
                        },
                        
                        regexp: {
                            regexp: /^[0-9\.]+$/,
                            message: 'NIK hanya dapat berisi angka'
                        }
                    }
                },
                inputkk: {
                    message: 'The username is not valid',
                    validators: {
                        
                        stringLength: {
                            min: 16,
                            max: 16,
                            message: 'KK harus berisi 16 digit'
                        },
                        
                        regexp: {
                            regexp: /^[0-9\.]+$/,
                            message: 'KK hanya dapat berisi angka'
                        }
                    }
                }
            }
        });
});
</script>

<?php */ ?>