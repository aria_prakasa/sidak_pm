<?php if ($this->uri->segment(1) == 'agr_penduduk'): ?>

<div class="col-sm-6">
	<div class="form-group">
    <label class="col-sm-4 control-label">Jenis Statistik <span class="text-danger">*</span></label>
    <div class="col-sm-8">
<?php
$options = array('' => ' - Pilih Statistik - ',
'1' => 'Agregat Penduduk Berdasarkan Jenis Kelamin',
'2' => 'Agregat Penduduk Berdasarkan Agama',
'3' => 'Agregat Penduduk Berdasarkan Golongan Darah',
'4' => 'Agregat Penduduk Berdasarkan Pendidikan',
'5' => 'Agregat Penduduk Berdasarkan Pekerjaan',
'6' => 'Agregat Penduduk Berdasarkan Umur 5 Tahunan',
'7' => 'Agregat Penduduk Berdasarkan Umur Khusus',
'8' => 'Agregat Penduduk Wajib KTP',
'9' => 'Agregat Penduduk Ber-KTP',
'10' => 'Agregat Penduduk >17 Tahun Ber-KTP',
'11' => 'Agregat Penduduk >17 Tahun Telah Menikah',
'12' => 'Agregat Kepala Keluarga',
);
echo form_dropdown('kd_agr', $options, $kd_agr, 'class="form-control input-sm"');
?>

    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Periode bulan <span class="text-danger">*</span></label>
    <div class="col-sm-3">
<?php 
echo form_input('bulan', $bulan, 'placeholder="MM/YYYY" type="text" class="form-control input-sm" data-mask="00/0000" id="bulan"'); 
?>
    </div>
  </div>
</div>
<?php elseif ($this->uri->segment(1) == 'agr_miskin'): ?>
<div class="col-sm-6">
  <div class="form-group">
    <label class="col-sm-4 control-label">Jenis Statistik <span class="text-danger">*</span></label>
    <div class="col-sm-8">
<?php
$options = array('' => ' - Pilih Statistik - ',
'1' => 'Penduduk Miskin Berdasarkan Jenis Kelamin',
'2' => 'Penduduk Miskin Berdasarkan Agama',
'3' => 'Penduduk Miskin Berdasarkan Golongan Darah',
'4' => 'Penduduk Miskin Berdasarkan Pendidikan',
'5' => 'Penduduk Miskin Berdasarkan Status Hubungan Keluarga',
'6' => 'Penduduk Miskin Berdasarkan Umur',
'7' => 'Penduduk Miskin Berdasarkan Umur Sekolah dan Produktif',
'8' => 'Penduduk Miskin Wajib KTP',
'9' => 'Penduduk Miskin Berdasarkan Jenis Pekerjaan',
'10' => 'Penduduk Miskin Berdasarkan Jenis Pekerjaan di SIAK',
'11' => 'Penduduk Miskin Peserta BPJS',
);
echo form_dropdown('kd_agr', $options, $kd_agr, 'class="form-control input-sm"');
?>
    </div>
  </div>
</div>
<?php endif;?>

<div class="col-sm-6">
    <div class="form-group">
      <div class="col-sm-4">
        <div align="left"><span style="color:red"><i>* Wajib diisi salah satu</i></span>
        </div>
      </div>
      <!-- <div class="col-sm-6" >
        <div align="left"><span style="color:red"><i>* Wajib diisi</i></span>
        </div>
      </div>  -->
    </div>
  </div>