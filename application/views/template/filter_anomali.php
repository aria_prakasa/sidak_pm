<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <label class="col-sm-4 control-label">Berdasarkan Status <span class="text-danger">*</span></label>
      <div class="col-sm-8">
<?php
$options = array('' => ' - Pilih Status Anomali - ',
'1' => 'Penduduk Pindah KK',
'2' => 'Penduduk Mati/Status Tidak Aktif',
);
echo form_dropdown('kd_filter', $options, $kd_filter, 'class="form-control input-sm"');
?>

      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label class="col-sm-4 control-label">Berdasarkan Pekerjaan <span class="text-danger">*</span></label>
      <div class="col-sm-8">
<?php
  echo form_dropdown('kd_pekerjaan', $opsi_pekerjaan, $kd_pekerjaan, 'class="form-control input-sm"');
?>

      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="form-group">
      <div class="col-sm-4">
        <div align="left"><span style="color:red"><i>* Wajib diisi salah satu</i></span>
        </div>
      </div>
      <!-- <div class="col-sm-6" >
        <div align="left"><span style="color:red"><i>* Wajib diisi</i></span>
        </div>
      </div>  -->
    </div>
  </div>
</div>