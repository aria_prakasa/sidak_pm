
<?php 
// $attributes = array('class' => 'form-validate form-horizontal', 'id' => 'check');
echo form_open('', 'id="check" method="get" class="form-validate form-horizontal"') 
// echo form_open('', $attributes);
?>
<div class="box-body">
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
      
        <table width="100%" class="table responsive no-border" >
          <tr>
            <td width="30%">
              <label class="col-sm-12 control-label">Tabel</label>
            </td>
            <td>
              <div class="col-sm-8">
              <?php
                  $options = array('' => ' - Pilih Tabel - ',
                  '1' => '1. Rumah Tangga',
                  '2' => '2. Individu',
                  );
                  echo form_dropdown('inputtable', $options, $inputtable, 'class="form-control input-sm" style="width: 200px;" required');
                ?>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <label class="col-sm-12 control-label">Tanggal Entri</label>
            </td>
            <td>
              <div class="col-sm-6" id="toggleEntri">
                <table>
                  <tr>
                    <td>                  
                      <?php $attributes = 'class="form-control input-sm datepicker" placeholder="tanggal awal" style="width: 100px;" required';
                        echo form_input('tgl_awal', '', $attributes); ?>
                    </td>
                    <td>&nbsp;&nbsp;s/d&nbsp;&nbsp;</td>
                    <td>
                      <?php $attributes = 'class="form-control input-sm datepicker" placeholder="tanggal akhir" style="width: 100px;" required';
                        echo form_input('tgl_akhir', '', $attributes); ?>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <label class="col-sm-4 control-label">&nbsp;</label>
            </td>
            <td>
              <div class="col-sm-8">
                <button type="submit" name="tampil" value="1" class="btn bg-maroon">
                <span class="fa fa-search"></span>&nbsp;Tampilkan</button>
              </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
    
  </div>
</div>

<?php echo form_close(); ?>

