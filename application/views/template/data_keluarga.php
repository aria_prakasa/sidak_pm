<div class="row">
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">No KK</label>
              <div class="col-sm-8">
                <input type="text" readonly="" class="form-control" value="<?php echo "$no_kk";?> ">
              </div>
            </div>
            <div class="form-group">
            <label class="col-sm-4 control-label">Kepala Keluarga</label>
            <div class="col-sm-8">
              <input type="text" name="kk" id="kk" readonly="" class="form-control" value="<?php echo "$nama_kep";?> ">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Alamat</label>
            <div class="col-sm-8">
              <textarea class="form-control" readonly="" rows="3"><?php echo "$alamat";?></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">&nbsp;</label>
            <label class="col-sm-2 control-label">No RT</label>
            <div class="col-sm-2">
              <input type="text" name="kk" id="kk" readonly="" class="form-control" value="<?php echo "$no_rt";?>">
            </div>
            <label class="col-sm-2 control-label">No RW</label>
            <div class="col-sm-2">
              <input type="text" name="kk" id="kk" readonly="" class="form-control" value="<?php echo "$no_rw";?>">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">Propinsi</label>
            <div class="col-sm-6">
              <input type="text" name="kk" id="kk" readonly="" class="form-control" value="<?php echo "$nama_prop";?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Kota / Kab</label>
            <div class="col-sm-6">
              <input type="text" name="kk" id="kk" readonly="" class="form-control" value="<?php echo "$nama_kab";?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Kecamatan</label>
            <div class="col-sm-6">
              <input type="text" name="kk" id="kk" readonly="" class="form-control" value="<?php echo "$nama_kec";?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Kelurahan</label>
            <div class="col-sm-6">
              <input type="text" name="kk" id="kk" readonly="" class="form-control" value="<?php echo "$nama_kel";?>">
            </div>
          </div>
        </div>
      </div>    
    </div>