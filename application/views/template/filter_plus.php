<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="col-sm-4">
          <div align="left">
              <label class="control-label">
                  <input <?php echo $toggleKk ?> id="toggleElementKk" type="checkbox" name="toggle" onchange="toggleStatusKk()"  />&nbsp;&nbsp;Nomor KK 
              </label>
          </div>
      </div>
      <div class="col-md-6" id="toggleKk">
          <input readonly placeholder="masukkan no.KK 16 digit angka" type="text" class="form-control input-sm" name="inputkk" id="inputkk" value="<?php echo "$inputkk" ?>" maxlength="16" />
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="form-group">
      <div class="col-sm-4">
          <div align="left">
              <label class="control-label">
                  <input <?php echo $toggleNik ?> id="toggleElementNik" type="checkbox" name="toggle" onchange="toggleStatusNik()" />&nbsp;&nbsp;NIK 
              </label>
              </label>
          </div>
      </div>
      <div class="col-sm-6" id="toggleNik">
          <input readonly placeholder="masukkan NIK 16 digit angka" type="text" class="form-control input-sm" name="inputnik" id="inputnik" value="<?php echo "$inputnik" ?>" maxlength="16" />
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="form-group">
      <div class="col-sm-4">
          <div align="left">
              <label class="control-label">
                  <input <?php echo $toggleNama ?> id="toggleElementNama" type="checkbox" name="toggle" onchange="toggleStatusNama()" />&nbsp;&nbsp;Nama Lengkap 
              </label>
              </label>
          </div>
      </div>
      <div class="col-sm-6" id="toggleNama">
          <input readonly placeholder="masukkan Nama" type="text" class="form-control input-sm" name="inputnama" id="inputnama" value="<?php echo "$inputnama" ?>" style="text-transform:uppercase" />
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="form-group">
      <div class="col-sm-4">
        <div align="left"><span style="color:red"><i>* Wajib diisi</i></span>
        </div>
      </div>
      <!-- <div class="col-sm-6" >
        <div align="left"><span style="color:red"><i>* Wajib diisi</i></span>
        </div>
      </div>  -->
    </div>
  </div>
</div>