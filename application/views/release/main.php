<div class="box box-danger">
  <div class="box-header with-border">
    <h3 class="box-title"><strong>Pencarian Pengajuan Rumah Tangga Keluarga Miskin (Dalam Proses Pengajuan) :</strong></h3>
  </div>
  <div class="box-body">
    <?php $this->load->view('template/tpl_filter'); ?>
  </div><!-- /.box body-->
</div><!-- /.box -->

<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title"><strong>Tampil Data :</strong></h3>
  </div>

  <div class="box-body">
    <div id="result">
  <?php  if ($show==0):?>
    <div class="alert alert-success" style="text-align: center">
      <h4><span style="font-size: 40px" class="icon fa fa-file-text"></span> <br /> Perhatian!</h4>
      Silahkan pilih menu diatas.
    </div>
  <?php elseif($show==1): ?>    
        <?php $this->load->view('release/result/result');?>
  <?php endif ?>
    </div>
  </div><!-- /.box body-->

</div><!-- /.box -->

<script>
// $(document).on('submit','#check',function() {
//  return false;
// });
$(document).on('click','#check button',function() {

  if($(this).attr('name') == "tampil") {

    $('#result').html('Tunggu sebentar...');
    $(this).val('Mengambil data...');

    $.ajax({
      url: '<?php echo base_url('index.php/release/index') ?>',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: $( this ).closest('form').serialize(),
      cache: false
    })
    .done(function(data) {
      console.log("success");
      $('#result').html(data);
      /*if($('select[name=kecamatan_id]').val() != "") {
        $('button[name=export]').removeClass('hide');
      }*/
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
      $(this).val('><span class="fa fa-search"></span>&nbsp;Tampilkan');
    });

    return false;
  }


});

$(document).on('click','.pagination a',function() {

  $.ajax({
    url: $(this).attr('href'),
    type: 'GET',
    cache: false
  })
  .done(function(data) {
    console.log("success");
    $('#result').html(data);
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

  return false;

});
</script>

 
