<div class="box-body">
	<?php /* ?><div class="row">
		<div class="col-sm-8">
			<table width="50%" class="table responsive" style="font-size: 0.9em">
				<tr>
					<td width="30%">Apakah anggota rumah tangga lain ?</td>
					<td width="20%"><?php echo form_dropdown('anggota_rt', $opsi_binary, '', 'class="form-control input-sm ddl_disabled_toggle" data-ddl_target="._kk"'); ?></td>
					<td><?php echo form_input('kk_rt', $no_kk_edit, 'placeholder="Masukkan NO. KK Rumah Tangga" data-suffix="_edit" maxlength="16" class="form-control _kk"'); ?>
					</td>
					<td><button id="Cari" class="btn btn-primary submit_kk" data-url="<?php echo site_url('bdt/ajax/get_rumahtangga'); ?>" data-target="kk_rt">Cari</button></td>
				</tr>
			</table>
		</div>
	</div><?php */ ?>

	<div class="row">	
		<div class="box-body">
		    <div style="text-align: center;" class="alert alert-warning" role="alert">
		      <b>I. PENGENALAN TEMPAT</b>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>1.</b></td>
			    		<td width="40%"><b>Provinsi</b></td>
			    		<td><?php echo form_input('provinsi_edit', $provinsi, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td width="20%"><?php echo form_input('no_prop', $no_prop, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="2" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>2.</b></td>
			    		<td width="40%"><b>Kabupaten/Kota</b></td>
			    		<td><?php echo form_input('kabupaten_edit', $kabupaten, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td width="20%"><?php echo form_input('no_kab', $no_kab, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="2" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>3.</b></td>
			    		<td width="40%"><b>Kecamatan</b></td>
			    		<td><?php echo form_input('kecamatan_edit', $kecamatan, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td width="20%"><?php echo form_input('no_kec', '0'.$no_kec, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="2" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>4.</b></td>
			    		<td width="40%"><b>Desa/Kelurahan/Nagari</b></td>
			    		<td><?php echo form_input('desa_edit', $desa, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td width="20%"><?php echo form_input('no_kel', $no_kel, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="4" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>5.</b></td>
			    		<td width="40%"><b>Nama SLS</b></td>
			    		<td colspan="2" width="50%"><?php echo form_input('nama_sls', '', 'class="form-control input-sm" style="text-transform:uppercase" maxlength="4" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>6.</b></td>
			    		<td width="30%"><b>Alamat</b></td>
			    		<td colspan="2" width="20%"><?php echo form_input('b1_r6_edit', $b1_r6.', No.RT/No.RW. '.$no_rt.'/'.$no_rw, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    	</tr>
		    	</table>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>7.</b></td>
			    		<td colspan="2" width="30%"><b>Nomor Urut Rumah Tangga (dari PBDT2015.FKP Kolom(1))</b></td>
			    		<td width="20%"><?php echo form_input('nomor_urut_rumah_tangga_edit', $nomor_urut_rumah_tangga, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="4" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>8.</b></td>
			    		<td width="30%"><b>No KK KRT</b></td>
			    		<td><?php echo form_input('no_kk_rumahtangga_edit', $no_kk_rumahtangga, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> </td>
			    		<td>&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>9.</b></td>
			    		<td width="30%"><b>NIK KRT</b></td>
			    		<td><?php echo form_input('nik_rumahtangga_edit', $nik_rumahtangga, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> </td>
			    		<td>&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>10.</b></td>
			    		<td width="30%"><b>Nama KRT</b></td>
			    		<td colspan="2" width="20%"><?php echo form_input('b1_r8_edit', $b1_r8, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>11.</b></td>
			    		<td width="30%"><b>Jumlah ART</b></td>
			    		<td>&nbsp;</td>
			    		<td width="20%"><?php echo form_input('b1_r9_edit', $b1_r9, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>12.</b></td>
			    		<td colspan="2" width="30%"><b>Jumlah Keluarga</b></td>
			    		<td width="20%"><?php echo form_input('b1_r10_edit', $b1_r10, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    	</tr>
		    	</table>
		    </div>
	    </div>
	</div>

	<div class="row">	
		<div class="box-body">
		    <div style="text-align: center;" class="alert alert-warning" role="alert">
		      <b>II. KETERANGAN PETUGAS DAN RESPONDEN</b>
		    </div>
		    <div class="col-sm-8">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="3%"><b>1.</b></td>
			    		<td width="30%"><b>Tanggal Pencacahan</b></td>
			    		<td width="20%"><?php $attributes = 'class="form-control input-sm" placeholder="tanggal cacah" disabled';
							echo form_input('tgl_cacah', format_tanggal($tgl_cacah), $attributes); ?></td>
			    		<td colspan="3"><i>harap diisi tanggal pencacahan</i></td>
			    		<?php /* ?>
			    		
			    		<td width="10%"><?php echo form_input('tgl_cacah', $tgl_cacah, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="2"'); ?></td>
			    		<td width="5%">Bulan</td>
			    		<td width="10%"><?php echo form_input('bln_cacah', $bln_cacah, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="2"'); ?></td>
			    		<td width="5%">Tahun</td>
			    		<td width="20%"><?php echo form_input('thn_cacah', $thn_cacah, 'class="form-control input-sm" style="text-transform:uppercase;text-align:right;" maxlength="4"'); ?></td>
			    		<?php */ ?>
			    	</tr>
			    	<tr>
			    		<td><b>2.</b></td>
			    		<td><b>Nama Pencacah</b></td>
			    		<td colspan="2"><?php echo form_input('nama_cacah', $petugas_cacah, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td>Kode</td>
			    		<td><?php echo form_input('kd_cacah', $kd_cacah, 'class="form-control input-sm" style="text-transform:uppercase" maxlength="3" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>3.</b></td>
			    		<td><b>Tanggal Pemeriksaan</b></td>
			    		<td><?php $attributes = 'class="form-control input-sm" placeholder="tanggal periksa" disabled';
							echo form_input('tgl_periksa', format_tanggal($tgl_periksa), $attributes); ?></td>
						<td colspan="3"><i>harap diisi tanggal pemeriksaan</i></td>
			    	</tr>
			    	<tr>
			    		<td><b>4.</b></td>
			    		<td><b>Nama Pemeriksa</b></td>
			    		<td colspan="2"><?php echo form_input('nama_periksa', $petugas_periksa, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td>Kode</td>
			    		<td><?php echo form_input('kd_periksa', $kd_periksa, 'class="form-control input-sm" style="text-transform:uppercase" maxlength="3" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>5.</b></td>
			    		<td><b>Hasil Pencacahan Rumah Tangga</b></td>
			    		<td colspan="4"><?php echo form_dropdown('hasil_cacah', $opsi_hasil_pencacahan, $hasil_cacah, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>6.</b></td>
			    		<td><b>Nama Responden</b></td>
			    		<td colspan="3"><?php echo form_input('nama_responden', $nama_responden, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td width="15%">&nbsp;</td>
			    	</tr>
		    	</table>
		    </div>
		</div>
	</div>

	<div class="row">
		<div class="box-body">
		    <div style="text-align: center;" class="alert alert-warning" role="alert">
		      <b>III. KETERANGAN PERUMAHAN</b>
		    </div>

		    <div class="col-sm-6">	    	
		    	<?php /*	 ?>
		    	<?php
					$arr = array(
					"a. Status kepemilikan bangunan tempat tinggal " => array('dropdown', array('b3_r1a_edit', $opsi_b3_r1a, $b3_r1a)),
					"b. Status kepemilikan lahan tempat tinggal " => array('dropdown', array('b3_r1b_edit', $opsi_b3_r1b, $b3_r1b))

					);
				?>
		    	<?php
					$i = $j = 1;
					foreach($arr as $key => $values)
					{
						list($form, $val) = $values;
						if ($form == 'dropdown') {
							# code...
						}
	  					$tmp_arr = array("key" => $key, "val" => $val, "no" => $no, "i" => $i);
	  					list($post, $opsi, $value) = $val;
	  					gen_option_dropdown($i,$key,$post,$opsi,$value);
	  					// echo $this->load->view("form/tpl_dropdown", $tmp_arr, TRUE);
					  	$i++;
					  	$j++;
					}
				?>
				</table>
				<?php 	*/ ?>
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>1.</b></td>
			    		<td width="60%"><b>a. Status kepemilikan bangunan tempat tinggal </b></td>
			    		<td colspan="2" width="40%"><?php echo form_dropdown('b3_r1a_edit', $opsi_b3_r1a, $b3_r1a, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>b. Status kepemilikan lahan tempat tinggal </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r1b_edit', $opsi_b3_r1b, $b3_r1b, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>2.</b></td>
			    		<td><b>Luas Lantai </b></td>
			    		<td><?php echo form_input('b3_r2_edit', $b3_r2, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td><td width="20%">&nbsp;&nbsp;&nbsp;m2</td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>3.</b></td>
			    		<td><b>Jenis lantai terluas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r3_edit', $opsi_b3_r3, $b3_r3, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>4.</b></td>
			    		<td><b>a. Jenis dinding terluas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r4a_edit', $opsi_b3_r4a, $b3_r4a, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>b. Kualitas dinding terluas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r4b_edit', $opsi_b3_r4b, $b3_r4b, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>5.</b></td>
			    		<td><b>a. Jenis atap terluas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r5a_edit', $opsi_b3_r5a, $b3_r5a, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>b. Kualitas atap terluas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r5b_edit', $opsi_b3_r5b, $b3_r5b, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>6.</b></td>
			    		<td><b>Jumlah kamar tidur </b></td>
			    		<td><?php echo form_input('b3_r6_edit', $b3_r6, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td width="20%">&nbsp;&nbsp;&nbsp;kamar</td>
			    	</tr>
		    	</table>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>7.</b></td>
			    		<td width="60%"><b>Sumber air minum  </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r7_edit', $opsi_b3_r7, $b3_r7, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>8.</b></td>
			    		<td><b>Cara memperoleh air minum </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r8_edit', $opsi_b3_r8, $b3_r8, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>9.</b></td>
			    		<td><b>a. Sumber penerangan utama </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r9a_edit', $opsi_b3_r9a, $b3_r9a, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>b. Daya listrik terpasang (PLN) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r9b_edit', $opsi_b3_r9b, $b3_r9b, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>10.</b></td>
			    		<td><b>Bahan bakar/energi utama untuk memasak </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r10_edit', $opsi_b3_r10, $b3_r10, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>11.</b></td>
			    		<td><b>a. Penggunaan fasilitas buang air besar </b></td>
			    		<td width="25%"><?php echo form_dropdown('b3_r11a_edit', $opsi_b3_r11a, $b3_r11a, 'class="form-control input-sm" disabled'); ?></td><td width="20%">&nbsp;&nbsp;&nbsp;m2</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>b. Jenis kloset </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r12_edit', $opsi_b3_r12, $b3_r12, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>12.</b></td>
			    		<td><b>Tempat pembuangan akhir tinja </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b3_r12_edit', $opsi_b3_r12, $b3_r12, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
		    	</table>
		    </div>
		</div>		
	</div>

	<div class="row">	
		<div class="box-body">	
		    <div style="text-align: center;" class="alert alert-warning" role="alert">
		      <b>V. KEPEMILIKAN ASET DAN KEIKUTSERTAAN PROGRAM</b>
		    </div>

		    <div class="col-sm-6">	    	
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>1.</b></td>
			    		<td colspan="4" width="95%"><b>Rumah tangga memiliki sendiri aset bergerak sebagai berikut : </b></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>a. Tabung gas 5.5 kg atau lebih </b></td>
			    		<td colspan="2" width="30%"><?php echo form_dropdown('b5_r1a_edit', $opsi_binary, $b5_r1a, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>b. Lemari es/kulkas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1b_edit', $opsi_binary_ii, $b5_r1b, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>c. AC </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1c_edit', $opsi_binary, $b5_r1c, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>d. Pemanas air (water heater) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1d_edit', $opsi_binary_ii, $b5_r1d, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>e. Telepon Rumah (PSTN) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1e_edit', $opsi_binary, $b5_r1e, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>f. Televisi </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1f_edit', $opsi_binary_ii, $b5_r1f, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>g. Emas/perhiasan & tabungan (senilai 10 gram emas) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1g_edit', $opsi_binary, $b5_r1g, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>h. komputer/laptop </b></td>
			    		<td colspan="2" width="30%"><?php echo form_dropdown('b5_r1h_edit', $opsi_binary_ii, $b5_r1h, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>i. Sepeda </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1i_edit', $opsi_binary, $b5_r1i, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>j. Sepeda motor </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1j_edit', $opsi_binary_ii, $b5_r1j, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>k. Mobil </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1k_edit', $opsi_binary, $b5_r1k, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>l. Perahu </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1l_edit', $opsi_binary_ii, $b5_r1l, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>m. Motor tempel </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1m_edit', $opsi_binary, $b5_r1m, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>n. Perahu motor </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1n_edit', $opsi_binary_ii, $b5_r1n, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>n. Kapal </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r1o_edit', $opsi_binary, $b5_r1o, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>2.</b></td>
			    		<td colspan="4" width="95%"><b>Rumah tangga memiliki aset tidak bergerak sebagai berikut : </b></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>a. Lahan </b></td>
			    		<td colspan="2" width="20%"><?php echo form_dropdown('b5_r3a1_edit', $opsi_binary, $b5_r3a1, 'class="form-control input-sm ddl_disabled_toggle" data-ddl_target="input[name=b5_r3a2_edit]" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"	align="right"><i>luas lahan</i></td>
			    		<!-- Luas lahan -->
			    		<td><?php echo form_input('b5_r3a2_edit', $b5_r3a2, 'class="form-control input-sm" disabled style="text-transform:uppercase" disabled'); ?> </td>
			    		<td>&nbsp;&nbsp;&nbsp;m2</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>b. Kepemilikan rumah di lokasi lain </b></td>
			    		<td colspan="2" width="20%"><?php echo form_dropdown('b5_r3a1_edit', $opsi_binary, $b5_r3a1, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
		    	</table>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>3.</b></td>
			    		<td colspan="4" width="95%"><b>Jumlah ternak yang dimiliki : </b></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>a. Sapi </b></td>
			    		<td width="20%"><?php echo form_input('b5_r4a_edit', $b5_r4a, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> </td>
			    		<td width="10%">&nbsp;&nbsp;&nbsp;ekor</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>b. Kerbau </b></td>
			    		<td><?php echo form_input('b5_r4b_edit', $b5_r4b, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> </td>
			    		<td width="10%">&nbsp;&nbsp;&nbsp;ekor</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>c. Kuda </b></td>
			    		<td><?php echo form_input('b5_r4c_edit', $b5_r4c, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> </td>
			    		<td width="10%">&nbsp;&nbsp;&nbsp;ekor</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>d. Babi </b></td>
			    		<td><?php echo form_input('b5_r4d_edit', $b5_r4d, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> </td>
			    		<td width="10%">&nbsp;&nbsp;&nbsp;ekor</td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>e. Kambing/Domba </b></td>
			    		<td><?php echo form_input('b5_r4e_edit', $b5_r4e, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> </td>
			    		<td width="10%">&nbsp;&nbsp;&nbsp;ekor</td>
			    	</tr>
			    	<tr>
			    		<td>4.</td>
			    		<td colspan="2"><b>a. Apakah ada ART yang memiliki usaha sendiri/bersama? </b></td>
			    		<td colspan="2" width="20%"><?php echo form_dropdown('b5_r5a_edit', $opsi_binary, $b5_r5a, 'class="form-control input-sm ddl_disabled_toggle" data-ddl_target="input[name=b5_r5a_isian_edit]" disabled'); ?>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"	align="right"><i>Isikan</i></td>
			    		<!-- Jika Ya, isikan -->
			    		<td colspan="2" width="30%"><?php echo form_input('b5_r5a_isian_edit', '', 'class="form-control input-sm" disabled style="text-transform:uppercase"'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>5.</b></td>
			    		<td colspan="4" width="95%"><b>Rumah tangga menjadi peserta program/memiliki kartu program berikut : </b></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>a. Kartu Keluarga Sejahtera (KKS)/Kartu Perlindungan Sosial(KPS) </b></td>
			    		<td colspan="2" width="30%"><?php echo form_dropdown('b5_r6a_edit', $opsi_binary, $b5_r6a, 'class="form-control input-sm" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>b. Kartu Indonesia Pintar (KIP)/Bantuan Siswa Miskin(BSM) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6b_edit', $opsi_binary_ii, $b5_r6b, 'class="form-control input-sm" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>c. Kartu Indonesia Sehat (KIS)/BPJS Kesehatan/Jamkesmas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6c_edit', $opsi_binary, $b5_r6c, 'class="form-control input-sm" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>d. BPJS Kesehatan peserta mandiri </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6d_edit', $opsi_binary_ii, $b5_r6d, 'class="form-control input-sm" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>e. Jaminan Sosial Tenaga Kerja (Jamsostek)/BPJS Ketenagakerjaan</b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6e_edit', $opsi_binary, $b5_r6e, 'class="form-control input-sm" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>f. Asuransi kesehatan lainnya</b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6f_edit', $opsi_binary_ii, $b5_r6f, 'class="form-control input-sm" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>g. Program Keluarga Harapan (PKH)</b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6g_edit', $opsi_binary, $b5_r6g, 'class="form-control input-sm" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>h. Beras untuk orang miskin (Raskin)</b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6h_edit', $opsi_binary_ii, $b5_r6h, 'class="form-control input-sm" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td colspan="2"><b>i. Kredit Usaha Rakyat (KUR)</b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r6i_edit', $opsi_binary, $b5_r6i, 'class="form-control input-sm" disabled'); ?> </td>
			    	</tr>
		    	</table>
		    </div>
		</div>
	</div>

</div><!-- /.box-body -->
<?php /* ?>
<script type="text/javascript">
	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle');
		$(document).on('change', '.ddl_disabled_toggle', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}
	});
</script>
<?php */ ?>