<div class="box-body">	
	<div class="row">
		<div class="col-sm-5">
			<table width="50%" class="table responsive" style="font-size: 0.9em">
				<?php  ?><tr>
		    		<td width="50%"><b>No. Kartu Keluarga </b></td>
		    		<td><?php echo form_input('no_kk', $no_kk, 'maxlength="16" class="form-control input-sm" readonly'); ?></td>
		    	</tr><?php  ?>
		    	<tr>
		    		<td><b>NIK Kepala Rumah Tangga</b></td>
		    		<td><?php echo form_input('nik_kep_rumahtangga', $nik_kep_rumahtangga, 'maxlength="16" class="form-control input-sm" readonly'); ?></td>
		    	</tr>
		    	<tr>
		    		<td><b>Nama Kepala Rumah Tangga</b></td>
		    		<td><?php echo form_input('b1_r8', $b1_r8, 'maxlength="16" class="form-control input-sm" readonly'); ?>
		    			<?php /*echo form_hidden('kodewilayah', $no_prop.$no_kab.'0'.$no_kec.$no_kel); ?>
		    			<?php echo form_hidden('no_prop', $no_prop); ?>
		    			<?php echo form_hidden('no_kab', $no_kab); ?>
		    			<?php echo form_hidden('no_kec', $no_kec); ?>
		    			<?php echo form_hidden('no_kel', $no_kel); ?>
		    			<?php echo form_hidden('provinsi', $provinsi); ?>
		    			<?php echo form_hidden('kabupaten', $kabupaten); ?>
		    			<?php echo form_hidden('kecamatan', $kecamatan); ?>
		    			<?php echo form_hidden('desa', $desa);*/ ?>
		    		</td>
		    	</tr>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="box-body">
		    <div style="text-align: center;" class="alert alert-success" role="alert">
		      <b>IV. KETERANGAN SOSIAL EKONOMI ANGGOTA RUMAH TANGGA</b>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>1.</b></td>
			    		<td width="50%"><b>No. Urut rumah tangga</b></td>
			    		<td width="15%"><?php echo form_dropdown('no_art', array_slice($opsi_no_urut, 1, $b1_r9, true), $no_art, 'class="form-control input-sm" disabled'); ?>
			    		<?php /*echo form_input('no_art', '', 'class="form-control input-sm" style="text-transform:uppercase" disabled');*/ ?></td>
			    		<td width="25%">&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td><b>2.</b></td>
			    		<td><b>Nama Lengkap</b></td>
			    		<td colspan="2"><?php echo form_input('nama', $nama_lgkp, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    	</tr>	
			    	<tr>
			    		<td></td>
			    		<td><b>NIK</b></td>
			    		<td width="15%" colspan="2" ><?php echo form_input('nik', $nik, 'maxlength="16" class="form-control input-sm" disabled'); ?></td>			    		
			    	</tr>
			    	<tr>
			    		<td><b>3.</b></td>
			    		<td><b>Hubungan dengan kepala rumah tangga </b></td>
			    		
			    		<td colspan="2"><?php echo form_dropdown('b4_k3', $opsi_b4_k3, $b4_k3, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>4.</b></td>
			    		<td><b>No. Urut keluarga</b></td>
			    		<td width="15%"><?php echo form_dropdown('b4_k4', array_slice($opsi_no_urutKK, 1, $b1_r10, true), $b4_k4, 'class="form-control input-sm" disabled'); ?>
			    		<?php /*echo form_input('b4_k4', '', 'class="form-control input-sm" style="text-transform:uppercase" disabled');*/ ?></td>
			    		<td>&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td><b>5.</b></td>
			    		<td><b>Hubungan dengan kepala keluarga </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k5', $opsi_b4_k5, $b4_k5, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>6.</b></td>
			    		<td><b>Jenis kelamin </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k6', $opsi_b4_k6, $jenis_klmin, 'class="form-control input-sm ddl_disabled_toggle_kelamin" data-ddl_target=".hamil" disabled'); ?>
						<?php /*echo form_hidden('b4_k6', $jenis_klmin, 'class="form-control input-sm ddl_disabled_toggle_kelamin" data-ddl_target=".hamil"');*/ ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>7.</b></td>
			    		<td><b>Umur (pada saat pendataan) </b></td>
			    		<td><?php echo form_input('b4_k7', $umur, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td>Tahun</td>
			    	</tr>
			    	<tr>
			    		<td><b>8.</b></td>
			    		<td><b>Status perkawinan </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k8', $opsi_b4_k8, $stat_kwn, 'class="form-control input-sm ddl_disabled_toggle_kawin" data-ddl_target=".kawin" disabled'); ?>
						<?php /*echo form_hidden('b4_k8', $stat_kwn);*/ ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>9.</b></td>
			    		<td><b>Kepemilikan buku nikah/akta cerai </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k9', $opsi_b4_k9, $b4_k9, 'class="form-control input-sm kawin" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>10.</b></td>
			    		<td><b>Tercantum dalam Kartu keluarga (KK) di rumah tangga </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k10', $opsi_binary, 1, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>11.</b></td>
			    		<td><b>Kepemilikan kartu identitas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k11', $opsi_b4_k11, $b4_k11, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>12.</b></td>
			    		<td width="50%"><b>Status kehamilan (wanita 10-48 tahun) </b></td>
			    		<td colspan="2" width="40%"><?php echo form_dropdown('b4_k12', $opsi_b4_k12, $b4_k12, 'class="form-control input-sm hamil" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>13.</b></td>
			    		<td><b>Jenis cacat </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k13', $opsi_b4_k13, $b4_k13, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>14.</b></td>
			    		<td><b>Penyakit kronis/menahun </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k14', $opsi_b4_k14, $b4_k14, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
		    	</table>
		    </div>
		    <div class="col-sm-6">
			    <table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<th colspan="5"><span>ISIAN KEBAWAH UNTUK ART 5 TAHUN KEATAS</span></th>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>15.</b></td>
			    		<td width="50%"><b>Partisipasi sekolah </b></td>
			    		<td colspan="2" width="40%"><?php echo form_dropdown('b4_k15', $opsi_b4_k15, $b4_k15, 'class="form-control input-sm ddl_disabled_toggle_pendidikan" data-ddl_target=".pendidikan" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<th colspan="5">ISI NO.16, 17, DAN 18 JIKA NO.15 = 1 atau 2</th>
			    	</tr>
			    	<tr>
			    		<td><b>16.</b></td>
			    		<td><b>Jenjang pendidikan tertinggi yang pernah/sedang diduduki saat pendataan </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k16', $opsi_b4_k16, $b4_k16, 'class="form-control input-sm pendidikan" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>17.</b></td>
			    		<td><b>Kelas tertinggi yang pernah/sedang diduduki saat pendataan </b></td>
			    		<td width="15%"><?php echo form_dropdown('b4_k17', $opsi_no_urut, $b4_k17, 'class="form-control input-sm pendidikan" disabled'); ?>
			    		<?php /*echo form_input('b4_k17', $b4_k17, 'class="form-control input-sm pendidikan" style="text-transform:uppercase" disabled');*/ ?> </td>
			    		<td>&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td><b>18.</b></td>
			    		<td><b>Izasah tertinggi </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k18', $opsi_b4_k18, $b4_k18, 'class="form-control input-sm pendidikan" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td><b>19.</b></td>
			    		<td><b>Bekerja/membantu bekerja seminggu yang lalu </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k19a', $opsi_binary, $b4_k19a, 'class="form-control input-sm ddl_disabled_toggle_bekerja" data-ddl_target=".bekerja" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>Jumlah jam kerja dalam seminggu terakhir </b></td>
			    		<td><?php echo form_input('b4_k19b', $b4_k19b, 'class="form-control input-sm bekerja" onkeyup="validAngka(this)" style="text-transform:uppercase" disabled'); ?></td>
			    		<td>Jam</td>
			    	</tr>
			    	<tr>
			    		<td><b>20.</b></td>
			    		<td><b>Lapangan usaha dari pekerjaan utama </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k20', $opsi_b4_k20, $b4_k20, 'class="form-control input-sm bekerja" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td><b>21.</b></td>
			    		<td><b>Status kedudukan dari pekerjaan utama </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k21', $opsi_b4_k21, $b4_k21, 'class="form-control input-sm ddl_disabled_toggle_usaha bekerja" data-ddl_target=".usaha" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>Lapangan Usaha <i>(tulis secara lengkap)</i> </b></td>
			    		<td colspan="2"><?php echo form_input('b5_r5b2', $b5_r5b2, 'class="form-control input-sm usaha" style="text-transform:uppercase" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>Jumlah Pekerja (Orang) </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r5b3', $opsi_no_urut, $b5_r5b3, 'class="form-control input-sm usaha" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>Tempat / Lokasi Usaha </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r5b4', $opsi_keberadaan, $b5_r5b4, 'class="form-control input-sm usaha" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>Omzet Usaha Perbulan </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b5_r5b5', $opsi_omzet, $b5_r5b5, 'class="form-control input-sm usaha" disabled'); ?> </td>
			    	</tr>
		    	</table>
		    </div>
	    </div>
    </div>
</div><!-- /.box-body -->