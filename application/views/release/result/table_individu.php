<?php
// var_dump($data);die;
if (!$data):
  die('DATA TIDAK ADA');
else:
?>
<table class="table table-bordered table-striped">
  <thead>
    <tr valign="top">
      <th>NO</th>
      <th>KODEWILAYAH</th>
      <th>PROVINSI</th>
      <th>KABUPATEN</th>
      <th>KECAMATAN</th>
      <th>DESA</th>
      <th>NO_ART</th>
      <th>NAMA</th>
      <th>RID_RUMAHTANGGA</th>
      <th>RID_INDIVIDU</th>
      <th>NIK</th>
      <th>B4_K3</th>
      <th>B4_K4</th>
      <th>B4_K5</th>
      <th>B4_K6</th>
      <th>B4_K7</th>
      <th>B4_K8</th>
      <th>B4_K9</th>
      <th>B4_K10</th>
      <th>B4_K11</th>
      <th>B4_K12</th>
      <th>B4_K13</th>
      <th>B4_K14</th>
      <th>B4_K15</th>
      <th>B4_K16</th>
      <th>B4_K17</th>
      <th>B4_K18</th>
      <th>B4_K19A</th>
      <th>B4_K19B</th>
      <th>B4_K20</th>
      <th>B4_K21</th>
      <th>B5_K5B2</th>
      <th>B5_K5B3</th>
      <th>B5_K5B4</th>
      <th>B5_K5B5</th>
      <th>NO_URUT_RT</th>
      <th>NO_KK</th>
      <th>TGL_ENTRI</th>
      <th>PETUGAS_ENTRI</th>
      <th>TGL_RUBAH</th>
      <th>PETUGAS_RUBAH</th>
      <th>NO_KK_INDIVIDU</th>
      <th>NO_ART_BDT</th>
    </tr>
  </thead>
  <tbody>
  <?php
$no = 1+$page;
foreach ($data as $row) {
$row = keysToLower($row);
extract((array) $row);
?>
    <tr valign="top">                      
      <td><?php echo $no++; ?></td>
      <td><?php echo $kodewilayah; ?></td>
      <td><?php echo $provinsi; ?></td>
      <td><?php echo $kabupaten; ?></td>
      <td><?php echo $kecamatan; ?></td>
      <td><?php echo $desa; ?></td>
      <td><?php echo $no_art; ?></td>
      <td><?php echo $nama; ?></td>
      <td><?php echo $rid_rumahtangga; ?></td>
      <td><?php echo $rid_individu; ?></td>
      <td><?php echo "'$nik"; ?></td>
      <td><?php echo $b4_k3; ?></td>
      <td><?php echo $b4_k4; ?></td>
      <td><?php echo $b4_k5; ?></td>
      <td><?php echo $b4_k6; ?></td>
      <td><?php echo $b4_k7; ?></td>
      <td><?php echo $b4_k8; ?></td>
      <td><?php echo $b4_k9; ?></td>
      <td><?php echo $b4_k10; ?></td>
      <td><?php echo $b4_k11; ?></td>
      <td><?php echo $b4_k12; ?></td>
      <td><?php echo $b4_k13; ?></td>
      <td><?php echo $b4_k14; ?></td>
      <td><?php echo $b4_k15; ?></td>
      <td><?php echo $b4_k16; ?></td>
      <td><?php echo $b4_k17; ?></td>
      <td><?php echo $b4_k18; ?></td>
      <td><?php echo $b4_k19a; ?></td>
      <td><?php echo $b4_k19b; ?></td>
      <td><?php echo $b4_k20; ?></td>
      <td><?php echo $b4_k21; ?></td>
      <td><?php echo $b5_k5b2; ?></td>
      <td><?php echo $b5_k5b3; ?></td>
      <td><?php echo $b5_k5b4; ?></td>
      <td><?php echo $b5_k5b5; ?></td>
      <td><?php echo $no_urut_rt; ?></td>
      <td><?php echo "'$no_kk"; ?></td>
      <td><?php echo $tgl_entri; ?></td>
      <td><?php echo $petugas_entri; ?></td>
      <td><?php echo $tgl_rubah; ?></td>
      <td><?php echo $petugas_rubah; ?></td>
      <td><?php echo "'$no_kk_individu"; ?></td>
      <td><?php echo $no_art_bdt; ?></td>
    </tr>
    <?php
    }
    ?>

  </tbody>
</table>

<?php endif;?>