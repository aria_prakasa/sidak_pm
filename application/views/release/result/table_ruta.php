<?php
// var_dump($data);die;
if (!$data):
  die('DATA TIDAK ADA');
else:
?>
<table class="table table-bordered table-striped">
  <thead>
    <tr valign="top">
      <th>NO</th>
      <th>KODEWILAYAH</th>
      <th>PROVINSI</th>
      <th>KABUPATEN</th>
      <th>KECAMATAN</th>
      <th>DESA</th>
      <th>B1_K5</th>
      <th>B1_K6</th>
      <th>B1_K7</th>
      <th>B1_K8</th>
      <th>RID_RUMAHTANGGA</th>
      <th>B1_K9</th>
      <th>B1_K10</th>
      <th>B3_K1A</th>
      <th>B3_K1B</th>
      <th>B3_K2</th>
      <th>B3_K3</th>
      <th>B3_K4A</th>
      <th>B3_K4B</th>
      <th>B3_K5A</th>
      <th>B3_K5B</th>
      <th>B3_K6</th>
      <th>B3_K7</th>
      <th>B3_K8</th>
      <th>B3_K9A</th>
      <th>B3_K9B</th>
      <th>B3_K10</th>
      <th>B3_K11A</th>
      <th>B3_K11B</th>
      <th>B3_K12</th>
      <th>B5_K1A</th>
      <th>B5_K1B</th>
      <th>B5_K1C</th>
      <th>B5_K1D</th>
      <th>B5_K1E</th>
      <th>B5_K1F</th>
      <th>B5_K1G</th>
      <th>B5_K1H</th>
      <th>B5_K1I</th>
      <th>B5_K1J</th>
      <th>B5_K1K</th>
      <th>B5_K1L</th>
      <th>B5_K1M</th>
      <th>B5_K1N</th>
      <th>B5_K1O</th>
      <th>B5_K3A1</th>
      <th>B5_K3A2</th>
      <th>B5_K3B</th>
      <th>B5_K4A</th>
      <th>B5_K4B</th>
      <th>B5_K4C</th>
      <th>B5_K4D</th>
      <th>B5_K4E</th>
      <th>B5_K5A</th>
      <th>B5_K6A</th>
      <th>B5_K6B</th>
      <th>B5_K6C</th>
      <th>B5_K6D</th>
      <th>B5_K6E</th>
      <th>B5_K6F</th>
      <th>B5_K6G</th>
      <th>B5_K6H</th>
      <th>B5_K6I</th>
      <th>NO_URUT_RT</th>
      <th>STATUS_RT</th>
      <th>NO_KK</th>
      <th>NIK_KEP_RUMAHTANGGA</th>
      <th>TGL_CACAH</th>
      <th>PETUGAS_CACAH</th>
      <th>TGL_PERIKSA</th>
      <th>PETUGAS_PERIKSA</th>
      <th>HASIL_CACAH</th>
      <th>NAMA_RESPONDEN</th>
      <th>TGL_ENTRI</th>
      <th>PETUGAS_ENTRI</th>
      <th>TGL_RUBAH</th>
      <th>PETUGAS_RUBAH</th>
    </tr>
  </thead>
  <tbody>
  <?php
$no = 1+$page;
foreach ($data as $row) {
$row = keysToLower($row);
extract((array) $row);
?>
    <tr valign="top">                      
      <td><?php echo $no++; ?></td>
      <td><?php echo $kodewilayah; ?></td>
      <td><?php echo $provinsi; ?></td>
      <td><?php echo $kabupaten; ?></td>
      <td><?php echo $kecamatan; ?></td>
      <td><?php echo $desa; ?></td>
      <td><?php echo $b1_k5; ?></td>
      <td><?php echo $b1_k6; ?></td>
      <td><?php echo $b1_k7; ?></td>
      <td><?php echo $b1_k8; ?></td>
      <td><?php echo $rid_rumahtangga; ?></td>
      <td><?php echo $b1_k9; ?></td>
      <td><?php echo $b1_k10; ?></td>
      <td><?php echo $b3_k1a; ?></td>
      <td><?php echo $b3_k1b; ?></td>
      <td><?php echo $b3_k2; ?></td>
      <td><?php echo $b3_k3; ?></td>
      <td><?php echo $b3_k4a; ?></td>
      <td><?php echo $b3_k4b; ?></td>
      <td><?php echo $b3_k5a; ?></td>
      <td><?php echo $b3_k5b; ?></td>
      <td><?php echo $b3_k6; ?></td>
      <td><?php echo $b3_k7; ?></td>
      <td><?php echo $b3_k8; ?></td>
      <td><?php echo $b3_k9a; ?></td>
      <td><?php echo $b3_k9b; ?></td>
      <td><?php echo $b3_k10; ?></td>
      <td><?php echo $b3_k11a; ?></td>
      <td><?php echo $b3_k11b; ?></td>
      <td><?php echo $b3_k12; ?></td>
      <td><?php echo $b5_k1a; ?></td>
      <td><?php echo $b5_k1b; ?></td>
      <td><?php echo $b5_k1c; ?></td>
      <td><?php echo $b5_k1d; ?></td>
      <td><?php echo $b5_k1e; ?></td>
      <td><?php echo $b5_k1f; ?></td>
      <td><?php echo $b5_k1g; ?></td>
      <td><?php echo $b5_k1h; ?></td>
      <td><?php echo $b5_k1i; ?></td>
      <td><?php echo $b5_k1j; ?></td>
      <td><?php echo $b5_k1k; ?></td>
      <td><?php echo $b5_k1l; ?></td>
      <td><?php echo $b5_k1m; ?></td>
      <td><?php echo $b5_k1n; ?></td>
      <td><?php echo $b5_k1o; ?></td>
      <td><?php echo $b5_k3a1; ?></td>
      <td><?php echo $b5_k3a2; ?></td>
      <td><?php echo $b5_k3b; ?></td>
      <td><?php echo $b5_k4a; ?></td>
      <td><?php echo $b5_k4b; ?></td>
      <td><?php echo $b5_k4c; ?></td>
      <td><?php echo $b5_k4d; ?></td>
      <td><?php echo $b5_k4e; ?></td>
      <td><?php echo $b5_k5a; ?></td>
      <td><?php echo $b5_k6a; ?></td>
      <td><?php echo $b5_k6b; ?></td>
      <td><?php echo $b5_k6c; ?></td>
      <td><?php echo $b5_k6d; ?></td>
      <td><?php echo $b5_k6e; ?></td>
      <td><?php echo $b5_k6f; ?></td>
      <td><?php echo $b5_k6g; ?></td>
      <td><?php echo $b5_k6h; ?></td>
      <td><?php echo $b5_k6i; ?></td>
      <td><?php echo $no_urut_rt; ?></td>
      <td><?php echo $status_rt; ?></td>
      <td><?php echo "'$no_kk"; ?></td>
      <td><?php echo "'$nik_kep_rumahtangga"; ?></td>
      <td><?php echo $tgl_cacah; ?></td>
      <td><?php echo $petugas_cacah; ?></td>
      <td><?php echo $tgl_periksa; ?></td>
      <td><?php echo $petugas_periksa; ?></td>
      <td><?php echo $hasil_cacah; ?></td>
      <td><?php echo $nama_responden; ?></td>
      <td><?php echo $tgl_entri; ?></td>
      <td><?php echo $petugas_entri; ?></td>
      <td><?php echo $tgl_rubah; ?></td>
      <td><?php echo $petugas_rubah; ?></td>
    </tr>
    <?php
    }
    ?>

  </tbody>
</table>

<?php endif;?>