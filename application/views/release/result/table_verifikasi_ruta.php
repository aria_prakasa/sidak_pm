<?php
// var_dump($data);die;
if (!$data):
  die('DATA TIDAK ADA');
else:
?>
<table class="table table-bordered table-striped">
  <thead>
    <tr valign="top">
      <th>No</th>
      <th>Kecamatan</th>
      <th>Desa</th>
      <th>Nama KRT</th>
      <th>Jenis Kelamin KRT</th>
      <th>Nama Pasangan KRT</th>
      <th>Nama Anggota RuTa selain KRT</th>
      <th>Alamat</th>
      <th>Rumah Tangga Baru</th>
      <th>Rumah Tangga BDT</th>
      <th>Pendaftaran Aktif/Pasif</th>
    </tr>
  </thead>
  <tbody>
  <?php
$no = 1+$page;
foreach ($data as $row) {
$row = keysToLower($row);
extract((array) $row);
?>
    <tr valign="top">                      
      <td><?php echo $no++; ?></td>
      <td><?php echo "$kecamatan"; ?></td>
      <td><?php echo "$desa"; ?></td>
      <td><?php echo "$b1_r8"; ?></td>
      <td style="text-align:center"><?php if ($kelamin == 1) {
            echo "LAKI-LAKI";
          } else {
            echo "PEREMPUAN";
          } ?></td>
      <td><?php echo "$nama_pasangan_krt"; ?></td>
      <td><?php echo "$nama_art"; ?></td>
      <td><?php echo "$b1_r6"; ?></td>
      <td style="text-align:center"><?php echo "$ruta_baru"; ?></td>
      <td style="text-align:center"><?php echo "$ruta_bdt"; ?></td>
      <td>&nbsp;</td>
    </tr>
    <?php
    }
    ?>

  </tbody>
</table>

<?php endif;?>