<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
        body { font-family:Arial; font-size:13px; }
        table.cetak tr td,
        table.cetak tr {
          padding: 1px 1px;
          font-size: 10px;
          margin: 0; }
        table.header {border: 0.5px solid #000000;}
        table.header tr th,
        table.header tr {
          text-align: left;
          padding: 1px 1px;
          font-size: 10px;
          margin: 0; }
        table.body tr td,
        table.body tr {
          vertical-align: text-top;;
          text-align: left;
          padding: 1px 0px;
          font-size: 10px;
          margin: 0; }
        table.footer tr td,
        table.footer tr {
          border: 0.5px solid #000000;
          padding: 1px 0px;
          font-size: 10px;
          margin: 1; }
        /*p { font-family:Arial; font-size:14px;} */
</style>
<link rel="stylesheet" href="<?php echo base_url('_assets/style/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url("_assets/style/css/print.css?v".date('YmdHi')); ?>" />
</head>

<body>
<div class="col-lg-12">
  <p align="center" style="font-weight:bold">
    <span style="font-size:20px"><?php echo $jenis_cetak; ?></span>  
  </p>
</div>

<div class="table-responsive">
  <table width="100%" class="header";> 
    <thead>
      <tr>
        <th colspan="4">
          <u>DATA WILAYAH :</u>
        </th>
      </tr>
      <tr>
        <th width="20%">
          1. Nama Propinsi
        </th>
        <th width="2%">
          :
        </th>
        <th width="10%">
          <?php echo "[".$no_prop."]"; ?>
        </th>
        <th>
          <?php echo $provinsi; ?>
        </th>
      </tr>
      <tr>
        <th>
          2. Nama Kabupaten
        </th>
        <th>
          :
        </th>
        <th>
          <?php echo "[".$no_kab."]"; ?>
        </th>
        <th>
          <?php echo $kabupaten; ?>
        </th>
      </tr>
      <tr>
        <th>
          3. Nama Kecamatan
        </th>
        <th>
          :
        </th>
        <th>
          <?php echo "[".$no_kec."]"; ?>
        </th>
        <th>
          <?php echo $kecamatan; ?>
        </th>
      </tr>
      <tr>
        <th>
          4. Nama Desa/Kelurahan
        </th>
        <th>
          :
        </th>
        <th>
          <?php echo "[".$no_kel."]"; ?>
        </th>
        <th>
          <?php echo $desa; ?>
        </th>
      </tr>
    </thead>
  </table>
</div>
<br />
<div class="table-responsive">
  <table width="100%" class="header"> 
    <thead>
      <tr>
        <th colspan="4">
          <u>DATA RUMAH TANGGA :</u>
        </th>
      </tr>
    </thead>
   
    <tr>
      <td width="50%" valign="top">
        <table class="body">                
          <tr>
            <td width="5%">1.</td>
            <td width="50%">Nomor KK</td>
            <td width="5%">:</td>
            <td width="40%"><?php echo $no_kk; ?></td>
          </tr>
          <tr>                  
            <td>2.</td>              
            <td>Nama Kepala Rumah Tangga </td>
            <td>:</td>
            <td><?php echo $b1_r8; ?></td>
          </tr>
          <tr>
            <td>3.</td>
            <td>Jumlah Anggota Keluarga </td>
            <td>:</td>
            <td><?php echo $b1_r9; ?></td>
          </tr>
          <tr>
            <td>4.</td>              
            <td>Status Kesejahteraan (Desil) </td>
            <td>:</td>
            <td><?php echo $status_kesejahteraan; ?></td>
          </tr>
          <tr>
            <td>5.</td>
            <td>Status kepemilikan bangunan tempat tinggal </td>
            <td>:</td>
            <td><?php echo $b3_r1a; ?></td>
          </tr>
          <tr>
            <td>6.</td>              
            <td>Status kepemilikan lahan tempat tinggal </td>
            <td>:</td>
            <td><?php echo $b3_r1b; ?></td>
          </tr>
          <tr>
            <td>7.</td>
            <td>Luas lantai </td>
            <td>:</td>
            <td><?php echo $b3_r2; ?> m2</td>
          </tr>
          <tr>
            <td>8.</td>              
            <td>Jenis lantai terluas </td>
            <td>:</td>
            <td><?php echo $b3_r3; ?></td>
          </tr>
          <tr>
            <td>9.</td>
            <td>Jenis dinding terluas </td>
            <td>:</td>
            <td><?php echo $b3_r4a; ?></td>
          </tr>
          <tr>
            <td>10.</td>              
            <td>Jenis atap terluas </td>
            <td>:</td>
            <td><?php echo $b3_r5a; ?></td>
          </tr>
          <tr>
            <td>11.</td>
            <td>Kualitas dinding terluas </td>
            <td>:</td>
            <td><?php echo $b3_r4b; ?></td>
          </tr>
          <tr>
            <td>12.</td>              
            <td>Kualitas atap terluas </td>
            <td>:</td>
            <td><?php echo $b3_r5b; ?></td>
          </tr>
          <tr>
            <td>13.</td>
            <td>Jumlah kamar tidur </td>
            <td>:</td>
            <td><?php echo $b3_r6; ?></td>
          </tr>
          <tr>
            <td>14.</td>              
            <td>Sumber air minum </td>
            <td>:</td>
            <td><?php echo $b3_r7; ?></td>
          </tr>
          <tr>
            <td>15.</td>
            <td>Cara memperoleh air minum </td>
            <td>:</td>
            <td><?php echo $b3_r8; ?></td>
          </tr>
          <tr>
            <td>16.</td>              
            <td>Penggunaan fasilitas buang air besar </td>
            <td>:</td>
            <td><?php echo $b3_r11a; ?></td>
          </tr>
          <tr>
            <td>17.</td>
            <td>Jenis kloset </td>
            <td>:</td>
            <td><?php echo $b3_r11b; ?></td>
          </tr>
          <tr>
            <td>18.</td>              
            <td>Tempat pembuangan akhir tinja </td>
            <td>:</td>
            <td><?php echo $b3_r12; ?></td>
          </tr>
          <tr>
            <td>19.</td>
            <td>Sumber penerangan utama </td>
            <td>:</td>
            <td><?php echo $b3_r9a; ?></td>
          </tr>
          <tr>
            <td>20.</td>
            <td>Daya listrik terpasang (PLN) </td>
            <td>:</td>
            <td><?php echo $b3_r9b; ?></td>
          </tr>
          <tr>
            <td>21.</td>              
            <td>Bahan bakar untuk memasak </td>
            <td>:</td>
            <td><?php echo $b3_r10; ?></td>
          </tr>
          <tr>
            <td>22.</td>
            <td>Kepemilikan tabung gas 5.5 kg atau lebih </td>
            <td>:</td>
            <td><?php echo $b5_r1a; ?></td>
          </tr>
          <tr>
            <td>23.</td>              
            <td>Kepemilikan sambungan telepon (PSTN) </td>
            <td>:</td>
            <td><?php echo $b5_r1e; ?></td>
          </tr>
          <tr>
            <td>24.</td>
            <td>Kepemilikan komputer/laptop </td>
            <td>:</td>
            <td><?php echo $b5_r1h; ?></td>
          </tr>
          <tr>
            <td>25.</td>              
            <td>Kepemilikan Sepeda </td>
            <td>:</td>
            <td><?php echo $b5_r1i; ?></td>
          </tr>
          <tr>
            <td>26.</td>
            <td>Kepemilikan Sepeda motor </td>
            <td>:</td>
            <td><?php echo $b5_r1j; ?></td>
          </tr>
          <tr>
            <td>27.</td>              
            <td>Kepemilikan Mobil </td>
            <td>:</td>
            <td><?php echo $b5_r1k; ?></td>
          </tr>
          <tr>
            <td>28.</td>
            <td>Kepemilikan Perahu </td>
            <td>:</td>
            <td><?php echo $b5_r1l; ?></td>
          </tr>
          <tr>
            <td>29.</td>              
            <td>Kepemilikan Motor tempel </td>
            <td>:</td>
            <td><?php echo $b5_r1m; ?></td>
          </tr>
          <tr>
            <td>30.</td>
            <td>Kepemilikan Perahu motor </td>
            <td>:</td>
            <td><?php echo $b5_r1n; ?></td>
          </tr>
          <tr>
            <td>32.</td>             
            <td>Kepemilikan Kapal </td>
            <td>:</td>
            <td><?php echo $b5_r1o; ?></td>
          </tr>
          <tr>
            <td>33.</td>              
            <td>Kepemilikan AC (penyejuk udara) </td>
            <td>:</td>
            <td><?php echo $b5_r1c; ?></td>
          </tr>
          <tr>
            <td>34.</td>
            <td>Kepemilikan Pemanas air (water heater) </td>
            <td>:</td>
            <td><?php echo $b5_r1d; ?></td>
          </tr>
        </table>
      </td>
    
      <td width="50%" valign="top">
        <table class="body"> 
          <tr>
            <td width="5%">35.</td>              
            <td width="50%">Kepemilikan Lemari es/kulkas </td>
            <td width="5%">:</td>
            <td width="40%"><?php echo $b5_r1b; ?></td>
          </tr>
          <tr>
            <td>36.</td>
            <td>Kepemilikan Televisi </td>
            <td>:</td>
            <td><?php echo $b5_r1f; ?></td>
          </tr>
          <tr>
            <td>37.</td>              
            <td>Kepemilikan Emas/perhiasan/tabungan senilai 10 gram emas </td>
            <td>:</td>
            <td><?php echo $b5_r1g; ?></td>
          </tr>
          <tr>
            <td>38.</td>
            <td>Jumlah nomor HP aktif </td>
            <td>:</td>
            <td><?php echo $b5_r2a; ?></td>
          </tr>
          <tr>
            <td>39.</td>              
            <td>Jumlah TV layar datar minimal 30inchi </td>
            <td>:</td>
            <td><?php echo $b5_r2b; ?></td>
          </tr>
          <tr>
            <td>40.</td>
            <td>Kepemilikan lahan </td>
            <td>:</td>
            <td><?php echo $b5_r3a1; ?></td>
          </tr>                 
          <tr>
            <td>41.</td>
            <td>Luas lahan yang dimiliki </td>
            <td>:</td>
            <td><?php echo $b5_r3a2; ?></td>
          </tr>
          <tr>
            <td>42.</td>
            <td>Kepemilikan rumah di lokasi lain </td>
            <td>:</td>
            <td><?php echo $b5_r3b; ?></td>
          </tr>
          <tr>
            <td>43.</td>
            <td>Ada anggota rumah tangga yang memiliki usaha sendiri/bersama </td>
            <td>:</td>
            <td><?php echo $b5_r5a; ?></td>
          </tr>
          <tr>
            <td>44.</td>
            <td>Jumlah ternak sapi yang dimilki </td>
            <td>:</td>
            <td><?php echo $b5_r4a; ?></td>
          </tr>
          <tr>
            <td>45.</td>
            <td>Jumlah ternak kerbau yang dimilki </td>
            <td>:</td>
            <td><?php echo $b5_r4b; ?></td>
          </tr>
          <tr>
            <td>46.</td>
            <td>Jumlah ternak kuda yang dimilki </td>
            <td>:</td>
            <td><?php echo $b5_r4c; ?></td>
          </tr>
          <tr>
            <td>47.</td>
            <td>Jumlah ternak babi yang dimilki </td>
            <td>:</td>
            <td><?php echo $b5_r4d; ?></td>
          </tr>
          <tr>
            <td>48.</td>
            <td>Jumlah ternak kambing/domba yang dimilki </td>
            <td>:</td>
            <td><?php echo $b5_r4e; ?></td>
          </tr>
          <tr>
            <td>49.</td>
            <td>Memiliki KKS/KPS </td>
            <td>:</td>
            <td><?php echo $b5_r6a; ?></td>
          </tr>
          <tr>
            <td>50.</td>
            <td>Peserta program PKH </td>
            <td>:</td>
            <td><?php echo $b5_r6g; ?></td>
          </tr>
          <tr>
            <td>51.</td>
            <td>Peserta program Raskin </td>
            <td>:</td>
            <td><?php echo $b5_r6h; ?></td>
          </tr>
          <tr>
            <td>52.</td>
            <td>Peserta program KUR </td>
            <td>:</td>
            <td><?php echo $b5_r6i; ?></td>
          </tr>
          <tr>
            <td>53.</td>
            <td>Kepesertaan KB </td>
            <td>:</td>
            <td><?php echo $b5_r9; ?></td>
          </tr>
          <tr>
            <td>54.</td>
            <td>Usia kawin pertama suami </td>
            <td>:</td>
            <td><?php echo $b5_r8a; ?></td>
          </tr>
          <tr>
            <td>55.</td>
            <td>Usia kawin pertama istri </td>
            <td>:</td>
            <td><?php echo $b5_r8b; ?></td>
          </tr>
          <tr>
            <td>56.</td>
            <td>Metode/jenis kontrasepsi yang digunakan </td>
            <td>:</td>
            <td><?php echo $b5_r10; ?></td>
          </tr>
          <tr>
            <td>57.</td>
            <td>Lama menggunakan kontrasepsi </td>
            <td>:</td>
            <td><?php echo $b5_r11a; ?> tahun <?php echo $b5_r11b; ?> bulan</td>
          </tr>
          <tr>
            <td>58.</td>
            <td>Tempat pelayanan KB yang sering digunakan </td>
            <td>:</td>
            <td><?php echo $b5_r12; ?></td>
          </tr>
          <tr>
            <td>59.</td>
            <td>Keinginan punya anak lagi </td>
            <td>:</td>
            <td><?php echo $b5_r13; ?></td>
          </tr>
          <tr>
            <td>60.</td>
            <td>Alasan tidak mengikuti KB </td>
            <td>:</td>
            <td><?php echo $b5_r14; ?></td>
          </tr>
          <tr>
            <td>61.</td>
            <td>Memiliki KIP/BSM </td>
            <td>:</td>
            <td><?php echo $b5_r6b; ?></td>
          </tr>
          <tr>
            <td>62.</td>
            <td>Memiliki KIS/BPJS Kesehatan/Jamkesmas </td>
            <td>:</td>
            <td><?php echo $b5_r6c; ?></td>
          </tr>
          <tr>
            <td>63.</td>
            <td>Memiliki BPJS Kesehatan peserta mandiri </td>
            <td>:</td>
            <td><?php echo $b5_r6d; ?></td>
          </tr>
          <tr>
            <td>64.</td>
            <td>Memiliki Jamsostek/BPJS ketenagakerjaan </td>
            <td>:</td>
            <td><?php echo $b5_r6e; ?></td>
          </tr>
          <tr>
            <td>65.</td>
            <td>Memiliki Asuransi kesehatan lainnya </td>
            <td>:</td>
            <td><?php echo $b5_r6f; ?></td>
          </tr>
        </table>
      </td>
    </tr> 
  </table>    
</div>
<br />
<div class="table-responsive">
  <table width="100%" class="header"> 
    <thead>
      <tr>
        <th colspan="4">
          <u>DATA INDIVIDU :</u>
        </th>
      </tr>
    </thead>
   
    <tr>
      <td width="100%" valign="top">
        <table class="body">                
          <tr>
            <td width="5%">1.</td>
            <td width="50%">N.I.K</td>
            <td width="5%">:</td>
            <td width="40%"><?php echo $nik; ?></td>
          </tr>
          <tr>
            <td>2.</td>              
            <td>No. Urut anggota keluarga</td>
            <td>:</td>
            <td><?php echo $no_art; ?></td>
          </tr>
          <tr>
            <td>3.</td>
            <td>Nama lengkap </td>
            <td>:</td>
            <td><?php echo $nama; ?></td>
          </tr>
          <tr>
            <td>4.</td>              
            <td>Hubungan dengan kepala rumah tangga </td>
            <td>:</td>
            <td><?php echo $b4_k3; ?></td>
          </tr>
          <tr>
            <td>5.</td>
            <td>Hubungan dengan kepala keluarga </td>
            <td>:</td>
            <td><?php echo $b4_k5; ?></td>
          </tr>
          <tr>
            <td>6.</td>              
            <td>Jenis kelamin </td>
            <td>:</td>
            <td><?php echo $b4_k6; ?> m2</td>
          </tr>
          <tr>
            <td>7.</td>
            <td>Umur (pada saat pendataan) </td>
            <td>:</td>
            <td><?php echo $b4_k7; ?></td>
          </tr>
          <tr>
            <td>8.</td>              
            <td>Status perkawinan </td>
            <td>:</td>
            <td><?php echo $b4_k8; ?></td>
          </tr>
          <tr>
            <td>9.</td>
            <td>Kepemilikan buku nikah/akta cerai </td>
            <td>:</td>
            <td><?php echo $b4_k9; ?></td>
          </tr>
          <tr>
            <td>10.</td>              
            <td>Tercantum dalam Kartu keluarga (KK) di rumah tangga </td>
            <td>:</td>
            <td><?php echo $b4_k10; ?></td>
          </tr>
          <tr>
            <td>11.</td>
            <td>Kepemilikan akta kelahiran/kartu pelajar/KTP/SIM </td>
            <td>:</td>
            <td><?php echo $b4_k11; ?></td>
          </tr>
          <tr>
            <td>12.</td>              
            <td>Partisipasi sekolah </td>
            <td>:</td>
            <td><?php echo $b4_k15; ?></td>
          </tr>
          <tr>
            <td>13.</td>
            <td>Jenjang pendidikan tertinggi (yang pernah/sedang diduduki saat pendataan) </td>
            <td>:</td>
            <td><?php echo $b4_k16; ?></td>
          </tr>
          <tr>
            <td>14.</td>              
            <td>Kelas tertinggi </td>
            <td>:</td>
            <td><?php echo $b4_k17; ?></td>
          </tr>
          <tr>
            <td>15.</td>
            <td>Izasah tertinggi </td>
            <td>:</td>
            <td><?php echo $b4_k18; ?></td>
          </tr>
          <tr>
            <td>16.</td>              
            <td>Bekerja/membantu bekerja seminggu terakhir (keaktifan ekonomi) </td>
            <td>:</td>
            <td><?php echo $b4_k19a; ?></td>
          </tr>
          <tr>
            <td>17.</td>
            <td>Jumlah jam kerja dalam seminggu terakhir </td>
            <td>:</td>
            <td><?php echo $b4_k19b; ?></td>
          </tr>
          <tr>
            <td>18.</td>              
            <td>Lapangan usaha dari pekerjaan utama </td>
            <td>:</td>
            <td><?php echo $b4_k20; ?></td>
          </tr>
          <tr>
            <td>19.</td>
            <td>Status kedudukan dari pekerjaan utama </td>
            <td>:</td>
            <td><?php echo $b4_k21; ?></td>
          </tr>
          <tr>
            
            <td>20.</td>
            <td>Jenis cacat </td>
            <td>:</td>
            <td><?php echo $b4_k13; ?></td>
          </tr>
          <tr>
            <td>21.</td>              
            <td>Penyakit kronis/menahun </td>
            <td>:</td>
            <td><?php echo $b4_k14; ?></td>
          </tr>
          <tr>
            <td>22.</td>
            <td>Status kehamilan (wanita 10-48 tahun) </td>
            <td>:</td>
            <td><?php echo $b4_k12; ?></td>
          </tr>
        </table>
      </td>
    </tr> 
  </table>    
</div>
  
</body>
</html>