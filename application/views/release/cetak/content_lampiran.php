<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php    ?>
<link rel="stylesheet" href="<?php echo base_url("_assets/css/screen.css?v".date('YmdHi')); ?>">
<link rel="stylesheet" href="<?php echo base_url("_assets/css/print.css?v".date('YmdHi')); ?>">
<?php    ?>
<style>
  body {font-family: "Times New Roman";}
  /*p {font-family: "Times New Roman";font-size: 12; }*/
  div.border_body {border: 2px solid #000000; padding: 5}
  table tr td {font-size: 14;}
  /*div.border {border: 1px solid #000000;}*/
</style>
</head>

<body>
<div class="border_body">
  <div class="col-lg-12">
    <p class="nomargin" align="center" style="font-weight:bold">
      <span style="font-size:18px"><?php echo $header; ?></span></p>
    <p align="center" style="font-weight:bold; margin: 7"> 
      <span style="font-size:12"><?php echo $sub_header; ?></span><br /></p>    
  </div>

  <p class="nobotmargin"><strong>INFORMASI PENDAFTARAN </strong></p><br/>
  <?php
  $arr = array(
        "NIK" => gen_option_single_value_long('<b>'.$nik.'</b>', ''),
        "Nama Lengkap Sesuai KTP" => gen_option_single_value_long('<b>'.$nama.'</b>', ''),
        "Jenis Kelamin" => gen_option_value($opsi_b4_k6, $b4_k6, 1),
        "Nomor KK" => gen_option_single_value_long('<b>'.$no_kk, ''),
        "Hubungan dengan Kepala Keluarga" => gen_option_value($opsi_b4_k5, $b4_k5, 4),
        "Hubungan dengan Kepala Rumah Tangga" => gen_option_value($opsi_b4_k3, $b4_k3, 4)
        );
  ?>

  <table class="list withnopadding responsive" width="100%">
  <?php
  $i = $j = 1;
  foreach($arr as $key => $val)
  {
    $tmp_arr = array("key" => $key, "val" => $val, "i" => $j);
    
      echo $this->load->view("cetak/tpl_row_no", $tmp_arr, TRUE);
    
    $i++;
    $j++;
  }
  ?>
  </table>

  <p class="nobotmargin"><strong>INFORMASI RUMAH TANGGA </strong></p>
  <br/>
  <?php
  $options = array('' => '- Pilih Opsi -',
            '1' => '1. Bekerja',
            '2' => '2. Tidak Bekerja',
            );
  $arr = array(
        "NIK Kepala Rumah Tangga" => gen_option_single_value_long('<b>'.$nik_rumahtangga.'</b>', ''),
        "Nomor KK Rumah Tangga" => gen_option_single_value_long('<b>'.$no_kk_rumahtangga.'</b>', ''),
        "Nama Kepala Rumah Tangga" => gen_option_single_value_long('<b>'.$nama_kep_rumahtangga.'</b>', ''),
        "Jenis Kelamin Kepala Rumah Tangga" => gen_option_value($opsi_b4_k6, $b4_k6_rumahtangga, 1),
        "Bulan Lahir Kepala Rumah Tangga" => gen_option_single_value_long(str_pad($bulan_lhr, 2, "0", STR_PAD_LEFT), ''),
        "Tahun Lahir Kepala Rumah Tangga" => gen_option_single_value_long($tahun_lhr, ''),
        "Status Pekerjaan Kepala Rumah Tangga" => gen_option_value($options, $bekerja_rumahtangga, 1),
        "Jumlah Anggota Rumah Tangga" => gen_option_single_value($b1_r9, 'Orang <br/><br/>'),
        "Provinsi" => gen_option_single_value_long($provinsi, ''),
        "Kabupaten / Kota" => gen_option_single_value_long($kabupaten, ''),
        "Kecamatan" => gen_option_single_value_long($kecamatan, ''),
        "Kelurahan / Desa" => gen_option_single_value_long($desa, ''),
        "Nama Jalan (RT/RW)" => gen_option_single_value_long($alamat.' RT/RW '.$no_rt.'/'.$no_rw, ''),
        "Status Kepemilikan Bangunan Tempat Tinggal" => gen_option_value($opsi_b3_r1a, $b3_r1a, 3),
        "Bahan Bangunan Utama Atap Rumah Terluas" => gen_option_value($opsi_b3_r5a, $b3_r5a, 5),
        "Bahan Bangunan Utama Dinding Rumah Terluas" => gen_option_value($opsi_b3_r4a, $b3_r4a, 4),
        "Penggunaan Fasilitas Tempat Buang Air Besar" => gen_option_value($opsi_b3_r11a, $b3_r11a, 2),
        "Apakah Rumah Tangga Memiliki Mobil?" => gen_option_value($opsi_binary, $b5_r1k, 1),
        "Apakah Rumah Tangga Memiliki AC?" => gen_option_value($opsi_binary, $b5_r1c, 1),
        "Apakah Rumah Tangga Memiliki Tabung Gas 5,5 Kg atau lebih?" => gen_option_value($opsi_binary, $b5_r1a, 1),
        "Pendidikan Tertinggi Anggota Rumah Tangga Yang Sudah Tidak Bersekolah?" => gen_option_value($opsi_b4_k18, $b4_k18, 3)
        );
  ?>
  <table class="list withnopadding responsive" width="100%">
  <?php
  $i = $j = 7;
  foreach($arr as $key => $val)
  {
    $tmp_arr = array("key" => $key, "val" => $val, "i" => $j);
    
      echo $this->load->view("cetak/tpl_row_no", $tmp_arr, TRUE);
    
    $i++;
    $j++;
  }
  ?>
  </table>

</div>
</body>

