<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
        body { font-family:arial; font-size:10px; }
        table tr td {border: 0.5px solid #000000;
          padding-left: 5px;
          padding-right: 5px;
          margin: 0;
          vertical-align:top}
        table tr th {font-weight: normal;text-align: left;vertical-align:top}
        table tr.header th {text-align: left;font-weight:bold;
          padding: 1px 1px;}
        table tr td.keterangan {border: 0px;
          padding: 0px 0px 0px 0px; 
          margin:0;}

        /*table.cetak tr td,
        table.cetak tr {
          padding: 1px 1px;
          font-size: 10px;
          margin: 0; }
        table.header 
        table.header tr th {border: 0.5px solid #000000;},
        table.header tr {
          text-align: left;
          padding: 1px 1px;
          font-size: 10px;
          margin: 0; }
        table.body tr td,
        table.body tr {
          vertical-align: text-top;;
          text-align: left;
          padding: 1px 0px;
          font-size: 10px;
          margin: 0; }
        table.footer tr td,
        table.footer tr {
          border: 0.5px solid #000000;
          padding: 1px 0px;
          font-size: 10px;
          margin: 1; }*/
        /*p { font-family:Arial; font-size:14px;} */
</style>
<link rel="stylesheet" href="<?php echo base_url('_assets/style/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url("_assets/style/css/print.css?v".date('YmdHi')); ?>" />
</head>

<body>
<div class="col-lg-12">
  <p align="center" style="font-weight:bold">
    <span style="font-size:18px"><?php echo $jenis_cetak; ?></span>  
  </p>
</div>

<div class="table-responsive">
  <table width="100%"> 
    <tr class="header">
      <th colspan="4">
        I. DATA WILAYAH :
      </th>
    </tr>
    <tr>
      <th width="3%">1.</th>
      <th width="25%">Kode-Nama Propinsi</th>
      <td width="7%" style="text-align:center;"><?php echo $no_prop; ?></td>
      <td><?php echo $provinsi; ?></td>
    </tr>
    <tr>
      <th>2.</th>
      <th>Kode-Nama Kabupaten</th>
      <td style="text-align:center"><?php echo $no_kab; ?></td>
      <td><?php echo $kabupaten; ?></td>
    </tr>
    <tr>
      <th>3.</th>
      <th>Kode-Nama Kecamatan</th>
      <td style="text-align:center"><?php echo str_pad($no_kec, 2, "0", STR_PAD_LEFT); ?></td>
      <td><?php echo $kecamatan; ?></td>
    </tr>
    <tr>
      <th>4.</th>
      <th>Kode-Nama Desa/Kelurahan</th>
      <td style="text-align:center"><?php echo $no_kel; ?></td>
      <td><?php echo $desa; ?></td>
    </tr>
  </table>
</div>
<br />
<div class="table-responsive">
  <table width="100%"> 
    <tr class="header">
      <th colspan="4">
        II. DATA RUMAH TANGGA :
      </th>
    </tr>   
    <tr>
      <th width="3%">1.</th>
      <th width="20%">Nomor Kartu Keluarga</th>
      <td colspan="2"><?php echo $no_kk; ?></td>
    </tr>
    <tr>
      <th>2.</th>
      <th>Nama Kepala Rumah Tangga </th>
      <td colspan="2"><?php echo $b1_r8; ?></td>
    </tr>
    <tr >
      <th>3.</th>
      <th>Jumlah Anggota Keluarga </th>
      <td colspan="2"><?php echo $b1_r9; ?></td>
    </tr>
    <?php
    $i=0;
    unset($opsi_status_kesejahteraan['']);
    foreach ($opsi_status_kesejahteraan as $key => $value) {
      ?>
    <tr>
      <?php if($i == 0) { ?>
      <th>4.</th>              
      <th>Status Kesejahteraan (Desil) </th>
      <td width="3%" style="border:1px solid;"><?php echo $status_kesejahteraan; ?></td>
      <?php } else { ?>
      <td colspan="3"></td>
      <?php } ?>
      <td class="keterangan">      
        <?php echo $key.". ".$value; ?>
      </td>
    </tr>
      <?php
      $i++;
    } ?>
    <?php
    unset($opsi_b3_r1a['']);
    ?>
    <tr>
      <th>5.</th>              
      <th>Status kepemilikan bangunan tempat tinggal </th>
      <td width="3%" style="border:1px solid;"><?php echo $b3_r1a; ?></td>
      <td class="keterangan">      
      <?php
      $i=0;
      foreach ($opsi_b3_r1a as $key => $value) {
        if($i%2 != 0) echo $key.". ".$value;
        $i++;
      }
      ?>
      </td>
    </tr>
    <tr>
      <td colspan="3"></td>
      <td class="keterangan">      
      <?php
      $i=0;
      foreach ($opsi_b3_r1a as $key => $value) {
        if($i%2 == 0) echo $key.". ".$value;
        $i++;
      }
      ?>
      </td>
    </tr>
    
  </table>    
</div>
<br />

  
</body>
</html>