<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php    ?>
<link rel="stylesheet" href="<?php echo base_url("_assets/css/screen.css?v".date('YmdHi')); ?>">
<link rel="stylesheet" href="<?php echo base_url("_assets/css/print.css?v".date('YmdHi')); ?>">
<?php    ?>
<style>
  div.border {border: 1px solid #000000;}
</style>
</head>

<body>
<div class="col-lg-12">
  <p align="center" style="font-weight:bold">
    <span style="font-size:18px"><?php echo $jenis_cetak; ?></span>
  </p>
</div>
<p class="nobotmargin"><strong>I. DATA WILAYAH</strong></p>
<?php
$arr = array(
      "Kode-Nama Propinsi" => array($no_prop, $provinsi),
      "Kode-Nama Kabupaten/Kota" => array($no_kab, $kabupaten),
      "Kode-Nama Kecamatan" => array('0'.$no_kec, $kecamatan),
      "Kode-Nama Kelurahan/ Desa" => array($no_kel, $desa)
      );
?>
<table class="list withnopadding" width="100%">
<?php
$i = $j = 1;
foreach($arr as $key => $value)
{
  if(!is_array($value))
  {
    $tmp_arr = array("key" => $key, "val" => $value);
    echo $this->load->view("cetak/tpl_row_wil", $tmp_arr, TRUE);
  }
  else
  {
    list($no, $val) = $value;
    $tmp_arr = array("key" => $key, "val" => $val, "no" => $no, "i" => $i);
    echo $this->load->view("cetak/tpl_row_wil_no_no", $tmp_arr, TRUE);
  }
  $i++;
  $j++;
}
?>
</table>

<p class="nobotmargin"><strong>II. DATA RUMAH TANGGA</strong></p>
<?php
$arr = array(
      "Nomor Kartu Keluarga" => gen_option_single_value_long($no_kk, ''),
      "Nama Kepala Rumah Tangga" => gen_option_single_value_long($b1_r8, ''),
      "Jumlah Anggota Keluarga" => gen_option_single_value($b1_r9, ''),
      "Status Kesejahteraan (Desil)" => gen_option_value($opsi_status_kesejahteraan, '-', 3),
      "Status kepemilikan bangunan tempat tinggal" => gen_option_value($opsi_b3_r1a, $b3_r1a, 2),
      "Status kepemilikan lahan tempat tinggal" => gen_option_value($opsi_b3_r1b, $b3_r1b, 2),
      "Luas lantai" => gen_option_single_value($b3_r2, 'm2'),
      "Jenis lantai terluas" => gen_option_value($opsi_b3_r3, $b3_r3, 5),
      "Jenis dinding terluas" => gen_option_value($opsi_b3_r4a, $b3_r4a, 7),
      "Jenis atap terluas" => gen_option_value($opsi_b3_r5a, $b3_r5a, 5),
      "Kualitas dinding terluas" => gen_option_value($opsi_b3_r4b, $b3_r4b, 3),
      "Kualitas atap terluas" => gen_option_value($opsi_b3_r5b, $b3_r5b, 3),
      "Jumlah kamar tidur" => gen_option_single_value($b3_r6, ''),
      "Sumber air minum" => gen_option_value($opsi_b3_r7, $b3_r7, 4),
      "Cara memperoleh air minum" => gen_option_value($opsi_b3_r8, $b3_r8, 3),
      "Penggunaan fasilitas buang air besar" => gen_option_value($opsi_b3_r11a, $b3_r11a, 4),
      "Jenis kloset" => gen_option_value($opsi_b3_r11b, $b3_r11b, 4),
      "Tempat pembuangan akhir tinja" => gen_option_value($opsi_b3_r12, $b3_r12, 3),
      "Sumber penerangan utama" => gen_option_value($opsi_b3_r9a, $b3_r9a, 3),
      "Daya listrik terpasang (PLN)" => gen_option_value($opsi_b3_r9b, $b3_r9b, 3),
      "Bahan bakar untuk memasak" => gen_option_value($opsi_b3_r10, $b3_r10, 3),
      "Kepemilikan tabung gas 5.5 kg atau lebih" => gen_option_value($opsi_binary, $b5_r1a, 1),
      "Kepemilikan sambungan telepon (PSTN)" => gen_option_value($opsi_binary, $b5_r1e, 1),
      "Kepemilikan komputer/laptop" => gen_option_value($opsi_binary_ii, $b5_r1h, 1),
      "Kepemilikan Sepeda" => gen_option_value($opsi_binary, $b5_r1i, 1),
      "Kepemilikan Sepeda motor" => gen_option_value($opsi_binary_ii, $b5_r1j, 1),
      "Kepemilikan Mobil" => gen_option_value($opsi_binary, $b5_r1k, 1),
      "Kepemilikan Perahu" => gen_option_value($opsi_binary_ii, $b5_r1j, 1),
      "Kepemilikan Motor tempel" => gen_option_value($opsi_binary, $b5_r1m, 1),
      "Kepemilikan Perahu motor" => gen_option_value($opsi_binary_ii, $b5_r1n, 1),
      "Kepemilikan Kapal" => gen_option_value($opsi_binary, $b5_r1o, 1),
      "Kepemilikan Lemari es/kulkas" => gen_option_value($opsi_binary_ii, $b5_r1b, 1),
      "Kepemilikan AC (penyejuk udara)" => gen_option_value($opsi_binary, $b5_r1c, 1),
      "Kepemilikan Pemanas air (water heater)" => gen_option_value($opsi_binary_ii, $b5_r1d, 1),
      "Kepemilikan Televisi" => gen_option_value($opsi_binary_ii, $b5_r1f, 1),
      "Kepemilikan Emas/perhiasan/tabungan senilai 10 gram emas" => gen_option_value($opsi_binary, $b5_r1g, 1),
      "Jumlah nomor HP aktif" => gen_option_single_value($b5_r2a, ''),
      "Jumlah TV layar datar minimal 30inchi" => gen_option_single_value($b5_r2b, ''),
      "Kepemilikan lahan" => gen_option_value($opsi_binary, $b5_r3a1, 1),
      "Luas lahan yang dimiliki" => gen_option_single_value($b5_r3a2, 'm2'),
      "Kepemilikan rumah di lokasi lain" => gen_option_value($opsi_binary_ii, $b5_r3b, 1),
      "Ada anggota rumah tangga yang memiliki usaha sendiri/bersama" => gen_option_value($opsi_binary, $b5_r5a, 1),
      "Jumlah ternak sapi yang dimilki" => gen_option_single_value($b5_r4a,''),
      "Jumlah ternak kerbau yang dimilki" => gen_option_single_value($b5_r4b,''),
      "Jumlah ternak kuda yang dimilki" => gen_option_single_value($b5_r4c,''),
      "Jumlah ternak babi yang dimilki" => gen_option_single_value($b5_r4d,''),
      "Jumlah ternak kambing/domba yang dimilki" => gen_option_single_value($b5_r4e,''),
      "Memiliki KKS/KPS" => gen_option_value($opsi_binary, $b5_r6a, 1),
      "Peserta program PKH" => gen_option_value($opsi_binary, $b5_r6g, 1),
      "Peserta program Raskin" => gen_option_value($opsi_binary_ii, $b5_r6h, 1),
      "Peserta program KUR" => gen_option_value($opsi_binary, $b5_r6i, 1),
      "Kepesertaan KB" => gen_option_value($opsi_binary, $b5_r9, 1),
      "Usia kawin pertama suami" => gen_option_single_value($b5_r8a, 'tahun'),
      "Usia kawin pertama istri" => gen_option_single_value($b5_r8b, 'tahun'),
      "Metode/jenis kontrasepsi yang digunakan" => gen_option_value($opsi_b5_r10, $b5_r10, 2),
      "Lama menggunakan kontrasepsi" => gen_option_double_value($b5_r11a, $b5_r11b),
      // "Lama menggunakan kontrasepsi" => gen_option_single_value_long($b5_r11a.' tahun '.$b5_r11b.' bulan', ''),
      "Tempat pelayanan KB yang sering digunakan" => gen_option_value($opsi_b5_r12, $b5_r12, 4),
      "Keinginan punya anak lagi" => gen_option_value($opsi_b5_r13, $b5_r13, 3),
      "Alasan tidak mengikuti KB" => gen_option_value($opsi_b5_r14, $b5_r14, 5),
      "Memiliki KIP/BSM" => gen_option_value($opsi_binary_ii, $b5_r6b, 1),
      "Memiliki KIS/BPJS Kesehatan/Jamkesmas" => gen_option_value($opsi_binary, $b5_r6c, 1),
      "Memiliki BPJS Kesehatan peserta mandiri" => gen_option_value($opsi_binary_ii, $b5_r6d, 1),
      "Memiliki Jamsostek/BPJS ketenagakerjaan" => gen_option_value($opsi_binary, $b5_r6e, 1),
      "Memiliki Asuransi kesehatan lainnya" => gen_option_value($opsi_binary_ii, $b5_r6f, 1)
      );
?>
<?php /*?> DATA INDIVIDU <?php */?>
<table class="list withnopadding" width="100%">
<?php
$i = $j = 1;
foreach($arr as $key => $val)
{
  $tmp_arr = array("key" => $key, "val" => $val, "i" => $j);
  // $tmp_arr['option'] = ' style"text-valign:top;"';
  // $tmp_arr['option'] = ' valign="top"';
  // if($j == 3 || $j == 6 || $j == 10 || $j == 12 || $j == 13 || $j == 14 || $j == 15 || $j == 18 || $j == 21 || $j == 22 || $j == 23 || $j == 24 || $j == 25)
  // {
    echo $this->load->view("cetak/tpl_row_no", $tmp_arr, TRUE);
  // }
  // elseif($i == 4)
  // {
  //   echo $this->load->view("cetak/tpl_row", $tmp_arr, TRUE);
  //   $i++;
  //   continue;
  // }
  // else
  // {
  //   echo $this->load->view("cetak/tpl_row_no_border", $tmp_arr, TRUE);
  // }
  $i++;
  $j++;
}
?>
</table>


