<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" media="all" href="<?php echo base_url("_assets/css/screen.css?v".date('YmdHi')); ?>">
<link rel="stylesheet" media="print" href="<?php echo base_url("_assets/css/print.css?v".date('YmdHi')); ?>">
</head>

<body>
<div class="col-lg-12">
  <p align="center" style="font-weight:bold">
    <span style="font-size:18px"><?php echo $jenis_cetak; ?></span>
  </p>
</div>
<?php  /*  ?>
<p class="nobotmargin"><strong>I. DATA WILAYAH</strong></p>
<?php
$arr = array(
      "Kode-Nama Propinsi" => array($no_prop, $provinsi),
      "Kode-Nama Kabupaten/Kota" => array($no_kab, $kabupaten),
      "Kode-Nama Kecamatan" => array('0'.$no_kec, $kecamatan),
      "Kode-Nama Kelurahan/ Desa" => array($no_kel, $desa)
      );
?>
<table class="list withnopadding" width="100%">
<?php
$i = $j = 1;
foreach($arr as $key => $value)
{
  if(!is_array($value))
  {
    $tmp_arr = array("key" => $key, "val" => $value);
    echo $this->load->view("cetak/tpl_row_wil", $tmp_arr, TRUE);
  }
  else
  {
    list($no, $val) = $value;
    $tmp_arr = array("key" => $key, "val" => $val, "no" => $no, "i" => $i);
    echo $this->load->view("cetak/tpl_row_wil_no_no", $tmp_arr, TRUE);
  }
  $i++;
  $j++;
}
?>
</table>
<?php   */ ?>
<p class="nobotmargin"><strong>III. DATA INDIVIDU</strong></p>
<?php
$arr = array(
      "Nomor Kartu Keluarga" => gen_option_single_value_long($no_kk, ''),
      "N.I.K" => gen_option_single_value_long($nik, ''),
      "Nama lengkap" => gen_option_single_value_long($nama, ''),
      "Hubungan dengan kepala rumah tangga" => gen_option_value($opsi_b4_k3, $b4_k3, 3),
      "Hubungan dengan kepala keluarga" => gen_option_value($opsi_b4_k5, $b4_k5, 3),
      "Jenis kelamin" => gen_option_value($opsi_b4_k6, $b4_k6, 1),
      "Umur (pada saat pendataan)" => gen_option_single_value($b4_k7, 'tahun'),
      "Status perkawinan" => gen_option_value($opsi_b4_k8, $b4_k8, 2),
      "Kepemilikan buku nikah/akta cerai" => gen_option_value($opsi_b4_k9, $b4_k9, 3),
      "Tercantum dalam Kartu keluarga (KK) di rumah tangga" => gen_option_value($opsi_binary, $b4_k10, 1),
      "Kepemilikan akta kelahiran/kartu pelajar/KTP/SIM" => gen_option_value($opsi_b4_k11, $b4_k11, 5),
      "Partisipasi sekolah" => gen_option_value($opsi_b4_k15, $b4_k15, 3),
      "Jenjang pendidikan tertinggi (yang pernah/sedang diduduki saat pendataan)" => gen_option_value($opsi_b4_k16, $b4_k16, 4),
      "Kelas tertinggi" => gen_option_single_value($b4_k17, ''),
      "Izasah tertinggi" => gen_option_value($opsi_b4_k18, $b4_k18, 2),
      "Bekerja/membantu bekerja seminggu terakhir (keaktifan ekonomi)" => gen_option_value($opsi_binary, $b4_k19a,1),
      "Jumlah jam kerja dalam seminggu terakhir" => gen_option_single_value($b4_k19b, 'jam'),
      "Lapangan usaha dari pekerjaan utama" => gen_option_value($opsi_b4_k20, $b4_k20, 11),
      "Status kedudukan dari pekerjaan utama" => gen_option_value($opsi_b4_k21, $b4_k21, 4),
      "Jenis cacat" => gen_option_value($opsi_b4_k13, $b4_k13, 6),
      "Penyakit kronis/menahun" => gen_option_value($opsi_b4_k14, $b4_k14, 3),
      "Status kehamilan (wanita 10-48 tahun)" => gen_option_value($opsi_b4_k12, $b4_k12, 2),
      );
?>
<?php /*?> DATA INDIVIDU <?php */?>
<table class="list withnopadding" width="100%">
<?php
$i = $j = 1;
foreach($arr as $key => $val)
{
  $tmp_arr = array("key" => $key, "val" => $val, "i" => $j);
  // $tmp_arr['option'] = ' style"text-valign:top;"';
  // $tmp_arr['option'] = ' valign="top"';
  // if($j == 3 || $j == 6 || $j == 10 || $j == 12 || $j == 13 || $j == 14 || $j == 15 || $j == 18 || $j == 21 || $j == 22 || $j == 23 || $j == 24 || $j == 25)
  // {
    echo $this->load->view("cetak/tpl_row_no", $tmp_arr, TRUE);
  // }
  // elseif($i == 4)
  // {
  //   echo $this->load->view("cetak/tpl_row", $tmp_arr, TRUE);
  //   $i++;
  //   continue;
  // }
  // else
  // {
  //   echo $this->load->view("cetak/tpl_row_no_border", $tmp_arr, TRUE);
  // }
  $i++;
  $j++;
}
?>
</table>


