<div class="callout callout-danger">
	<h4>PERHATIAN !</h4>
	<p>Ikuti instruksi pengisian data berikut ini : <br/>
	1. Isikan terlebih dahulu Variabel Kemiskinan Rumah Tangga pada bagian <b>Data Keluarga</b> dengan cara klik tombol Input Data Kemiskinan. <br/>
	2. Lanjut lakukan pengisian Variabel Kemiskinan Individu pada <b>Data Anggota Keluarga</b> dengan cara klik tombol pada kolom paling kanan pada tabel. <br/>
	3. Isikan secara urut sesuai urutan keluarga, <b>Kepala Keluarga </b>untuk didahulukan.</p>
</div>
<div class="box box-info">	
	<!-- content -->
	<div class="box-body">
		<div class="row">	
			<div class="box-body">
				<div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-info" role="alert">
				      <b>DATA KELUARGA</b>
				    </div>
			    </div>
			    <div class="col-sm-8">
			    	<table width="100%" class="table responsive" class="nobotmargin" style="font-size: 0.9em">
				    	<tr>
				    		<td width="5%"><b>1.</b></td>
				    		<td width="40%"><b>Provinsi</b></td>
				    		<td width="5%">:</td>
				    		<td><?php echo form_input('', $provinsi, 'placeholder="Propinsi" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>2.</b></td>
				    		<td><b>Kabupaten/Kota</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $kabupaten, 'placeholder="Kabupaten" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>3.</b></td>
				    		<td><b>Kecamatan</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $kecamatan, 'placeholder="Kecamatan" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>4.</b></td>
				    		<td><b>Kelurahan/Desa</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $desa, 'placeholder="Kelurahan/Desa" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>5.</b></td>
				    		<td><b>Alamat</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $b1_r6, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td><b>6.</b></td>
				    		<td><b>Nomor Kartu Keluarga</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $no_kk, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td colspan="2">&nbsp;</td>
				    		<td>&nbsp;</td>
				    		<td>
				    		<?php  if ($this->cu->USER_LEVEL == 4): ?>
				    			<a href="<?php echo base_url();?>index.php/release/lihat_rtangga?<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Edit Data Rumah Tangga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-success btn-sm"><i class="fa fa-search"></i> Lihat Data Rumah Tangga</span></a>
				    		<?php else: ?>
				    			<a href="<?php echo base_url();?>index.php/release/edit_rtangga?<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Edit Data Rumah Tangga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-primary btn-sm">+ <i class="fa fa-pencil"></i> Edit Data Rumah Tangga</span></a>	
				    		<?php endif ?>
				    		</td>
				    	</tr>
				    </table>
			    </div>
			    <?php if ($updated == 2): ?>
				    <div class="col-sm-4">
				    	<div class="alert alert-success" style="text-align: center;vertical-align: middle;"><span style="font-size: 77px" class="icon fa fa-info-circle"></span><br /><h4>Data Rumah Tangga sudah di masukkan ... !!</h4></div>
				    </div>
				<?php endif ?>
		    </div>
	    </div>

		<div class="row">	
			<div class="box-body">
				<div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-info" role="alert">
				      <b>DATA ANGGOTA KELUARGA</b>
				    </div>
				</div>

			    <div class="col-sm-12">
			    	<div class="table-responsive">
			    	<table width="100%" class="table table-bordered table-striped responsive" style="font-size: 0.9em">
						<thead>
							<tr>
								<th style="text-align:center">NO</th>
								<th style="text-align:center">NO KK</th>
								<th style="text-align:center">NIK</th>
								<th style="text-align:center">NO. ART</th>
								<th style="text-align:center">NO. URUT KK</th>
								<th style="text-align:center">NAMA LENGKAP</th>
								<th style="text-align:center">L/P</th>
								<th style="text-align:center">UMUR</th>
								<th style="text-align:center">STATUS KAWIN</th>
								<?php /* ?><th style="text-align:center">TGL LAHIR</th><?php */ ?>
								<th style="text-align:center">HUB. RUMAHTANGGA</th>
								<th style="text-align:center">HUB. KELUARGA</th>
								<?php /* ?><th style="text-align:center">AGAMA</th><?php */ ?>
								<?php /* ?><th style="text-align:center">PENDIDIKAN</th><?php */ ?>
								<?php /* ?><th style="text-align:center">PEKERJAAN</th><?php */ ?>
								<th style="text-align:center">KATEGORI</th>
								<?php /* ?><th style="text-align:center">STATUS</th><?php */ ?>
								<th style="text-align:center"><i class="fa fa-tags"></i></th>
							</tr>
						</thead>
						<tbody>
						<?php
						$no = 1;
						$id_rt = $nomor_urut_rumah_tangga;
						foreach ($data as $row) {
							extract((array) $row);
							?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo "$no_kk"; ?></td>
								<td><?php echo "$nik"; ?></td>
								<td><?php echo "$no_art"; ?></td>
								<td><?php echo "$b4_k4"; ?></td>
								<td><?php echo "$nama"; ?></td>
								<td><?php echo "$b4_k6"; ?></td>
								<td><?php echo "$b4_k7"; ?></td>
								<td><?php echo "$b4_k8"; ?></td>
								<?php /* ?><td><?php echo format_tanggal($tgl_lhr); ?></td><?php */ ?>
								<td><?php echo "$b4_k3"; ?></td>
								<td><?php echo "$b4_k5"; ?></td>
								<?php /* ?><td><?php echo "$agama_desc"; ?></td><?php */ ?>
								<?php /* ?><td><?php echo "$pendidikan"; ?></td><?php */ ?>
								<?php /* ?><td><?php echo "$pekerjaan_siak"; ?></td><?php */ ?>
								<td align="center">
				              	<?php if ($rid_individu): ?>					                
					                <span class="label label-success" data-original-title="Pendaftar Lama (Terdaftar di dalam PPFM/BDT.2015)" data-toggle="tooltip" data-placement="bottom">001
				              	<?php else: ?> 
					                <span class="label label-primary" data-original-title="Pendaftar Baru (Tidak Terdaftar di dalam PPFM/BDT.2015)" data-toggle="tooltip" data-placement="bottom">000
				              	<?php endif ?>
				              		</span>
					            </td>
					            <?php /* ?><td align="center">
				              	<?php if ($flag_status == 0): ?>
					                <span class="label label-success" data-original-title="Data Penduduk Aktif" data-toggle="tooltip" data-placement="bottom">1
					            <?php elseif ($flag_status == 1): ?>
					                <span class="label label-danger" data-original-title="Data Penduduk Meninggal" data-toggle="tooltip" data-placement="bottom">2
				              	<?php elseif ($flag_status == 2 || $flag_status == 8): ?>
					                <span class="label label-primary" data-original-title="Data Penduduk Pindah Domisili" data-toggle="tooltip" data-placement="bottom">3
					            <?php else: ?> 
					                <span class="label label-warning" data-original-title="Data Penduduk Bermasalah / Butuh Konfirmasi" data-toggle="tooltip" data-placement="bottom">4
				              	<?php endif ?>
				              		</span>
					            </td><?php */ ?>
								<td align="center">
								<?php  if ($this->cu->USER_LEVEL == 4): ?>
									<a href="<?php echo base_url();?>index.php/release/lihat_individu?nik=<?php echo $nik; ?>&amp;<?php echo get_params($this->input->get(), array('nik')); ?>" class="ajaxify fa-item tooltips" data-original-title="Lihat Data Individu" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-xs"><i class="fa fa-search"></i></span></a>
								<?php else: ?>
									<a href="<?php echo base_url();?>index.php/release/edit_individu?nik=<?php echo $nik; ?>&amp;<?php echo get_params($this->input->get(), array('nik')); ?>" class="ajaxify fa-item tooltips" data-original-title="Edit Data Individu" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-primary btn-xs">+ <i class="fa fa-pencil"></i></span></a>
								<?php endif ?>
								</td>
							</tr>
						<?php 
						}
						?>
		                </tbody>
					</table>
					</div>
				</div><!-- /.box-body -->
				<br/>
				<div class="col-md-2">
					<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/release/index?<?php echo get_params($this->input->get(), array('nik','no_kk')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
				</div>
			
			</div>
		</div>

	</div><!-- /.box-body -->


	<!-- /content -->	
	
</div><!-- /.box -->
