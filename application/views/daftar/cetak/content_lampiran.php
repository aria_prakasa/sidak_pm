<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body>
<div class="border_body">
  <div class="col-lg-12">
    <p class="nomargin" align="center" style="font-weight:bold">
      <span style="font-size:18px"><?php echo $header; ?></span></p>
    <p align="center" style="font-weight:bold; margin: 7"> 
      <span style="font-size:12"><?php echo $sub_header; ?></span><br /></p>    
  </div>

  <p class="nobotmargin"><strong>INFORMASI PENDAFTARAN </strong></p><br/>
  <?php
  $arr = array(
        "NIK" => gen_option_single_value_long('<b>'.$nik_pendaftar.'</b>', ''),
        "Nama Lengkap Sesuai KTP" => gen_option_single_value_long('<b>'.$nama_pendaftar.'</b>', ''),
        "Jenis Kelamin" => gen_option_value($opsi_b4_k6, $jenis_klmin_pendaftar, 1),
        "Nomor KK" => gen_option_single_value_long('<b>'.$no_kk, ''),
        "Hubungan dengan Kepala Keluarga" => gen_option_single_value_long(format_stat_hbkel($hubungan_keluarga), ''),
        "Hubungan dengan Kepala Rumah Tangga" => gen_option_single_value_long(format_stat_rtangga($hubungan_rumahtangga_induk), '')
        );
  ?>

  <table class="list withnopadding responsive" width="100%">
  <?php
  $i = $j = 1;
  foreach($arr as $key => $val)
  {
    $tmp_arr = array("key" => $key, "val" => $val, "i" => $j);
    
      echo $this->load->view("cetak/tpl_row_no", $tmp_arr, TRUE);
    
    $i++;
    $j++;
  }
  ?>
  </table>

  <p class="nobotmargin"><strong>INFORMASI RUMAH TANGGA </strong></p>
  <br/>
  <?php
  $options = array('' => '- Pilih Opsi -',
            '1' => '1. Bekerja',
            '2' => '2. Tidak Bekerja',
            );
  $arr = array(
        "NIK Kepala Rumah Tangga" => gen_option_single_value_long('<b>'.$nik_rumahtangga.'</b>', ''),
        "Nama Kepala Rumah Tangga" => gen_option_single_value_long('<b>'.$nama_kep_rumahtangga.'</b>', ''),
        "Jenis Kelamin Kepala Rumah Tangga" => gen_option_value($opsi_b4_k6, $jenis_klmin_rumahtangga, 1),
        "Bulan Lahir Kepala Rumah Tangga" => gen_option_single_value_long(str_pad($bulan_lhr_rumahtangga, 2, "0", STR_PAD_LEFT), ''),
        "Tahun Lahir Kepala Rumah Tangga" => gen_option_single_value_long($tahun_lhr_rumahtangga, ''),
        "Status Pekerjaan Kepala Rumah Tangga" => gen_option_value($options, $status_pkrjn_rumahtangga, 1),
        "Jumlah Anggota Rumah Tangga" => gen_option_single_value($b1_r9, 'Orang <br/><br/>'),
        "Provinsi" => gen_option_single_value_long($provinsi, ''),
        "Kabupaten / Kota" => gen_option_single_value_long($kabupaten, ''),
        "Kecamatan" => gen_option_single_value_long($kecamatan, ''),
        "Kelurahan / Desa" => gen_option_single_value_long($desa, ''),
        "Nama Jalan (RT/RW)" => gen_option_single_value_long($alamat.' RT/RW '.$no_rt.'/'.$no_rw, ''),
        "Status Kepemilikan Bangunan Tempat Tinggal" => gen_option_value($opsi_b3_r1a, $b3_r1a, 3),
        "Bahan Bangunan Utama Atap Rumah Terluas" => gen_option_value($opsi_b3_r5a, $b3_r5a, 5),
        "Bahan Bangunan Utama Dinding Rumah Terluas" => gen_option_value($opsi_b3_r4a, $b3_r4a, 4),
        "Bahan Bangunan Utama Lantai Rumah Terluas" => gen_option_value($opsi_b3_r3, $b3_r3, 5),
        "Penggunaan Fasilitas Tempat Buang Air Besar" => gen_option_value($opsi_b3_r11a, $b3_r11a, 2),
        "Apakah Rumah Tangga Memiliki Mobil?" => gen_option_value($opsi_binary, $b5_r1k, 1),
        "Apakah Rumah Tangga Memiliki AC?" => gen_option_value($opsi_binary, $b5_r1c, 1),
        "Apakah Rumah Tangga Memiliki Tabung Gas 5,5 Kg atau lebih?" => gen_option_value($opsi_binary, $b5_r1a, 1),
        "Pendidikan Tertinggi Anggota Rumah Tangga Yang Sudah Tidak Bersekolah?" => gen_option_value($opsi_b4_k18, $b4_k18, 3)
        );
  ?>
  <table class="list withnopadding responsive" width="100%">
  <?php
  $i = $j = 7;
  foreach($arr as $key => $val)
  {
    $tmp_arr = array("key" => $key, "val" => $val, "i" => $j);
    
      echo $this->load->view("cetak/tpl_row_no", $tmp_arr, TRUE);
    
    $i++;
    $j++;
  }
  ?>
  </table>

</div>
<br />
<br />
<br />
<br />


</body>

