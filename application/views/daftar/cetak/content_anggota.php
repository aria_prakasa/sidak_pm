<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body>
<div>
  <div class="col-lg-12">
    <p class="nomargin" align="center" style="font-weight:bold">
      <span style="font-size:18px"><?php echo $header; ?></span></p>
    <p align="center" style="font-weight:bold; margin: 7"> 
      <span style="font-size:12"><?php echo $sub_header; ?></span><br /></p>    
  </div>

  <p class="nobotmargin"><strong>INFORMASI ANGGOTA RUMAH TANGGA </strong></p><br/>
  <p ><strong>Status Pengajuan : <u><?php echo ($status_pengajuan == '000')?"Data Pengajuan BARU":"Perubahan Data LAMA"; ?></strong></u></p>  <br />
    
  <table class="table table-bordered border" cellspacing="0">
    <thead>
      <tr>
        <th style="text-align:center">NO</th>
        <th style="text-align:center">NIK</th>
        <th style="text-align:center">NAMA LENGKAP</th>
        <th style="text-align:center">L/P</th>
        <th style="text-align:center">TANGGAL LAHIR</th>
        <th style="text-align:center">UMUR</th>
        <th style="text-align:center">HUB. KELUARGA</th>
        <th style="text-align:center">NO. URUT<br> KELUARGA</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $no = 1+$page;
    foreach ($anggota as $row) {
      $row = keysToLower($row);
      extract((array) $row);
      ?>
    <tr>
      <td style="font-size: 12px"><?php echo $no++; ?></td>
      <td style="font-size: 12px" align="center"><?php echo $nik; ?></td>
      <td style="font-size: 12px"><?php echo "$nama_lgkp"; ?></td>
      <td style="font-size: 12px" align="center"><?php echo formatkelamin($jenis_klmin); ?></td>
      <td style="font-size: 12px"><?php echo format_tanggal($tgl_lhr); ?></td>
      <td style="font-size: 12px" align="center"><?php echo $umur; ?></td>
      <td style="font-size: 12px"><?php echo format_shdk($stat_hbkel); ?></td>
      <td style="font-size: 12px" align="center"><?php echo $b4_k4; ?></td>
    </tr>
    <?php
    }
    ?>
    
    </tbody>
  </table>

</div>

</body>

