<div class="row">	
	<div class="box-body">
		<div class="col-sm-12">
		    <div style="text-align: center;" class="alert alert-info" role="alert">
		      <b>DATA ANGGOTA KELUARGA</b>
		    </div>
	    </div>

	    <div class="col-sm-12">
	    	<div class="table-responsive">
	    	<table width="100%" class="table table-bordered table-striped" style="font-size: 0.9em">
				<thead>
					<tr>
						<th style="text-align:center">NO</th>
						<th style="text-align:center">NIK</th>
						<th style="text-align:center">NAMA LENGKAP</th>
						<th style="text-align:center">L/P</th>
						<th style="text-align:center">TANGGAL LAHIR</th>
						<th style="text-align:center">HUB. KELUARGA</th>
					</tr>
				</thead>
				<tbody>
				<?php /*var_dump($anggota);die;*/ ?>
				<?php
				
				$no = 1;
				// $id_rt = $nomor_urut_rumah_tangga;
				foreach ($anggota as $row) {
					extract((array) $row);
					?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td align="center"><?php echo $nik; ?></td>
						<td><?php echo "$nama_lgkp"; ?></td>
						<td align="center"><?php echo formatkelamin($jenis_klmin); ?></td>
						<td><?php echo format_tanggal($tgl_lhr); ?></td>
						<td><?php echo format_shdk($stat_hbkel); ?></td>				
					</tr>
				<?php 
				}
				?>
                </tbody>
			</table>
			</div>
		</div><!-- /.box-body -->
		<br/>
		<?php /* ?><div class="col-md-2">
			<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/entry/index?<?php echo get_params($this->input->get(), array('nik','no_kk')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
		</div><?php */ ?>
	
	</div>
</div>