<div class="box-body">	
	<div class="row">
		<div class="col-sm-5">
			<table width="50%" class="table responsive" style="font-size: 0.9em">
				<tr>
		    		<td width="50%"><b>No.KK Kepala Rumah Tangga</b></td>
		    		<td><?php echo form_input('no_kk_rumahtangga', $no_kk_rumahtangga, 'maxlength="16" class="form-control input-sm" disabled'); ?> </td>
		    	</tr>
		    	<tr>
		    		<td><b>NIK Kepala Rumah Tangga</b></td>
		    		<td><?php echo form_input('nik_rumahtangga', $nik_rumahtangga, 'maxlength="16" class="form-control input-sm" disabled'); ?></td>
		    	</tr>
		    	<tr>
		    		<td><b>Nama Kepala Rumah Tangga</b></td>
		    		<td><?php echo form_input('nama_kep_rumahtangga', $nama_kep_rumahtangga, 'maxlength="16" class="form-control input-sm" disabled'); ?></td>
		    	</tr>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="box-body">
		    <div style="text-align: center;" class="alert alert-success" role="alert">
		      <b>IV. KETERANGAN SOSIAL EKONOMI ANGGOTA RUMAH TANGGA</b>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>1.</b></td>
			    		<td width="50%"><b>No. Kartu Keluarga</b></td>
			    		<td colspan="2" width="40%"><?php echo form_input('no_kk', $no_kk, 'maxlength="16" class="form-control input-sm" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td><b>2.</b></td>
			    		<td><b>NIK</b></td>
			    		<td colspan="2"><?php echo form_input('nik', $nik, 'maxlength="16" class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>3.</b></td>
			    		<td><b>Nama Lengkap</b></td>
			    		<td colspan="2"><?php echo form_input('nama', $nama, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>4.</b></td>
			    		<td><b>Hubungan dengan kepala rumah tangga </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k3', $opsi_b4_k3, $b4_k3, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>No. Urut rumah tangga</b></td>
			    		<td width="15%"><?php echo form_input('no_art', $no_art, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td>&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td><b>5.</b></td>
			    		<td><b>Hubungan dengan kepala keluarga </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k5', $opsi_b4_k5, $b4_k5, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td></td>
			    		<td><b>No. Urut keluarga</b></td>
			    		<td width="15%"><?php echo form_input('b4_k4', $b4_k4, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td>&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td><b>6.</b></td>
			    		<td><b>Jenis kelamin </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k6', $opsi_b4_k6, $b4_k6, 'class="form-control input-sm" disabled'); ?>
						<?php echo form_hidden('b4_k6', $b4_k6); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>7.</b></td>
			    		<td><b>Umur (pada saat pendataan) </b></td>
			    		<td><?php echo form_input('b4_k7', $b4_k7, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?></td>
			    		<td>&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td><b>8.</b></td>
			    		<td><b>Status perkawinan </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k8', $opsi_b4_k8, $b4_k8, 'class="form-control input-sm" disabled'); ?>
						<?php echo form_hidden('b4_k8', $b4_k8); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>9.</b></td>
			    		<td><b>Kepemilikan buku nikah/akta cerai </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k9', $opsi_b4_k9, $b4_k9, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>10.</b></td>
			    		<td><b>Tercantum dalam Kartu keluarga (KK) di rumah tangga </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k10', $opsi_binary, $b4_k10, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>11.</b></td>
			    		<td><b>Kepemilikan kartu identitas </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k11', $opsi_b4_k11, $b4_k11, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
		    	</table>
		    </div>
		    <div class="col-sm-6">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<td width="5%"><b>12.</b></td>
			    		<td width="50%"><b>Status kehamilan (wanita 10-48 tahun) </b></td>
			    		<td colspan="2" width="40%"><?php echo form_dropdown('b4_k12', $opsi_b4_k12, $b4_k12, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>13.</b></td>
			    		<td><b>Jenis cacat </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k13', $opsi_b4_k13, $b4_k13, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>14.</b></td>
			    		<td><b>Penyakit kronis/menahun </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k14', $opsi_b4_k14, $b4_k14, 'class="form-control input-sm" disabled'); ?></td>
			    	</tr>
			    </table>
			    <table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<th colspan="5"><span>ISIAN KEBAWAH UNTUK ART 5 TAHUN KEATAS</span></th>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>15.</b></td>
			    		<td width="50%"><b>Partisipasi sekolah </b></td>
			    		<td colspan="2" width="40%"><?php echo form_dropdown('b4_k15', $opsi_b4_k15, $b4_k15, 'class="form-control input-sm ddl_disabled_toggle_pendidikan" data-ddl_target=".pendidikan" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<th colspan="5">ISI NO.16, 17, DAN 18 JIKA NO.15 = 1 atau 2</th>
			    	</tr>
			    	<tr>
			    		<td><b>16.</b></td>
			    		<td><b>Jenjang pendidikan tertinggi yang pernah/sedang diduduki saat pendataan </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k16', $opsi_b4_k16, $b4_k16, 'class="form-control input-sm pendidikan" disabled'); ?></td>
			    	</tr>
			    	<tr>
			    		<td><b>17.</b></td>
			    		<td><b>Kelas tertinggi yang pernah/sedang diduduki saat pendataan </b></td>
			    		<td width="15%"><?php echo form_input('b4_k17', $b4_k17, 'class="form-control input-sm pendidikan" style="text-transform:uppercase" disabled'); ?> </td>
			    		<td>&nbsp;</td>
			    	</tr>
			    	<tr>
			    		<td><b>18.</b></td>
			    		<td><b>Izasah tertinggi </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k18', $opsi_b4_k18, $b4_k18, 'class="form-control input-sm pendidikan" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td><b>19.</b></td>
			    		<td><b>Bekerja/membantu bekerja seminggu yang lalu </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k19a', $opsi_binary, $b4_k19a, 'class="form-control input-sm ddl_disabled_toggle_bekerja" data-ddl_target=".bekerja" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td><b>20.</b></td>
			    		<td><b>Lapangan usaha dari pekerjaan utama  </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k20', $opsi_b4_k20, $b4_k20, 'class="form-control input-sm bekerja" disabled'); ?> </td>
			    	</tr>
			    	<tr>
			    		<td><b>21.</b></td>
			    		<td><b>Status kedudukan dari pekerjaan utama </b></td>
			    		<td colspan="2"><?php echo form_dropdown('b4_k21', $opsi_b4_k21, $b4_k21, 'class="form-control input-sm bekerja" disabled'); ?> </td>
			    	</tr>
		    	</table>
		    </div>
	    </div>
    </div>
</div><!-- /.box-body -->
<?php /* ?>
<script type="text/javascript">
	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle_pendidikan');
		$(document).on('change', '.ddl_disabled_toggle_pendidikan', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1 || $(sel).val() == 2) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}
	});

	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle_bekerja');
		$(document).on('change', '.ddl_disabled_toggle_bekerja', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}
	});
</script>
<?php */ ?>