<?php 
$attributes = array('novalidate' => '', 'class' => 'form-validate form-horizontal form-label-left', 'id' => 'feedback_form', 'data-parsley-validate' => '');
echo form_open('mohon_individu/submit', $attributes);
?>

<?php echo form_hidden('get_params', get_params($this->input->get())); ?>
<?php echo form_hidden('no_kk', $no_kk); ?>

<?php /* ?><div class="nav-tabs-custom">	
	<ul class="nav nav-tabs">
		<li role="presentation" class="active"><a href="#rtangga" aria-controls="rtangga" role="tab" data-toggle="tab">Data Rumah Tangga</a></li>
		<li role="presentation"><a href="#individu" aria-controls="individu" role="tab" data-toggle="tab">Data Individu</a></li>
	</ul>
	<br />

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="rtangga">
			<div class="box box-info">
				
				<div class="box-header with-border">
					<h3 class="box-title">Data Rumah Tangga: </h3>
				</div>
				<div class="form-horizontal">
				<!-- content -->
					<?php $this->load->view('mohon/view/data_rumahtangga_edit'); ?>
				<!-- /content -->	
				</div>
			</div><!-- /.box -->	
		</div>

		<div role="tabpanel" class="tab-pane" id="individu">
			<div class="box box-info">
				
				<div class="box-header with-border">
					<h3 class="box-title">Data Individu: </h3>
				</div>
				<div class="form-horizontal">
				<!-- content -->
					<?php $this->load->view('mohon/view/data_individu_edit'); ?>
				<!-- /content -->	
				</div>
				
			</div><!-- /.box -->	
		</div>
	</div>
</div><?php */ ?>
<div class="box box-warning">
	<?php /* ?><div class="box-header with-border">
		<h3 class="box-title">Data Rumah Tangga : </h3>
	</div><?php */ ?>

	<!-- content -->
		<?php $this->load->view('mohon/view/data_individu_edit'); ?>
	<!-- /content -->	
	
</div><!-- /.box -->

<div class="row form-group">
	<div class="col-md-2">
		<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/mohon_individu/view?<?php echo get_params($this->input->get(), array('nik','id_rt')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		<button type="submit" name="proses" value="update" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
		<button type="button" class="btn btn-danger" name="reset_form" onclick="this.form.reset();"><i class="fa fa-times"></i> Kembalikan Awal</button>
		<!-- <input type="button" class="btn btn-danger" name="reset_form" value="Hapus" onclick="this.form.reset();"> -->
	</div>
</div>	

<?php echo form_close(); ?>

<script>
function clearForm(oForm) {
    
  var elements = oForm.elements; 
    
  oForm.reset();

  for(i=0; i<elements.length; i++) {
      
    field_type = elements[i].type.toLowerCase();
    
    switch(field_type) {
    
      case "text": 
      case "password": 
      case "textarea":
            case "hidden":  
        
        elements[i].value = ""; 
        break;
          
      case "radio":
      case "checkbox":
          if (elements[i].checked) {
            elements[i].checked = false; 
        }
        break;

      case "select-one":
      case "select-multi":
                  elements[i].selectedIndex = -1;
        break;

      default: 
        break;
    }
  }
}

</script>
			