<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><strong>Data Rumah Tangga :</strong></h3>
	</div>
	<div classs="row" style="margin: 15px">
		
		<table width="100%" style="font-size: 0.9em">
			<tr>
				<td width="25%"><b>No Kartu Keluarga Rumah Tangga</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$no_kk"; ?>&nbsp;&nbsp;&nbsp;
				<?php if ($status_kk == 1): ?>	
					<a href="<?php echo base_url();?>index.php/mohon_rumahtangga/edit?<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Edit Pengajuan Rumah Tangga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-xs"> Edit &nbsp;+<i class="fa fa-pencil"></i></span></a>
                <?php endif ?>
				</td>
			</tr>
			<tr>
				<td><b>Nama Kepala Rumah Tangga</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$nama_kep"; ?></td>
			</tr>
			<tr>
				<td><b>Alamat</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$alamat, No.RT $no_rt / No.RW. $no_rw"; ?></td>
			</tr>
			<tr>
				<td><b>Kelurahan</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$kelurahan"; ?></td>
			</tr>
			<tr>
				<td><b>Kecamatan</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$kecamatan"; ?></td>
			</tr>
			<tr>
				<td><b>Status Pengajuan</b></td>
				<td>:</td>
				<td>&emsp;<?php /*echo "$flag_bdt";*/ ?>
				<?php /*if ($proses == 1): ?>
					<span class="label label-default">Dalam Pengajuan</span>
				<?php elseif ($proses == 2): ?> 
					<span class="label label-warning">Dalam Proses</span>
				<?php elseif ($proses == 3): ?> 
					<span class="label label-success">Diterima</span>
				<?php elseif ($proses == 4): ?> 
					<span class="label label-danger">Ditolak</span>  
				<?php endif*/ ?>
				<?php if ($status_kk == 1): ?>
					<span class="label label-default">Dalam Pengajuan</span>
				<?php elseif ($status_kk == 2): ?>
					<span class="label label-warning">Sudah Diproses</span>
				<?php elseif ($status_kk == 3): ?>
					<span class="label label-success">Masuk Data Pusat</span>
				<?php else: ?>
					<span class="label label-primary">Tidak Masuk Data Pusat</span>
				<?php endif ?>
			</tr>
		</table>
		
	</div>
	
	<div classs="row" style="margin: 15px">
		<div class="box-body table-responsive no-padding">
			<table width="100%" class="table table-bordered table-striped" style="font-size: 0.9em">
				<thead>
					<tr>
						<th>NO</th>
						<th>NO_KK</th>
						<th>NIK</th>
						<th>NAMA LENGKAP</th>
						<th>L/P</th>
						<th>TGL LAHIR / USIA</th>
						<th>HUB. KELUARGA</th>
						<th>AGAMA</th>
						<th>PENDIDIKAN</th>
						<th>PEKERJAAN SIAK</th>
						<th>PEKERJAAN</th>
						<th>STATUS</th>
						<th style="text-align:center"><i class="fa fa-tags"></i></th>
						<th>CETAK</th>
					</tr>
				</thead>
				<tbody>
<?php
$no = 1;
$id_rt = $nomor_urut_rumah_tangga;
foreach ($data as $row) {
	extract((array) $row);
	?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo "$no_kk"; ?></td>
						<td><?php echo "$nik"; ?></td>
						<td><?php echo "$nama_lgkp"; ?></td>
						<td><?php echo "$kelamin"; ?></td>
						<td><?php echo format_tanggal($tgl_lhr)." / ".$usia." Tahun"; ?></td>
						<td><?php echo "$shdk"; ?></td>
						<td><?php echo "$agama_desc"; ?></td>
						<td><?php echo "$pendidikan"; ?></td>
						<td><?php echo "$pekerjaan_siak"; ?></td>
						<td><?php echo "$pekerjaan_miskin"; ?></td>
						<td>
							<?php if ($proses == 1): ?>
								<span class="label label-default">Dalam Pengajuan</span>
							<?php elseif ($proses == 2): ?> 
								<span class="label label-warning">Dalam Proses</span>
							<?php elseif ($proses == 3): ?> 
								<span class="label label-success">Diterima</span>
							<?php elseif ($proses == 4): ?> 
								<span class="label label-danger">Ditolak</span>  
							<?php endif ?>
						</td>						
						<td align="center">
							<a href="<?php echo base_url();?>index.php/mohon_individu/lihat?no_kk=<?php echo $row->no_kk; ?>&amp;nik=<?php echo $row->nik; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Lihat Individu Pengajuan" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-xs"> <i class="fa fa-search"></i></span></a>
							<?php if ($proses == 1): ?>
								<a href="<?php echo base_url();?>index.php/mohon_individu/edit?nik=<?php echo $row->nik; ?>&amp;id_rt=<?php echo $id_rt; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Edit Pengajuan" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-xs"> <i class="fa fa-pencil"></i></span></a>
								<?php /*if ($stat_hbkel == 1): ?>
									<?php if ($no_art == 1): ?>
										<a href="<?php echo base_url();?>index.php/mohon_rumahtangga/delete_rtangga?no_kk=<?php echo $row->no_kk; ?>&amp;<?php echo get_params($this->input->get(), array('no_kk')); ?>" class="ajaxify fa-item tooltips" data-original-title="Hapus Kepala Rumah Tangga" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Data adalah Kepala Keluarga, Pengajuan dalam satu KK akan terhapus. Yakin akan menghapus ?')"><span class="btn btn-default btn-xs"><i class="fa fa-times"></i></span></a> 
									<?php else: ?> 
										<a href="<?php echo base_url();?>index.php/mohon_rumahtangga/delete?no_kk=<?php echo $row->no_kk; ?>&amp;<?php echo get_params($this->input->get(), array('no_kk')); ?>" class="ajaxify fa-item tooltips" data-original-title="Hapus Kepala Keluarga" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Data adalah Kepala Keluarga, Pengajuan dalam satu KK akan terhapus. Yakin akan menghapus ?')"><span class="btn btn-default btn-xs"><i class="fa fa-times"></i></span></a>  
									<?php endif ?>									
								<?php else: ?>
									<a href="<?php echo base_url();?>index.php/mohon_individu/delete?nik=<?php echo $row->nik; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Hapus Anggota Keluarga" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Data Pengajuan akan terhapus. Yakin akan menghapus ?')"><span class="btn btn-default btn-xs"><i class="fa fa-times"></i></span></a>
								<?php endif*/ ?>
							
							<?php endif ?>
						</td>
						<td align="center">
							<?php echo anchor(base_url()."index.php/mohon_individu/edit?nik=".$nik."&amp;no_kk=".$no_kk."&amp;".get_params($this->input->get())."&print=1", '<i class="fa fa-print"></i>', 'class="btn btn-xs btn-success" target="_blank"'); ?>
			            </td>
					</tr>
<?php 
}
?>
                </tbody>
			</table>

		</div><!-- /.box-body -->
		<br/>
		<div class="row">
			<div class="col-md-2">
				<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/mohon_rumahtangga/index?<?php echo get_params($this->input->get(), array('no_kk')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
			</div>
		</div><!-- /.row -->
		<br/>
		<?php echo form_close(); ?>
  	</div><!-- /.row-->

</div><!-- /.box -->

