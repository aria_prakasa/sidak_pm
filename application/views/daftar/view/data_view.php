<div class="nav-tabs-custom">	
	<ul class="nav nav-tabs">
		<li role="presentation" class="active"><a href="#rtangga" aria-controls="rtangga" role="tab" data-toggle="tab">Data Rumah Tangga</a></li>
		<li role="presentation"><a href="#individu" aria-controls="individu" role="tab" data-toggle="tab">Data Individu</a></li>
	</ul>
	<br />

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="rtangga">
			<?php /* ?>
			<div class="box box-info">
				
				<div class="box-header with-border">
					<h3 class="box-title">Data Rumah Tangga: </h3>
				</div>
			<?php */ ?>
				<div class="form-horizontal">
				<!-- content -->
					<?php $this->load->view('mohon/view/data_rumahtangga_view'); ?>
				<!-- /content -->	
				</div>
			<?php /* ?>	
			</div><!-- /.box -->	
			<?php */ ?>	
		</div>

		<div role="tabpanel" class="tab-pane" id="individu">
			<?php /* ?>
			<div class="box box-info">
				
				<div class="box-header with-border">
					<h3 class="box-title">Data Individu: </h3>
				</div>
				<?php */ ?>
				<div class="form-horizontal">
				<!-- content -->
					<?php $this->load->view('mohon/view/data_individu_view'); ?>
				<!-- /content -->	
				</div>
				
			<?php /* ?>	
			</div><!-- /.box -->	
			<?php */ ?>		
		</div>
	</div>
</div>
<?php /* ?>
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Data Rumah Tangga : </h3>
	</div>

	<div class="form-horizontal">
	<!-- content -->
		<?php $this->load->view('data/view/data_rumahtangga'); ?>
	<!-- /content -->	
	</div>
	
</div><!-- /.box -->
<?php */ ?>

<div class="row form-group">
	<div class="col-md-2">
		<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/mohon_individu/view?<?php echo get_params($this->input->get(), array('nik')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
	</div>
</div>