<?php
// var_dump($data);die;
if (!$data):
  die('DATA TIDAK ADA');
else:
?>
<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th rowspan="2">No</th>
      <th colspan="7">Alamat Lengkap Sesuai Domisili</th>
      <th rowspan="2">NIK</th>
      <th rowspan="2">Nama Lengkap Sesuai KTP</th>
      <th rowspan="2">Nomor KK</th>
      <th rowspan="2">Nama Kepala Rumah Tangga</th>
      <th rowspan="2">Status Pekerjaan Kepala Ru Ta</th>
      <th rowspan="2">Jumlah Anggota Ru Ta</th>
      <th rowspan="2">Jenis Kelamin</th>
      <th rowspan="2">Bulan Lahir</th>
      <th rowspan="2">Tahun Lahir</th>
      <th rowspan="2">Hubungan Dengan Kepala Ru Ta</th>
      <th rowspan="2">Hubungan Dengan Kepala Keluarga</th>
      <th rowspan="2">No. Urut BDT(2015)</th>
      <th rowspan="2">Tanggal Daftar</th>
    </tr>
    <tr>
      <th>Provinsi</th> 
      <th>Kota</th>
      <th>Kecamatan</th>
      <th>Kelurahan</th>
      <th>Alamat</th>
      <th>NO RT</th>
      <th>NO RW</th>
    </tr>
  </thead>
  <tbody>
  <?php
$no = 1+$page;
foreach ($data as $row) {
$row = keysToLower($row);
extract((array) $row);
?>
    <tr>                      
      <td><?php echo $no++; ?></td>
      <td><?php echo "$provinsi"; ?></td>
      <td><?php echo "$kabupaten"; ?></td>
      <td><?php echo "$kecamatan"; ?></td>
      <td><?php echo "$desa"; ?></td>
      <td><?php echo "$alamat"; ?></td>
      <td><?php echo "$no_rt"; ?></td>
      <td><?php echo "$no_rw"; ?></td>
      <td><?php echo "'$nik_pendaftar"; ?></td>
      <td><?php echo "$nama_pendaftar"; ?></td>
      <td><?php echo "'$no_kk"; ?></td>
      <td><?php echo "$nama_kep_rumahtangga"; ?></td>
      <td><?php echo formatpekerjaan($status_pkrjn_rumahtangga); ?></td>
      <td><?php echo "$b1_r9"; ?></td>
      <td><?php echo formatkelamin($jenis_klmin_rumahtangga); ?></td>
      <td><?php echo "$bulan_lhr_rumahtangga"; ?></td>
      <td><?php echo "$tahun_lhr_rumahtangga"; ?></td>
      <td><?php echo format_stat_rtangga($hubungan_rumahtangga_induk); ?></td>
      <td><?php echo format_stat_hbkel($hubungan_keluarga); ?></td>
      <td><?php echo "$nomor_urut_rumah_tangga"; ?></td>
      <td><?php echo format_tanggal($tgl_entri); ?></td>
    </tr>
    <?php
    }
    ?>

  </tbody>
</table>

<?php endif;?>