  <?php /*if($error != "") {
	echo validation_errors();
	} else { */

if ($show==0):?>
  <div class="alert alert-danger" style="text-align: center">
    <h4><span style="font-size: 40px" class="icon fa fa-warning"></span> <br /> Peringatan!</h4>
    Maaf, data tidak ditemukan !!.
  </div>
<?php else: ?>

<div class="table-responsive">
<?php echo form_hidden('get_params', get_params($this->input->get())); ?>
<p style="font-size: 0.9em"><i>Menampilkan <b><?php echo count($data); ?></b> dari <b><?php echo $total_data; ?></b> data.</i></p>
	<table width="100%" class="table table-bordered table-striped table-desa" style="font-size: 0.9em">
		<thead>
          <tr>
            <th>NO</th> 
            <th>NO KK</th>
            <th>NIK</th>
            <th>NAMA LENGKAP</th>
            <th>ALAMAT</th>
            <th>KELURAHAN</th>
            <th>KECAMATAN</th>
            <th><i class="fa fa-tags"></i></th>
            <th>CETAK</i></th>
          </tr>
        </thead>
        <tbody>
      	<?php
		$no = 1+$page;
		foreach ($data as $row) {
			$row = keysToLower($row);
			extract((array) $row);
		?>
          <tr>                      
            <td><?php echo $no++; ?></td>
            <td><?php echo "$no_kk"; ?></td>
            <td><?php echo "$nik"; ?></td>
            <td><?php echo "$nama"; ?></td>            
            <td><?php echo "$alamat"; ?></td>
            <td><?php echo "$desa"; ?></td>
            <td><?php echo "$kecamatan"; ?></td>
            <td align="center">
              <a href="<?php echo base_url();?>index.php/mohon_individu/edit?nik=<?php echo $row->nik; ?>&amp;no_kk=<?php echo $row->no_kk; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Edit Pengajuan" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-block btn-default btn-xs"> <i class="fa fa-pencil"></i></span></a>
              <?php if ($shdk == 1): ?>
              <a href="<?php echo base_url();?>index.php/mohon_individu/delete?nik=<?php echo $nik; ?>&amp;shdk=<?php echo $shdk; ?>&amp;no_kk=<?php echo $no_kk; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Hapus Pengajuan" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Penduduk adalah Kepala Keluarga, seluruh pemohon dalam satu KK akan terhapus. Yakin akan menghapus ?')"><span class="btn btn-block btn-default btn-xs"><i class="fa fa-times"></i></span></a>
              <?php else: ?>
              <a href="<?php echo base_url();?>index.php/mohon_individu/delete?nik=<?php echo $nik; ?>&amp;no_kk=<?php echo $no_kk; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Hapus Pengajuan" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Yakin akan menghapus ?')"><span class="btn btn-block btn-default btn-xs"><i class="fa fa-times"></i></span></a>
              <?php endif ?>
            </td>
            <td align="center">
            <?php /* ?>
              <a href="<?php echo base_url();?>index.php/mohon_individu/edit?nik=<?php echo $row->nik; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Cetak Pengajuan" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-success btn-xs"> <i class="fa fa-print"></i></span></a>
            <?php */ ?>
              <?php echo anchor(base_url()."index.php/mohon_individu/edit?nik=".$nik."&amp;no_kk=".$no_kk."&amp;".get_params($this->input->get())."&print=1", '<i class="fa fa-print"></i>', 'class="btn btn-xs btn-success" target="_blank"'); ?>
            </td>
          </tr>
          <?php
          }
          ?>

        </tbody>
  </table>


  <?php echo $pagination; ?>
</div>
<?php endif ?>
<?php /*}*/ ?>