<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><strong>Data Keluarga :</strong></h3>
	</div>
	<div classs="row" style="margin: 15px">
		
		<table width="50%" style="font-size: 0.9em">
			<tr>
				<td><b>No Kartu Keluarga</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$no_kk"; ?></td>
			</tr>
			<tr>
				<td><b>Nama Kepala Keluarga</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$nama_kep"; ?></td>
			</tr>
			<tr>
				<td><b>Alamat</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$alamat, No.RT $no_rt / No.RW. $no_rw"; ?></td>
			</tr>
			<tr>
				<td><b>Kelurahan</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$kelurahan"; ?></td>
			</tr>
			<tr>
				<td><b>Kecamatan</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$kecamatan"; ?></td>
			</tr>
			<tr>
				<td><b>Status Data Kemiskinan Pusat</b></td>
				<td>:</td>
				<td>&emsp;<?php /*echo "$flag_bdt";*/ ?>
				<?php if ($status_kk == 1): ?>
					<span class="label label-default">Dalam Pengajuan</span>
				<?php elseif ($status_kk == 2): ?>
					<span class="label label-warning">Sudah Diproses</span>
				<?php elseif ($status_kk == 3): ?>
					<span class="label label-success">Masuk Data Pusat</span>
				<?php else: ?>
					<span class="label label-primary">Tidak Masuk Data Pusat</span>
				<?php endif ?>
				</td>
			</tr>
		</table>
		
	</div>
	
	<div classs="row" style="margin: 15px">
		<div class="box-body table-responsive no-padding">
			<table width="100%" class="table table-bordered table-striped" style="font-size: 0.9em">
				<thead>
					<tr>
						<th>NO</th>
						<th>NIK</th>
						<th>NAMA LENGKAP</th>
						<th>L/P</th>
						<th>TGL LAHIR</th>
						<th>HUB. KELUARGA</th>
						<th>AGAMA</th>
						<th>PENDIDIKAN</th>
						<th>PEKERJAAN DI SIAK</th>
						<th>BD.TERPADU</th>
						<th>BPJS</th>
						<th style="text-align:center"><i class="fa fa-tags"></i></th>
					</tr>
				</thead>
				<tbody>
<?php
$no = 1;
$id_rt = $nomor_urut_rumah_tangga;
foreach ($data as $row) {
	extract((array) $row);
	?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo "$nik"; ?></td>
						<td><?php echo "$nama_lgkp"; ?></td>
						<td><?php echo "$kelamin"; ?></td>
						<td><?php echo format_tanggal($tgl_lhr); ?></td>
						<td><?php echo "$shdk"; ?></td>
						<td><?php echo "$agama_desc"; ?></td>
						<td><?php echo "$pendidikan"; ?></td>
						<td><?php echo "$pekerjaan_siak"; ?></td>								
						<td align="center">
			              <?php if ($flag_bdt == 2): ?>
			                <span class="label label-success">Ya</span>
			              <?php else: ?>    
			                <span class="label label-primary">Tidak</span>
			              <?php endif ?>
			            </td> 
			            <td align="center">
			              <?php if ($flag_bpjs == 2): ?>
			                <span class="label label-success">Ya</span>
			              <?php else: ?>    
			                <span class="label label-primary">Tidak</span>
			              <?php endif ?>
			            </td> 
			            
					</tr>
<?php 
}
?>
                </tbody>
			</table>

			<br />
			<div class="row">
				<div class="col-md-2">
					<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/anomali_pindah/<?php echo "$uri"; ?>?<?php echo get_params($this->input->get(), array('no_kk')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
				</div>
			</div><!-- /.row -->
		</div><!-- /.box-body -->
		
		<?php echo form_close(); ?>
  	</div><!-- /.row-->

</div><!-- /.box -->

