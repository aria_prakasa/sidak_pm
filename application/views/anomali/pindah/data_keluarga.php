<div class="box box-primary">	
	<!-- content -->
	<div class="box-body">
		<div class="row">	
			<div class="box-body">
				<div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-info" role="alert">
				      <b>DATA KELUARGA</b>
				    </div>
				</div>

			    <div class="col-sm-8">
			    	<table width="100%" class="table responsive" class="nobotmargin" style="font-size: 0.9em">
				    	<tr>
				    		<td width="5%"><b>1.</b></td>
				    		<td width="40%"><b>Provinsi</b></td>
				    		<td width="5%">:</td>
				    		<td><?php echo form_input('', $propinsi, 'placeholder="Propinsi" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>2.</b></td>
				    		<td><b>Kabupaten/Kota</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $kabupaten, 'placeholder="Kabupaten" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>3.</b></td>
				    		<td><b>Kecamatan</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $kecamatan, 'placeholder="Kecamatan" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>4.</b></td>
				    		<td><b>Kelurahan/Desa</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $kelurahan, 'placeholder="Kelurahan/Desa" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>5.</b></td>
				    		<td><b>Alamat</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $alamat.' RT/RW '.$no_rt.'/'.$no_rw, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td><b>6.</b></td>
				    		<td><b>Nomor Kartu Keluarga</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $no_kk, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    </table>
			    </div>
		    </div>
	    </div>

		<div class="row">	
			<div class="box-body">
				<div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-info" role="alert">
				      <b>DATA ANGGOTA KELUARGA</b>
				    </div>
			    </div>

			    <div class="col-sm-12">
			    	<table width="100%" class="table table-bordered table-striped responsive" style="font-size: 0.9em">
						<thead>
							<tr>
								<th style="text-align:center">NO</th>
								<th style="text-align:center">NIK</th>
								<th style="text-align:center">NAMA LENGKAP</th>
								<th style="text-align:center">L/P</th>
								<th style="text-align:center">TANGGAL LAHIR</th>
								<th style="text-align:center">HUB. KELUARGA</th>
								<th style="text-align:center">STATUS</th>
								<th style="text-align:center">AGAMA</th>
								<th style="text-align:center">PENDIDIKAN</th>
								<th style="text-align:center">PEKERJAAN</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$no = 1;
						$id_rt = $nomor_urut_rumah_tangga;
						foreach ($data as $row) {
							extract((array) $row);
							?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td align="center"><?php echo "$nik"; ?></td>
								<td><?php echo "$nama_lgkp"; ?></td>
								<td align="center"><?php echo "$kelamin"; ?></td>
								<td><?php echo format_tanggal($tgl_lhr); ?></td>
								<td><?php echo "$shdk"; ?></td>
								<td><?php echo "$stat_kwn_desc"; ?></td>
								<td><?php echo "$agama_desc"; ?></td>
								<td><?php echo "$pendidikan"; ?></td>
								<td><?php echo "$pekerjaan_siak"; ?></td>
							</tr>
						<?php 
						}
						?>
		                </tbody>
					</table>
					
				</div><!-- /.box-body -->
				<br/>
				<div class="col-md-2">
					<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/<?php echo $this->uri->segment(1); ?>/<?php echo $uri; ?>?<?php echo get_params($this->input->get(), array('no_kk')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
				</div>
			
			</div>
		</div>

	</div><!-- /.box-body -->


	<!-- /content -->	
	
</div><!-- /.box -->