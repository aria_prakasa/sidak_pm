<?php if ($show==0): ?>
  <div class="alert alert-danger" style="text-align: center">
    <h4><span style="font-size: 40px" class="icon fa fa-warning"></span> <br /> Peringatan!</h4>
    Maaf, data tidak ditemukan !!.
  </div>
<?php else: ?>

<div class="table-responsive">
<?php echo form_hidden('get_params', get_params($this->input->get())); ?>
  <div class="row">
    <div class="col-sm-10">
      <p style="font-size: 0.9em"><i>Menampilkan <b><?php echo count($biodata_list); ?></b> dari <b><?php echo $total_data; ?></b> data.</i></p>
    </div>
    <div class="col-sm-2">
      <p class="text-right">
        <?php echo anchor(current_url()."?".get_params($this->input->get())."&export=1", '<i class="fa fa-download"></i> Download Excel', 'class="btn btn-sm btn-primary" target="_blank"'); ?>
        <?php /*echo '<button type="submit" name="export" value="1" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Download Excel</button>'*/ ?>
      </p>
    </div>
  </div>
	<table width="100%" class="table table-bordered table-striped table-desa" style="font-size: 0.9em">
		<thead>
          <tr>
            <th>NO</th> 
            <th>NO KK</th>
            <th>NIK</th>
            <th>NAMA LENGKAP</th>
            <th>ALAMAT</th>
            <th>KELURAHAN</th>
            <th>KECAMATAN</th>
            <th style="text-align: center"><i class="fa fa-tags"></i></th>
          </tr>
        </thead>
        <tbody>
      	<?php
		$no = 1+$page;
		foreach ($biodata_list as $row) {
			$row = keysToLower($row);
			extract((array) $row);
		?>
          <tr>                      
            <td><?php echo $no++; ?></td>
            <td><?php echo "$no_kk"; ?></td>
            <td><?php echo "$nik"; ?></td>
            <td><?php echo "$nama_lgkp"; ?></td>
            <td><?php echo $alamat." RT.".$no_rt."/RW.".$no_rw; ?></td>
            <td><?php echo "$kelurahan"; ?></td>
            <td><?php echo "$kecamatan"; ?></td>
            <td align="center">
              <a href="<?php echo base_url();?>index.php/anomali_pindah/view?no_kk=<?php echo $row->no_kk; ?>&amp;uri=<?php echo $this->uri->segment(2); ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Lihat Anggota Keluarga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-xs"> <i class="fa fa-search"></i></span></a>
            </td>
          </tr>
          <?php
          }
          ?>

        </tbody>
  </table>


  <?php echo $pagination; ?>
</div>
<?php endif ?>
