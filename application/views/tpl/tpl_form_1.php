<table class="table-content-wrapper full">
  <caption class="title text-center">I. PENGENALAN TEMPAT</caption>
  <tbody>
    <tr>
      <td class="half">
        <table class="full">
          <tbody>
            <tr>
              <td>1. Provinsi</td>
              <td>&nbsp;</td>
              <td class="text-right"></td>
            </tr>
            <tr>
              <td>2. Kabupaten/Kota</td>
              <td>&nbsp;</td>
              <td class="text-right"></td>
            </tr>
            <tr>
              <td>3. Kecamatan</td>
              <td>&nbsp;</td>
              <td class="text-right"></td>
            </tr>
            <tr>
              <td>4. Desa/Kelurahan/Nagari</td>
              <td>&nbsp;</td>
              <td class="text-right"></td>
            </tr>
            <tr>
              <td>5. Nama SLS</td>
              <td colspan="2">&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="half">
        <table class="full">
          <tbody>
            <tr>
              <td>6. Alamat</td>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">7. Nomor urut rumah tangga </td>
              <td class="text-right"></td>
            </tr>
            <tr>
              <td>8. Nama KRT</td>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">9. Jumlah ART</td>
              <td class="text-right"></td>
            </tr>
            <tr>
              <td colspan="2">10. Jumlah Keluarga</td>
              <td class="text-right"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
