<table class="table-content-wrapper full">
  <caption class="title text-center">III. KETERANGAN PERUMAHAN</caption>
  <tbody>
    <tr>
      <td>1a. Status penguasaan bangunan tempat tinggal yang ditempati</td>
      <td>
        <?php
        $arr = array(
          "milik sendiri",
          "kontrak/sewa",
          "bebas sewa",
          "dinas",
          "lainnya",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo $i.". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">a. ...</td>
    </tr>
    <tr>
      <td>&nbsp;b. Status lahan tempat tinggal yang ditempati</td>
      <td>
        <?php
        $arr = array(
          "milik sendiri",
          "milik orang lain",
          "tanah negara",
          "lainnya",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo $i.". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">b. ...</td>
    </tr>

    <tr>
      <td>2. Luas Lantai</td>
      <td>........... m<sup>2</sup></td>
      <td class="text-right">... ... ...</td>
    </tr>

    <tr>
      <td>3. Jenis lantai terluas</td>
      <td>
        <?php
        $arr = array(
          "marmer/granit",
          "keramik",
          "parket/vinil/permadani",
          "ubin/tegel/teraso",
          "kayu/papan kualitas tinggi",
          "semen/bata merah",
          "bambu",
          "kayu/papan kualitas rendah",
          "tanah",
          "lainnya",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo str_pad($i,2,"0").". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">... ...</td>
    </tr>

    <tr>
      <td>4a. Jenis dinding terluas</td>
      <td>
        <?php
        $arr = array(
          "tembok",
          "plesteran anyaman bambu/ kawat",
          "kayu",
          "anyaman bambu",
          "batang kayu",
          "bambu",
          "lainnya",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo $i.". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">a. ...</td>
    </tr>
    <tr>
      <td>&nbsp;b. Jika R.4a berkode 1, 2 atau 3, kondisi dinding</td>
      <td>
        <?php
        $arr = array(
          "bagus/kualitas tinggi",
          "jelek/kualitas rendah",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo $i.". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">b. ...</td>
    </tr>

    <tr>
      <td>5a. Jenis atap terluas</td>
      <td>
        <?php
        $arr = array(
          "beton/genteng beton",
          "genteng keramik",
          "genteng metal",
          "genteng tanah liat",
          "asbes",
          "seng",
          "sirap",
          "bambu",
          "jerami/ijuk/daun daunan/rumbia",
          "lainnya",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo str_pad($i,2,"0").". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">a. ......</td>
    </tr>
    <tr>
      <td>&nbsp;b. Jika R.5a berkode 1, 2, 3, 4, 5, 6 atau 7, kondisi atap</td>
      <td>
        <?php
        $arr = array(
          "bagus/kualitas tinggi",
          "jelek/kualitas rendah",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo $i.". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">b. ...</td>
    </tr>

    <tr>
      <td>6. Jumlah kamar tidur</td>
      <td>........... kamar</td>
      <td class="text-right">...</td>
    </tr>

    <tr>
      <td>7. Sumber air minum</td>
      <td>
        <?php
        $arr = array(
          "air kemasan bermerek",
          "air isi ulang",
          "leding meteran",
          "leding eceran",
          "sumur bor/pompa",
          "sumur terlindung",
          "sumur tak terlindung",
          "mata air terlindung",
          "mata air tak terlindung",
          "air sungai/danau/waduk",
          "air hujan",
          "lainnya",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo str_pad($i,2,"0").". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">... ...</td>
    </tr>

    <tr>
      <td>8. Cara memperoleh air minum</td>
      <td>
        <?php
        $arr = array(
          "membeli eceran",
          "langganan",
          "tidak membeli",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo $i.". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">...<td>
    </tr>

    <tr>
      <td>4a. Sumber penerangan utama</td>
      <td>
        <?php
        $arr = array(
          "listrik PLN",
          "listrik non PLN",
          "bukan listrik",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo $i.". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">a. ...</td>
    </tr>
    <tr>
      <td>&nbsp;b. Jika R.9a berkode 1, daya terpasang</td>
      <td>
        <?php
        $arr = array(
          "450 watt",
          "900 watt",
          );
        $div = ceil(count($arr)/3);
        $i = 0;
        ?>
        <table class="full">
          <tbody>
            <tr>
              <td>
              <?php
              foreach ($arr as $key => $value):
                $i++;
                echo $i.". ".ucfirst($value);
                if($i%$div == 0):
                  echo '
                  </td>
                  <td>
                  ';
                endif;
              endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td class="text-right">b. ...</td>
    </tr>
  </tbody>
</table>
