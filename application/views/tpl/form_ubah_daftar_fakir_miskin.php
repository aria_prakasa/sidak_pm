<style>
  .full {
    width: 100%;
  }
  .half {
    width: 50%;
  }
  .text-center {
    text-align: center;
  }
  .text-right {
    text-align: right;
  }
</style>
<table class="table-wrapper table-noline full">
  <tbody>
    <tr>
      <td class="half">
      <?php $this->load->view('tpl/tpl_form_2'); ?>
      </td>
      <td class="half">
      <?php $this->load->view('tpl/tpl_form_1'); ?>
      </td>
    </tr>
  </tbody>
</table>
<table class="table-wrapper table-noline full">
  <tbody>
    <tr>
      <td class="half">
      <?php $this->load->view('tpl/tpl_form_5'); ?>
      </td>
      <td class="half">
      <?php $this->load->view('tpl/tpl_form_3'); ?>
      </td>
    </tr>
  </tbody>
</table>
