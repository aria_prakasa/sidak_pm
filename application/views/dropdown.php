 <!-- page content -->
        <div id="kecamatan" style="width:250px;float:left;">
        Kecamatan : <br/>
        <?php
            echo form_dropdown("kecamatan_id",$option_kecamatan,"","id='kecamatan_id'");
        ?>
        </div>
        <div id="kelurahan">
        Kelurahan :<br/>
        <?php
            echo form_dropdown("kelurahan_id",array('Pilih Kelurahan'=>'Pilih Kecamatan Dahulu'),'','disabled');
        ?>
        </div>
        <script type="text/javascript">
            $("#kecamatan_id").change(function(){
                    var kecamatan_id = {kecamatan_id:$("#kecamatan_id").val()};
                    $.ajax({
                            type: "POST",
                            url : "<?php echo site_url('multi_dropdown/select_kelurahan')?>",
                            data: kecamatan_id,
                            success: function(msg){
                                $('#kelurahan').html(msg);
                            }
                        });
            });
        </script>