<div class="row">
  <div class="col-md-12">

  <?php
print_status();
if (validation_errors()) {
    echo print_error(validation_errors());
}

?>
  <?php echo form_open('', 'class="form-horizontal"') ?>

  <div class="col-md-6">

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Ubah Profil</h3>
        <?php /* ?>
<div class="box-tools pull-right">
<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
<i class="fa fa-minus"></i></button>
<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
<i class="fa fa-times"></i></button>
</div>
<?php //*/?>
      </div>

        <div class="box-body">

          <div class="form-group">
            <?php echo form_label('Nama', 'nama_lengkap', array('class' => 'col-sm-5 control-label')); ?>
            <div class="col-sm-7">
              <?php echo form_input('nama_lengkap', (set_value('nama_lengkap') ? set_value('nama_lengkap') : $nama_lengkap), 'id="nama_lengkap" class="form-control" placeholder="Nama Lengkap"'); ?>
            </div>
          </div>
          <div class="form-group">
            <?php echo form_label('N I P', 'nip', array('class' => 'col-sm-5 control-label')); ?>
            <div class="col-sm-7">
              <?php echo form_input('nip', (set_value('nip') ? set_value('nip') : $nip), 'id="nip" class="form-control" placeholder="Nomor Induk"'); ?>
              <div class="help-block">Masukkan N I P tanpa spasi 18 digit.</div>
            </div>
          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <?php echo anchor('', 'Cancel', 'class="btn btn-default"'); ?>
          <button type="submit" class="btn btn-primary pull-right">Ubah</button>
        </div>
        <!-- /.box-footer -->
    </div>

  </div>
  <div class="col-md-6">

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Ubah Password</h3>
        <?php /* ?>
<div class="box-tools pull-right">
<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
<i class="fa fa-minus"></i></button>
<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
<i class="fa fa-times"></i></button>
</div>
<?php //*/?>
      </div>

        <div class="box-body">

          <div class="form-group">
            <?php echo form_label('Password Sekarang', 'old_password', array('class' => 'col-sm-5 control-label')); ?>
            <div class="col-sm-7">
              <?php echo form_password('old_password', '', 'id="old_password" class="form-control" placeholder="Password sekarang"'); ?>
            </div>
          </div>
          <div class="form-group">
            <?php echo form_label('Password Baru', 'new_password', array('class' => 'col-sm-5 control-label')); ?>
            <div class="col-sm-7">
              <?php echo form_password('new_password', '', 'id="new_password" class="form-control" placeholder="Password baru"'); ?>
            </div>
          </div>
          <div class="form-group">
            <?php echo form_label('Konfirmasi Password Baru', 'conf_new_password', array('class' => 'col-sm-5 control-label')); ?>
            <div class="col-sm-7">
              <?php echo form_password('conf_new_password', '', 'id="conf_new_password" class="form-control" placeholder="Konfirmasi password baru"'); ?>
            </div>
          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <?php echo anchor('', 'Cancel', 'class="btn btn-default"'); ?>
          <button type="submit" class="btn btn-primary pull-right">Ubah</button>
        </div>
        <!-- /.box-footer -->
    </div>

  </div>

  <?php echo form_close(); ?>

  </div>
</div>
