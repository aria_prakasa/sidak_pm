<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>_assets/dist/img/user.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $this->cu->NAMA_LENGKAP; ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> <?php echo user_desc($this->cu->USER_LEVEL); ?></a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">NAVIGASI UTAMA</li>
            <li class="<?php echo $home_nav; ?> treeview">
              <a href="<?php echo base_url(); ?>index.php/home">
                <i class="glyphicon glyphicon-home"></i> <span> Halaman Utama</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $dash_nav; ?>"><a href="<?php echo base_url(); ?>index.php/home"><i class="fa fa-caret-right"></i> Dashboard </a></li>
                <li class="<?php echo $about_nav; ?>"><a href="<?php echo base_url(); ?>index.php/about"><i class="fa fa-caret-right"></i> Tentang Kami </a></li>
              </ul>
            </li>
            <?php if ($this->cu->USER_LEVEL >= 2 || $this->cu->USER_LEVEL == 0): ?>
            <li class="<?php echo $daftar_nav; ?> treeview">
              <a href="<?php echo base_url(); ?>index.php/home">
                <i class="glyphicon glyphicon-plus"></i> <span> Pendaftaran</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $entry_nav; ?>"><a href="<?php echo base_url(); ?>index.php/entry"><i class="fa fa-caret-right"></i> Entri Data </a></li>
                <li class="<?php echo $mohon_nav; ?>"><a href="<?php echo base_url(); ?>index.php/daftar"><i class="fa fa-caret-right"></i> Daftar Pengajuan </a></li>
              </ul>
            </li>
          <?php endif ?>
            <?php if ($this->cu->USER_LEVEL < 2 || $this->cu->USER_LEVEL == 4): ?>
            <li class="<?php echo $verify_nav; ?> treeview">
              <a href="#">
                <i class="fa fa-check-square-o"></i> <span> Verifikasi Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
              <?php if ($this->cu->USER_LEVEL < 2): ?>  
                <li class="<?php echo $prelist_nav; ?>"><a href="<?php echo base_url(); ?>index.php/prelist"><i class="fa fa-caret-right"></i> Daftar Prelist / Verifikasi</a>                  
                </li>
              <?php endif ?>
                <li class="<?php echo $release_nav; ?>"><a href="<?php echo base_url(); ?>index.php/release"><i class="fa fa-caret-right"></i> Daftar / Cetak Verifikasi</a>                  
                </li>
                <?php /* ?>
                <li class="<?php echo $list_nav; ?>">
                <a href="#"><i class="fa fa-caret-right"></i> Data Lulus Verifikasi<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $list_rumahtangga_nav; ?>"><a href="<?php echo base_url(); ?>index.php/list_rumahtangga"><i class="fa fa-caret-right"></i> Data Rumah Tangga</a>
                    </li>
                    <li class="<?php echo $list_keluarga_nav; ?>"><a href="<?php echo base_url(); ?>index.php/list_keluarga"><i class="fa fa-caret-right"></i> Data Keluarga</a>
                    </li>
                    <li class="<?php echo $list_individu_nav; ?>"><a href="<?php echo base_url(); ?>index.php/list_individu"><i class="fa fa-caret-right"></i> Data Individu</a>
                    </li>
                  </ul>
                </li>
                <li class="<?php echo $tolak_nav; ?>">
                <a href="#"><i class="fa fa-caret-right"></i> Data Tidak Lulus<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $tolak_rumahtangga_nav; ?>"><a href="<?php echo base_url(); ?>index.php/tolak_rumahtangga"><i class="fa fa-caret-right"></i> Data Rumah Tangga</a>
                    </li>
                    <li class="<?php echo $tolak_keluarga_nav; ?>"><a href="<?php echo base_url(); ?>index.php/tolak_keluarga"><i class="fa fa-caret-right"></i> Data Keluarga</a>
                    </li>
                    <li class="<?php echo $tolak_individu_nav; ?>"><a href="<?php echo base_url(); ?>index.php/tolak_individu"><i class="fa fa-caret-right"></i> Data Individu</a>
                    </li>
                  </ul>
                </li>
                <?php */ ?>
                <?php /* ?>
                <li class="<?php echo $proses_nav; ?>"><a href="<?php echo base_url(); ?>index.php/proses"><i class="fa fa-caret-right"></i> Proses Pendaftaran</a></li>
                <li class="<?php echo $rubah_nav; ?>">
                  <a href="#"><i class="fa fa-caret-right"></i> Perubahan Data (lama)<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $data_nav; ?>"><a href="<?php echo base_url(); ?>index.php/rubah_data"><i class="fa fa-caret-right"></i>  Status (Elemen Data)</a>
                    </li>
                    <li class="<?php echo $dalam_nav; ?>"><a href="<?php echo base_url(); ?>index.php/rubah_pindah/dalam"><i class="fa fa-caret-right"></i> Pindah Alamat</a>
                    </li>
                  </ul>
                </li>
                <li class="<?php echo $edit_nav; ?>">
                <a href="#"><i class="fa fa-caret-right"></i> Verifikasi Pendaftaran<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $edit_baru_nav; ?>"><a href="<?php echo base_url(); ?>index.php/edit_rumahtangga"><i class="fa fa-caret-right"></i> Pengajuan Baru</a>
                    </li>
                    <li class="<?php echo $edit_rubah_nav; ?>"><a href="<?php echo base_url(); ?>index.php/edit_rumahtangga_rubah"><i class="fa fa-caret-right"></i> Pengajuan Perubahan</a>
                    </li>
                  </ul>
                </li>
                <li class="<?php echo $penetapan_nav; ?>">
                  <a href="#"><i class="fa fa-caret-right"></i> Penetapan <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $pengajuan_baru_nav; ?>"><a href="<?php echo base_url(); ?>index.php/penetapan"><i class="fa fa-caret-right"></i> Pengajuan Baru</a>
                    </li>
                    <li class="<?php echo $pengajuan_rubah_nav; ?>"><a href="<?php echo base_url(); ?>index.php/penetapan_rubah"><i class="fa fa-caret-right"></i> Pengajuan Perubahan</a>
                    </li>
                  </ul>
                </li>
                
                <li class="<?php echo $hasil_nav; ?>">
                  <a href="#"><i class="fa fa-caret-right"></i> Data Hasil Verifikasi<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $terima_nav; ?>"><a href="<?php echo base_url(); ?>index.php/hasil/terima"><i class="fa fa-caret-right"></i> Diterima</a></li>
                    <li class="<?php echo $tolak_nav; ?>"><a href="<?php echo base_url(); ?>index.php/hasil/tolak"><i class="fa fa-caret-right"></i> Ditolak</a></li>
                  </ul>
                </li>
                <?php */ ?>
              </ul>
            </li>
            <?php endif ?>
            <li class="<?php echo $kemiskinan_nav; ?> treeview">
              <a href="#">
                <i class="fa fa-book"></i><span> Data Induk Kemiskinan</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $induk_rt_nav; ?>">
                  <a href="<?php echo base_url(); ?>index.php/induk"><i class="fa fa-caret-right"></i> Data Rumah Tangga <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $induk_sink_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk_rt"><i class="fa fa-caret-right"></i>Sinkronisasi PBDT.2015</a>
                    </li>
                    <li class="<?php echo $induk_prelist_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk_rt/prelist"><i class="fa fa-caret-right"></i>Prelist</a>
                    </li>
                    <li class="<?php echo $induk_ver_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk_rt/verifikasi"><i class="fa fa-caret-right"></i>Lolos Verifikasi</a>
                    </li>
                  </ul>
                </li>
                <li class="<?php echo $induk_nav; ?>">
                  <a href="<?php echo base_url(); ?>index.php/induk"><i class="fa fa-caret-right"></i> Data Individu <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $induk_sink_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk"><i class="fa fa-caret-right"></i>Sinkronisasi PBDT.2015</a>
                    </li>
                    <li class="<?php echo $induk_ver_nav; ?>"><a href="<?php echo base_url(); ?>index.php/induk/verifikasi"><i class="fa fa-caret-right"></i>Lolos Verifikasi</a>
                    </li>
                  </ul>
                </li>
                <li class="<?php echo $pindah_nav; ?>">
                  <a href="#"><i class="fa fa-caret-right"></i> Data Sinkronisasi Pindah <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li class="<?php echo $dalam_nav; ?>"><a href="<?php echo base_url(); ?>index.php/anomali_pindah/dalam"><i class="fa fa-caret-right"></i> Alamat Baru / Pisah KK</a>
                    </li>
                    <li class="<?php echo $keluarga_nav; ?>"><a href="<?php echo base_url(); ?>index.php/anomali_pindah/keluarga"><i class="fa fa-caret-right"></i> Pindah Keluarga Lain</a>
                    </li>
                    <li class="<?php echo $luar_nav; ?>"><a href="<?php echo base_url(); ?>index.php/anomali_pindah/luar"><i class="fa fa-caret-right"></i> Pindah Keluar Kota</a>
                    </li>
                  </ul>
                </li>
                <li class="<?php echo $mati_nav; ?>"><a href="<?php echo base_url(); ?>index.php/anomali_mati"><i class="fa fa-caret-right"></i> Data Sinkronisasi Mati</a>
                </li>
              </ul>
            </li>
            <?php if ($this->cu->USER_LEVEL < 2): ?>  
            <li class="<?php echo $stat_nav; ?> treeview">
              <a href="<?php echo base_url(); ?>index.php/statistik">
                <i class="glyphicon glyphicon-stats"></i><span> Statistik</span>
              </a>            
            </li>
            <li class="<?php echo $sink_nav; ?> treeview">
              <a href="#">
                <i class="glyphicon glyphicon-refresh"></i><span> Sinkronisasi Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php echo $sink_invalid_nav; ?>"><a href="<?php echo base_url(); ?>index.php/sink_invalid"><i class="fa fa-caret-right"></i> Update NIK Invalid</a></li>
                <li class="<?php echo $sink_kosong_nav; ?>"><a href="<?php echo base_url(); ?>index.php/sink_kosong"><i class="fa fa-caret-right"></i> Update NIK Kosong</a></li>
                <li class="<?php echo $sink_data_nav; ?>"><a href="<?php echo base_url(); ?>index.php/sink_data"><i class="fa fa-caret-right"></i> Update Data Pusat(by Name)</a></li>
              </ul>
            </li>
            <?php endif ?>
            
            <?php if ($this->cu->USER_LEVEL == 0 || $this->cu->USER_LEVEL == 4): ?>
            <li class="<?php echo $konsolidasi_nav; ?> treeview">
              <a href="<?php echo base_url(); ?>index.php/konsolidasi">
                <i class="fa fa-link"></i><span> Konsolidasi MPM</span>
              </a>            
            </li>
            <?php endif ?>
          </ul>
      </section>
        <!-- /.sidebar -->
</aside>