<script>
/*function tampilKabupaten()
 {
    $('#kabupaten_id').removeAttr('disabled');
    $('#kecamatan_id').attr({'disabled': 'disabled'});
    $('#kelurahan_id').attr({'disabled': 'disabled'});
     // $('#kecamatan_id :select').val("");
     kdprop = $("#provinsi_id").val();
     $.ajax({
         url:"<?php echo base_url(); ?>index.php/search/pilih_kabupaten/"+kdprop+"",
         success: function(response){
            $("#kabupaten_id").html(response);
         },
         dataType:"html"
     });
     return false;
 }*/

 $(function() {
 	tampilKelurahan();
 });

 /*function tampilKecamatan()
 {
    // $('#kecamatan_id').removeAttr('disabled');
    // $('#kecamatan_id').attr({'disabled': 'disabled'});
    $('#kelurahan_id').attr({'disabled': 'disabled'});
     kdkab = $("#kabupaten_id").val();
     $.ajax({
         url:"<?php echo base_url(); ?>index.php/search/pilih_kecamatan/"+kdkab+"",
         success: function(response){
            $("#kecamatan_id").html(response);
         },
         dataType:"html"
     });
     return false;
 }*/

 function tampilKelurahan()
 {
    if($('#kelurahan_id').length <= 0) return;
    $('#kelurahan_id').removeAttr('disabled');
    kdkec = $("#kecamatan_id").val();
    kdkel = $("#kelurahan_id_wrap").data('id');
    $.ajax({
        method: "POST",
        data: {'kelurahan_id': kdkel},
        url:"<?php echo base_url(); ?>index.php/search/pilih_kelurahan/"+kdkec+"",
        success: function(response){
            $("#kelurahan_id").html(response);
        },
        dataType:"html"
    });
    return false;
 }

</script>

<script type="text/javascript" src="<?php echo base_url(); ?>_assets/dist/js/jquery.mask.min.js"></script>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <a href="#">Badan Perencanaan Dan Pembangunan Daerah</a></br>
        Pemerintah Kota Pasuruan</br>
    <strong>Copyright &copy; 2016</strong> | All rights reserved.
</footer>


    </div><!-- ./wrapper -->

<script>
    // Wait for window load
    $(window).load(function() {
      // Animate loader off screen
      $(".se-pre-con").fadeOut("slow");
    });
    var check_submit_pullout = function(t) {
      if( ! $('form.pullout').length) return;
      $('.pullout [type=submit]').prop('disabled', true);
      // console.log($('.pullout [required]').length);
      var count = 0;
      $('.pullout [required]').each(function(){
        if($(this).val().trim() !== "") count = count + 1;
      });
      // console.log(count);
      if(count == $('.pullout [required]').length)
      {
        t.parents('form.pullout').find('[type=submit]').prop('disabled', false);
      }
    };
    /*$(document).ready(function() {

      check_submit_pullout();
      $(document).on('keyup blur change', '.pullout input, .pullout select, .pullout textarea', function(){
        check_submit_pullout($(this));
      });
      $(document).on('click', '.pullout .action a', function(){
        check_submit_pullout();
      });

      $(document).bind("ajaxSend", function() {
        $(".se-pre-con").show();
      }).bind("ajaxComplete", function(){
        $(".se-pre-con").fadeOut("slow");
      }).bind("ajaxStop", function() {
        $(".se-pre-con").fadeOut("slow");
      }).bind("ajaxError", function() {
        $(".se-pre-con").fadeOut("slow");
      });

      // Datemask mm/yyyy
      // $(".input-mask").inputmask("mm/yyyy", {"placeholder": "mm/yyyy"});

      $('[required]').closest('.form-group').find('label .text-danger').remove();
      $('[required]').closest('.form-group').find('label').append('<span class="text-danger">*</span>');

    });*/
  </script>
  </body>
</html>
