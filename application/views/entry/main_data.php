<div class="callout callout-danger">
	<h4><u>	PERHATIAN !!</u></h4>
	<table width="100%" class="responsive" class="nobotmargin" >	
		<tr>	
			<td colspan="2" align="top">
			Ikuti instruksi pengajuan data berikut ini : 					
			</td>
		</tr>
		<tr>	
			<td valign="top">1.&nbsp;</td>	
			<td valign="top">Lihat pada bagian <b>Data Keluarga</b>, pastikan data yang tampil sama dengan dokumen KK pengajuan pendaftar. </td>
		</tr>
		<tr>	
			<td valign="top">2.&nbsp;</td>	
			<td valign="top">Jika benar lanjutkan pada Bagian <b>Data Anggota Keluarga</b>. Pastikan jumlah anggota keluarga sama dengan jumlah di dokumen KK pendaftar.	</td>
		</tr>
		<tr>	
			<td valign="top">3.&nbsp;</td>	
			<td valign="top">Jika tidak sama, arahkan penduduk untuk melakukan pembaruan Dokumen Kependudukan di Dinas Kependudukan dan Pencatatan Sipil Kota Pasuruan. </td>
		</tr>
		<tr>	
			<td valign="top">4.&nbsp;</td>	
			<td valign="top">Jika dipastikan sama, maka lanjutkan proses pendaftaran penduduk miskin dengan cara klik tombol daftar di sebelah kiri kolom <b>NIK</b> pada baris penduduk yang melakukan pengajuan pendaftaran. </td>
		</tr>
		<tr>	
			<td valign="top">5.&nbsp;</td>	
			<td valign="top">Untuk <b>Panduan Lengkap</b> pengoperasian pengajuan data MPM bagi kelurahan, silahkan unduh disini >> &nbsp;&nbsp;
			<?php echo anchor(current_url()."?".get_params($this->input->get())."&download=1", '<i>Panduan Operator Kelurahan.pdf</i>', 'target="_blank"'); ?>
			<?php /* ?><a href="download/file">Panduan Operator Keluarahn.pdf</a><?php */ ?>
			</td>
		</tr>
	</table> 	  
</div>

<div class="box box-primary">	
	<!-- content -->
	<div class="box-body">
		<div class="row">	
			<div class="box-body">
				<div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-info" role="alert">
				      <b>DATA KELUARGA</b>
				    </div>
				</div>

			    <div class="col-sm-8">
			    	<table width="100%" class="table responsive" class="nobotmargin" style="font-size: 0.9em">
				    	<tr>
				    		<td width="5%"><b>1.</b></td>
				    		<td width="40%"><b>Provinsi</b></td>
				    		<td width="5%">:</td>
				    		<td><?php echo form_input('', $propinsi, 'placeholder="Propinsi" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>2.</b></td>
				    		<td><b>Kabupaten/Kota</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $kabupaten, 'placeholder="Kabupaten" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>3.</b></td>
				    		<td><b>Kecamatan</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $kecamatan, 'placeholder="Kecamatan" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>4.</b></td>
				    		<td><b>Kelurahan/Desa</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $kelurahan, 'placeholder="Kelurahan/Desa" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
				    	</tr>
				    	<tr>
				    		<td><b>5.</b></td>
				    		<td><b>Alamat</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $alamat.' RT/RW '.$no_rt.'/'.$no_rw, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td><b>6.</b></td>
				    		<td><b>Nomor Kartu Keluarga</b></td>
				    		<td>:</td>
				    		<td><?php echo form_input('', $no_kk, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				    		</td>
				    	</tr>
				    	<?php /* ?><tr>
				    		<td colspan="2">&nbsp;</td>
				    		<td>&nbsp;</td>
				    		<td>
				    			<a href="<?php echo base_url();?>index.php/entry/daftar_rtangga?no_kk=<?php echo $row->no_kk; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Isikan Variabel Rumah Tangga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-info btn-sm">Isikan Data Kemiskinan + <i class="fa fa-clipboard"></i></span></a>
				    		</td>
				    	</tr><?php */ ?>
				    </table>
			    </div>
			    <?php if ($pengajuan_kk == 2): ?>
				    <div class="col-sm-4">
				    	<div class="alert alert-success" style="text-align: center;vertical-align: middle;"><span style="font-size: 77px" class="icon fa fa-check"></span><br /><h4>Keluarga berhasil didaftarkan ... !!</h4></div>
				    </div>
				<?php endif ?>
		    </div>
	    </div>

		<div class="row">	
			<div class="box-body">
				<div class="col-sm-12">
				    <div style="text-align: center;" class="alert alert-info" role="alert">
				      <b>DATA ANGGOTA KELUARGA</b>
				    </div>
			    </div>

			    <div class="col-sm-12">
			    	<div class="table-responsive">
			    	<table width="100%" class="table table-bordered table-striped" style="font-size: 0.9em">
						<thead>
							<tr>
								<th style="text-align:center">NO</th>
								<th style="text-align:center"><i class="fa fa-tags"></i></th>
								<th style="text-align:center">NIK</th>
								<th style="text-align:center">NAMA LENGKAP</th>
								<th style="text-align:center">L/P</th>
								<th style="text-align:center">TANGGAL LAHIR</th>
								<th style="text-align:center">HUB. KELUARGA</th>
								<th style="text-align:center">KATEGORI</th>
								<th style="text-align:center">STATUS</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$no = 1;
						$id_rt = $nomor_urut_rumah_tangga;
						foreach ($data as $row) {
							extract((array) $row);
							?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td align="center">
								<?php  if ($this->cu->USER_LEVEL == 4) {
						            $attrib = 'hidden';
						        } ?>
									<?php if ($pengajuan != 2): ?>
										<a href="<?php echo base_url();?>index.php/entry/daftar?nik=<?php echo $row->nik; ?>&amp;<?php echo get_params($this->input->get(), array('nik')); ?>" class="ajaxify fa-item tooltips" data-original-title="Ajukan sebagai pendaftar" data-toggle="tooltip" data-placement="bottom" <?php echo $attrib; ?>><span class="btn btn-success btn-xs">+ <i class="fa fa-clipboard"></i></span></a>
									<?php /*else: ?> 
										<a href="<?php echo base_url();?>index.php/prelist/edit?nik=<?php echo $row->nik; ?>&amp;id_rt=<?php echo $id_rt; ?>&amp;<?php echo get_params($this->input->get(), array('nik')); ?>" class="ajaxify fa-item tooltips" data-original-title="Edit Pengajuan" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-xs"> <i class="fa fa-pencil"></i></span></a>
									<?php */ ?>
									<?php endif ?>
								</td>
								<td align="center"><?php echo "$nik"; ?></td>
								<td><?php echo "$nama_lgkp"; ?></td>
								<td align="center"><?php echo "$kelamin"; ?></td>
								<td><?php echo format_tanggal($tgl_lhr); ?></td>
								<td><?php echo "$shdk"; ?></td>
								<td align="center">
					              <?php if ($kategori == 3): ?>
					                <i class="fa fa-info-circle" data-original-title="Tidak masuk dalam Data PPFM/BDT.2015" data-toggle="tooltip" data-placement="bottom" style="color:blue;font-size: 17px"></i>
					              <?php else: ?> 
					                <i class="fa fa-info-circle" data-original-title="Masuk dalam Data PPFM/BDT.2015" data-toggle="tooltip" data-placement="bottom" style="color:green;font-size: 17px"></i>
					              <?php endif ?>
					            </td>
								<td align="center">
									<?php if ($pengajuan == 2): ?>
										<span class="label label-success" data-original-title="Keluarga sudah diajukan" data-toggle="tooltip" data-placement="bottom">
									<?php else: ?> 
										<span class="label label-default" data-original-title="Belum melakukan pengajuan" data-toggle="tooltip" data-placement="bottom">  
									<?php endif ?>
									<?php echo $pengajuan; ?></span>
								</td>						
							</tr>
						<?php 
						}
						?>
		                </tbody>
					</table>
					</div>
				</div><!-- /.box-body -->
				<br/>
				<div class="col-md-2">
					<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/entry/index?<?php echo get_params($this->input->get(), array('nik','no_kk')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
				</div>
			
			</div>
		</div>

	</div><!-- /.box-body -->


	<!-- /content -->	
	
</div><!-- /.box -->
