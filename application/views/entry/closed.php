<div class="box box-danger">
  <div class="box-header with-border">
    <h3 class="box-title"><strong>INFORMASI</strong></h3>
  </div>
  
  <div class="text-center" align="center">
    <br>
    <h2><img src="<?=base_url('_assets/img/warning.png')?>" alt="" height="70"><br /> Pemberitahuan !</h2>
      <p>Untuk pendaftaran MPM Tahap II sudah ditutup, <br />
      harap daftarkan kembali pada tahap selanjutnya</p><br /><br /><br />
  </div>


</div><!-- /.box -->

