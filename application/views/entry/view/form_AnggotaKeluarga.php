<?php 
$attributes = array('novalidate' => '', 'class' => 'form-validate form-horizontal form-label-left pullout', 'id' => 'feedback_form', 'data-parsley-validate' => '');
echo form_open('entry/submit', $attributes);
?>

<?php echo form_hidden('get_params', get_params($this->input->get())); ?>
<?php echo form_hidden('wilayah_id', '35750'.$no_kec); ?>
<?php echo form_hidden('no_kk', $no_kk); ?>
<?php echo form_hidden('no_prop', $no_prop); ?>
<?php echo form_hidden('no_kab', $no_kab); ?>
<?php echo form_hidden('no_kec', $no_kec); ?>
<?php echo form_hidden('no_kel', $no_kel); ?>

<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li role="presentation" class="active"><a href="#rtangga" aria-controls="rtangga" role="tab" data-toggle="tab">Data Rumah Tangga</a></li>
		<li role="presentation"><a href="#individu" aria-controls="individu" role="tab" data-toggle="tab">Data Individu</a></li>
	</ul>
	<br />

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="rtangga">
			<?php /* ?>
			<div class="box box-info">
				
				<div class="box-header with-border">
					<h3 class="box-title">Data Rumah Tangga: </h3>
				</div>
			<?php */ ?>
				<div class="form-horizontal">
				<!-- content -->
					<?php $this->load->view('entry/view/data_rumahtangga'); ?>
				<!-- /content -->	
				</div>
				
			<?php /* ?>	
			</div><!-- /.box -->	
			<?php */ ?>			
		</div>

		<div role="tabpanel" class="tab-pane" id="individu">
			<?php /* ?>
			<div class="box box-info">
				
				<div class="box-header with-border">
					<h3 class="box-title">Data Individu: </h3>
				</div>
			<?php */ ?>
				<div class="form-horizontal">
				<!-- content -->
					<?php $this->load->view('entry/view/data_individu'); ?>
				<!-- /content -->	
				</div>
				
			<?php /* ?>	
			</div><!-- /.box -->	
			<?php */ ?>	
		</div>
	</div>
</div>	
<?php /* ?>
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Data Rumah Tangga : </h3>
	</div>

	<div class="form-horizontal">
	<!-- content -->
		<?php $this->load->view('data/view/data_rumahtangga'); ?>
	<!-- /content -->	
	</div>
	
</div><!-- /.box -->
<?php */ ?>

<div class="row form-group">
	<div class="col-md-2">
		<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/entry/lihat?<?php echo get_params($this->input->get(), array('nik','id_rt')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		<button type="submit" name="proses" value="anggota" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
		<button type="button" class="btn btn-danger" name="reset_form" onclick="this.form.reset();"><i class="fa fa-times"></i> Kembalikan Awal</button>
		<!-- <input type="button" class="btn btn-danger" name="reset_form" value="Hapus" onclick="this.form.reset();"> -->
	</div>
</div>

<?php echo form_close(); ?>

<script>
function clearForm(oForm) {
    
  var elements = oForm.elements; 
    
  oForm.reset();

  for(i=0; i<elements.length; i++) {
      
    field_type = elements[i].type.toLowerCase();
    
    switch(field_type) {
    
      case "text": 
      case "password": 
      case "textarea":
            case "hidden":  
        
        elements[i].value = ""; 
        break;
          
      case "radio":
      case "checkbox":
          if (elements[i].checked) {
            elements[i].checked = false; 
        }
        break;

      case "select-one":
      case "select-multi":
                  elements[i].selectedIndex = -1;
        break;

      default: 
        break;
    }
  }
}

</script>

<script>
/*$(function() {
  $(document).on('click', '.submit_bdt', function(e) {
        // alert('submitted');
        var urlx = $(this).data('url');
        var target = $(this).data('target'); // target to nik
        var target_selector = $('input[name='+target+']');
        var id = target_selector.val();
        var suffix = target_selector.data('suffix');
        $.post(urlx,
        {
            id: id,
        },
        function(data, status){
            // alert("Data: " + data + "\nStatus: " + status);
            if(data === 'error') {
                alert('Nomor tidak ditemukan!');
            }
            else {
                var obj = $.parseJSON(data);
                console.log(obj);
                $('input[name=kodewilayah'+suffix+']').val(obj.kodewilayah);
				$('input[name=provinsi'+suffix+']').val(obj.provinsi);
				$('input[name=kabupaten'+suffix+']').val(obj.kabupaten);
				$('input[name=kecamatan'+suffix+']').val(obj.kecamatan);
				$('input[name=desa'+suffix+']').val(obj.desa);
				$('input[name=b1_r6'+suffix+']').val(obj.b1_r6);
				$('input[name=b1_r8'+suffix+']').val(obj.b1_r8);
				$('input[name=b1_r9'+suffix+']').val(obj.b1_r9);
				$('input[name=b1_r10'+suffix+']').val(obj.b1_r10);
				$('select[name=b3_r1a'+suffix+']').val(obj.b3_r1a);
				$('select[name=b3_r1b'+suffix+']').val(obj.b3_r1b);
				$('input[name=b3_r2'+suffix+']').val(obj.b3_r2);
				$('select[name=b3_r3'+suffix+']').val(obj.b3_r3);
				$('select[name=b3_r4a'+suffix+']').val(obj.b3_r4a);
				$('select[name=b3_r4b'+suffix+']').val(obj.b3_r4b);
				$('select[name=b3_r5a'+suffix+']').val(obj.b3_r5a);
				$('select[name=b3_r5b'+suffix+']').val(obj.b3_r5b);
				$('select[name=b3_r6'+suffix+']').val(obj.b3_r6);
				$('select[name=b3_r7'+suffix+']').val(obj.b3_r7);
				$('select[name=b3_r8'+suffix+']').val(obj.b3_r8);
				$('select[name=b3_r9a'+suffix+']').val(obj.b3_r9a);
				$('select[name=b3_r9b'+suffix+']').val(obj.b3_r9b);
				$('select[name=b3_r10'+suffix+']').val(obj.b3_r10);
				$('select[name=b3_r11a'+suffix+']').val(obj.b3_r11a);
				$('select[name=b3_r11b'+suffix+']').val(obj.b3_r11b);
				$('select[name=b3_r12'+suffix+']').val(obj.b3_r12);
				$('select[name=b5_r1a'+suffix+']').val(obj.b5_r1a);
				$('select[name=b5_r1b'+suffix+']').val(obj.b5_r1b);
				$('select[name=b5_r1c'+suffix+']').val(obj.b5_r1c);
				$('select[name=b5_r1d'+suffix+']').val(obj.b5_r1d);
				$('select[name=b5_r1e'+suffix+']').val(obj.b5_r1e);
				$('select[name=b5_r1f'+suffix+']').val(obj.b5_r1f);
				$('select[name=b5_r1g'+suffix+']').val(obj.b5_r1g);
				$('select[name=b5_r1h'+suffix+']').val(obj.b5_r1h);
				$('select[name=b5_r1i'+suffix+']').val(obj.b5_r1i);
				$('select[name=b5_r1j'+suffix+']').val(obj.b5_r1j);
				$('select[name=b5_r1k'+suffix+']').val(obj.b5_r1k);
				$('select[name=b5_r1l'+suffix+']').val(obj.b5_r1l);
				$('select[name=b5_r1m'+suffix+']').val(obj.b5_r1m);
				$('select[name=b5_r1n'+suffix+']').val(obj.b5_r1n);
				$('select[name=b5_r1o'+suffix+']').val(obj.b5_r1o);
				$('input[name=b5_r2a'+suffix+']').val(obj.b5_r2a);
				$('input[name=b5_r2b'+suffix+']').val(obj.b5_r2b);
				$('select[name=b5_r3a1'+suffix+']').val(obj.b5_r3a1);
				$('select[name=b5_r3a2'+suffix+']').val(obj.b5_r3a2);
				$('select[name=b5_r3b'+suffix+']').val(obj.b5_r3b);
				$('input[name=b5_r4a'+suffix+']').val(obj.b5_r4a);
				$('input[name=b5_r4b'+suffix+']').val(obj.b5_r4b);
				$('input[name=b5_r4c'+suffix+']').val(obj.b5_r4c);
				$('input[name=b5_r4d'+suffix+']').val(obj.b5_r4d);
				$('input[name=b5_r4e'+suffix+']').val(obj.b5_r4e);
				$('select[name=b5_r5a'+suffix+']').val(obj.b5_r5a);
				$('select[name=b5_r6a'+suffix+']').val(obj.b5_r6a);
				$('select[name=b5_r6b'+suffix+']').val(obj.b5_r6b);
				$('select[name=b5_r6c'+suffix+']').val(obj.b5_r6c);
				$('select[name=b5_r6d'+suffix+']').val(obj.b5_r6d);
				$('select[name=b5_r6e'+suffix+']').val(obj.b5_r6e);
				$('select[name=b5_r6f'+suffix+']').val(obj.b5_r6f);
				$('select[name=b5_r6g'+suffix+']').val(obj.b5_r6g);
				$('select[name=b5_r6h'+suffix+']').val(obj.b5_r6h);
				$('select[name=b5_r6i'+suffix+']').val(obj.b5_r6i);
				$('select[name=b5_r7'+suffix+']').val(obj.b5_r7);
				$('select[name=b5_r8a'+suffix+']').val(obj.b5_r8a);
				$('select[name=b5_r8b'+suffix+']').val(obj.b5_r8b);
				$('select[name=b5_r9'+suffix+']').val(obj.b5_r9);
				$('select[name=b5_r10'+suffix+']').val(obj.b5_r10);
				$('select[name=b5_r11a'+suffix+']').val(obj.b5_r11a);
				$('select[name=b5_r11b'+suffix+']').val(obj.b5_r11b);
				$('select[name=b5_r12'+suffix+']').val(obj.b5_r12);
				$('select[name=b5_r13'+suffix+']').val(obj.b5_r13);
				$('select[name=b5_r14'+suffix+']').val(obj.b5_r14);
				$('select[name=status_kesejahteraan'+suffix+']').val(obj.status_kesejahteraan);
				$('input[name=nomor_urut_rumah_tangga'+suffix+']').val(obj.nomor_urut_rumah_tangga);
				$('input[name=kk]').val("");
            }
        });
        e.preventDefault();
  });
});

$(function() {
  $(document).on('click', '.submit_kk', function(e) {
        // alert('submitted');
        var urlx = $(this).data('url');
        var target = $(this).data('target'); // target to nik
        var target_selector = $('input[name='+target+']');
        var kk = target_selector.val();
        var suffix = target_selector.data('suffix');
        $.post(urlx,
        {
            kk: kk,
        },
        function(data, status){
            // alert("Data: " + data + "\nStatus: " + status);
            if(data === 'error') {
                alert('Nomor tidak ditemukan!');
            }
            else {
                var obj = $.parseJSON(data);
                console.log(obj);
                $('input[name=kodewilayah'+suffix+']').val(obj.kodewilayah);
				$('input[name=provinsi'+suffix+']').val(obj.provinsi);
				$('input[name=kabupaten'+suffix+']').val(obj.kabupaten);
				$('input[name=kecamatan'+suffix+']').val(obj.kecamatan);
				$('input[name=desa'+suffix+']').val(obj.desa);
				$('input[name=b1_r6'+suffix+']').val(obj.b1_r6);
				$('input[name=b1_r8'+suffix+']').val(obj.b1_r8);
				$('input[name=b1_r9'+suffix+']').val(obj.b1_r9);
				$('input[name=b1_r10'+suffix+']').val(obj.b1_r10);
				$('select[name=b3_r1a'+suffix+']').val(obj.b3_r1a);
				$('select[name=b3_r1b'+suffix+']').val(obj.b3_r1b);
				$('input[name=b3_r2'+suffix+']').val(obj.b3_r2);
				$('select[name=b3_r3'+suffix+']').val(obj.b3_r3);
				$('select[name=b3_r4a'+suffix+']').val(obj.b3_r4a);
				$('select[name=b3_r4b'+suffix+']').val(obj.b3_r4b);
				$('select[name=b3_r5a'+suffix+']').val(obj.b3_r5a);
				$('select[name=b3_r5b'+suffix+']').val(obj.b3_r5b);
				$('select[name=b3_r6'+suffix+']').val(obj.b3_r6);
				$('select[name=b3_r7'+suffix+']').val(obj.b3_r7);
				$('select[name=b3_r8'+suffix+']').val(obj.b3_r8);
				$('select[name=b3_r9a'+suffix+']').val(obj.b3_r9a);
				$('select[name=b3_r9b'+suffix+']').val(obj.b3_r9b);
				$('select[name=b3_r10'+suffix+']').val(obj.b3_r10);
				$('select[name=b3_r11a'+suffix+']').val(obj.b3_r11a);
				$('select[name=b3_r11b'+suffix+']').val(obj.b3_r11b);
				$('select[name=b3_r12'+suffix+']').val(obj.b3_r12);
				$('select[name=b5_r1a'+suffix+']').val(obj.b5_r1a);
				$('select[name=b5_r1b'+suffix+']').val(obj.b5_r1b);
				$('select[name=b5_r1c'+suffix+']').val(obj.b5_r1c);
				$('select[name=b5_r1d'+suffix+']').val(obj.b5_r1d);
				$('select[name=b5_r1e'+suffix+']').val(obj.b5_r1e);
				$('select[name=b5_r1f'+suffix+']').val(obj.b5_r1f);
				$('select[name=b5_r1g'+suffix+']').val(obj.b5_r1g);
				$('select[name=b5_r1h'+suffix+']').val(obj.b5_r1h);
				$('select[name=b5_r1i'+suffix+']').val(obj.b5_r1i);
				$('select[name=b5_r1j'+suffix+']').val(obj.b5_r1j);
				$('select[name=b5_r1k'+suffix+']').val(obj.b5_r1k);
				$('select[name=b5_r1l'+suffix+']').val(obj.b5_r1l);
				$('select[name=b5_r1m'+suffix+']').val(obj.b5_r1m);
				$('select[name=b5_r1n'+suffix+']').val(obj.b5_r1n);
				$('select[name=b5_r1o'+suffix+']').val(obj.b5_r1o);
				$('input[name=b5_r2a'+suffix+']').val(obj.b5_r2a);
				$('input[name=b5_r2b'+suffix+']').val(obj.b5_r2b);
				$('select[name=b5_r3a1'+suffix+']').val(obj.b5_r3a1);
				$('select[name=b5_r3a2'+suffix+']').val(obj.b5_r3a2);
				$('select[name=b5_r3b'+suffix+']').val(obj.b5_r3b);
				$('input[name=b5_r4a'+suffix+']').val(obj.b5_r4a);
				$('input[name=b5_r4b'+suffix+']').val(obj.b5_r4b);
				$('input[name=b5_r4c'+suffix+']').val(obj.b5_r4c);
				$('input[name=b5_r4d'+suffix+']').val(obj.b5_r4d);
				$('input[name=b5_r4e'+suffix+']').val(obj.b5_r4e);
				$('select[name=b5_r5a'+suffix+']').val(obj.b5_r5a);
				$('select[name=b5_r6a'+suffix+']').val(obj.b5_r6a);
				$('select[name=b5_r6b'+suffix+']').val(obj.b5_r6b);
				$('select[name=b5_r6c'+suffix+']').val(obj.b5_r6c);
				$('select[name=b5_r6d'+suffix+']').val(obj.b5_r6d);
				$('select[name=b5_r6e'+suffix+']').val(obj.b5_r6e);
				$('select[name=b5_r6f'+suffix+']').val(obj.b5_r6f);
				$('select[name=b5_r6g'+suffix+']').val(obj.b5_r6g);
				$('select[name=b5_r6h'+suffix+']').val(obj.b5_r6h);
				$('select[name=b5_r6i'+suffix+']').val(obj.b5_r6i);
				$('select[name=b5_r7'+suffix+']').val(obj.b5_r7);
				$('select[name=b5_r8a'+suffix+']').val(obj.b5_r8a);
				$('select[name=b5_r8b'+suffix+']').val(obj.b5_r8b);
				$('select[name=b5_r9'+suffix+']').val(obj.b5_r9);
				$('select[name=b5_r10'+suffix+']').val(obj.b5_r10);
				$('select[name=b5_r11a'+suffix+']').val(obj.b5_r11a);
				$('select[name=b5_r11b'+suffix+']').val(obj.b5_r11b);
				$('select[name=b5_r12'+suffix+']').val(obj.b5_r12);
				$('select[name=b5_r13'+suffix+']').val(obj.b5_r13);
				$('select[name=b5_r14'+suffix+']').val(obj.b5_r14);
				$('select[name=status_kesejahteraan'+suffix+']').val(obj.status_kesejahteraan);
				$('input[name=nomor_urut_rumah_tangga'+suffix+']').val(obj.nomor_urut_rumah_tangga);
				$('input[name=id]').val("");
            }
        });
        e.preventDefault();
  });
});*/
</script>			