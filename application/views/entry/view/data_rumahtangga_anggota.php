<div class="box-body">
	<div class="row form-group">
		<div class="col-sm-12">					
			<label for="id_rt" class="control-label col-sm-2">No. Urut Rumah Tangga</label>  
			<div class="col-sm-2">
			  <?php echo form_input('id', $nomor_urut_rumah_tangga, 'class="form-control input-sm _bdt" disabled'); ?>
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-12">					
			<label for="nik" class="control-label col-sm-2">No. Kartu Keluarga Kepala Rumah Tangga</label>  
			<div class="col-sm-2">
			  <?php echo form_input('kk', $no_kk_rumah_tangga, 'maxlength="16" class="form-control input-sm _mhn" disabled'); ?>
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-12">					
			<label class="col-sm-2 control-label">Kode Wilayah </label>
			<div class="col-sm-2">
				<?php /*$kode = substr($kodewilayah,6);*/ ?>
				<?php echo form_input('kodewilayah_bdt', $kodewilayah, 'class="form-control input-sm" style="text-transform:uppercase" maxlength="10" disabled'); ?> 
			</div>
			<span class="col-sm-7 control-label" style="text-align: left;color:red"><i>* Isikan Kode Wilayah 10 digit, diawali kode 3575. </i></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Nama Propinsi </label>
			<div class="col-sm-4">
				<?php echo form_input('provinsi_bdt', $propinsi, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?>
				<?php echo form_hidden('provinsi_bdt', $propinsi); ?> 
			</div>					
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Nama Kabupaten </label>
			<div class="col-sm-4">
				<?php echo form_input('kabupaten_bdt', $kabupaten, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?>
				<?php echo form_hidden('kabupaten_bdt', $kabupaten); ?> 
			</div>					
		</div>
	</div>			
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Nama Kecamatan </label>
			<div class="col-sm-4">
				<?php echo form_input('kecamatan_bdt', $kecamatan, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?>
				<?php echo form_hidden('kecamatan_bdt', $kecamatan); ?> 
			</div>					
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Nama Kelurahan </label>
			<div class="col-sm-4">
				<?php echo form_input('desa_bdt', $kelurahan, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?>
				<?php echo form_hidden('desa_bdt', $kelurahan); ?> 
			</div>					
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Alamat tempat tinggal </label>
			<div class="col-sm-6">
				<?php echo form_input('b1_r6', $alamat.', No.RT/No.RW. '.$no_rt.'/'.$no_rw, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" disabled'); ?>
				<?php /* ?>
				<textarea class="form-control input-sm" name = "b1_r6" disabled="" rows="3" style="text-transform:uppercase"><?php echo "$alamat, No.RT/No.RW. $no_rt/$no_rw"; ?></textarea>  
				<?php */ ?>
			</div>
		</div>
		<div class="col-sm-6">	
			<label class="col-sm-4 control-label">Nama Kepala Rumah Tangga </label>
			<div class="col-sm-4">
				<?php echo form_input('b1_r8', $nama_kep, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div> 
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah Anggota Keluarga </label>
			<div class="col-sm-2">
				<?php echo form_input('b1_r9', $b1_r9, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div> 
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Status Kesejahteraan (Desil) </label>
			<div class="col-sm-8"> 
				<?php echo form_dropdown('status_kesejahteraan', $opsi_status_kesejahteraan, $status_kesejahteraan, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Status kepemilikan bangunan tempat tinggal </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b3_r1a', $opsi_b3_r1a, $b3_r1a, 'class="form-control input-sm" disabled'); ?>
			</div> 
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Status kepemilikan lahan tempat tinggal </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b3_r1b', $opsi_b3_r1b, $b3_r1b, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Luas lantai </label>
			<div class="col-sm-2">
				<?php echo form_input('b3_r2', $b3_r2, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div> 
			<span class="col-sm-4 control-label" style="text-align: left;"> m2 </span>
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jenis lantai terluas </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b3_r3', $opsi_b3_r3, $b3_r3, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jenis dinding terluas </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b3_r4a', $opsi_b3_r4a, $b3_r4a, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jenis atap terluas </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b3_r5a', $opsi_b3_r5a, $b3_r5a, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kualitas dinding terluas </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r4b', $opsi_b3_r4b, $b3_r4b, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kualitas atap terluas </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r5b', $opsi_b3_r5b, $b3_r5b, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah kamar tidur </label>
			<div class="col-sm-2">
				<?php echo form_input('b3_r6', $b3_r6, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Sumber air minum </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r7', $opsi_b3_r7, $b3_r7, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Cara memperoleh air minum </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r8', $opsi_b3_r8, $b3_r8, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Penggunaan fasilitas buang air besar </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b3_r11a', $opsi_b3_r11a, $b3_r11a, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jenis kloset </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r11b', $opsi_b3_r11b, $b3_r11b, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Tempat pembuangan akhir tinja </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b3_r12', $opsi_b3_r12, $b3_r12, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Sumber penerangan utama </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b3_r9a', $opsi_b3_r9a, $b3_r9a, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Daya listrik terpasang (PLN) </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b3_r9b', $opsi_b3_r9b, $b3_r9b, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Bahan bakar untuk memasak </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r10', $opsi_b3_r10, $b3_r10, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan tabung gas 5.5 kg atau lebih </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1a', $opsi_binary, $b5_r1a, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan sambungan telepon (PSTN) </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1e', $opsi_binary, $b5_r1e, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan komputer/laptop </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1h', $opsi_binary_ii, $b5_r1h, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Sepeda </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1i', $opsi_binary, $b5_r1i, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Sepeda motor </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1j', $opsi_binary_ii, $b5_r1j, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Mobil </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1k', $opsi_binary, $b5_r1k, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Perahu </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1l', $opsi_binary_ii, $b5_r1l, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Motor tempel </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1m', $opsi_binary, $b5_r1m, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Perahu motor </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1n', $opsi_binary_ii, $b5_r1n, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Kapal </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1o', $opsi_binary, $b5_r1o, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Lemari es/kulkas </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1b', $opsi_binary_ii, $b5_r1b, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan AC (penyejuk udara) </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1c', $opsi_binary, $b5_r1c, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Pemanas air (water heater) </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1d', $opsi_binary_ii, $b5_r1d, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Televisi </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1f', $opsi_binary_ii, $b5_r1f, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Emas/perhiasan/tabungan senilai 10 gram emas </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1g', $opsi_binary, $b5_r1g, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah nomor HP aktif </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r2a', $b5_r2a, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah TV layar datar minimal 30inchi </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r2b', $b5_r2b, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan lahan </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r3a1', $opsi_binary, $b5_r3a1, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Luas lahan yang dimiliki </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r3a2', $b5_r3a2, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
			<span class="col-sm-4 control-label" style="text-align: left;"> m2 </span>
		</div> 
	</div> 
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan rumah di lokasi lain </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r3b', $opsi_binary_ii, $b5_r3b, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Ada anggota rumah tangga yang memiliki usaha sendiri/bersama </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r5a', $opsi_binary, $b5_r5a, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div> 
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah ternak sapi yang dimilki </label>
			<div class="col-sm-3">
				<?php echo form_input('b5_r4a', $b5_r4a, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah ternak kerbau yang dimilki </label>
			<div class="col-sm-3">
				<?php echo form_input('b5_r4b', $b5_r4b, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah ternak kuda yang dimilki </label>
			<div class="col-sm-3">
				<?php echo form_input('b5_r4c', $b5_r4c, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah ternak babi yang dimilki </label>
			<div class="col-sm-3">
				<?php echo form_input('b5_r4d', $b5_r4d, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah ternak kambing/domba yang dimilki </label>
			<div class="col-sm-3">
				<?php echo form_input('b5_r4e', $b5_r4e, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki KKS/KPS </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6a', $opsi_binary, $b5_r6a, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Peserta program PKH </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6g', $opsi_binary, $b5_r6g, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Peserta program Raskin </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6h', $opsi_binary_ii, $b5_r6h, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Peserta program KUR </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6i', $opsi_binary, $b5_r6i, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepesertaan KB </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r9', $opsi_binary, $b5_r9, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Usia kawin pertama suami </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r8a', $b5_r8a, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
			<span class="col-sm-4 control-label" style="text-align: left;"> tahun </span>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Usia kawin pertama istri </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r8b', $b5_r8b, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
			<span class="col-sm-4 control-label" style="text-align: left;"> tahun </span>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Metode/jenis kontrasepsi yang digunakan </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r10', $opsi_b5_r10, $b5_r10, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Lama menggunakan kontrasepsi </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r11a', $b5_r11a, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
			<span class="col-sm-2 control-label" style="text-align: left;"> tahun </span> 
			<div class="col-sm-2">
				<?php echo form_input('b5_r11b', $b5_r11b, 'class="form-control input-sm" style="text-transform:uppercase" disabled'); ?> 
			</div>
			<span class="col-sm-2 control-label" style="text-align: left;"> bulan </span>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Tempat pelayanan KB yang sering digunakan </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b5_r12', $opsi_b5_r12, $b5_r12, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Keinginan punya anak lagi </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b5_r13', $opsi_b5_r13, $b5_r13, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Alasan tidak mengikuti KB </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b5_r14', $opsi_b5_r14, $b5_r14, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki KIP/BSM </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6b', $opsi_binary_ii, $b5_r6b, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki KIS/BPJS Kesehatan/Jamkesmas </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6c', $opsi_binary, $b5_r6c, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki BPJS Kesehatan peserta mandiri </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6d', $opsi_binary_ii, $b5_r6d, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki Jamsostek/BPJS ketenagakerjaan </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6e', $opsi_binary, $b5_r6e, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki Asuransi kesehatan lainnya </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6f', $opsi_binary_ii, $b5_r6f, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>

</div><!-- /.box-body -->