<div class="box-body">
	<div class="row">	
		<div class="box-body">
			<div class="col-sm-12">
			    <div style="text-align: center;" class="alert alert-warning" role="alert">
			      <b>MEKANISME PEMUTAKHIRAN MANDIRI<br />
			      Formulir Pendaftaran Rumah Tangga Miskin dan Kurang Mampu</b>
			    </div>
			</div>
		    <div class="col-sm-8">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
		    		<tr>
			    		<th colspan="2">Jenis Pengajuan</th>
			    		<td>:</td>
			    		<td>
			    		<?php
						$options = array('' => ' - Pilih Opsi - ',
						'000' => '1. Pengajuan Data Baru',
						'001' => '2. Perubahan atas Data Lama',
						);
						echo form_dropdown('status_pengajuan', $options, '', 'class="form-control input-sm" required');
						?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th colspan="4">INFORMASI PENDAFTAR</th>
			    	</tr>
			    	<tr>
			    		<th width="5%">1.</th>
			    		<th width="40%">NIK</th>
			    		<td width="5%">:</td>
			    		<td>
			    		<?php echo form_input('nik_pendaftar', $nik, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>2.</th>
			    		<th>Nama Lengkap sesuai KTP</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_input('nama_pendaftar', $nama_lgkp, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>3.</th>
			    		<th>Jenis Kelamin</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_dropdown('jenis_klmin', $opsi_b4_k6, $jenis_klmin, 'class="form-control input-sm" disabled'); ?>
						<?php echo form_hidden('jenis_klmin_pendaftar', $jenis_klmin); ?>
						</td>
			    	</tr>
			    	<tr>
			    		<th>4.</th>
			    		<th>Nomor KK</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_input('no_kk', $no_kk, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>5.</th>
			    		<th>Hubungan dengan Kepala Keluarga</th>
			    		<td>:</td>
			    		<td>
			    		<?php if ($stat_hbkel == 2 || $stat_hbkel == 3) {
			    			$stat_hbkel = 2;
			    		} elseif ($stat_hbkel == 4) {
			    			$stat_hbkel = 3;
			    		} elseif ($stat_hbkel == 5) {
			    			$stat_hbkel = 4;
			    		} elseif ($stat_hbkel == 6) {
			    			$stat_hbkel = 5;
			    		} elseif ($stat_hbkel == 7 || $stat_hbkel == 8) {
			    			$stat_hbkel = 6;
			    		} elseif ($stat_hbkel == 10) {
			    			$stat_hbkel = 7;
			    		} elseif ($stat_hbkel == 9 || $stat_hbkel == 11) {
			    			$stat_hbkel = 8;
			    		}?>
			    		<?php echo form_dropdown('hubungan_keluarga', $opsi_stat_hbkel, $stat_hbkel, 'class="form-control input-sm" disabled'); ?>
			    		<?php echo form_hidden('hubungan_keluarga', $stat_hbkel); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>6.</th>
			    		<th>Hubungan dengan Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_dropdown('hubungan_rumahtangga_induk', $opsi_stat_rtangga, '', 'class="form-control input-sm" required'); ?>
			    		</td>
			    	</tr>
			    </table>
		    </div>

		    <div class="col-sm-8">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<th colspan="6">INFORMASI RUMAH TANGGA</th>
			    	</tr>
			    	<tr>
			    		<th colspan="2" width="30%">Apakah anggota rumah tangga lain ?</th>
						<td width="5%">:</td>
						<td width="15%"><?php echo form_dropdown('anggota_rt', $opsi_binary, '', 'class="form-control input-sm ddl_disabled_toggle" data-ddl_target="._kk"'); ?></td>
						<td><?php echo form_input('no_kk_induk', '', 'placeholder="Masukkan NO. KK Rumah Tangga" data-suffix="_induk" maxlength="16" class="form-control _kk input-sm" required'); ?>
						</td>
						<td><button id="Cari" class="btn btn-primary btn-sm submit_kk" data-url="<?php echo site_url('skrining/ajax/get_rumahtangga'); ?>" data-target="no_kk_induk">Cari</button></td>
			    	</tr>
			    	<tr>
			    		<th width="5%"></th>
			    		<th width="40%">a. NIK Rumah Tangga Induk</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php echo form_input('nik_rumahtangga_induk', '', 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th width="5%"></th>
			    		<th width="40%">b. Nama Kepala Rumah Tangga Induk</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php echo form_input('nama_kep_rumahtangga_induk', '', 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th width="5%">7.</th>
			    		<th width="40%">NIK Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php echo form_input('nik_rumahtangga', $nik_kepala, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>8.</th>
			    		<th>Nama Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php echo form_input('nama_kep_rumahtangga', $nama_kepala, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<th>9.</th>
			    		<th>Jenis Kelamin Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php echo form_dropdown('jenis_klmin_rumahtangga', $opsi_b4_k6, $jenis_klmin_kepala, 'class="form-control input-sm" disabled'); ?>
						<?php echo form_hidden('jenis_klmin_rumahtangga', $jenis_klmin_kepala); ?></td>
			    	</tr>
			    	<tr>
			    		<th>10.</th>
			    		<th>Bulan Lahir Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td width="15%">
			    		<?php 
			    		$bulan_arr = explode("-", $tgl_lhr_kepala);
			    		$bulan = $bulan_arr[1];
			    		?>
			    		<?php echo form_input('bulan_lhr_rumahtangga', str_pad($bulan, 2, "0", STR_PAD_LEFT), 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    		<td colspan="2">MM</td>
			    	</tr>
			    	<tr>
			    		<th>11.</th>
			    		<th>Tahun Lahir Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td width="10%">
			    		<?php 
			    		$tahun_arr = explode("-", $tgl_lhr_kepala);
			    		$tahun = $bulan_arr[2];
			    		?>
			    		<?php echo form_input('tahun_lhr_rumahtangga', $tahun, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    		</td>
			    		<td colspan="2">YYYY</td>
			    	</tr>
			    	<tr>
			    		<th>12.</th>
			    		<th>Status Pekerjaan Kepala Rumah Tangga</th>
			    		<td>:</td>
			    		<td colspan="3">
			    		<?php
						$options = array('' => ' - Pilih Opsi - ',
						'1' => '1. Bekerja',
						'2' => '2. Tidak Bekerja',
						);
						echo form_dropdown('status_pkrjn_rumahtangga', $options, '', 'class="form-control input-sm" required');
						?>
						</td>
			    	</tr>
			    	<tr>
			    		<th>13.</th>
			    		<th>Jumlah Anggota Rumah Tangga</th>
			    		<td>:</td>
			    		<td>
			    		<?php echo form_input('b1_r9', $b1_r9, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?>
			    		</td>
			    		<td colspan="2">Orang</td>
			    	</tr>
			    </table>
		    </div>
		</div>
	</div>
	<div class="row">	
		<div class="box-body">
		    <div class="col-sm-12">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<th colspan="3">INFORMASI WILAYAH</th>
			    		<th width="25%">DATA LAMA</th>
			    		<th width="25%">DATA BARU</th>
			    	</tr>
			    	<tr>
			    		<td width="2%"><b>14.</b></td>
			    		<td width="20%"><b>Provinsi</b></td>
			    		<td width="2%">:</td>
			    		<td><?php echo form_input('', $propinsi_lama, 'placeholder="Propinsi" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    		<td><?php echo form_input('provinsi', $propinsi, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    			<?php echo form_hidden('no_prop', $no_prop); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<td><b>15.</b></td>
			    		<td><b>Kabupaten/Kota</b></td>
			    		<td>:</td>
			    		<td><?php echo form_input('', $kabupaten_lama, 'placeholder="Kabupaten" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    		<td><?php echo form_input('kabupaten', $kabupaten, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    			<?php echo form_hidden('no_kab', $no_kab); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<td><b>16.</b></td>
			    		<td><b>Kecamatan</b></td>
			    		<td>:</td>
			    		<td><?php echo form_input('', $kecamatan_lama, 'placeholder="Kecamatan" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    		<td><?php echo form_input('kecamatan', $kecamatan, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    			<?php echo form_hidden('no_kec', $no_kec); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<td><b>17.</b></td>
			    		<td><b>Kelurahan/Desa</b></td>
			    		<td>:</td>
			    		<td><?php echo form_input('', $kelurahan_lama, 'placeholder="Kelurahan/Desa" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    		<td><?php echo form_input('desa', $kelurahan, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    			<?php echo form_hidden('no_kel', $no_kel); ?>
			    		</td>
			    	</tr>
			    	<tr>
			    		<td><b>18.</b></td>
			    		<td><b>Nama Jalan (RT/RW)</b></td>
			    		<td>:</td>
			    		<td><?php echo form_input('b1_r6', $b1_r6, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?></td>
			    		<td><?php echo form_input('', $alamat.' RT/RW '.$no_rt.'/'.$no_rw, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			    			<?php echo form_hidden('alamat', $alamat); ?>
			    			<?php echo form_hidden('no_rt', $no_rt); ?>
			    			<?php echo form_hidden('no_rw', $no_rw); ?>
			    		</td>
			    	</tr>
			    </table>
		    </div>
		</div>
	</div>
	<div class="row">	
		<div class="box-body">	    
		    <div class="col-sm-8">
		    	<table width="100%" class="table responsive" style="font-size: 0.9em">
			    	<tr>
			    		<th colspan="4">DATA KUESIONER</th>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>19.</b></td>
			    		<td width="50%"><b>Status Kepemilikan Bangunan Tempat Tinggal</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b3_r1a_bdt', $opsi_b3_r1a, $b3_r1a, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>20.</b></td>
			    		<td width="50%"><b>Bahan Bangunan Utama Atap Rumah Terluas</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b3_r5a_bdt', $opsi_b3_r5a, $b3_r5a, 'class="form-control input-sm kunci" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>21.</b></td>
			    		<td width="50%"><b>Bahan Bangunan Utama Dinding Rumah Terluas</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b3_r4a_bdt', $opsi_b3_r4a, $b3_r4a, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>22.</b></td>
			    		<td width="50%"><b>Bahan Bangunan Utama Lantai Rumah Terluas</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b3_r3_bdt', $opsi_b3_r3, $b3_r3, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>23.</b></td>
			    		<td width="50%"><b>Penggunaan Fasilitas Tempat Buang Air Besar</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b3_r11a_bdt', $opsi_b3_r11a, $b3_r11a, 'class="form-control input-sm" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>24.</b></td>
			    		<td width="50%"><b>Apakah Rumah Tangga Memiliki Mobil?</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b5_r1k_bdt', $opsi_binary, $b5_r1k, 'class="form-control input-sm kunci" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>25.</b></td>
			    		<td width="50%"><b>Apakah Rumah Tangga Memiliki AC?</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b5_r1c_bdt', $opsi_binary, $b5_r1c, 'class="form-control input-sm kunci" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>26.</b></td>
			    		<td width="50%"><b>Apakah Rumah Tangga Memiliki Tabung Gas 5,5 Kg atau lebih?</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b5_r1a_bdt', $opsi_binary, $b5_r1a, 'class="form-control input-sm kunci" required'); ?></td>
			    	</tr>
			    	<tr>
			    		<td width="5%"><b>27.</b></td>
			    		<td width="50%"><b>Pendidikan Tertinggi Anggota Rumah Tangga Yang Sudah Tidak Bersekolah?</b></td>
			    		<td width="5%">:</td>
			    		<td><?php echo form_dropdown('b4_k18_bdt', $opsi_b4_k18, $b4_k18, 'class="form-control input-sm pendidikan kunci" required'); ?></td>
			    	</tr>
			    </table>
		    </div>	
		</div>
	</div>

</div><!-- /.box-body -->

<script type="text/javascript">
	$(function(){
		ddl_disabled_check('.ddl_disabled_toggle');
		$(document).on('change', '.ddl_disabled_toggle', function() {
			ddl_disabled_check($(this));
		});
		function ddl_disabled_check(sel) {
			var target = $(sel).attr('data-ddl_target');
			if( ! target) {
				return false;
			}
			if($(sel).val() == 1) {
				target_ddl_disabled_on(target);
			}
			else {
				$(target).val('');
				target_ddl_disabled_off(target);
			}
		}
		function target_ddl_disabled_on(target_selector) {
			$(target_selector).removeAttr('disabled');
		}
		function target_ddl_disabled_off(target_selector) {
			$(target_selector).attr('disabled', 'disabled');
		}

		check_kunci('.kunci');
		$(document).on('change', '.kunci', function() {
			// alert('masuk gak c?');
			check_kunci('.kunci');
		});
		function check_kunci(target_selector) {
			var total_selector = $(target_selector).length;
			var count = 0;
			var max = 2;
			$(target_selector).each(function() {
				var name = $(this).attr('name');
				var val = $(this).val();
				val = parseInt(val);
				if(name == "b3_r5a_bdt") {
					if(val == 1 || val == 2 || val == 3 || val == 4) {
						count = count+1;
					}
				}
				if(name == "b4_k18_bdt") {
					if(val == 5 || val == 6) {
						count = count+1;
					}
				}
				if(name == "b5_r1k_bdt") {
					if(val == 1) {
						count = count+1;
					}
				}
				if(name == "b5_r1c_bdt") {
					if(val == 1) {
						count = count+1;
					}
				}
				if(name == "b5_r1a_bdt") {
					if(val == 1) {
						count = count+1;
					}
				}
				/*else {
					if(val == 1) {
						 count = count+1;
					}
				}*/
				// console.log('name: '+name+ ' | val: '+val);
			});
			// console.log('count: '+count+' | max: '+max+' | total: '+total_selector);
			if(count > max) {
				$('form [type=submit]').attr('disabled', 'disabled');
			} else {
				$('form [type=submit]').removeAttr('disabled');
			}
		}

		// check_required_form();
		// $(document).on('change blur', 'form [required]', function() {
		// 	check_required_form();
		// });
		function check_required_form() {
			var target_selector = 'form [required]';
			var total_selector = $(target_selector).length;
			var count = 0;
			$(target_selector).each(function(){
				if($(this).val() != "") {
					count++;
				}
			});
			if(count != total_selector) {
				$('form [type=submit]').attr('disabled', 'disabled');
			} else {
				$('form [type=submit]').removeAttr('disabled');
			}
		}
 	});
</script>