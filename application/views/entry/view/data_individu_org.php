<div class="box-body">
	<div class="row form-group">
		<div class="col-sm-6">					
			<label class="col-sm-4 control-label">No. Urut rumah tangga </label>
			<div class="col-sm-4">
				<?php echo form_input('nomor_urut_rumah_tangga_bdt', $nomor_urut_rumah_tangga, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			</div>
		</div>
		<div class="col-sm-6">					
			<label class="col-sm-4 control-label">No. Kartu Keluarga Individu </label>
			<div class="col-sm-4">
				<?php echo form_input('no_kk', $no_kk, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			</div>
		</div>
	</div>
	<div class="row form-group">
		<?php /* ?>
		<div class="col-sm-6 hide">	
			<label class="col-sm-4 control-label">No. Urut anggota keluarga </label>
			<div class="col-sm-2">
				<?php
			        $options = array('' => ' - Pilih - ',
			        '1' => '1',
			        '2' => '2',
			        '3' => '3',
			        '4' => '4',
			        '5' => '5',
			        '6' => '6',
			        '7' => '7',
			        '8' => '8',
			        '9' => '9',
			        '10' => '10',
			        );
			        echo form_dropdown('no_art', $options, $no_art, 'class="form-control input-sm"');
		        ?>
			</div>
		</div>
		<?php */ ?>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">N.I.K </label>
			<div class="col-sm-4">
				<?php echo form_input('nik', $nik, 'maxlength="16" class="form-control input-sm" readonly'); ?>
			</div>					
		</div>
	</div>			
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Nama lengkap </label>
			<div class="col-sm-6">
				<?php echo form_input('nama', $nama_lgkp, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			</div>
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Hubungan dengan kepala rumah tangga </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b4_k3', $opsi_b4_k3, $b4_k3, 'class="form-control input-sm" required'); ?>
			</div>					
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Hubungan dengan kepala keluarga </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b4_k5', $opsi_b4_k5, $b4_k5, 'class="form-control input-sm" required'); ?>
				<?php /*echo form_input('shdk', $shdk, 'class="form-control input-sm" style="text-transform:uppercase" required');*/ ?>
				<?php /*echo form_hidden('b4_k5', $stat_hbkel);*/ ?>
			</div>
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jenis kelamin </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b4_k6', $opsi_b4_k6, $jenis_klmin, 'class="form-control input-sm" disabled'); ?>
				<?php echo form_hidden('b4_k6', $jenis_klmin); ?>
			</div>
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Umur (pada saat pendataan) </label>
			<div class="col-sm-2">
				<?php echo form_input('b4_k7', $umur, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?> 
			</div> 
			<label class="col-sm-4 control-label" style="text-align: left;"> Tahun </label>
		</div>
		<div class="col-sm-6">	
			<label class="col-sm-4 control-label">Status perkawinan </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b4_k8', $opsi_b4_k8, $stat_kwn, 'class="form-control input-sm" disabled'); ?>
				<?php echo form_hidden('b4_k8', $stat_kwn); ?>
			</div> 
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan buku nikah/akta cerai </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b4_k9', $opsi_b4_k9, $akta_kwn, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Tercantum dalam Kartu keluarga (KK) di rumah tangga </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b4_k10', $opsi_binary, $b4_k10, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
	</div>		
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan akta kelahiran/kartu pelajar/KTP/SIM </label>
			<div class="col-sm-8">
				<?php echo form_dropdown('b4_k11', $opsi_b4_k11, $b4_k11, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Partisipasi sekolah </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b4_k15', $opsi_b4_k15, $b4_k15, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jenjang pendidikan tertinggi (yang pernah/sedang diduduki saat pendataan) </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b4_k16', $opsi_b4_k16, $b4_k16, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kelas tertinggi </label>
			<div class="col-sm-2">
				<?php echo form_input('b4_k17', $b4_k17, 'placeholder="Kelas" class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div> 
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Izasah tertinggi </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b4_k18', $opsi_b4_k18, $b4_k18, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Bekerja/membantu bekerja seminggu terakhir (keaktifan ekonomi) </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b4_k19a', $opsi_binary, $b4_k19a, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">	
			<label class="col-sm-4 control-label">Jumlah jam kerja dalam seminggu terakhir </label>
			<div class="col-sm-2">
				<?php echo form_input('b4_k19b', $b4_k19b, 'placeholder="Jam" class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div> 
			<span class="col-sm-4 control-label" style="text-align: left;"> Jam </span> 
		</div>
		<div class="col-sm-6">	
			<label class="col-sm-4 control-label">Lapangan usaha dari pekerjaan utama </label>
			<div class="col-sm-8">
				<?php echo form_dropdown('b4_k20', $opsi_b4_k20, $b4_k20, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">	
			<label class="col-sm-4 control-label">Status kedudukan dari pekerjaan utama </label>
			<div class="col-sm-8">
				<?php echo form_dropdown('b4_k21', $opsi_b4_k21, $b4_k21, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
		<div class="col-sm-6">	
			<label class="col-sm-4 control-label">Jenis cacat </label>
			<div class="col-sm-8">
				<?php echo form_dropdown('b4_k13', $opsi_b4_k13, $b4_k13, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">	
			<label class="col-sm-4 control-label">Penyakit kronis/menahun </label>
			<div class="col-sm-8">
				<?php echo form_dropdown('b4_k14', $opsi_b4_k14, $b4_k14, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>	
		<div class="col-sm-6">	
			<label class="col-sm-4 control-label">Status kehamilan (wanita 10-48 tahun) </label>
			<div class="col-sm-8">
				<?php echo form_dropdown('b4_k12', $opsi_b4_k12, $b4_k12, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
	</div>
</div><!-- /.box-body -->