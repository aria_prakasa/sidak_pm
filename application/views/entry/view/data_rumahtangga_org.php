<div class="box-body">
	<?php /* ?>
	<div class="row form-group">
		<div class="col-sm-12">					
			<label for="id_rt" class="control-label col-sm-2">No. Urut Rumah Tangga</label>  
			<div class="col-sm-2">
			  <?php echo form_input('id', $nomor_urut_rumah_tangga, 'placeholder="Masukkan No. Urut Rumah Tangga" data-suffix="_bdt" class="form-control input-sm _bdt"'); ?>
			</div>
			<div class="col-sm-1">
			<button id="Cari" class="btn btn-primary submit_bdt" data-url="<?php echo site_url('pull_bdt/ajax/get_data_rumahtangga'); ?>" data-target="id"><i class="fa fa-search"></i></button>
			</div>
			<span class="col-sm-7 control-label" style="text-align: left;color:red"><i>* Isikan jika data rumah tangga mengikuti data BDT yang sudah terdaftar </i></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-12">					
			<label for="nik" class="control-label col-sm-2">No. Kartu Keluarga Kepala Rumah Tangga</label>  
			<div class="col-sm-2">
			  <?php echo form_input('kk', $kk, 'placeholder="Masukkan No. Kartu Keluarga" data-suffix="_bdt" maxlength="16" class="form-control input-sm _mhn"'); ?>
			</div>
			<div class="col-sm-1">
			<button id="Cari" class="btn btn-primary submit_kk" data-url="<?php echo site_url('pull_mohon/ajax/get_mohon_rumahtangga'); ?>" data-target="kk"><i class="fa fa-search"></i></button>
			</div>
			<span class="col-sm-7 control-label" style="text-align: left;color:red"><i>* Isikan jika data rumah tangga mengikuti data permohonan sebelumnya </i></span>
		</div>
	</div>
	<?php */ ?>
	<div class="row form-group">
		<div class="col-sm-12">					
			<label class="col-sm-2 control-label">Kode Wilayah </label>
			<div class="col-sm-2">
				<?php /*if ($kodewilayah) { ?>
					<?php echo form_input('kodewilayah', $kodewilayah, 'class="form-control input-sm" style="text-transform:uppercase" maxlength="10" readonly'); ?></div>
				<?php } else { */?>
					<?php echo form_input('kodewilayah', $no_prop.$no_kab.'0'.$no_kec.$no_kel, 'class="form-control input-sm" style="text-transform:uppercase" maxlength="10" readonly'); ?></div> <span class="col-sm-7 control-label" style="text-align: left;color:red"><i>* Isikan Kode Wilayah 10 digit, diawali kode 3575. </i></span>
				<?php /*}*/ ?>		
			
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Nama Propinsi </label>
			<div class="col-sm-4">
				<?php echo form_input('provinsi_bdt', $propinsi, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			</div>					
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Nama Kabupaten </label>
			<div class="col-sm-4">
				<?php echo form_input('kabupaten_bdt', $kabupaten, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			</div>					
		</div>
	</div>			
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Nama Kecamatan </label>
			<div class="col-sm-4">
				<?php echo form_input('kecamatan_bdt', $kecamatan, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			</div>					
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Nama Kelurahan </label>
			<div class="col-sm-4">
				<?php echo form_input('desa_bdt', $kelurahan, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
			</div>					
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Alamat tempat tinggal </label>
			<div class="col-sm-6">
				<?php echo form_input('b1_r6_bdt', $alamat.', No.RT/No.RW. '.$no_rt.'/'.$no_rw, 'placeholder="Alamat" class="form-control input-sm" style="text-transform:uppercase" readonly'); ?>
				<?php /* ?>
				<textarea class="form-control input-sm" name = "b1_r6_bdt" readonly="" rows="3" style="text-transform:uppercase"><?php echo "$alamat, No.RT/No.RW. $no_rt/$no_rw"; ?></textarea>  
				<?php */ ?>
			</div>
		</div>
		<div class="col-sm-6">	
			<label class="col-sm-4 control-label">Nama Kepala Rumah Tangga </label>
			<div class="col-sm-4">
				<?php echo form_input('b1_r8_bdt', $nama_kep, 'class="form-control input-sm" style="text-transform:uppercase" readonly'); ?> 
			</div> 
		</div>
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah Anggota Keluarga </label>
			<div class="col-sm-2">
				<?php echo form_input('b1_r9_bdt', $b1_r9, 'class="form-control input-sm" style="text-transform:uppercase"'); ?> 
			</div> 
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Status Kesejahteraan (Desil) </label>
			<div class="col-sm-8"> 
				<?php /*echo form_input('status_kesejahteraan_bdt', '', 'class="form-control input-sm" style="text-transform:uppercase" readonly');*/ ?> 
				<?php echo form_dropdown('status_kesejahteraan_bdt', $opsi_status_kesejahteraan, $status_kesejahteraan, 'class="form-control input-sm" disabled'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Status kepemilikan bangunan tempat tinggal </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b3_r1a_bdt', $opsi_b3_r1a, $b3_r1a, 'class="form-control input-sm" required'); ?>
			</div> 
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Status kepemilikan lahan tempat tinggal </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b3_r1b_bdt', $opsi_b3_r1b, $b3_r1b, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Luas lantai </label>
			<div class="col-sm-2">
				<?php echo form_input('b3_r2_bdt', $b3_r2, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div> 
			<span class="col-sm-4 control-label" style="text-align: left;"> m2 </span>
		</div>
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jenis lantai terluas </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b3_r3_bdt', $opsi_b3_r3, $b3_r3, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jenis dinding terluas </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b3_r4a_bdt', $opsi_b3_r4a, $b3_r4a, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jenis atap terluas </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b3_r5a_bdt', $opsi_b3_r5a, $b3_r5a, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>	
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kualitas dinding terluas </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r4b_bdt', $opsi_b3_r4b, $b3_r4b, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kualitas atap terluas </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r5b_bdt', $opsi_b3_r5b, $b3_r5b, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah kamar tidur </label>
			<div class="col-sm-2">
				<?php echo form_input('b3_r6_bdt', $b3_r6, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Sumber air minum </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r7_bdt', $opsi_b3_r7, $b3_r7, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Cara memperoleh air minum </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r8_bdt', $opsi_b3_r8, $b3_r8, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Penggunaan fasilitas buang air besar </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b3_r11a_bdt', $opsi_b3_r11a, $b3_r11a, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jenis kloset </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r11b_bdt', $opsi_b3_r11b, $b3_r11b, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Tempat pembuangan akhir tinja </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b3_r12_bdt', $opsi_b3_r12, $b3_r12, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Sumber penerangan utama </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b3_r9a_bdt', $opsi_b3_r9a, $b3_r9a, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Daya listrik terpasang (PLN) </label>
			<div class="col-sm-4">
				<?php echo form_dropdown('b3_r9b_bdt', $opsi_b3_r9b, $b3_r9b, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Bahan bakar untuk memasak </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b3_r10_bdt', $opsi_b3_r10, $b3_r10, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan tabung gas 5.5 kg atau lebih </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1a_bdt', $opsi_binary, $b5_r1a, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan sambungan telepon (PSTN) </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1e_bdt', $opsi_binary, $b5_r1e, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan komputer/laptop </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1h_bdt', $opsi_binary_ii, $b5_r1h, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Sepeda </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1i_bdt', $opsi_binary, $b5_r1i, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Sepeda motor </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1j_bdt', $opsi_binary_ii, $b5_r1j, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Mobil </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1k_bdt', $opsi_binary, $b5_r1k, 'class="form-control input-sm"'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Perahu </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1l_bdt', $opsi_binary_ii, $b5_r1l, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Motor tempel </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1m_bdt', $opsi_binary, $b5_r1m, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Perahu motor </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1n_bdt', $opsi_binary_ii, $b5_r1n, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Kapal </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1o_bdt', $opsi_binary, $b5_r1o, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Lemari es/kulkas </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1b_bdt', $opsi_binary_ii, $b5_r1b, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan AC (penyejuk udara) </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1c_bdt', $opsi_binary, $b5_r1c, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Pemanas air (water heater) </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1d_bdt', $opsi_binary_ii, $b5_r1d, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Televisi </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1f_bdt', $opsi_binary_ii, $b5_r1f, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan Emas/perhiasan/tabungan senilai 10 gram emas </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r1g_bdt', $opsi_binary, $b5_r1g, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah nomor HP aktif </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r2a_bdt', $b5_r2a, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah TV layar datar minimal 30inchi </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r2b_bdt', $b5_r2b, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan lahan </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r3a1_bdt', $opsi_binary, $b5_r3a1, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Luas lahan yang dimiliki </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r3a2_bdt', $b5_r3a2, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
			<span class="col-sm-4 control-label" style="text-align: left;"> m2 </span>
		</div> 
	</div> 
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepemilikan rumah di lokasi lain </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r3b_bdt', $opsi_binary_ii, $b5_r3b, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Ada anggota rumah tangga yang memiliki usaha sendiri/bersama </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r5a_bdt', $opsi_binary, $b5_r5a, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div> 
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah ternak sapi yang dimilki </label>
			<div class="col-sm-3">
				<?php echo form_input('b5_r4a_bdt', $b5_r4a, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah ternak kerbau yang dimilki </label>
			<div class="col-sm-3">
				<?php echo form_input('b5_r4b_bdt', $b5_r4b, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah ternak kuda yang dimilki </label>
			<div class="col-sm-3">
				<?php echo form_input('b5_r4c_bdt', $b5_r4c, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah ternak babi yang dimilki </label>
			<div class="col-sm-3">
				<?php echo form_input('b5_r4d_bdt', $b5_r4d, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Jumlah ternak kambing/domba yang dimilki </label>
			<div class="col-sm-3">
				<?php echo form_input('b5_r4e_bdt', $b5_r4e, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki KKS/KPS </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6a_bdt', $opsi_binary, $b5_r6a, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Peserta program PKH </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6g_bdt', $opsi_binary, $b5_r6g, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Peserta program Raskin </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6h_bdt', $opsi_binary_ii, $b5_r6h, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Peserta program KUR </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6i_bdt', $opsi_binary, $b5_r6i, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Kepesertaan KB </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r9_bdt', $opsi_binary, $b5_r9, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Usia kawin pertama suami </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r8a_bdt', $b5_r8a, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
			<span class="col-sm-4 control-label" style="text-align: left;"> tahun </span>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Usia kawin pertama istri </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r8b_bdt', $b5_r8b, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
			<span class="col-sm-4 control-label" style="text-align: left;"> tahun </span>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Metode/jenis kontrasepsi yang digunakan </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r10_bdt', $opsi_b5_r10, $b5_r10, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Lama menggunakan kontrasepsi </label>
			<div class="col-sm-2">
				<?php echo form_input('b5_r11a_bdt', $b5_r11a, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
			<span class="col-sm-2 control-label" style="text-align: left;"> tahun </span> 
			<div class="col-sm-2">
				<?php echo form_input('b5_r11b_bdt', $b5_r11b, 'class="form-control input-sm" style="text-transform:uppercase" required'); ?> 
			</div>
			<span class="col-sm-2 control-label" style="text-align: left;"> bulan </span>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Tempat pelayanan KB yang sering digunakan </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b5_r12_bdt', $opsi_b5_r12, $b5_r12, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Keinginan punya anak lagi </label>
			<div class="col-sm-6">
				<?php echo form_dropdown('b5_r13_bdt', $opsi_b5_r13, $b5_r13, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Alasan tidak mengikuti KB </label>
			<div class="col-sm-5">
				<?php echo form_dropdown('b5_r14_bdt', $opsi_b5_r14, $b5_r14, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki KIP/BSM </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6b_bdt', $opsi_binary_ii, $b5_r6b, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki KIS/BPJS Kesehatan/Jamkesmas </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6c_bdt', $opsi_binary, $b5_r6c, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki BPJS Kesehatan peserta mandiri </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6d_bdt', $opsi_binary_ii, $b5_r6d, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki Jamsostek/BPJS ketenagakerjaan </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6e_bdt', $opsi_binary, $b5_r6e, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
		<div class="col-sm-6">
			<label class="col-sm-4 control-label">Memiliki Asuransi kesehatan lainnya </label>
			<div class="col-sm-3">
				<?php echo form_dropdown('b5_r6f_bdt', $opsi_binary_ii, $b5_r6f, 'class="form-control input-sm" required'); ?>
			</div>
		</div> 
	</div>

</div><!-- /.box-body -->