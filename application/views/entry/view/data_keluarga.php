<?php /* ?><div class="callout callout-info">
	<h4>Perhatian!</h4>
	<p>Harap daftarkan Rumah Tangga terlebih dahulu sebelum mendaftarkan Individu Keluarga</p>
</div><?php */ ?>

<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><strong>Data Keluarga :</strong></h3>
	</div>
	<div classs="row" style="margin: 15px">
		
		<table width="100%" style="font-size: 0.9em">
			<tr>
				<td width="25%"><b>No Kartu Keluarga</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$no_kk"; ?>&nbsp;&nbsp;&nbsp;
				<?php if ($status_kk == 0 || $status_kk == 3): ?>	
					<a href="<?php echo base_url();?>index.php/entry/daftar?no_kk=<?php echo $no_kk; ?>&amp;nik=<?php echo $nik; ?>&amp;<?php echo get_params($this->input->get(), array('nik', 'no_kk')); ?>" class="ajaxify fa-item tooltips" data-original-title="Ajukan ke Rumah Tangga Miskin" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-info btn-xs">Daftarkan Keluarga <i class="fa fa-clipboard"></i></span></a>
				<?php endif ?>
				</td>
			</tr>
			<tr>
				<td><b>Nama Kepala Keluarga</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$nama_kep"; ?></td>
			</tr>
			<tr>
				<td><b>Alamat</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$alamat, No.RT $no_rt / No.RW. $no_rw"; ?></td>
			</tr>
			<tr>
				<td><b>Kelurahan</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$kelurahan"; ?></td>
			</tr>
			<tr>
				<td><b>Kecamatan</b></td>
				<td>:</td>
				<td>&emsp;<?php echo "$kecamatan"; ?></td>
			</tr>
			<tr>
				<td><b>Status Keluarga Miskin</b></td>
				<td>:</td>
				<td>&emsp;<?php /*echo "$flag_bdt";*/ ?>
				<?php if ($status_kk == 1): ?>
					<span class="label label-default">Dalam Pengajuan</span>
				<?php elseif ($status_kk == 2): ?>
					<span class="label label-warning">Sudah Diproses</span>
				<?php elseif ($status_kk == 3): ?>
					<span class="label label-success">No. KK Masuk Data Pusat</span>
				<?php else: ?>
					<span class="label label-primary">No. KK Tidak Masuk Data Pusat</span>
				<?php endif ?>
				</td>
			</tr>
		</table>
		
	</div>

	<div classs="row" style="margin: 15px">
		<div class="box-body table-responsive no-padding">
			<table width="100%" class="table table-bordered table-striped" style="font-size: 0.9em">
				<thead>
					<tr>
						<th>NO</th>
						<th>NIK</th>
						<th>NAMA LENGKAP</th>
						<th>L/P</th>
						<th>TGL LAHIR</th>
						<th>HUB. KELUARGA</th>
						<th>AGAMA</th>
						<th>PENDIDIKAN</th>
						<th>PEKERJAAN SIAK</th>
						<th>PEKERJAAN</th>
						<th>STATUS</th>
						<th style="text-align:center"><i class="fa fa-tags"></i></th>
					</tr>
				</thead>
				<tbody>
<?php
$no = 1;
$id_rt = $nomor_urut_rumah_tangga;
foreach ($data as $row) {
	extract((array) $row);
	?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo "$nik"; ?></td>
						<td><?php echo "$nama_lgkp"; ?></td>
						<td><?php echo "$kelamin"; ?></td>
						<td><?php echo format_tanggal($tgl_lhr); ?></td>
						<td><?php echo "$shdk"; ?></td>
						<td><?php echo "$agama_desc"; ?></td>
						<td><?php echo "$pendidikan"; ?></td>
						<td><?php echo "$pekerjaan_siak"; ?></td>
						<td><?php echo "$pekerjaan_miskin"; ?></td>
						<td align="left">								
							<?php if ($status == 1): ?>
								<span class="label label-default">Dalam Pengajuan</span> 
							<?php elseif ($status == 2): ?>    
								<span class="label label-warning">Dalam Proses</span> 
							<?php elseif ($status == 3): ?> 
								<span class="label label-success">Pengajuan Diterima</span>   
							<?php elseif ($status == 4): ?> 
								<span class="label label-danger">Pengajuan Ditolak</span>
							<?php elseif ($status == 5): ?> 
								<span class="label label-success">Masuk PBDT 2015</span>
							<?php else: ?> 
								<span class="label label-primary">Tidak ada</span>
							<?php endif ?>
						</td> 
						<td align="center">
						<?php if ($status_kk != 0 && $status_kk != 3): ?>	
							<!-- jika belum masuk BDT -->
							<?php if ($status == 5 || $status == 0): ?> 
								<!-- jika Status dalam pengajuan, muncul tombol delete -->	
									<?php /*if ($stat_hbkel == 1): ?>
										<a href="<?php echo base_url(); ?>index.php/entry/delete?no_kk=<?php echo $no_kk; ?>&amp;stat_hbkel=<?php echo $stat_hbkel; ?>&amp;<?php echo get_params($this->input->get(), array('stat_hbkel')); ?>" class="ajaxify fa-item tooltips delete" data-original-title="Hapus" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Penduduk adalah Kepala Keluarga, seluruh pemohon dalam satu KK akan terhapus. Yakin akan menghapus ?')"><span class="btn btn-block btn-default btn-xs"><i class="fa fa-times"></i></span></a>		
									<?php else: ?> 
										<a href="<?php echo base_url(); ?>index.php/entry/delete?nik=<?php echo $row->nik; ?>&amp;stat_hbkel=<?php echo $stat_hbkel; ?>&amp;<?php echo get_params($this->input->get(), array('nik', 'stat_hbkel')); ?>" class="ajaxify fa-item tooltips delete" data-original-title="Hapus" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-block btn-default btn-xs"><i class="fa fa-times"></i></span></a>
									<?php endif*/ ?>
								<!-- jika kepala keluarga -->
								<?php /*if ($stat_hbkel == 1): ?> 
									<a href="<?php echo base_url();?>index.php/entry/daftar_kepala?nik=<?php echo $row->nik; ?>&amp;<?php echo get_params($this->input->get(), array('nik')); ?>" class="ajaxify fa-item tooltips" data-original-title="Daftarkan Kepala Keluarga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-block btn-default btn-xs">+ <i class="fa fa-clipboard"></i></span></a>

								<!-- jika anggota keluarga -->	
								<?php else: ?> 
									<a href="<?php echo base_url();?>index.php/entry/daftar_anggota?nik=<?php echo $row->nik; ?>&amp;<?php echo get_params($this->input->get(), array('nik')); ?>" class="ajaxify fa-item tooltips" data-original-title="Daftarkan Anggota Keluarga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-block btn-default btn-xs">+ <i class="fa fa-clipboard"></i></span></a>
								<?php endif*/ ?>
								<?php /* ?><a href="<?php echo base_url();?>index.php/entry/daftar_individu?nik=<?php echo $row->nik; ?>&amp;<?php echo get_params($this->input->get(), array('nik')); ?>" class="ajaxify fa-item tooltips" data-original-title="Daftarkan Kepala Keluarga" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-block btn-default btn-xs">+ <i class="fa fa-clipboard"></i></span></a><?php */ ?>
							<?php endif ?>
						<?php endif ?>
			            </td>
					</tr>
<?php 
}
?>
                </tbody>
			</table>
		</div><!-- /.box-body -->
		
		<br/>
		<div class="row">
			<div class="col-md-2">
				<a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/entry/index?<?php echo get_params($this->input->get(), array('no_kk')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
			</div>
		</div><!-- /.row -->
		<br/>
		<?php echo form_close(); ?>
  	</div><!-- /.row-->

</div><!-- /.box -->

