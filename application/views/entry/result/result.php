<?php 
if($error != "") {
	echo validation_errors();
	} else { 

if ($show == 0):?>
  <div class="alert alert-danger" style="text-align: center">
    <h4><span style="font-size: 40px" class="icon fa fa-warning"></span> <br /> Peringatan!</h4>
    Maaf, data tidak ditemukan !!.
  </div>
<?php elseif ($show == 2):?>
  <div class="alert alert-success" style="text-align: center">
      <h4><span style="font-size: 40px" class="icon fa fa-file-text"></span> <br /> Perhatian!</h4>
      Silahkan pilih menu diatas.
    </div>
<?php  ?>
<?php else: ?>
<div class="table-responsive">
<p style="font-size: 0.9em"><i>Menampilkan <b><?php echo count($data); ?></b> dari <b><?php echo $total_data; ?></b> data.</i></p>
	<table width="100%" class="table table-bordered table-striped" style="font-size: 0.9em">
		<thead>
          <tr>
            <th style="text-align:center">NO</th> 
            <th style="text-align:center">NO KK</th>
            <th style="text-align:center">NIK</th>
            <th style="text-align:center">NAMA LENGKAP</th>
            <?php /*?>
            <th style="text-align:center">NIK</th>
            <th style="text-align:center">NAMA LENGKAP</th>
            <?php */?>
            <th style="text-align:center">ALAMAT</th>
            <th style="text-align:center">KELURAHAN</th>
            <th style="text-align:center">KECAMATAN</th>
            <th style="text-align:center"><i class="fa fa-tags"></i></th>
          </tr>
        </thead>
        <tbody>
      	<?php
		$no = 1+$page;
		foreach ($data as $row) {
			$row = keysToLower($row);
			extract((array) $row);
		?>
          <tr>                      
            <td><?php echo $no++; ?></td>
            <td align="center"><?php echo "$no_kk"; ?></td>
            <td align="center"><?php echo "$nik"; ?></td>
            <td><?php echo "$nama_lgkp"; ?></td>
            <?php /* ?>
            <td><?php echo "$nik"; ?></td>
            <td><?php echo "$nama_lgkp"; ?></td>
            <?php */ ?>
            <td><?php echo "$alamat, No.RT/No.RW. $no_rt/$no_rw"; ?></td>
            <td align="center"><?php echo "$kelurahan"; ?></td>
            <td align="center"><?php echo "$kecamatan"; ?></td>
            <td align="center">
              <a href="<?php echo base_url();?>index.php/entry/lihat?no_kk=<?php echo $no_kk; ?>&amp;<?php echo get_params($this->input->get()); ?>" class="ajaxify fa-item tooltips" data-original-title="Lihat Detail" data-toggle="tooltip" data-placement="bottom"><span class="btn btn-default btn-xs"><i class="fa fa-search"></i></span></a>
            </td>
          </tr>
          <?php
          }
          ?>

        </tbody>
	</table>

	

	<?php echo $pagination; ?>
</div>
<?php endif ?>
<?php } ?>