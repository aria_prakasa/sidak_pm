<?php $this->load->view(ADM_VIEW . 'template/header');?>
    <!-- Left side column. contains the logo and sidebar -->

    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->

        <?php $this->load->view(ADM_VIEW . 'template/side');?>

        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php if (isset($page_title) || isset($page_subtitle)): ?>
        <section class="content-header">
            <?php if (isset($page_title)): ?>
            <h1>
                <?php echo $page_title; ?>
                <?php if (isset($page_subtitle)): ?>
                <small><?php echo $page_subtitle; ?></small>
                <?php endif;?>
            </h1>
            <?php endif;?>
            <?php if (isset($page_title)): ?>
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active"><?php echo $page_title; ?></li>
            </ol>
            <?php endif;?>
        </section>
        <?php endif;?>

        <!-- Main content -->
        <section class="content">

                <?php echo $contents; ?>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

<?php $this->load->view(FR_VIEW . 'footer_view');?>