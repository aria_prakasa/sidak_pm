<?php
if ($list):
    keysToLower($list);
    ?>
		<?php //var_dump($list); ?>
		  <p><?php echo anchor($this->curpage . '/add', '<i class="glyphicon glyphicon-pencil"></i> Create New', 'class="btn btn-sm btn-primary"'); ?></p>

		  <div class="table-responsive">
		    <table class="table table-striped table-condensed table-hover table-bordered">
		    <thead>
		      <tr>
		        <th width="2%">No</th>
		        <th>N I P</th>
		        <th>Nama Lengkap</th>
		        <th>Username</th>
                <th>Level</th>
		        <th>Kecamatan</th>
		        <th>Kelurahan</th>
		        <th width="20%"></th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php
    $i = 1;
    foreach ($list as $r) {
        extract(get_object_vars($r));
        $nama_kec = "";
        $nama_kel = "";
        if ($no_kec != "" && $no_kel != "") {
            $Otmp     = new Setup_kel($no_kec, $no_kel);
            $nama_kel = $Otmp->get_name();
        }
        if ($no_kec != "") {
            $Otmp     = new Setup_kec($no_kec);
            $nama_kec = $Otmp->get_name();
        }
        if ($user_level != "") {
            $Otmp     = new Master_user($user_level);
            $level = $Otmp->get_name();
        }
        ?>
		        <tr>
		          <td><?php echo $i; ?></td>
		          <td><?php echo $nip; ?></td>
		          <td><?php echo $nama_lengkap; ?></td>
                  <td><?php echo $username; ?></td>
		          <td><?php echo $level; ?></td>
		          <td><?php echo $nama_kec; ?></td>
		          <td><?php echo $nama_kel; ?></td>
		          <td>
		          <?php echo anchor($this->curpage . '/edit/' . $nip, '<i class="glyphicon glyphicon-edit"></i> Edit', 'class="btn btn-sm btn-warning"'); ?> &nbsp;
		          <?php echo anchor($this->curpage . '/delete/' . $nip, '<i class="glyphicon glyphicon-trash"></i> Delete', 'class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure?\')"'); ?>
		          </td>
		        </tr>
		        <?php
    $i++;
    }
    ?>
		    </tbody>
		    </table>
		  </div>
		<?php
endif;
?>