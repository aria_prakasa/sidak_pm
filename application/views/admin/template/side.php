<?php
$navs = array("home", "users", "setting");
foreach ($navs as $nav) {
    $nav  = $nav . "_nav";
    $$nav = "";
}
$active = 'active';
$uri    = $this->uri->segment(2);
if (!empty($uri)) {
    $nav  = $uri . "_nav";
    $$nav = $active;
} else {
    $home_nav = $active;
}

?>
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>_assets/dist/img/avatar5.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $this->ca->USERNAME; ?></p>
        <?php /* ?>
<a href="#"><i class="fa fa-circle text-success"></i> <?php echo user_desc($this->cu->USER_LEVEL); ?></a>
<?php //*/?>
      </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">NAVIGASI UTAMA</li>

      <li class="<?php echo $home_nav; ?>">
        <a href="<?php echo site_url(ADM_BASE . ''); ?>">
          <i class="glyphicon glyphicon-home"></i> <span> Halaman Utama</span>
        </a>
      </li>

      <li class="<?php echo $users_nav; ?>">
        <a href="<?php echo site_url(ADM_BASE . 'users'); ?>">
          <i class="glyphicon glyphicon-user"></i> <span> Users</span>
        </a>
      </li>

      <li class="<?php echo $setting_nav; ?>">
        <a href="<?php echo site_url(ADM_BASE . 'setting'); ?>">
          <i class="glyphicon glyphicon-cog"></i> <span> Pengaturan</span>
        </a>
      </li>

      <li><?php echo anchor(ADM_BASE . 'home/logout', '<i class="glyphicon glyphicon-off"></i> <span>Logout</span>', 'onclick="return confirm(\'Are you sure?\');"'); ?></li>


    </ul>
</section>