<?php $this->load->view('header_view'); ?>
      <!-- Left side column. contains the logo and sidebar -->
      
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        
        <?php $this->load->view('menu_view'); ?>

        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        
        <!-- Main content -->
        <section class="content">
        <?php $this->load->view($content); ?>
        </section>
        <!-- /.content -->
      </div>      
      <!-- /.content-wrapper -->

<?php $this->load->view('footer_view'); ?>      