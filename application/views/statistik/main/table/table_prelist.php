<div class="table-responsive">
	<table width="100%" class="table table-bordered table-striped table-desa" style="font-size: 0.9em">
		<thead>
          <tr>
            <th>NO</th> 
            <th>NO. KK</th> 
            <th>NIK</th> 
            <th>NAMA KEPALA RUMAH TANGGA</th>
            <th>L/P</th>
            <th>TGL LAHIR</th>
            <th>ALAMAT</th>
            <th>NO RT</th>
            <th>NO RW</th>
            <th>KELURAHAN</th>
            <th>KECAMATAN</th>
          </tr>
        </thead>
        <tbody>
      	<?php
		$no = 1+$page;
		foreach ($data as $row) {
			$row = keysToLower($row);
			extract((array) $row);
		?>
          <tr>                      
            <td><?php echo $no++; ?></td>
            <td><?php echo "'".$nik; ?></td>
            <td><?php echo "'".$no_kk; ?></td>
            <td><?php echo "$nama_lgkp"; ?></td>
            <td><?php echo "$kelamin"; ?></td>
            <td><?php echo "'$tgl_lhr"; ?></td>
            <td><?php echo "$alamat"; ?></td>
            <td><?php echo "$no_rt"; ?></td>
            <td><?php echo "$no_rw"; ?></td>
            <td><?php echo "$kelurahan"; ?></td>
            <td><?php echo "$kecamatan"; ?></td>
          </tr>
          <?php
          }
          ?>

        </tbody>
	</table>
</div>
        