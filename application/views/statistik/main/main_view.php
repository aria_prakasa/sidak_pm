<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<div class="box box-danger">
  <div class="box-header with-border">
      <h3 class="box-title"><strong>Statistik Data Penduduk Miskin</strong></h3>
  </div>
  <div class="box-body">
    <?php $this->load->view('template/tpl_filter'); ?>
  </div><!-- /.box body-->

</div><!-- /.box -->

<?php 
if($this->input->get()){ ?>
<?php 
// var_dump($agregat);die;
?>

<!-- Nav tabs -->
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li role="presentation" class="active"><a href="#table" aria-controls="table" role="tab" data-toggle="tab">Table</a></li>
    <li role="presentation"><a href="#grafik" aria-controls="grafik" role="tab" data-toggle="tab">Grafik</a></li>
  </ul>
  <br />

  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="table">
      <div id="result">
        
        <div class="table-responsive">    
        
        <?php 
        if($this->input->get('kecamatan_id'))
        {
            if($this->input->get('kelurahan_id'))
            {
                $this->load->view('statistik/main/tpl_kelurahan'); 
            }
            else 
            {
                $this->load->view('statistik/main/tpl_kecamatan');
            }
        }
        else 
        {
            $this->load->view('statistik/main/tpl_kota'); 
        }           
        
        ?>     

          <div class="row col-sm-12">
          <p class="text-right"><?php echo anchor(current_url()."?".get_params($this->input->get())."&export=1", '<i class="fa fa-download"></i> Download Excel', 'class="btn btn-sm btn-primary" target="_blank"'); ?></p>
          </div>  
        </div>
       
      </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="grafik">
      <?php
        $this->load->view('statistik/main/tpl_bar');
      ?>
    </div>
  </div>
</div>

<?php 
}
?>

<script>
$(document).ready(function() {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      // e.target // newly activated tab
      // e.relatedTarget // previous active tab
      // console.log('clicked');
      drawChart();
      // drawChartPie();
    });
});
</script>