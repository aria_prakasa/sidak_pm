<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title"><strong>Tampil Data :</strong></h3>
  </div>

  <div class="box-body">
    <div id="result">

        <?php 
        // var_dump('masuk');die;
         // var_dump($show); 

        if($error != "") {
        	echo validation_errors();
        	} else { 

        if ($show==0):?>
          <div class="alert alert-danger" style="text-align: center">
            <h4><span style="font-size: 40px" class="icon fa fa-warning"></span> <br /> Peringatan!</h4>
            Maaf, data tidak ditemukan !!.
          </div>
        <?php else: ?>
        <div class="table-responsive">
          <div class="row">
            <div class="col-sm-10">
              <p style="font-size: 0.9em"><i>Menampilkan <b><?php echo count($data); ?></b> dari <b><?php echo $total_data; ?></b> data.</i></p>
            </div>
            <div class="col-sm-2">
              <p class="text-right">
                <?php echo anchor(current_url()."?".get_params($this->input->get())."&export=1", '<i class="fa fa-download"></i> Download Excel', 'class="btn btn-sm btn-primary" target="_blank"'); ?>
                <?php /*echo '<button type="submit" name="export" value="1" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Download Excel</button>'*/ ?>
              </p>
            </div>
          </div>

        	<table width="100%" class="table table-bordered table-striped table-desa" style="font-size: 0.9em">
        		<thead>
                  <tr>
                    <th>NO</th> 
                    <th>NIK</th> 
                    <th>NAMA LENGKAP</th>
                    <th>ALAMAT</th>
                    <th>KELURAHAN</th>
                    <th>KECAMATAN</th>
                  </tr>
                </thead>
                <tbody>
              	<?php
        		$no = 1+$page;
        		foreach ($data as $row) {
        			$row = keysToLower($row);
        			extract((array) $row);
        		?>
                  <tr>                      
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $nik; ?></td>
                    <td><?php echo "$nama_lgkp"; ?></td>
                    <td><?php echo "$alamat, No.RT/No.RW. $no_rt/$no_rw"; ?></td>
                    <td><?php echo "$kelurahan"; ?></td>
                    <td><?php echo "$kecamatan"; ?></td>
                  </tr>
                  <?php
                  }
                  ?>

                </tbody>
        	</table>

        	

        	<?php echo $pagination; ?>
        </div>
        <?php endif ?>
        <?php } ?>

    </div>
    <br />
    <div class="row">
      <div class="col-md-2">
        <a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/statistik/index?<?php echo get_params($this->input->get(), array('kode', 'page', 'no_kec', 'no_kel')) ?>" > <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
      </div>
    </div><!-- /.row -->
  </div><!-- /.box body-->

</div><!-- /.box -->