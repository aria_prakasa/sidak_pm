<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', { 'packages': ['bar'] });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

      var id = "#rekap";

      // hasilnya seperti di atas
      var tmp_arr = [];
      var tmp_value_arr = [];
      var i = 0;
      $(id + ' thead tr th.grafik').each(function(index, el) {
          if (!$(this).hasClass('total')) {
              tmp_arr.push($(this).text());
          }
      });
      tmp_value_arr[i] = tmp_arr;

      // console.log(tmp_value_arr);

      $(id + ' tbody tr').each(function(index, el) {
          tmp_arr = [];
          i++;
          $(this).find('td.grafik').each(function(index, el) {
              if (!$(this).hasClass('total')) {
                  if ($(this).hasClass('value')) {
                      tmp_arr.push(parseInt($(this).text().replace(",", "")));
                  } else {
                      tmp_arr.push($(this).text());
                  }
              }
          });
          tmp_value_arr[i] = tmp_arr;
      });

      console.log(tmp_value_arr);

      var data = google.visualization.arrayToDataTable(tmp_value_arr);

      var options = {
          chart: {
              title: '',
              subtitle: '',
          },
          bars: 'vertical' // Required for Material Bar Charts.
      };

      var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

      chart.draw(data, options);
  }
</script>

<div id="columnchart_material" style="width: 100%; height: 500px;"></div>

