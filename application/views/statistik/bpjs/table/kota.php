<?php 
if($agregat): ?>
<p> DATA AGREGAT PENDUDUK KATEGORI BPJS<br>KOTA PASURUAN
</p>
<!-- TAMPIL KABUPATEN -->
<table id="rekap" class="table table-bordered table-striped" style="font-size: 0.9em">
  <thead>
    <tr>
      <th style="text-align: center">#</th>
      <th class="grafik title" style="text-align: center">NAMA KECAMATAN</th>
      <th class="grafik value" style="text-align: center">LAKI-LAKI</th>
      <th class="grafik value" style="text-align: center">PEREMPUAN</th>
      <th class="grafik total" style="text-align: center">JUMLAH</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
foreach ($agregat as $row) {
  extract((array) $row);
  $total_lk += $lk;
  $total_pr += $pr;
  $total_jumlah += $jumlah;
  ?>
    <tr>
      <th style="text-align: center" scope="row"><?php echo $no++; ?></th>
      <td class="grafik title"><?php echo "$nama_kec"; ?></td>
      <td class="grafik value" style="text-align: center"><?php echo $lk; ?></td>
      <td class="grafik value" style="text-align: center"><?php echo $pr; ?></td>
      <td class="grafik total" style="text-align: center"><?php echo $lk+$pr; ?></td>
    </tr>
<?php
}
?>
  </tbody>
  <tfoot>
    <tr>
      <th class="grafik title" style="text-align: center" colspan="2">TOTAL</th>
      <th class="grafik value" style="text-align: center"><?php echo $total_lk; ?></th>
      <th class="grafik value" style="text-align: center"><?php echo $total_pr; ?></th>
      <th class="grafik total" style="text-align: center"><?php echo $total_lk+$total_pr; ?></th>
    </tr>
  </tfoot>
</table>

<?php 
elseif($agregatAgama):
  // var_dump($agregatAgama); die;
?>
<p> DATA AGREGAT PENDUDUK KATEGORI BPJS<br>KOTA PASURUAN<br>BERDASARKAN AGAMA </p>

<table id="rekap" class="table table-bordered table-striped" style="font-size: 0.9em">          
  <thead>
    <tr class="text-center">
      <th style="text-align: center">#</th>
      <th class="grafik title" style="text-align: center">NAMA KECAMATAN</th>
      <th class="grafik value" style="text-align: center">ISLAM</th>
    <th class="grafik value" style="text-align: center">KRISTEN</th>
    <th class="grafik value" style="text-align: center">KATHOLIK</th>
    <th class="grafik value" style="text-align: center">HINDU</th>
    <th class="grafik value" style="text-align: center">BUDHA</th>
    <th class="grafik value" style="text-align: center">KONGHUCHU</th>
    <th class="grafik value" style="text-align: center">LAINNYA</th>
    <th class="grafik total" style="text-align: center">JUMLAH</th>
    </tr>
  </thead>
    
  <tbody>
  <?php   
      $no=1;
      foreach ($agregatAgama as $row)
      {
        extract((array) $row);
        $total_islam += $islam;
        $total_kristen += $kristen;
        $total_katholik += $katholik;
        $total_hindu += $hindu;
        $total_budha += $budha;
        $total_konghuchu += $konghuchu;
        $total_lainnya += $lainnya;     
        
        $total_jumlah += $jumlah;
        ?>
    <tr>                      
      <td><?php echo $no++; ?></td>
      <td class="grafik title"><?php echo "$nama_kec"; ?></td>
      <td class="grafik value" style='text-align:center'><?php echo format_angka($islam); ?></td>
    <td class="grafik value" style='text-align:center'><?php echo format_angka($kristen); ?></td>
    <td class="grafik value" style='text-align:center'><?php echo format_angka($katholik); ?></td>
    <td class="grafik value" style='text-align:center'><?php echo format_angka($hindu); ?></td>
    <td class="grafik value" style='text-align:center'><?php echo format_angka($budha); ?></td>
    <td class="grafik value" style='text-align:center'><?php echo format_angka($konghuchu); ?></td>
    <td class="grafik value" style='text-align:center'><?php echo format_angka($lainnya); ?></td>
    <td class="grafik total" style='text-align:center'><?php echo format_angka($jumlah); ?></td>
    </tr>
        <?php
      }
      ?>
                            
  </tbody>
  <tfoot>
    <tr>
      <th class="grafik title" colspan="2" style="text-align:center">TOTAL</th>
      <th class="grafik value" style="text-align:center"><?php echo format_angka($total_islam); ?></th>
      <th class="grafik value" style="text-align:center"><?php echo format_angka($total_kristen); ?></th>
      <th class="grafik value" style="text-align:center"><?php echo format_angka($total_katholik); ?></th>
      <th class="grafik value" style="text-align:center"><?php echo format_angka($total_hindu); ?></th>
      <th class="grafik value" style="text-align:center"><?php echo format_angka($total_budha); ?></th>
      <th class="grafik value" style="text-align:center"><?php echo format_angka($total_konghuchu); ?></th>
      <th class="grafik value" style="text-align:center"><?php echo format_angka($total_lainnya); ?></th>
      <th class="grafik total" style="text-align:center"><?php echo format_angka($total_jumlah); ?></th>
    </tr>
  </tfoot>
</table>

<?php 
elseif($agregatPkrjn):
?>
<p> DATA AGREGAT PENDUDUK KATEGORI BPJS<br>KOTA PASURUAN<br>BERDASARKAN PEKERJAAN </p>
<table id="rekap" class="table table-bordered table-striped" style="font-size: 0.9em">  
  <thead>
    <tr class="text-center">
      <th style="text-align: center">#</th>
      <th class="grafik title" style="text-align: center">JENIS PEKERJAAN</th>
      <th class="grafik value" style="text-align: center">LAKI-LAKI</th>
      <th class="grafik value" style="text-align: center">PEREMPUAN</th>
      <th class="grafik total" style="text-align: center">JUMLAH</th>
    </tr>
    </thead>
    
  <tbody>
  <?php   
      $no=1;
      foreach ($agregatPkrjn as $row)
      {
        extract((array) $row);
        $total_lk += $lk;
      $total_pr += $pr;
      $total_jumlah += $jumlah;
    ?>
    <tr>                      
        <td><?php echo $no++; ?></td>
        <td class="grafik title"><?php echo "$pekerjaan"; ?></td>
        <td class="grafik value" style="text-align:center"><?php echo format_angka($lk); ?></td>
        <td class="grafik value" style="text-align:center"><?php echo format_angka($pr); ?></td>
        <td class="grafik total" style="text-align:center"><?php echo format_angka($lk+$pr); ?></td>
    </tr>
        <?php
      }
      ?>
                      
  </tbody>
  <tfoot>
    <tr>
      <th class="grafik title" colspan="2" style="text-align:center">TOTAL</th>
      <th class="grafik value" style="text-align: center"><?php echo format_angka($total_lk); ?></th>
      <th class="grafik value" style="text-align: center"><?php echo format_angka($total_pr); ?></th>
      <th class="grafik total" style="text-align: center"><?php echo format_angka($total_lk+$total_pr); ?></th>
    </tr>
  </tfoot>
</table>

<?php 
elseif($agregatGoldrh):
?>
  <div class="box-body">
    <div class="table-responsive">  
      <div id="agregat_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
          </br>
            <table id="agregatGoldrh" class="table table-bordered table-striped">
            <?php if(!$agregatGoldrh): ?>
                  <tr>
                    <td colspan="5">
                    <?php echo "TIDAK ADA DATA !!"; ?>
                    </td>
                  </tr>
                <?php else: ?>
                <thead>
                  <tr class="text-center">
                    <th width="5%">NO</th> 
                    <th class="chart title">NAMA KECAMATAN</th>
                    <th class="chart value">A</th>
              <th class="chart value">B</th>
              <th class="chart value">AB</th>
              <th class="chart value">O</th>
              <th class="chart value">A+</th>
              <th class="chart value">A-</th>
              <th class="chart value">B+</th>
              <th class="chart value">B-</th>
              <th class="chart value">AB+</th>
              <th class="chart value">AB-</th>
              <th class="chart value">O+</th>
              <th class="chart value">O-</th>
              <th class="chart value">TDK TAHU</th>
              <th class="chart total">JUMLAH</th>
                  </tr>
                </thead>
                
                <tbody>
                  <?php   
                    $no=1;
                    foreach ($agregatGoldrh as $row)
                    {
                      extract((array) $row);
                      ?>
                  <tr>                      
                        <td><?php echo $no++; ?></td>
                        <td class="chart title"><?php echo "$nama_kec"; ?></td>
                        <td class="chart value" style='text-align:right'><?php echo number_format($a); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($b); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($ab); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($o); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($a_plus); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($a_min); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($b_plus); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($b_min); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($ab_plus); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($ab_min); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($o_plus); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($o_min); ?></td>
              <td class="chart value" style='text-align:right'><?php echo number_format($tdk_tahu); ?></td>
              <td class="chart total" style='text-align:right'><?php echo number_format($jumlah); ?></td>
                  </tr>
                      <?php
                    }
                    ?>
                <?php endif; ?>
                                          
                </tbody>
                <tfoot>
              <tr>
                <th class="chart title" colspan="2" style="text-align:center">TOTAL</th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart total" style="text-align:right"></th>
              </tr>
            </tfoot>
                </table>
            </div>
        </div>
      </div>
    </div>  
    </div>
<?php 
elseif($agregatPddkan):
?>
<p> DATA AGREGAT PENDUDUK KATEGORI BPJS<br>KOTA PASURUAN<br>BERDASARKAN TINGKAT PENDIDIKAN </p>
<table id="rekap" class="table table-bordered table-striped" style="font-size: 0.9em">  
    <thead>
    <tr class="text-center">
      <th width="5%">NO</th> 
      <th class="grafik title">NAMA KECAMATAN</th>
      <th class="grafik value">TDK SKLH</th>
      <th class="grafik value">BLM TMT SD</th>
      <th class="grafik value">TMT SD</th>
      <th class="grafik value">SLTP</th>
      <th class="grafik value">SLTA</th>
      <th class="grafik value">D I</th>
      <th class="grafik value">D III</th>
      <th class="grafik value">S I</th>
      <th class="grafik value">S II</th>
      <th class="grafik value">S III</th>
      <th class="grafik total">JUMLAH</th>
    </tr>
    </thead>
    
    <tbody>
    <?php   
        $no=1;
        foreach ($agregatPddkan as $row)
        {
          extract((array) $row);
          $total_tdk_sklh += $tdk_sklh;
        $total_blm_tmt_sd += $blm_tmt_sd;
        $total_tmt_sd += $tmt_sd;
        $total_sltp += $sltp;
        $total_slta += $slta;
        $total_d_i += $d_i;
        $total_d_iii += $d_iii;
        $total_s_i += $s_i;
        $total_s_ii += $s_ii;
        $total_s_iii += $s_iii;
        
        $total_jumlah += $jumlah;
    ?>
    <tr>                      
      <td><?php echo $no++; ?></td>
      <td class="grafik title"><?php echo "$nama_kec"; ?></td>
      <td class="grafik value" style='text-align:right'><?php echo format_angka($tdk_sklh); ?></td>
      <td class="grafik value" style='text-align:right'><?php echo format_angka($blm_tmt_sd); ?></td>
      <td class="grafik value" style='text-align:right'><?php echo format_angka($tmt_sd); ?></td>
      <td class="grafik value" style='text-align:right'><?php echo format_angka($sltp); ?></td>
      <td class="grafik value" style='text-align:right'><?php echo format_angka($slta); ?></td>
      <td class="grafik value" style='text-align:right'><?php echo format_angka($d_i); ?></td>
      <td class="grafik value" style='text-align:right'><?php echo format_angka($d_iii); ?></td>
      <td class="grafik value" style='text-align:right'><?php echo format_angka($s_i); ?></td>
      <td class="grafik value" style='text-align:right'><?php echo format_angka($s_ii); ?></td>
      <td class="grafik value" style='text-align:right'><?php echo format_angka($s_iii); ?></td>
      <td class="grafik total" style='text-align:right'><?php echo format_angka($jumlah); ?></td>
    </tr>
          <?php
        }
        ?>
                              
    </tbody>
    <tfoot>
    <tr>
      <th class="grafik title" colspan="2" style="text-align:center">TOTAL</th>
      <th class="grafik value" style='text-align:right'><?php echo format_angka($total_tdk_sklh); ?></th>
      <th class="grafik value" style='text-align:right'><?php echo format_angka($total_blm_tmt_sd); ?></th>
      <th class="grafik value" style='text-align:right'><?php echo format_angka($total_tmt_sd); ?></th>
      <th class="grafik value" style='text-align:right'><?php echo format_angka($total_sltp); ?></th>
      <th class="grafik value" style='text-align:right'><?php echo format_angka($total_slta); ?></th>
      <th class="grafik value" style='text-align:right'><?php echo format_angka($total_d_i); ?></th>
      <th class="grafik value" style='text-align:right'><?php echo format_angka($total_d_iii); ?></th>
      <th class="grafik value" style='text-align:right'><?php echo format_angka($total_s_i); ?></th>
      <th class="grafik value" style='text-align:right'><?php echo format_angka($total_s_ii); ?></th>
      <th class="grafik value" style='text-align:right'><?php echo format_angka($total_s_iii); ?></th>
      <th class="grafik total" style='text-align:right'><?php echo format_angka($total_jumlah); ?></th>
    </tr>
  </tfoot>
</table>
            
<?php 
elseif($agregatSHDK):
?>
  <div class="box-body">
    <div class="table-responsive">
      <div id="agregat_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
          </br>
            <table id="agregatSHDK" class="table table-bordered table-striped">
            <?php if(!$agregatSHDK): ?>
                  <tr>
                    <td colspan="5">
                    <?php echo "TIDAK ADA DATA !!"; ?>
                    </td>
                  </tr>
                <?php else: ?>
                <thead>
                  <tr class="text-center">
                    <th width="5%">NO</th> 
                    <th class="chart title">NAMA KECAMATAN</th>
                    <th class='chart value'>KEPALA_KELUARGA</th>
              <th class='chart value'>SUAMI</th>
              <th class='chart value'>ISTRI</th>
              <th class='chart value'>ANAK</th>
              <th class='chart value'>MENANTU</th>
              <th class='chart value'>CUCU</th>
              <th class='chart value'>ORANG_TUA</th>
              <th class='chart value'>MERTUA</th>
              <th class='chart value'>FAMILI_LAIN</th>
              <th class='chart value'>PEMBANTU</th>
              <th class='chart value'>LAINNYA</th>
              <th class="chart total">JUMLAH</th>
                  </tr>
                </thead>
                
                <tbody>
                  <?php   
                    $no=1;
                    foreach ($agregatSHDK as $row)
                    {
                      extract((array) $row);
                      ?>
                  <tr>                      
                        <td><?php echo $no++; ?></td>
                        <td class="chart title"><?php echo "$nama_kec"; ?></td>
                        <td class='chart value' style='text-align:right'><?php echo number_format($kepala_keluarga); ?></td>
              <td class='chart value' style='text-align:right'><?php echo number_format($suami); ?></td>
              <td class='chart value' style='text-align:right'><?php echo number_format($istri); ?></td>
              <td class='chart value' style='text-align:right'><?php echo number_format($anak); ?></td>
              <td class='chart value' style='text-align:right'><?php echo number_format($menantu); ?></td>
              <td class='chart value' style='text-align:right'><?php echo number_format($cucu); ?></td>
              <td class='chart value' style='text-align:right'><?php echo number_format($orang_tua); ?></td>
              <td class='chart value' style='text-align:right'><?php echo number_format($mertua); ?></td>
              <td class='chart value' style='text-align:right'><?php echo number_format($famili_lain); ?></td>
              <td class='chart value' style='text-align:right'><?php echo number_format($pembantu); ?></td>
              <td class='chart value' style='text-align:right'><?php echo number_format($lainnya); ?></td>
              <td class="chart total" style='text-align:right'><?php echo number_format($jumlah); ?></td>
                  </tr>
                      <?php
                    }
                    ?>
                <?php endif; ?>
                                          
                </tbody>
                <tfoot>
              <tr>
                <th class="chart title" colspan="2" style="text-align:center">TOTAL</th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart value" style="text-align:right"></th>
                <th class="chart total" style="text-align:right"></th>
                <th class="chart total" style="text-align:right"></th>
              </tr>
            </tfoot>
                </table>
            </div>
        </div>
      </div>
    </div>
    </div>
<?php 
elseif($agregatUmur):
?>
<p> DATA AGREGAT PENDUDUK KATEGORI BPJS<br>KOTA PASURUAN<br>BERDASARKAN UMUR </p>
<table id="rekap" class="table table-bordered table-striped" style="font-size: 0.9em">  
  <thead>
    <tr class="text-center">
      <th width="5%">NO</th> 
      <th class="grafik title">STRUKTUR UMUR</th>
      <th class="grafik value" width="15%">LAKI-LAKI</th>
      <th class="grafik value" width="15%">PEREMPUAN</th>
      <th class="grafik total" width="15%">JUMLAH</th>
    </tr>
  </thead>

  <tbody>
  <?php   
  $no=1;
  foreach ($agregatUmur as $row)
  {
  extract((array) $row);
        $total_lk += $lk;
      $total_pr += $pr;
      $total_jumlah += $jumlah;
    ?>
    <tr>                      
        <td><?php echo $no++; ?></td>
        <td class="grafik title" ><?php echo "$struktur_umur"; ?></td>
        <td class="grafik value" style="text-align:center"><?php echo format_angka($lk); ?></td>
        <td class="grafik value" style="text-align:center"><?php echo format_angka($pr); ?></td>
        <td class="grafik total" style="text-align:center"><?php echo format_angka($lk+$pr); ?></td>
    </tr>
        <?php
      }
      ?>
                      
  </tbody>
  <tfoot>
    <tr>
      <th class="grafik title" colspan="2" style="text-align:center">TOTAL</th>
      <th class="grafik value" style="text-align: center"><?php echo format_angka($total_lk); ?></th>
      <th class="grafik value" style="text-align: center"><?php echo format_angka($total_pr); ?></th>
      <th class="grafik total" style="text-align: center"><?php echo format_angka($total_lk+$total_pr); ?></th>
    </tr>
  </tfoot>
</table>

<?php endif ?>