<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_wilayah extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function ambil_provinsi() {
        $this->db->where('NO_PROP', NO_PROP);
        $sql_prov = $this->db->get('SETUP_PROP');
        if ($sql_prov->num_rows() > 0) {
            foreach ($sql_prov->result_array() as $row) {
                // $result['']= '- Pilih Provinsi -';
                $result[$row['NO_PROP']] = ucwords(strtolower($row['NAMA_PROP']));
            }
            return $result;
        }
    }

    public function ambil_kabupaten() {
        $this->db->where('NO_PROP', NO_PROP);
        $this->db->where('NO_KAB', NO_KAB);
        $this->db->order_by('NO_KAB', 'ASC');
        $sql_kabupaten = $this->db->get('SETUP_KAB');
        if ($sql_kabupaten->num_rows() > 0) {

            foreach ($sql_kabupaten->result_array() as $row) {
                // $result['']= '- Pilih Kota/Kab -';
                $result[$row['NO_KAB']] = ucwords(strtolower($row['NAMA_KAB']));
            }
        } else {
            $result[''] = '- Belum Ada Kabupaten -';
        }
        return $result;
    }

    public function ambil_kecamatan($in_array = null) {
        $this->db->where('NO_PROP', NO_PROP);
        $this->db->where('NO_KAB', NO_KAB);
        $this->db->order_by('NO_KEC', 'ASC');
        $sql_kecamatan = $this->db->get('SETUP_KEC');
        if ($sql_kecamatan->num_rows() > 0) {
            foreach ($sql_kecamatan->result_array() as $row) {
                if(is_array($in_array) && count($in_array) > 0){
                    if(!in_array($row['NO_KEC'], $in_array)) {
                        continue;
                    }
                } else {
                    $result[''] = '- Pilih Kecamatan -';
                }
                $result[$row['NO_KEC']] = ucwords(strtolower($row['NAMA_KEC']));
            }
        } else {
            $result[''] = '- Belum Ada Kecamatan -';
        }
        return $result;
    }

    public function ambil_kelurahan($kode_kec = 0, $in_array = null) {

        $this->db->where('NO_PROP', NO_PROP);
        $this->db->where('NO_KAB', NO_KAB);
        if (empty($kode_kec)) {
            $result[''] = '- Pilih Kecamatan Terlebih Dahulu -';
        }
        $this->db->where('NO_KEC', $kode_kec);
        $this->db->order_by('NO_KEL', 'ASC');
        $sql_kelurahan = $this->db->get('SETUP_KEL');
        if ($sql_kelurahan->num_rows() > 0) {

            foreach ($sql_kelurahan->result_array() as $row) {
                // var_dump(json_encode($in_array));
                // return $kode_kec."-".$row['NO_KEL'];
                if(is_array($in_array) && count($in_array) > 0){
                    if(!in_array($kode_kec."-".$row['NO_KEL'], $in_array)) {
                        continue;
                    }
                } else {
                    $result[''] = '- Pilih Kelurahan -';
                }
                $result[$row['NO_KEC'] . "-" . $row['NO_KEL']] = ucwords(strtolower($row['NAMA_KEL']));
            }
        } else {
            $result[''] = '- Pilih Kecamatan Terlebih Dahulu -';
        }
        return $result;
    }

    /*public function getDataWilayah($params = array()) {

        if (count($params) <= 0) {
            return false;
        }

        // $data = null;
        // foreach ($params as $key => $value) {
        //  $key = strtoupper($key);
        //  $data[$key] = $value;
        // }

        $data = array_change_key_case($params, CASE_UPPER);


        if ($data['NO_KEC']) {
            if ($data['NO_KEL']) {
                $this->db->select('KELURAHAN, 
                    NO_KEL', false);
                $this->db->where('NO_KEL', $data['NO_KEL']);
                $this->db->group_by('KELURAHAN');
                $this->db->group_by('NO_KEL');
            }
            $this->db->select('KECAMATAN,
                    NO_KEC,
                    SUM(TOT)AS TOT, 
                    SUM(LK) AS LK, 
                    SUM(PR) AS PR,
                    SUM(KK) AS KK'
                    , false);
            $this->db->where('NO_KEC', $data['NO_KEC']);
            $this->db->group_by('KECAMATAN');   
            $this->db->group_by('NO_KEC');                  
        }
        else{
            $this->db->select("SUM(TOT)AS TOT, 
                    SUM(LK) AS LK, 
                    SUM(PR) AS PR,
                    SUM(KK) AS KK", false);
        }
        $this->db->where('NO_KAB', NO_KAB);
        $this->db->where('NO_PROP', NO_PROP);
        $query = $this->db->get('VIEW_DATA_WILAYAH');
        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }
    }*/

    
}
