<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_home extends CI_model
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        // $this->db_pullout = $this->load->database('pullout', true);
    }

    public function getdetailMiskin()
    {

        $query = $this->db->get('OVERVIEW_BIO_MISKIN');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }

    public function getheaderPddk()
    {
        // $this->db->select('JML_KK', FALSE);
        $query = $this->db->get('OVERVIEW_KK');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }

    public function getdetailPddk()
    {

        $query = $this->db->get('OVERVIEW_BIODATA');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }

    public function getmohonMiskin()
    {

        $query = $this->db->get('OVERVIEW_MOHON');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }

}
