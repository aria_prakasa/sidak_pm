<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup_kel extends Otable{

    public $table_name = "SETUP_KEL";
    public $row = NULL;
    public $ID = NULL;

    public function __construct($KEC_ID=NULL, $ID=NULL, $type="ID")
    {        
        if(!empty($KEC_ID) && !empty($ID))
        {
            $arr = array("NO_PROP" => NO_PROP, "NO_KAB" => NO_KAB, "NO_KEC" => $KEC_ID, "NO_KEL" => $ID);
            if($type != "ID") $arr = array("URL_TITLE" => $ID);
            // var_dump($arr);
            // $res = $this->db->get_where($this->table_name,$arr);
            if(is_array($arr) && count($arr) > 0)
                $this->db->where($arr);
            $res = $this->db->get($this->table_name);
            if(!emptyres($res))
            {
                $this->row = $res->row();
                $this->ID = $this->row->NO_KEL;
            }
        }
    }

    function setup($row=NULL)
    {
        if(is_object($row) && intval($row->NO_KEL) > 0)
        {
            $this->row = $row;
            $this->ID = $row->NO_KEL;
        }
        return FALSE;
    }

    function get_name()
    {
        return $this->row->NAMA_KEL;
    }

    function get_penduduk_totals($table_suffix="", $add_params=NULL, $type="all")
    {
        $params = array("filter_by" => "jenis_kelamin");
        if( ! empty($add_params) && count($add_params) > 0)
        {
            $params = array_merge($add_params,$params);
        }
        $table_data = $this->get_list_filter($params,$table_suffix,$type);
        $total = $male = $female = 0;
        extract(array_values($table_data)[0]);

        $ret = array(
                        "total_penduduk" => $total,
                        "total_male" => $male,
                        "total_female" => $female
                        );
        return json_encode($ret);
    }

    function get_kk_total()
    {
        $arr = array("NO_PROP" => intval($this->row->NO_PROP),
                        "NO_KAB" => intval($this->row->NO_KAB),
                        "NO_KEC" => intval($this->row->NO_KEC),
                        "NO_KEL" => intval($this->ID)
                        );
        $this->db->where($arr);
        $this->db->from('DATA_KELUARGA');
        $ret = $this->db->count_all_results();

        return $ret;
    }

    function get_data_keluarga($start=0,$limit=0,$orderby = "NO_KK ASC",$where = "")
    {
        $arr = array("NO_PROP" => intval($this->row->NO_PROP),
                        "NO_KAB" => intval($this->row->NO_KAB),
                        "NO_KEC" => intval($this->row->NO_KEC),
                        "NO_KEL" => intval($this->ID)
                        );
        $this->db->where($arr);

        if(trim($where) != "") $this->db->where($where,NULL,FALSE);
        if(trim($orderby) != "") $this->db->order_by($orderby);
        if(intval($limit) > 0) $this->db->limit($limit, $offset);

        $res = $this->db->get('DATA_KELUARGA');

        if(emptyres($res)) return FALSE;
        return $res->result();
    }

    function get_biodata_wni($start=0,$limit=0,$orderby = "STAT_HBKEL ASC, NIK ASC",$where = "")
    {
        $arr = array("NO_PROP" => intval($this->row->NO_PROP),
                        "NO_KAB" => intval($this->row->NO_KAB),
                        "NO_KEC" => intval($this->row->NO_KEC),
                        "NO_KEL" => intval($this->ID)
                        );
        $this->db->where($arr);

        if(trim($where) != "") $this->db->where($where,NULL,FALSE);
        if(trim($orderby) != "") $this->db->order_by($orderby);
        if(intval($limit) > 0) $this->db->limit($limit, $offset);

        $res = $this->db->get('BIODATA_WNI');

        if(emptyres($res)) return FALSE;
        return $res->result();
    }

    function get_biodata_wni_arr($start=0,$limit=0,$orderby = "STAT_HBKEL ASC, NIK ASC",$where = "")
    {
        $list = $this->get_biodata_wni($start,$limit,$orderby,$where);
        if( ! $list) return FALSE;
        
        $arr = NULL;
        foreach($list as $r)
        {
            $arr[$r->NIK] = $r->NIK." - ".$r->NAMA_LGKP;
        }
        return $arr;
    }

    function get_list_filter($params=NULL,$table_suffix="",$type="items")
    {
        $params['no_prop'] = $this->row->NO_PROP;
        $params['no_kab'] = $this->row->NO_KAB;
        $params['no_kec'] = $this->row->NO_KEC;
        $params['no_kel'] = $this->ID;
        $params['filter_kel'] = $this->row->NO_PROP."-".$this->row->NO_KAB."-".$this->row->NO_KEC."-".$this->ID;
        // $params['filter_by'] = "jenis_kelamin";
        $this->load->model('oagregat');
        $oa = new Oagregat;
        return $oa->get_data($params,$table_suffix,$type);
    }

    function drop_down_select_by_kec($no_prop,$no_kab,$no_kec,$name,$selval,$optional = "",$default="",$readonly="")
    {
        if(empty($no_prop) || empty($no_kab) || empty($no_kec)) return;
        
        $list = $this->get_list(0, 0, "NO_PROP ASC, NO_KAB ASC, NO_KEC ASC, NO_KEL ASC", "NO_PROP=".intval($no_prop)." AND NO_KAB=".intval($no_kab)." AND NO_KEC=".intval($no_kec));
        if( ! $list) return FALSE;
        
        foreach($list as $r)
        {
            // $arr[$r->NO_PROP."-".$r->NO_KAB."-".$r->NO_KEC."-".$r->NO_KEL] = "[".$r->NO_PROP."-".$r->NO_KAB."-".$r->NO_KEC."-".$r->NO_KEL."] ".$r->NAMA_KEL;
            $arr[$r->NO_PROP."-".$r->NO_KAB."-".$r->NO_KEC."-".$r->NO_KEL] = $r->NAMA_KEL;
        }
        return dropdown($name,$arr,$selval,$optional,$default,$readonly);
    }

    function get_link()
    {
        extract(get_object_vars($this->row));
        return "/kelurahan/{$NO_PROP}-{$NO_KAB}-{$NO_KEC}-{$NO_KEL}";
    }


    /* LAMPID */

    // function get_lampid_lahir($start=0,$limit=0,$orderby = "id ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_lahir');

    //   if(emptyres($res)) return FALSE;
    //   return $res->result();
    // }
    // function get_lampid_lahir_total($where = "")
    // {
    //   $this->dbd->select("count(*) as total", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);

    //   $res = $this->dbd->get('lampid_lahir');
    //   //var_dump($this->dbd->last_query());

    //   if(emptyres($res)) return FALSE;
    //   return $res->row()->total;
    // }
    // function get_penduduk_lampid_lahir($start=0,$limit=0,$orderby = "id ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_lahir');
    //   //var_dump($this->dbd->last_query());

    //   if(!emptyres($res))
    //   {
    //     $results = $res->result();
    //     foreach ($results as $penduduk_row)
    //     {
    //       if(intval($penduduk_row->kelamin_id) == 1) {
    //         $total_penduduk_pria++;
    //       }
    //       else {
    //         $total_penduduk_wanita++;
    //       }
    //       $total_penduduk++;
    //     }
    //   }
    //   $ret = array(
    //           "total_penduduk" => $total_penduduk,
    //           "total_male" => $total_penduduk_pria,
    //           "total_female" => $total_penduduk_wanita
    //           );
    //   return json_encode($ret);
    // }

    // function get_lampid_mati($start=0,$limit=0,$orderby = "ID ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_mati');

    //   if(emptyres($res)) return FALSE;
    //   return $res->result();
    // }
    // function get_lampid_mati_total($where = "")
    // {
    //   $this->dbd->select("count(*) as total", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);

    //   $res = $this->dbd->get('lampid_mati');

    //   if(emptyres($res)) return FALSE;
    //   return $res->row()->total;
    // }
    // function get_penduduk_lampid_mati($start=0,$limit=0,$orderby = "id ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_mati');

    //   if(!emptyres($res))
    //   {
    //     $results = $res->result();
    //     foreach ($results as $penduduk_row)
    //     {
    //       if(intval($penduduk_row->kelamin_id) == 1) {
    //         $total_penduduk_pria++;
    //       }
    //       else {
    //         $total_penduduk_wanita++;
    //       }
    //       $total_penduduk++;
    //     }
    //   }
    //   $ret = array(
    //           "total_penduduk" => $total_penduduk,
    //           "total_male" => $total_penduduk_pria,
    //           "total_female" => $total_penduduk_wanita
    //           );
    //   return json_encode($ret);
    // }

    // function get_lampid_pindah($start=0,$limit=0,$orderby = "id ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_pindah');

    //   if(emptyres($res)) return FALSE;
    //   return $res->result();
    // }
    // function get_lampid_pindah_total($where = "")
    // {
    //   $this->dbd->select("count(*) as total", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);

    //   $res = $this->dbd->get('lampid_pindah');

    //   if(emptyres($res)) return FALSE;
    //   return $res->row()->total;
    // }
    // function get_penduduk_lampid_pindah($start=0,$limit=0,$orderby = "id ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_pindah');

    //   if(!emptyres($res))
    //   {
    //     $results = $res->result();
    //     foreach ($results as $penduduk_row)
    //     {
    //       if(intval($penduduk_row->kelamin_id) == 1) {
    //         $total_penduduk_pria++;
    //       }
    //       else {
    //         $total_penduduk_wanita++;
    //       }
    //       $total_penduduk++;
    //     }
    //   }
    //   $ret = array(
    //           "total_penduduk" => $total_penduduk,
    //           "total_male" => $total_penduduk_pria,
    //           "total_female" => $total_penduduk_wanita
    //           );
    //   return json_encode($ret);
    // }

    // function get_lampid_datang($start=0,$limit=0,$orderby = "id ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_datang');

    //   if(emptyres($res)) return FALSE;
    //   return $res->result();
    // }
    // function get_lampid_datang_total($where = "")
    // {
    //   $this->dbd->select("count(*) as total", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);

    //   $res = $this->dbd->get('lampid_datang');

    //   if(emptyres($res)) return FALSE;
    //   return $res->row()->total;
    // }
    // function get_penduduk_lampid_datang($start=0,$limit=0,$orderby = "id ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->row->NO_KEC),
    //           "no_kel" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_datang');

    //   if(!emptyres($res))
    //   {
    //     $results = $res->result();
    //     foreach ($results as $penduduk_row)
    //     {
    //       if(intval($penduduk_row->kelamin_id) == 1) {
    //         $total_penduduk_pria++;
    //       }
    //       else {
    //         $total_penduduk_wanita++;
    //       }
    //       $total_penduduk++;
    //     }
    //   }
    //   $ret = array(
    //           "total_penduduk" => $total_penduduk,
    //           "total_male" => $total_penduduk_pria,
    //           "total_female" => $total_penduduk_wanita
    //           );
    //   return json_encode($ret);
    // }
    
}