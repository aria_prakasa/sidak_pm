<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup_kec extends Otable {

    public $table_name = "SETUP_KEC";
    public $row = null;
    public $ID = null;

    public function __construct($ID = null, $type = "ID")
    {
        if(!empty($ID))
        {
            $arr = array("NO_PROP" => NO_PROP, "NO_KAB" => NO_KAB, "NO_KEC" => intval($ID));
            if($type != "ID") $arr = array("URL_TITLE" => $ID);
            // var_dump($arr);
            // $res = $this->db->get_where($this->table_name,$arr);
            if(is_array($arr) && count($arr) > 0)
                $this->db->where($arr);
            $res = $this->db->get($this->table_name);
            if(!emptyres($res))
            {
                $this->row = $res->row();
                $this->ID = $this->row->NO_KEC;
            }
        }
    }

    function setup($row=NULL)
    {
        if(is_object($row) && intval($row->NO_KEC) > 0)
        {
            $this->row = $row;
            $this->ID = $row->NO_KEC;
        }
        return FALSE;
    }

    function get_name()
    {
        if(!$this->row) return FALSE;
        return $this->row->NAMA_KEC;
    }

    function get_penduduk_totals($table_suffix="", $add_params=NULL, $type="all")
    {
        $params = array("filter_by" => "jenis_kelamin");
        if( ! empty($add_params) && count($add_params) > 0)
        {
            $params = array_merge($add_params,$params);
        }
        $table_data = $this->get_list_filter($params,$table_suffix,$type);
        // var_dump($table_data); die;
        $total = $male = $female = 0;
        extract(array_values($table_data)[0]);

        $ret = array(
                        "total_penduduk" => $total,
                        "total_male" => $male,
                        "total_female" => $female
                        );
        return json_encode($ret);
    }

    function get_kk_total()
    {
        $arr = array("NO_PROP" => intval($this->row->NO_PROP),
                        "NO_KAB" => intval($this->row->NO_KAB),
                        "NO_KEC" => intval($this->ID)
                        );
        $this->db->where($arr);
        $this->db->from('DATA_KELUARGA');
        $ret = $this->db->count_all_results();

        return $ret;
    }

    function get_kel($offset=0,$limit=0,$orderby="",$where="")
    {
        $where_arr = array(
            "NO_PROP = ".intval($this->row->NO_PROP),
            "NO_KAB = ".intval($this->row->NO_KAB),
            "NO_KEC = ".intval($this->ID)
            );
        if(!empty($where)) $where_arr[] = $where;
        $this->load->model('setup_kel');
        return $this->setup_kel->get_list($offset,$limit,$orderby,implode(" AND ", $where_arr));
    }

    function get_kel_total($where="")
    {
        $where_arr = array(
            "NO_PROP = ".intval($this->row->NO_PROP),
            "NO_KAB = ".intval($this->row->NO_KAB),
            "NO_KEC = ".intval($this->ID)
            );
        if(!empty($where)) $where_arr[] = $where;
        $this->load->model('setup_kel');
        return $this->setup_kel->get_list_total(implode(" AND ", $where_arr));
    }

    function is_my_kel($kel_id=NULL)
    {
        if(empty($kel_id)) return FALSE;

        $where_arr = array(
            "NO_PROP = ".intval($this->row->NO_PROP),
            "NO_KAB = ".intval($this->row->NO_KAB),
            "NO_KEC = ".intval($this->ID),
            "NO_KEL = ".intval($kel_id)
            );
        $this->load->model('setup_kel');
        return $this->setup_kel->get_list($offset,$limit,$orderby,implode(" AND ", $where_arr));
    }

    function kel_drop_down_select($name,$selval,$optional="",$default="",$readonly="")
    {
        $list = $this->get_kel();
        if( ! $list) return FALSE;
        
        $O = new Setup_kel;
        foreach($list as $r)
        {
            $O->setup($r);
            $arr[$O->ID] = "[".$O->ID."] ".$O->get_name();
        }
        unset($O);
        return dropdown($name,$arr,$selval,$optional,$default,$readonly);
    }

    function get_list_filter($params=NULL,$table_suffix="",$type="")
    {
        $params['no_prop'] = $this->row->NO_PROP;
        $params['no_kab'] = $this->row->NO_KAB;
        $params['no_kec'] = $this->ID;
        $params['filter_kec'] = $this->row->NO_PROP."-".$this->row->NO_KAB."-".$this->ID;
        // $params['filter_by'] = "jenis_kelamin";
        // var_dump($params); die;
        $this->load->model('oagregat');
        $oa = new Oagregat;
        return $oa->get_data($params,$table_suffix,$type);
    }

    function drop_down_select_by_kab($no_prop,$no_kab,$name,$selval,$optional = "",$default="",$readonly="")
    {
        if(empty($no_prop) || empty($no_kab)) return;
        
        $list = $this->get_list(0, 0, "NO_PROP ASC, NO_KAB ASC, NO_KEC ASC", "NO_PROP=".intval($no_prop)." AND NO_KAB=".intval($no_kab));
        if( ! $list) return FALSE;
        
        foreach($list as $r)
        {
            // $arr[$r->NO_PROP."-".$r->NO_KAB."-".$r->NO_KEC] = "[".$r->NO_PROP."-".$r->NO_KAB."-".$r->NO_KEC."] ".$r->NAMA_KEC;
            $arr[$r->NO_PROP."-".$r->NO_KAB."-".$r->NO_KEC] = $r->NAMA_KEC;
        }
        return dropdown($name,$arr,$selval,$optional,$default,$readonly);
    }

    function get_link()
    {
        extract(get_object_vars($this->row));
        return "/kecamatan/{$NO_PROP}-{$NO_KAB}-{$NO_KEC}";
    }


    /* LAMPID */

    // function get_lampid_lahir($start=0,$limit=0,$orderby = "id ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_lahir');
    //   // var_dump($this->dbd->last_query());

    //   if(emptyres($res)) return FALSE;
    //   return $res->result();
    // }
    // function get_lampid_lahir_total($where = "")
    // {
    //   $this->dbd->select("count(*) as total", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);

    //   $res = $this->dbd->get('lampid_lahir');
    //   //var_dump($this->dbd->last_query());

    //   if(emptyres($res)) return FALSE;
    //   return $res->row()->total;
    // }

    // function get_lampid_mati($start=0,$limit=0,$orderby = "ID ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_mati');

    //   if(emptyres($res)) return FALSE;
    //   return $res->result();
    // }
    // function get_lampid_mati_total($where = "")
    // {
    //   $this->dbd->select("count(*) as total", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);

    //   $res = $this->dbd->get('lampid_mati');

    //   if(emptyres($res)) return FALSE;
    //   return $res->row()->total;
    // }

    // function get_lampid_pindah($start=0,$limit=0,$orderby = "id ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_pindah');

    //   if(emptyres($res)) return FALSE;
    //   return $res->result();
    // }
    // function get_lampid_pindah_total($where = "")
    // {
    //   $this->dbd->select("count(*) as total", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);

    //   $res = $this->dbd->get('lampid_pindah');

    //   if(emptyres($res)) return FALSE;
    //   return $res->row()->total;
    // }

    // function get_lampid_datang($start=0,$limit=0,$orderby = "id ASC",$where = "")
    // {
    //   $this->dbd->select("SQL_CALC_FOUND_ROWS *", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);
    //   if(trim($orderby) != "") $this->dbd->order_by($orderby);
    //   if(intval($limit) > 0) $this->dbd->limit($limit, $offset);

    //   $res = $this->dbd->get('lampid_datang');

    //   if(emptyres($res)) return FALSE;
    //   return $res->result();
    // }
    // function get_lampid_datang_total($where = "")
    // {
    //   $this->dbd->select("count(*) as total", FALSE);
    //   $arr = array("no_prop" => intval($this->row->NO_PROP),
    //           "no_kab" => intval($this->row->NO_KAB),
    //           "no_kec" => intval($this->ID)
    //           );
    //   $this->dbd->where($arr);

    //   if(trim($where) != "") $this->dbd->where($where,NULL,FALSE);

    //   $res = $this->dbd->get('lampid_datang');

    //   if(emptyres($res)) return FALSE;
    //   return $res->row()->total;
    // }

 
}