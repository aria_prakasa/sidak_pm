<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_s2_home extends CI_model
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        // $this->db_pullout = $this->load->database('pullout', true);
    }

    public function getAgregat($kd_kec, $kd_kel) {
        
        if ($kd_kec) {
            $select_kec = 'GETNAMAKEC (NO_KEC, NO_KAB, NO_PROP) NAMA_KEC, NO_KEC,';
            $where_kec = 'AND NO_KEC = '.$kd_kec;
            $group_kec = 'NO_KEC,';
            if ($kd_kel) {
                $select_kel = 'GETNAMAKEL (NO_KEL,NO_KEC,NO_KAB,NO_PROP) NAMA_KEL, NO_KEL,';
                $where_kel = 'AND NO_KEL = '.$kd_kel;
                $group_kel = 'NO_KEL,';
            }
        }
        $query = $this->db->query("SELECT ".$select_kec." ".$select_kel."
                      COUNT (DECODE (JENIS_KLMIN, 1, 1)) LK,
                      COUNT (DECODE (JENIS_KLMIN, 2, 2)) PR,
                      COUNT (*) JUMLAH,
                      NO_PROP,
                      NO_KAB
                 FROM BIODATA_PENDUDUK
                WHERE FLAG_STATUS IN ('0') AND NO_PROP = 35 AND NO_KAB = 75 ".$where_kec." ".$where_kel."
             GROUP BY ".$group_kel." ".$group_kec."NO_KAB, NO_PROP");

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getKepala($kd_kec, $kd_kel) {
        
        if ($kd_kec) {
            $select_kec = 'GETNAMAKEC (NO_KEC, NO_KAB, NO_PROP) NAMA_KEC, NO_KEC,';
            $where_kec = 'AND NO_KEC = '.$kd_kec;
            $group_kec = 'NO_KEC,';
            if ($kd_kel) {
                $select_kel = 'GETNAMAKEL (NO_KEL,NO_KEC,NO_KAB,NO_PROP) NAMA_KEL, NO_KEL,';
                $where_kel = 'AND NO_KEL = '.$kd_kel;
                $group_kel = 'NO_KEL,';
            }
        }
        $query = $this->db->query("SELECT ".$select_kec." ".$select_kel."
                      COUNT (DECODE (JENIS_KLMIN, 1, 1)) LK,
                      COUNT (DECODE (JENIS_KLMIN, 2, 2)) PR,
                      COUNT (*) JUMLAH,
                      NO_PROP,
                      NO_KAB
                 FROM BIODATA_PENDUDUK
                WHERE     FLAG_STATUS IN ('0')
                      AND STAT_HBKEL IN (1)
                      AND NO_PROP = 35
                      AND NO_KAB = 75 ".$where_kec." ".$where_kel."
             GROUP BY ".$group_kel." ".$group_kec."NO_KAB, NO_PROP");

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getMiskin($kd_kec, $kd_kel) {
        
        if ($kd_kec) {
            $select_kec = 'GETNAMAKEC (NO_KEC, NO_KAB, NO_PROP) NAMA_KEC, NO_KEC,';
            $where_kec = 'AND NO_KEC = '.$kd_kec;
            $group_kec = 'NO_KEC,';
            if ($kd_kel) {
                $select_kel = 'GETNAMAKEL (NO_KEL,NO_KEC,NO_KAB,NO_PROP) NAMA_KEL, NO_KEL,';
                $where_kel = 'AND NO_KEL = '.$kd_kel;
                $group_kel = 'NO_KEL,';
            }
        }
        $query = $this->db->query("SELECT ".$select_kec." ".$select_kel."
                      COUNT (DECODE (JENIS_KLMIN, 1, 1)) LK,
                      COUNT (DECODE (JENIS_KLMIN, 2, 2)) PR,
                      COUNT (*) JUMLAH,
                      NO_PROP,
                      NO_KAB
                 FROM S2_DATA_INDUK
                WHERE NO_PROP = 35
                      AND NO_KAB = 75 ".$where_kec." ".$where_kel."
             GROUP BY ".$group_kel." ".$group_kec."NO_KAB, NO_PROP");

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getKepalaMiskin($kd_kec, $kd_kel) {
        
        if ($kd_kec) {
            $select_kec = 'GETNAMAKEC (NO_KEC, NO_KAB, NO_PROP) NAMA_KEC, NO_KEC,';
            $where_kec = 'AND NO_KEC = '.$kd_kec;
            $group_kec = 'NO_KEC,';
            if ($kd_kel) {
                $select_kel = 'GETNAMAKEL (NO_KEL,NO_KEC,NO_KAB,NO_PROP) NAMA_KEL, NO_KEL,';
                $where_kel = 'AND NO_KEL = '.$kd_kel;
                $group_kel = 'NO_KEL,';
            }
        }
        $query = $this->db->query("SELECT ".$select_kec." ".$select_kel."
                      COUNT (DECODE (JENIS_KLMIN, 1, 1)) LK,
                      COUNT (DECODE (JENIS_KLMIN, 2, 2)) PR,
                      COUNT (*) JUMLAH,
                      NO_PROP,
                      NO_KAB
                 FROM S2_DATA_INDUK
                WHERE STAT_HBKEL IN (1) 
                      AND NO_PROP = 35
                      AND NO_KAB = 75 ".$where_kec." ".$where_kel."
             GROUP BY ".$group_kel." ".$group_kec."NO_KAB, NO_PROP");

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getMohon($kd_kec, $kd_kel) {
        
        if ($kd_kec) {
            $select_kec = 'GETNAMAKEC (NO_KEC, NO_KAB, NO_PROP) NAMA_KEC, NO_KEC,';
            $where_kec = 'AND NO_KEC = '.$kd_kec;
            $group_kec = 'NO_KEC,';
            if ($kd_kel) {
                $select_kel = 'GETNAMAKEL (NO_KEL,NO_KEC,NO_KAB,NO_PROP) NAMA_KEL, NO_KEL,';
                $where_kel = 'AND NO_KEL = '.$kd_kel;
                $group_kel = 'NO_KEL,';
            }
        }
        $query = $this->db->query("SELECT ".$select_kec." ".$select_kel."
                      COUNT (DECODE (JENIS_KLMIN_RUMAHTANGGA, 1, 1)) LK,
                      COUNT (DECODE (JENIS_KLMIN_RUMAHTANGGA, 2, 2)) PR,
                      COUNT (*) JUMLAH,
                      NO_PROP,
                      NO_KAB
                 FROM SKRINING_RUMAHTANGGA
                WHERE NO_PROP = 35
                      AND NO_KAB = 75 ".$where_kec." ".$where_kel."
             GROUP BY ".$group_kel." ".$group_kec."NO_KAB, NO_PROP");

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getProses($kd_kec, $kd_kel) {
        
        if ($kd_kec) {
            $select_kec = 'GETNAMAKEC (NO_KEC, NO_KAB, NO_PROP) NAMA_KEC, NO_KEC,';
            $where_kec = 'AND NO_KEC = '.$kd_kec;
            $group_kec = 'NO_KEC,';
            if ($kd_kel) {
                $select_kel = 'GETNAMAKEL (NO_KEL,NO_KEC,NO_KAB,NO_PROP) NAMA_KEL, NO_KEL,';
                $where_kel = 'AND NO_KEL = '.$kd_kel;
                $group_kel = 'NO_KEL,';
            }
        }
        $query = $this->db->query("SELECT ".$select_kec." ".$select_kel."
                      COUNT (DECODE (JENIS_KLMIN_RUMAHTANGGA, 1, 1)) LK,
                      COUNT (DECODE (JENIS_KLMIN_RUMAHTANGGA, 2, 2)) PR,
                      COUNT (*) JUMLAH,
                      NO_PROP,
                      NO_KAB
                 FROM SKRINING_RUMAHTANGGA
                WHERE VERIFIKASI NOT IN (1) 
                      AND NO_PROP = 35
                      AND NO_KAB = 75 ".$where_kec." ".$where_kel."
             GROUP BY ".$group_kel." ".$group_kec."NO_KAB, NO_PROP");

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getVerifikasi($kd_kec, $kd_kel) {
        
        if ($kd_kec) {
            $select_kec = 'GETNAMAKEC (NO_KEC, NO_KAB, NO_PROP) NAMA_KEC, NO_KEC,';
            $where_kec = 'AND NO_KEC = '.$kd_kec;
            $group_kec = 'NO_KEC,';
            if ($kd_kel) {
                $select_kel = 'GETNAMAKEL (NO_KEL,NO_KEC,NO_KAB,NO_PROP) NAMA_KEL, NO_KEL,';
                $where_kel = 'AND NO_KEL = '.$kd_kel;
                $group_kel = 'NO_KEL,';
            }
        }
        /*$query = $this->db->query("SELECT ".$select_kec." ".$select_kel."
                      COUNT (DECODE (JENIS_KLMIN_RUMAHTANGGA, 1, 1)) LK,
                      COUNT (DECODE (JENIS_KLMIN_RUMAHTANGGA, 2, 2)) PR,
                      COUNT (*) JUMLAH,
                      NO_PROP,
                      NO_KAB
                 FROM SKRINING_RUMAHTANGGA
                WHERE VERIFIKASI NOT IN (1) 
                      AND NO_PROP = 35
                      AND NO_KAB = 75 ".$where_kec." ".$where_kel."
             GROUP BY ".$group_kel." ".$group_kec."NO_KAB, NO_PROP");*/
        
        $query = $this->db->query("SELECT ".$select_kec." ".$select_kel."
                      COUNT (DECODE (JENIS_KLMIN_RUMAHTANGGA, 1, 1)) LK,
                      COUNT (DECODE (JENIS_KLMIN_RUMAHTANGGA, 2, 2)) PR,
                      COUNT (*) JUMLAH,
                      NO_PROP,
                      NO_KAB
                 FROM S2_GETDATA_PRELIST
                WHERE ((VERIFIKASI IN (2) AND NO_KK_INDUK IN (SELECT NO_KK FROM LIST_RUMAHTANGGA))
                      OR VERIFIKASI IN (3))   
                      AND NO_PROP = 35
                      AND NO_KAB = 75 ".$where_kec." ".$where_kel."
             GROUP BY ".$group_kel." ".$group_kec."NO_KAB, NO_PROP");

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getPenetapan($kd_kec, $kd_kel) {
        
        if ($kd_kec) {
            $select_kec = 'GETNAMAKEC (NO_KEC, NO_KAB, NO_PROP) NAMA_KEC, NO_KEC,';
            $where_kec = 'AND NO_KEC = '.$kd_kec;
            $group_kec = 'NO_KEC,';
            if ($kd_kel) {
                $select_kel = 'GETNAMAKEL (NO_KEL,NO_KEC,NO_KAB,NO_PROP) NAMA_KEL, NO_KEL,';
                $where_kel = 'AND NO_KEL = '.$kd_kel;
                $group_kel = 'NO_KEL,';
            }
        }
        /*$query = $this->db->query("SELECT ".$select_kec." ".$select_kel."
                      COUNT (DECODE (VERIFIKASI, 2, 1)) LOLOS,
                      COUNT (DECODE (VERIFIKASI, 3, 2)) TIDAK_LOLOS,
                      COUNT (*) JUMLAH,
                      NO_PROP,
                      NO_KAB
                 FROM SKRINING_RUMAHTANGGA
                WHERE VERIFIKASI NOT IN (1) 
                      AND NO_PROP = 35
                      AND NO_KAB = 75 ".$where_kec." ".$where_kel."
             GROUP BY ".$group_kel." ".$group_kec."NO_KAB, NO_PROP");*/

        $query = $this->db->query("SELECT ".$select_kec." ".$select_kel."
                      COUNT (DECODE (VERIFIKASI, 2, 1)) LOLOS,
                      COUNT (DECODE (VERIFIKASI, 3, 2)) TIDAK_LOLOS,
                      COUNT (*) JUMLAH,
                      NO_PROP,
                      NO_KAB
                 FROM S2_GETDATA_PRELIST
                WHERE ((VERIFIKASI IN (2) AND NO_KK_INDUK IN (SELECT NO_KK FROM LIST_RUMAHTANGGA))
                      OR VERIFIKASI IN (3))   
                      AND NO_PROP = 35
                      AND NO_KAB = 75 ".$where_kec." ".$where_kel."
             GROUP BY ".$group_kel." ".$group_kec."NO_KAB, NO_PROP");

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    /*public function getdetailMiskin()
    {

        $query = $this->db->get('OVERVIEW_BIO_MISKIN');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }

    public function getheaderPddk()
    {
        // $this->db->select('JML_KK', FALSE);
        $query = $this->db->get('OVERVIEW_KK');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }

    public function getdetailPddk()
    {

        $query = $this->db->get('OVERVIEW_BIODATA');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }

    public function getmohonMiskin()
    {

        $query = $this->db->get('OVERVIEW_MOHON');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }

        return false;
    }
*/
}
