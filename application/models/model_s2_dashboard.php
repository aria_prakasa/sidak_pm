    <?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_s2_dashboard extends CI_model {

    public function __construct() {
        parent::__construct();
        //Do your magic here
        // $this->db_pullout = $this->load->database('pullout', true);
    }

    public function getAgregat($kd_kec, $kd_kel) {
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if ($kd_kec) {
            $this->db->where('NO_KEC', $kd_kec);
            if ($kd_kel) {
                $this->db->where('NO_KEL', $kd_kel);
                $this->db->select('NAMA_KEC,NO_KEC,NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
            } else {
                $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
            }
        } else {
            $this->db->select('SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
        }

        $query = $this->db->get('V_STAT_AGR');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getKepala($kd_kec, $kd_kel) {
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if ($kd_kec) {
            $this->db->where('NO_KEC', $kd_kec);
            if ($kd_kel) {
                $this->db->where('NO_KEL', $kd_kel);
                $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
            } else {
                $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
            }
        } else {
            $this->db->select('SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
        }

        $query = $this->db->get('V_STAT_KEPALA');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }


    public function getMiskin($kd_kec, $kd_kel) {
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if ($kd_kec) {
            $this->db->where('NO_KEC', $kd_kec);
            if ($kd_kel) {
                $this->db->where('NO_KEL', $kd_kel);
                $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
            } else {
                $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
            }
        } else {
            $this->db->select('SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
        }

        $query = $this->db->get('V_STAT_PUSAT');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getKepalaMiskin($kd_kec, $kd_kel) {
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if ($kd_kec) {
            $this->db->where('NO_KEC', $kd_kec);
            if ($kd_kel) {
                $this->db->where('NO_KEL', $kd_kel);
                $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
            } else {
                $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
            }
        } else {
            $this->db->select('SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
        }

        $query = $this->db->get('V_STAT_KEPALA_MISKIN');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getMohon($kd_kec, $kd_kel) {
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if ($kd_kec) {
            $this->db->where('NO_KEC', $kd_kec);
            if ($kd_kel) {
                $this->db->where('NO_KEL', $kd_kel);
                $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
            } else {
                $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
            }
        } else {
            $this->db->select('SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
        }

        $query = $this->db->get('V_STAT_MOHON');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getProses($kd_kec, $kd_kel) {
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if ($kd_kec) {
            $this->db->where('NO_KEC', $kd_kec);
            if ($kd_kel) {
                $this->db->where('NO_KEL', $kd_kel);
                $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
            } else {
                $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
            }
        } else {
            $this->db->select('SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
        }

        $query = $this->db->get('V_STAT_PROSES');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getVerifikasi($kd_kec, $kd_kel) {
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if ($kd_kec) {
            $this->db->where('NO_KEC', $kd_kec);
            if ($kd_kel) {
                $this->db->where('NO_KEL', $kd_kel);
                $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
            } else {
                $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
            }
        } else {
            $this->db->select('SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
        }

        $query = $this->db->get('V_STAT_VERIFIKASI');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }

    public function getPenetapan($kd_kec, $kd_kel) {
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if ($kd_kec) {
            $this->db->where('NO_KEC', $kd_kec);
            if ($kd_kel) {
                $this->db->where('NO_KEL', $kd_kel);
                $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
            } else {
                $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
            }
        } else {
            $this->db->select('SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
        }

        $query = $this->db->get('V_STAT_PENETAPAN');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row_array());
        }

        return false;
    }
}
