<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_security extends CI_model
{

    public function getsecurity()
    {
        if (!$this->session->userdata('username')) {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

}
