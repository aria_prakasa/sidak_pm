<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_s2_proses extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function getBiodataMohon($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by(array('NO_KEC', 'NO_KEL', 'NO_KK', 'NAMA'));
		// $this->db->order_by('NO_KK', 'ASC');
		$query = $this->db->get('S2_VIEW_PROSES_IND');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getBiodataMohonTotal($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		$this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_PROSES_IND');

		// var_dump($this->db->last_query());die;
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	public function getRumahTanggaMohon($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_RUMAHTANGGA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		$this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_PENGAJUAN_RT');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getRumahTanggaMohonTotal($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_RUMAHTANGGA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		$this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_PENGAJUAN_RT');

		// var_dump($this->db->last_query());die;
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	public function getRumahTanggaProses($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_RUMAHTANGGA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_PROSES_MAIN');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getRumahTanggaProsesTotal($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_RUMAHTANGGA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_PROSES_MAIN');

		// var_dump($this->db->last_query());die;
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	public function getIndividu($nik) {

		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('PRS_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getRumahTangga($no_kk) {

		$this->db->where(array('NO_KK_RUMAHTANGGA' => $no_kk));
		$query = $this->db->get('PRS_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	/*public function insertIndividu($nik, $petugas){
	    
	    $res = $this->db->query("INSERT INTO PRS_INDIVIDU (KODEWILAYAH,
                          NO_PROP,
                          NO_KAB,
                          NO_KEC,
                          NO_KEL,
                          PROVINSI,
                          KABUPATEN,
                          KECAMATAN,
                          DESA,
                          NO_ART,
                          NAMA,
                          NIK,
                          B4_K3,
                          B4_K4,
                          B4_K5,
                          B4_K6,
                          B4_K7,
                          B4_K8,
                          B4_K9,
                          B4_K10,
                          B4_K11,
                          B4_K12,
                          B4_K13,
                          B4_K14,
                          B4_K15,
                          B4_K16,
                          B4_K17,
                          B4_K18,
                          B4_K19A,
                          B4_K19B,
                          B4_K20,
                          B4_K21,
                          NOMOR_URUT_RUMAH_TANGGA,
                          TGL_PERMOHONAN,
                          PETUGAS_PERMOHONAN,
                          NO_KK,
                          NO_KK_RUMAH_TANGGA,
                          PROSES,
						  NIK_RUMAHTANGGA,
						  NAMA_KEP_RUMAHTANGGA)
   SELECT KODEWILAYAH,
          NO_PROP,
          NO_KAB,
          NO_KEC,
          NO_KEL,
          PROVINSI,
          KABUPATEN,
          KECAMATAN,
          DESA,
          NO_ART,
          NAMA,
          NIK,
          B4_K3,
          B4_K4,
          B4_K5,
          B4_K6,
          B4_K7,
          B4_K8,
          B4_K9,
          B4_K10,
          B4_K11,
          B4_K12,
          B4_K13,
          B4_K14,
          B4_K15,
          B4_K16,
          B4_K17,
          B4_K18,
          B4_K19A,
          B4_K19B,
          B4_K20,
          B4_K21,
          NOMOR_URUT_RUMAH_TANGGA,
          TGL_PERMOHONAN,
          PETUGAS_PERMOHONAN,
          NO_KK,
          NO_KK_RUMAH_TANGGA,
          PROSES,
		  NIK_RUMAHTANGGA,
		  NAMA_KEP_RUMAHTANGGA
     FROM MHN_INDIVIDU
    WHERE NIK = ".$nik);
	    $res = $this->db->query("UPDATE PRS_INDIVIDU set PETUGAS_PROSES = '".$petugas."', PROSES = 2 WHERE NIK = ".$nik);
	    // die(var_dump($this->db->last_query()));

	    return $res;
	}*/

	public function insertIndividu_all($no_kk, $petugas){
	    
	    $res = $this->db->query("INSERT INTO PRS_INDIVIDU (KODEWILAYAH,
                          NO_PROP,
                          NO_KAB,
                          NO_KEC,
                          NO_KEL,
                          PROVINSI,
                          KABUPATEN,
                          KECAMATAN,
                          DESA,
                          NO_ART,
                          NAMA,
                          NIK,
                          B4_K3,
                          B4_K4,
                          B4_K5,
                          B4_K6,
                          B4_K7,
                          B4_K8,
                          B4_K9,
                          B4_K10,
                          B4_K11,
                          B4_K12,
                          B4_K13,
                          B4_K14,
                          B4_K15,
                          B4_K16,
                          B4_K17,
                          B4_K18,
                          B4_K19A,
                          B4_K19B,
                          B4_K20,
                          B4_K21,
                          NOMOR_URUT_RUMAH_TANGGA,
                          TGL_PERMOHONAN,
                          PETUGAS_PERMOHONAN,
                          NO_KK,
                          NO_KK_RUMAHTANGGA,
                          PROSES,
						  NIK_RUMAHTANGGA,
						  NAMA_KEP_RUMAHTANGGA)
   SELECT KODEWILAYAH,
          NO_PROP,
          NO_KAB,
          NO_KEC,
          NO_KEL,
          PROVINSI,
          KABUPATEN,
          KECAMATAN,
          DESA,
          NO_ART,
          NAMA,
          NIK,
          B4_K3,
          B4_K4,
          B4_K5,
          B4_K6,
          B4_K7,
          B4_K8,
          B4_K9,
          B4_K10,
          B4_K11,
          B4_K12,
          B4_K13,
          B4_K14,
          B4_K15,
          B4_K16,
          B4_K17,
          B4_K18,
          B4_K19A,
          B4_K19B,
          B4_K20,
          B4_K21,
          NOMOR_URUT_RUMAH_TANGGA,
          TGL_PERMOHONAN,
          PETUGAS_PERMOHONAN,
          NO_KK,
          NO_KK_RUMAHTANGGA,
          PROSES,
		  NIK_RUMAHTANGGA,
		  NAMA_KEP_RUMAHTANGGA
     FROM MHN_INDIVIDU
    WHERE NO_KK_RUMAHTANGGA = ".$no_kk);
	    // die(var_dump($this->db->last_query()));
	    $res = $this->db->query("UPDATE PRS_INDIVIDU set PETUGAS_PROSES = '".$petugas."', PROSES = 2 WHERE NO_KK_RUMAHTANGGA = ".$no_kk);
	    $res = $this->db->query("UPDATE MHN_INDIVIDU set PROSES = 2 WHERE NO_KK_RUMAHTANGGA = ".$no_kk);
	    return $res;
	}

	public function insertRumahTangga($no_kk, $petugas){
	    
	    $res = $this->db->query("INSERT INTO PRS_RUMAHTANGGA (KODEWILAYAH,
                             NO_PROP,
                             NO_KAB,
                             NO_KEC,
                             NO_KEL,
                             PROVINSI,
                             KABUPATEN,
                             KECAMATAN,
                             DESA,
                             B1_R6,
                             B1_R8,
                             B1_R9,
                             B1_R10,
                             B3_R1A,
                             B3_R1B,
                             B3_R2,
                             B3_R3,
                             B3_R4A,
                             B3_R4B,
                             B3_R5A,
                             B3_R5B,
                             B3_R6,
                             B3_R7,
                             B3_R8,
                             B3_R9A,
                             B3_R9B,
                             B3_R10,
                             B3_R11A,
                             B3_R11B,
                             B3_R12,
                             B5_R1A,
                             B5_R1B,
                             B5_R1C,
                             B5_R1D,
                             B5_R1E,
                             B5_R1F,
                             B5_R1G,
                             B5_R1H,
                             B5_R1I,
                             B5_R1J,
                             B5_R1K,
                             B5_R1L,
                             B5_R1M,
                             B5_R1N,
                             B5_R1O,
                             B5_R2A,
                             B5_R2B,
                             B5_R3A1,
                             B5_R3A2,
                             B5_R3B,
                             B5_R4A,
                             B5_R4B,
                             B5_R4C,
                             B5_R4D,
                             B5_R4E,
                             B5_R5A,
                             B5_R6A,
                             B5_R6B,
                             B5_R6C,
                             B5_R6D,
                             B5_R6E,
                             B5_R6F,
                             B5_R6G,
                             B5_R6H,
                             B5_R6I,
                             B5_R7,
                             B5_R8A,
                             B5_R8B,
                             B5_R9,
                             B5_R10,
                             B5_R11A,
                             B5_R11B,
                             B5_R12,
                             B5_R13,
                             B5_R14,
                             STATUS_KESEJAHTERAAN,
                             NOMOR_URUT_RUMAH_TANGGA,
                             TGL_PERMOHONAN,
                             PETUGAS_PERMOHONAN,
                             NO_KK,
          					 NIK,
          					 NAMA_KK,
		  					 NO_KK_RUMAHTANGGA,
		  					 NIK_RUMAHTANGGA,
		  					 KD_CACAH,
		  					 TGL_CACAH,
		  					 PETUGAS_CACAH,
		  					 KD_PERIKSA,
		  					 TGL_PERIKSA,
		  					 PETUGAS_PERIKSA,
		  					 HASIL_CACAH,
		  					 NAMA_RESPONDEN,
		  					 PROSES,
		  					 KD_PENGAJUAN,
		  					 NO_URUT_KK)
   SELECT KODEWILAYAH,
          NO_PROP,
          NO_KAB,
          NO_KEC,
          NO_KEL,
          PROVINSI,
          KABUPATEN,
          KECAMATAN,
          DESA,
          B1_R6,
          B1_R8,
          B1_R9,
          B1_R10,
          B3_R1A,
          B3_R1B,
          B3_R2,
          B3_R3,
          B3_R4A,
          B3_R4B,
          B3_R5A,
          B3_R5B,
          B3_R6,
          B3_R7,
          B3_R8,
          B3_R9A,
          B3_R9B,
          B3_R10,
          B3_R11A,
          B3_R11B,
          B3_R12,
          B5_R1A,
          B5_R1B,
          B5_R1C,
          B5_R1D,
          B5_R1E,
          B5_R1F,
          B5_R1G,
          B5_R1H,
          B5_R1I,
          B5_R1J,
          B5_R1K,
          B5_R1L,
          B5_R1M,
          B5_R1N,
          B5_R1O,
          B5_R2A,
          B5_R2B,
          B5_R3A1,
          B5_R3A2,
          B5_R3B,
          B5_R4A,
          B5_R4B,
          B5_R4C,
          B5_R4D,
          B5_R4E,
          B5_R5A,
          B5_R6A,
          B5_R6B,
          B5_R6C,
          B5_R6D,
          B5_R6E,
          B5_R6F,
          B5_R6G,
          B5_R6H,
          B5_R6I,
          B5_R7,
          B5_R8A,
          B5_R8B,
          B5_R9,
          B5_R10,
          B5_R11A,
          B5_R11B,
          B5_R12,
          B5_R13,
          B5_R14,
          STATUS_KESEJAHTERAAN,
          NOMOR_URUT_RUMAH_TANGGA,
          TGL_PERMOHONAN,
          PETUGAS_PERMOHONAN,
          NO_KK,
          NIK,
          NAMA_KK,
		  NO_KK_RUMAHTANGGA,
		  NIK_RUMAHTANGGA,
		  KD_CACAH,
		  TGL_CACAH,
		  PETUGAS_CACAH,
		  KD_PERIKSA,
		  TGL_PERIKSA,
		  PETUGAS_PERIKSA,
		  HASIL_CACAH,
		  NAMA_RESPONDEN,
		  PROSES,
		  KD_PENGAJUAN,
		  NO_URUT_KK
     FROM MHN_RUMAHTANGGA
    WHERE NO_KK_RUMAHTANGGA = ".$no_kk);
	    // die(var_dump($this->db->last_query()));	
	    $this->db->query("UPDATE PRS_RUMAHTANGGA set PETUGAS_PROSES = '".$petugas."', PROSES = 2 WHERE NO_KK_RUMAHTANGGA = ".$no_kk);
	    $this->db->query("UPDATE MHN_RUMAHTANGGA SET PROSES = 2 WHERE NO_KK_RUMAHTANGGA = ".$no_kk);

	    return $res;
	}

	public function editIndividu($nik = NULL, $params = array()){
		
		if (count($params) <= 0 || $nik == NULL) {
			return false;
		}

		$individu = array_change_key_case($params, CASE_UPPER);
		// var_dump($individu);die;
		$this->db->where('NIK', $nik);
		$this->db->query("update PRS_INDIVIDU set TGL_RUBAH = SYSDATE where nik = ".$nik);
		$res = $this->db->update('PRS_INDIVIDU', $individu);
		// die(var_dump($this->db->last_query()));

		return $res;
	}
	
	public function editRumahTangga($no_kk = NULL, $params = array()){
		
		if (count($params) <= 0 || $no_kk == NULL) {
			return false;
		}

		$rumahtangga = array_change_key_case($params, CASE_UPPER);
		// var_dump($rumahtangga);die;
		$this->db->where('NO_KK_RUMAHTANGGA', $no_kk);
		$this->db->query("update PRS_RUMAHTANGGA set TGL_RUBAH = SYSDATE where no_kk = ".$no_kk);
		$res = $this->db->update('PRS_RUMAHTANGGA', $rumahtangga);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function updateIndividu($nik){
		
		$data = array(
               'PROSES' => 2
            );
		$this->db->where('NIK', $nik);
		// $this->db->query("update MHN_INDIVIDU set PROSES = 2");
		$res = $this->db->update('MHN_INDIVIDU', $data);

		// die(var_dump($this->db->last_query()));

		return $res;
	}
	
	public function updateRumahTangga($no_kk){
		
	    $this->db->query("UPDATE MHN_RUMAHTANGGA SET PROSES = 2 WHERE NO_KK_RUMAHTANGGA = ".$no_kk);
		$res = $this->db->query("UPDATE PRS_RUMAHTANGGA SET (KODEWILAYAH,
                             NO_PROP,
                             NO_KAB,
                             NO_KEC,
                             NO_KEL,
                             PROVINSI,
                             KABUPATEN,
                             KECAMATAN,
                             DESA,
                             B1_R6,
                             B1_R8,
                             B1_R9,
                             B1_R10,
                             B3_R1A,
                             B3_R1B,
                             B3_R2,
                             B3_R3,
                             B3_R4A,
                             B3_R4B,
                             B3_R5A,
                             B3_R5B,
                             B3_R6,
                             B3_R7,
                             B3_R8,
                             B3_R9A,
                             B3_R9B,
                             B3_R10,
                             B3_R11A,
                             B3_R11B,
                             B3_R12,
                             B5_R1A,
                             B5_R1B,
                             B5_R1C,
                             B5_R1D,
                             B5_R1E,
                             B5_R1F,
                             B5_R1G,
                             B5_R1H,
                             B5_R1I,
                             B5_R1J,
                             B5_R1K,
                             B5_R1L,
                             B5_R1M,
                             B5_R1N,
                             B5_R1O,
                             B5_R2A,
                             B5_R2B,
                             B5_R3A1,
                             B5_R3A2,
                             B5_R3B,
                             B5_R4A,
                             B5_R4B,
                             B5_R4C,
                             B5_R4D,
                             B5_R4E,
                             B5_R5A,
                             B5_R6A,
                             B5_R6B,
                             B5_R6C,
                             B5_R6D,
                             B5_R6E,
                             B5_R6F,
                             B5_R6G,
                             B5_R6H,
                             B5_R6I,
                             B5_R7,
                             B5_R8A,
                             B5_R8B,
                             B5_R9,
                             B5_R10,
                             B5_R11A,
                             B5_R11B,
                             B5_R12,
                             B5_R13,
                             B5_R14,
                             STATUS_KESEJAHTERAAN,
                             NOMOR_URUT_RUMAH_TANGGA,
                             TGL_PERMOHONAN,
                             PETUGAS_PERMOHONAN,
                             NO_KK,
          					 NIK,
          					 NAMA_KK,
		  					 NO_KK_RUMAHTANGGA,
		  					 NIK_RUMAHTANGGA,
		  					 KD_CACAH,
		  					 TGL_CACAH,
		  					 PETUGAS_CACAH,
		  					 KD_PERIKSA,
		  					 TGL_PERIKSA,
		  					 PETUGAS_PERIKSA,
		  					 HASIL_CACAH,
		  					 NAMA_RESPONDEN,
		  					 PROSES,
		  					 KD_PENGAJUAN,
		  					 NO_URUT_KK) = 
   (SELECT KODEWILAYAH,
          NO_PROP,
          NO_KAB,
          NO_KEC,
          NO_KEL,
          PROVINSI,
          KABUPATEN,
          KECAMATAN,
          DESA,
          B1_R6,
          B1_R8,
          B1_R9,
          B1_R10,
          B3_R1A,
          B3_R1B,
          B3_R2,
          B3_R3,
          B3_R4A,
          B3_R4B,
          B3_R5A,
          B3_R5B,
          B3_R6,
          B3_R7,
          B3_R8,
          B3_R9A,
          B3_R9B,
          B3_R10,
          B3_R11A,
          B3_R11B,
          B3_R12,
          B5_R1A,
          B5_R1B,
          B5_R1C,
          B5_R1D,
          B5_R1E,
          B5_R1F,
          B5_R1G,
          B5_R1H,
          B5_R1I,
          B5_R1J,
          B5_R1K,
          B5_R1L,
          B5_R1M,
          B5_R1N,
          B5_R1O,
          B5_R2A,
          B5_R2B,
          B5_R3A1,
          B5_R3A2,
          B5_R3B,
          B5_R4A,
          B5_R4B,
          B5_R4C,
          B5_R4D,
          B5_R4E,
          B5_R5A,
          B5_R6A,
          B5_R6B,
          B5_R6C,
          B5_R6D,
          B5_R6E,
          B5_R6F,
          B5_R6G,
          B5_R6H,
          B5_R6I,
          B5_R7,
          B5_R8A,
          B5_R8B,
          B5_R9,
          B5_R10,
          B5_R11A,
          B5_R11B,
          B5_R12,
          B5_R13,
          B5_R14,
          STATUS_KESEJAHTERAAN,
		  NOMOR_URUT_RUMAH_TANGGA,
		  TGL_PERMOHONAN,
		  PETUGAS_PERMOHONAN,
		  NO_KK,
		  NIK,
		  NAMA_KK,
		  NO_KK_RUMAHTANGGA,
		  NIK_RUMAHTANGGA,
		  KD_CACAH,
		  TGL_CACAH,
		  PETUGAS_CACAH,
		  KD_PERIKSA,
		  TGL_PERIKSA,
		  PETUGAS_PERIKSA,
		  HASIL_CACAH,
		  NAMA_RESPONDEN,
		  PROSES,
		  KD_PENGAJUAN,
		  NO_URUT_KK FROM MHN_RUMAHTANGGA WHERE NO_KK_RUMAHTANGGA = ".$no_kk.")     
    WHERE NO_KK_RUMAHTANGGA = ".$no_kk);
	    // die(var_dump($this->db->last_query()));	
	    $res = $this->db->query("UPDATE PRS_RUMAHTANGGA set PETUGAS_PROSES = '".$petugas."', PROSES = 2 WHERE NO_KK_RUMAHTANGGA = ".$no_kk);

	    return $res;
	}

	public function penetapanIndividu($nik, $proses){
		
		if ($proses == 3) {
			$penetapan = 2;
		}else if ($proses == 4) {
			$penetapan = 3;
		}

		$data = array(
               'PROSES' => $proses,
               'PENETAPAN' => $penetapan,
               'PETUGAS_PENETAPAN' => $this->cu->NAMA_LENGKAP
            );

		$this->db->where('NIK', $nik);
		$this->db->query("update PRS_INDIVIDU set TGL_PENETAPAN = SYSDATE where NIK = ".$nik);
		$res = $this->db->update('PRS_INDIVIDU', $data);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function penetapanRumahTangga($no_kk, $proses){
		
		if ($proses == 3) {
			$penetapan = 2;
		}else if ($proses == 4) {
			$penetapan = 3;
		}

		$data = array(
               'PROSES' => $proses,
               'PENETAPAN' => $penetapan,
               'PETUGAS_PENETAPAN' => $this->cu->NAMA_LENGKAP
            );

		$this->db->where('NO_KK', $no_kk);
		$this->db->query("update PRS_RUMAHTANGGA set TGL_PENETAPAN = SYSDATE where no_kk = ".$no_kk);
		$res = $this->db->update('PRS_RUMAHTANGGA', $data);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function insertPerubahanIndividu($nik = NULL, $params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    $petugas = $this->cu->NAMA_LENGKAP;
	    $res = $this->db->insert('PRS_INDIVIDU', $data);
	    $res = $this->db->query("UPDATE PRS_INDIVIDU set PETUGAS_PROSES = '".$petugas."', PROSES = 2 , TGL_PERMOHONAN = SYSDATE WHERE NIK = ".$nik);
	    // die(var_dump($this->db->last_query()));

	    return $res;
	}

	public function insertPerubahanRumahTangga($no_kk, $tgl_cacah, $tgl_periksa, $params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    $petugas = $this->cu->NAMA_LENGKAP;
	    $res = $this->db->insert('PRS_RUMAHTANGGA', $data);
	    // die(var_dump($this->db->last_query()));	    	
	    $this->db->query("UPDATE PRS_RUMAHTANGGA SET TGL_CACAH = TO_DATE('".$tgl_cacah."', 'DD-MM-YYYY'), TGL_PERIKSA = TO_DATE('".$tgl_periksa."', 'DD-MM-YYYY'), PETUGAS_PROSES = '".$petugas."', PROSES = 2, TGL_PERMOHONAN = SYSDATE WHERE NO_KK = ".$no_kk);
	    // $this->db->query("UPDATE PRS_RUMAHTANGGA SET PETUGAS_PROSES = '".$petugas."', PROSES = 2, TGL_PERMOHONAN = SYSDATE WHERE NO_KK = ".$no_kk);

	    return $res;
	}

	public function updatePerubahanIndividu($nik = NULL, $params = array()){
		
		if (count($params) <= 0 || $nik == NULL) {
			return false;
		}

		$individu = array_change_key_case($params, CASE_UPPER);
		// var_dump($individu);die;
		$petugas = $this->cu->NAMA_LENGKAP;
		$this->db->where('NIK', $nik);
		$this->db->query("UPDATE PRS_INDIVIDU set PETUGAS_PROSES = '".$petugas."', TGL_RUBAH = SYSDATE where NIK = ".$nik);
		$res = $this->db->update('PRS_INDIVIDU', $individu);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function updatePerubahanRumahTangga($no_kk = NULL, $params = array()){
		
		if (count($params) <= 0 || $no_kk == NULL) {
			return false;
		}

		$rumahtangga = array_change_key_case($params, CASE_UPPER);
		// var_dump($rumahtangga);die;
		$petugas = $this->cu->NAMA_LENGKAP;
		$this->db->where('NO_KK', $no_kk);
		$this->db->query("UPDATE PRS_RUMAHTANGGA set PETUGAS_PROSES = '".$petugas."', TGL_RUBAH = SYSDATE where NO_KK = ".$no_kk);
		$res = $this->db->update('PRS_RUMAHTANGGA', $rumahtangga);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function updateRumahTanggaProses($no_kk = NULL, $params = array()){
		
		if (count($params) <= 0 || $no_kk == NULL) {
			return false;
		}

		$rumahtangga = array_change_key_case($params, CASE_UPPER);
		// var_dump($rumahtangga);die;
		$this->db->where('NO_KK_RUMAHTANGGA', $no_kk);
		$this->db->query("UPDATE PRS_RUMAHTANGGA SET TGL_RUBAH = SYSDATE WHERE NO_KK_RUMAHTANGGA = ".$no_kk);
		$res = $this->db->update('PRS_RUMAHTANGGA', $rumahtangga);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function verifikasi($nik){
		
		$petugas = $this->cu->NAMA_LENGKAP;
		$this->db->where('NIK', $nik);
		$this->db->query("UPDATE PRS_INDIVIDU set PETUGAS_VERIFIKASI = '".$petugas."', TGL_VERIFIKASI = SYSDATE, VERIFIKASI = 2 where NIK = ".$nik);
		// die(var_dump($this->db->last_query()));

		return $res;
	}
}
