    <?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_s2_agr_stat extends CI_model {

    public function __construct() {
        parent::__construct();
        //Do your magic here
        // $this->db_pullout = $this->load->database('pullout', true);
    }

    public function getAgregat($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                    }
                    $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
                } else {
                    $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                    $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
                }
            }
        }

        $query = $this->db->get('V_STAT_AGR');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getagrSinkronisasi($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                    }
                    $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
                } else {
                    $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                    $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
                }
            }
        }

        $query = $this->db->get('V_STAT_SINKRONISASI');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getagrPrelist($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                    }
                    $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
                } else {
                    $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                    $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
                }
            }
        }

        $query = $this->db->get('V_STAT_PRELIST');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getagrAgama($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                        
                    }                    
                    $this->db->order_by('NO_KEL');
                    $query = $this->db->get('V_STAT_AGAMA');
                    // var_dump($this->db->last_query());die;
                } else {
                    $this->db->select('NO_KEC,
                                    NAMA_KEC,
                                    SUM(ISLAM) ISLAM,
                                    SUM(KRISTEN) KRISTEN,
                                    SUM(KATHOLIK) KATHOLIK,
                                    SUM(HINDU) HINDU,
                                    SUM(BUDHA) BUDHA,
                                    SUM(KONGHUCHU) KONGHUCHU,
                                    SUM(LAINNYA) LAINNYA,
                                    SUM(JUMLAH) AS JUMLAH', false);
                    $this->db->order_by('NO_KEC');
                    $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
                    $query = $this->db->get('V_STAT_AGAMA');
                }
            }
        }
        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getagrGoldrh_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                        /*$this->db->select('BLN,
                                                KODE,
                                                SUM (LK) AS LK,
                                                SUM (PR) AS PR,
                                                (SUM (LK) + SUM (PR)) AS JUMLAH', FALSE);
                                                $this->db->order_by('KODE');
                                                $this->db->group_by(array('BLN','KODE'));
                                                $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
                        */
                    }
                    /*else
                                            {
                                            $this->db->order_by('NO_KEL');
                                            $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
                                            $query = $this->db->get('V_AGR_GOL_DRH');
                    */
                    $this->db->order_by('NO_KEL');
                    // $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
                    $query = $this->db->get('V_AGR_GOL_DARAH');
                } else {
                    $this->db->select('NO_KEC,
                                     NAMA_KEC,
                                     SUM (A) A,
                                     SUM (B) B,
                                     SUM (AB) AB,
                                     SUM (O) O,
                                     SUM (A_PLUS) A_PLUS,
                                     SUM (A_MIN) A_MIN,
                                     SUM (B_PLUS) B_PLUS,
                                     SUM (B_MIN) B_MIN,
                                     SUM (AB_PLUS) AB_PLUS,
                                     SUM (AB_MIN) AB_MIN,
                                     SUM (O_PLUS) O_PLUS,
                                     SUM (O_MIN) O_MIN,
                                     SUM (TDK_TAHU) TDK_TAHU,
                                     SUM(JUMLAH) AS JUMLAH', false);
                    $this->db->order_by('NO_KEC');
                    $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
                    // $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
                    $query = $this->db->get('V_AGR_GOL_DARAH');
                }
            }
        }
        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getagrPddkan($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                    }
                    $this->db->order_by('NO_KEL');
                    $query = $this->db->get('V_STAT_PENDIDIKAN');
                } else {
                    $this->db->select('KODE,
                                     NO_KEC,
                                     NAMA_KEC,
                                     SUM (TDK_SKLH) TDK_SKLH,
                                     SUM (BLM_TMT_SD) BLM_TMT_SD,
                                     SUM (TMT_SD) TMT_SD,
                                     SUM (SLTP) SLTP,
                                     SUM (SLTA) SLTA,
                                     SUM (D_I) D_I,
                                     SUM (D_III) D_III,
                                     SUM (S_I) S_I,
                                     SUM (S_II) S_II,
                                     SUM (S_III) S_III,
                                     SUM(JUMLAH) AS JUMLAH', false);
                    $this->db->order_by('NO_KEC');
                    $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
                    $query = $this->db->get('V_STAT_PENDIDIKAN');
                }
            }
        }
        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getagrSHDK_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                        /*$this->db->select('BLN,
                                                KODE,
                                                SUM (LK) AS LK,
                                                SUM (PR) AS PR,
                                                (SUM (LK) + SUM (PR)) AS JUMLAH', FALSE);
                                                $this->db->order_by('KODE');
                                                $this->db->group_by(array('BLN','KODE'));
                                                $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
                        */
                    }
                    /*else
                                            {
                                            $this->db->order_by('NO_KEL');
                                            $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
                                            $query = $this->db->get('STT_AGR_PENDIDIKAN');
                    */
                    $this->db->order_by('NO_KEL');
                    // $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
                    $query = $this->db->get('V_AGR_STAT_HBKEL');
                } else {
                    $this->db->select('NO_KEC,
                                    NAMA_KEC,
                                    SUM(KEPALA_KELUARGA) AS  KEPALA_KELUARGA,
                                    SUM(SUAMI) AS  SUAMI,
                                    SUM(ISTRI) AS  ISTRI,
                                    SUM(ANAK) AS  ANAK,
                                    SUM(MENANTU) AS  MENANTU,
                                    SUM(CUCU) AS  CUCU,
                                    SUM(ORANG_TUA) AS  ORANG_TUA,
                                    SUM(MERTUA) AS  MERTUA,
                                    SUM(FAMILI_LAIN) AS  FAMILI_LAIN,
                                    SUM(PEMBANTU) AS  PEMBANTU,
                                    SUM(LAINNYA) AS  LAINNYA,
                                    SUM(JUMLAH) AS JUMLAH', false);
                    $this->db->order_by('NO_KEC');
                    $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
                    // $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
                    $query = $this->db->get('V_AGR_STAT_HBKEL');
                }
            }
        }
        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getagrUmur($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                    }
                }
            }
        }
        $this->db->select('KODE,
                         STRUKTUR_UMUR,
                         SUM (LK) AS LK,
                         SUM (PR) AS PR,
                         (SUM (LK) + SUM (PR)) AS JUMLAH', false);
        $this->db->order_by('KODE');
        $this->db->group_by(array('KODE', 'STRUKTUR_UMUR'));
        $query = $this->db->get('V_STAT_UMUR');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getagrUmurBppd_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                    }
                }
            }
        }
        $this->db->select('USIA,
                         STRUKTUR_UMUR,
                         SUM (LK) AS LK,
                         SUM (PR) AS PR,
                         (SUM (LK) + SUM (PR)) AS JUMLAH', false);
        $this->db->order_by('USIA');
        $this->db->group_by(array('USIA', 'STRUKTUR_UMUR'));
        // $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
        $query = $this->db->get('V_AGR_UMUR_BPPD');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getagrWajibktp_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                    }
                    $this->db->select('NAMA_KEL,NO_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
                } else {
                    $this->db->select('NAMA_KEC, NO_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
                    $this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
                }
            }
        }
        // $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
        $query = $this->db->get('V_AGR_WAJIB_KTP');

        //var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getagrPkrjn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
        if ($kd_prop) {
            $this->db->where('NO_PROP', $kd_prop);
            if ($kd_kab) {
                $this->db->where('NO_KAB', $kd_kab);
                if ($kd_kec) {
                    $this->db->where('NO_KEC', $kd_kec);
                    if ($kd_kel) {
                        $this->db->where('NO_KEL', $kd_kel);
                    }
                }
            }
        }
        $this->db->select('KODE,
                         PEKERJAAN,
                         SUM (LK) AS LK,
                         SUM (PR) AS PR,
                         (SUM (LK) + SUM (PR)) AS JUMLAH', false);
        $this->db->order_by('KODE');
        $this->db->group_by(array('KODE', 'PEKERJAAN'));
        $query = $this->db->get('V_STAT_PEKERJAAN');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getTable($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
        $data = null;
        if ($params) {          
            foreach ($params as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                $key = strtoupper($key);
                $data[$key] = $value;
                /*if (stristr($key, 'kk')) {
                    $this->db->where(array('NO_KK' => $value));
                }*/
                /*if (stristr($key, 'nik')) {
                    $this->db->where(array('NIK' => $value));
                }*/
                if (stristr($key, 'kec')) {
                    $this->db->where(array('NO_KEC' => $value));
                }
                if (stristr($key, 'kel')) {
                    $kelurahan_code = $value;
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    if (count($kelurahan_code_arr) == 2) {
                        $entri['kd_kec'] = $kelurahan_code_arr[0];
                        $value = $kelurahan_code_arr[1];
                    }
                    $this->db->where(array('NO_KEL' => $value));
                }
                /*if (stristr($key, 'kode')) {
                    $this->db->where(array('JENIS_PKRJN' => $value));
                }*/
            }
        }
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if (intval($limit) > 0) {
            $this->db->limit($limit, $offset);
        }
        if (trim($orderby) != "") {
            $this->db->order_by($orderby);
        }
        if (trim($where) != "") {
            $this->db->where($where);
        }

        $query = $this->db->get('S2_INDUK');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return 0;
    }

    public function getTableTotal($where = "", $params = array()) {
        $data = null;
        if ($params) {          
            foreach ($params as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                $key = strtoupper($key);
                $data[$key] = $value;
                /*if (stristr($key, 'kk')) {
                    $this->db->where(array('NO_KK' => $value));
                }*/
                /*if (stristr($key, 'nik')) {
                    $this->db->where(array('NIK' => $value));
                }*/
                if (stristr($key, 'kec')) {
                    $this->db->where(array('NO_KEC' => $value));
                }
                if (stristr($key, 'kel')) {
                    $kelurahan_code = $value;
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    if (count($kelurahan_code_arr) == 2) {
                        $entri['kd_kec'] = $kelurahan_code_arr[0];
                        $value = $kelurahan_code_arr[1];
                    }
                    $this->db->where(array('NO_KEL' => $value));
                }
                /*if (stristr($key, 'kode')) {
                    $this->db->where(array('JENIS_PKRJN' => $value));
                }*/
            }
        }
        // $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if (trim($where) != "") {
            $this->db->where($where);
        }

        $query = $this->db->get('S2_INDUK');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }

        return 0;
    }

    public function getTableMiskin($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
        $data = null;
        if ($params) {          
            foreach ($params as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                $key = strtoupper($key);
                $data[$key] = $value;
                if (stristr($key, 'kec')) {
                    $this->db->where(array('NO_KEC' => $value));
                }
                if (stristr($key, 'kel')) {
                    $kelurahan_code = $value;
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    if (count($kelurahan_code_arr) == 2) {
                        $entri['kd_kec'] = $kelurahan_code_arr[0];
                        $value = $kelurahan_code_arr[1];
                    }
                    $this->db->where(array('NO_KEL' => $value));
                }
                if (stristr($key, 'kode')) {
                    $this->db->where(array('JENIS_PKRJN' => $value));
                }
            }
        }
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if (intval($limit) > 0) {
            $this->db->limit($limit, $offset);
        }
        if (trim($orderby) != "") {
            $this->db->order_by($orderby);
        }
        if (trim($where) != "") {
            $this->db->where($where);
        }

        $query = $this->db->get('S2_INDUK_SINKRONISASI');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return 0;
    }

    public function getTableMiskinTotal($where = "", $params = array()) {
        $data = null;
        if ($params) {          
            foreach ($params as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                $key = strtoupper($key);
                $data[$key] = $value;
                if (stristr($key, 'kec')) {
                    $this->db->where(array('NO_KEC' => $value));
                }
                if (stristr($key, 'kel')) {
                    $kelurahan_code = $value;
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    if (count($kelurahan_code_arr) == 2) {
                        $entri['kd_kec'] = $kelurahan_code_arr[0];
                        $value = $kelurahan_code_arr[1];
                    }
                    $this->db->where(array('NO_KEL' => $value));
                }
                /*if (stristr($key, 'kode')) {
                    $this->db->where(array('JENIS_PKRJN' => $value));
                }*/
            }
        }
        // $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if (trim($where) != "") {
            $this->db->where($where);
        }

        $query = $this->db->get('S2_INDUK_SINKRONISASI');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }

        return 0;
    }

    public function getTablePrelist($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
        $data = null;
        if ($params) {          
            foreach ($params as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                $key = strtoupper($key);
                $data[$key] = $value;
                if (stristr($key, 'kec')) {
                    $this->db->where(array('NO_KEC' => $value));
                }
                if (stristr($key, 'kel')) {
                    $kelurahan_code = $value;
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    if (count($kelurahan_code_arr) == 2) {
                        $entri['kd_kec'] = $kelurahan_code_arr[0];
                        $value = $kelurahan_code_arr[1];
                    }
                    $this->db->where(array('NO_KEL' => $value));
                }
                if (stristr($key, 'kode')) {
                    $this->db->where(array('JENIS_PKRJN' => $value));
                }
            }
        }
        $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if (intval($limit) > 0) {
            $this->db->limit($limit, $offset);
        }
        if (trim($orderby) != "") {
            $this->db->order_by($orderby);
        }
        if (trim($where) != "") {
            $this->db->where($where);
        }

        $query = $this->db->get('S2_INDUK_PRELIST');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return 0;
    }

    public function getTablePrelistTotal($where = "", $params = array()) {
        $data = null;
        if ($params) {          
            foreach ($params as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                $key = strtoupper($key);
                $data[$key] = $value;
                if (stristr($key, 'kec')) {
                    $this->db->where(array('NO_KEC' => $value));
                }
                if (stristr($key, 'kel')) {
                    $kelurahan_code = $value;
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    if (count($kelurahan_code_arr) == 2) {
                        $entri['kd_kec'] = $kelurahan_code_arr[0];
                        $value = $kelurahan_code_arr[1];
                    }
                    $this->db->where(array('NO_KEL' => $value));
                }
                /*if (stristr($key, 'kode')) {
                    $this->db->where(array('JENIS_PKRJN' => $value));
                }*/
            }
        }
        // $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
        if (trim($where) != "") {
            $this->db->where($where);
        }

        $query = $this->db->get('S2_INDUK_PRELIST');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }

        return 0;
    }

}
