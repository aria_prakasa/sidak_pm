<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_s2_induk extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function getSinkronisasi($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_LGKP', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->order_by('NO_KEC,NO_KEL,NO_KK,STAT_HBKEL');
		$query = $this->db->get('S2_INDUK_SINKRONISASI');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getSinkronisasiTotal($where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			if (stristr($key, 'kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_LGKP', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$query = $this->db->get('S2_INDUK_SINKRONISASI');

		// var_dump($this->db->last_query());die;	

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return 0;
	}

	public function getPrelist($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_LGKP', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->order_by('NO_KEC,NO_KEL,NO_KK,STAT_HBKEL');
		$query = $this->db->get('S2_INDUK_PRELIST');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getPrelistTotal($where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			if (stristr($key, 'kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_LGKP', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$query = $this->db->get('S2_INDUK_PRELIST');

		// var_dump($this->db->last_query());die;	

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return 0;
	}

	public function getList($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_LGKP', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
			if (stristr($key, 'view')) {
				$view = $value;
			}
		}
		// var_dump($view);die;

		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$table = 'S2_INDUK_LIST_'.$view;
		$query = $this->db->get($table);

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getListTotal($where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			if (stristr($key, 'kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_LGKP', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
			if (stristr($key, 'view')) {
				$view = $value;
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$table = 'S2_INDUK_LIST_'.$view;
		$query = $this->db->get($table);
		// $query = $this->db->get('S2_INDUK_LIST_'.$view);

		// var_dump($this->db->last_query());die;	

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return 0;
	}

	public function getView($no_kk) {

		// $this->db->where(array('NO_KK' => $no_kk, 'STAT_HBKEL' => 1));
		// $query = $this->db->get('S2_GETBIODATA_DETAIL');
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('S2_GETDATAKELUARGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getAnggota($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$this->db->order_by('STAT_HBKEL', 'ASC');
		// $this->db->order_by('TGL_LAHIR', 'ASC');
		$query = $this->db->get('S2_GETBIODATA_DETAIL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getAnggotaSinkronisasi($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$this->db->order_by('STAT_HBKEL', 'ASC');
		$this->db->order_by('UMUR', 'DESC');
		// $this->db->order_by('TGL_LAHIR', 'ASC');
		$query = $this->db->get('S2_INDUK_SINKRONISASI');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getAnggotaVerifikasi($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$this->db->order_by('STAT_HBKEL', 'ASC');
		$this->db->order_by('UMUR', 'DESC');
		// $this->db->order_by('TGL_LAHIR', 'ASC');
		$query = $this->db->get('S2_INDUK_LIST_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getIndividu($nik) {

		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('S2_GETBIODATA_DETAIL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function updateIndividu($nik) {
		// set flag proses, 1 = mohon, 2 = accept.
		$this->db->set('FLAG_PROSES', '1');
		$this->db->where(array('NIK' => $nik));
		return $this->db->update('BIODATA_PENDUDUK');
	}

	public function deleteIndividu($nik) {
		// set flag proses, 1 = mohon, 2 = accept.
		$this->db->set('FLAG_PROSES', '0');
		$this->db->where(array('NIK' => $nik));
		return $this->db->update('BIODATA_PENDUDUK');
	}

	public function deleteIndividuRumahTangga($no_kk) {
		// set flag proses, 1 = mohon, 2 = accept.
		$this->db->set('FLAG_PROSES', '0');
		$this->db->where(array('NO_KK' => $no_kk, 'FLAG_PROSES' => '1'));
		return $this->db->update('BIODATA_PENDUDUK');
	}

}
