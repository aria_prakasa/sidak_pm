<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_s2_masterbdt extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getbinary()
    {
        $sql = $this->db->get('MASTER_BINARY');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getbinary_ii()
    {
        $sql = $this->db->get('MASTER_BINARY_II');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k3()
    {
        $sql = $this->db->get('MASTER_B4_K3');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k5()
    {
        $sql = $this->db->get('MASTER_B4_K5');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k6()
    {
        $sql = $this->db->get('MASTER_B4_K6');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k8()
    {
        $sql = $this->db->get('MASTER_B4_K8');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k9()
    {
        $sql = $this->db->get('MASTER_B4_K9');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k11()
    {
        $this->db->order_by('NO');
        $sql = $this->db->get('MASTER_B4_K11');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k12()
    {
        $sql = $this->db->get('MASTER_B4_K12');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k13()
    {
        $sql = $this->db->get('MASTER_B4_K13');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k14()
    {
        $sql = $this->db->get('MASTER_B4_K14');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k15()
    {
        $sql = $this->db->get('MASTER_B4_K15');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k16()
    {
        $sql = $this->db->get('MASTER_B4_K16');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k18()
    {
        $sql = $this->db->get('MASTER_B4_K18');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k20()
    {
        $sql = $this->db->get('MASTER_B4_K20');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb4_k21()
    {
        $sql = $this->db->get('MASTER_B4_K21');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getstatus_kesejahteraan()
    {
        $sql = $this->db->get('MASTER_STATUS_KESEJAHTERAAN');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r1a()
    {
        $sql = $this->db->get('MASTER_B3_R1A');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r1b()
    {
        $sql = $this->db->get('MASTER_B3_R1B');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r3()
    {
        $sql = $this->db->get('MASTER_B3_R3');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r4a()
    {
        $sql = $this->db->get('MASTER_B3_R4A');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r5a()
    {
        $sql = $this->db->get('MASTER_B3_R5A');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r4b()
    {
        $sql = $this->db->get('MASTER_B3_R4B');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r5b()
    {
        $sql = $this->db->get('MASTER_B3_R5B');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r7()
    {
        $sql = $this->db->get('MASTER_B3_R7');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r8()
    {
        $sql = $this->db->get('MASTER_B3_R8');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r11a()
    {
        $sql = $this->db->get('MASTER_B3_R11A');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r11b()
    {
        $sql = $this->db->get('MASTER_B3_R11B');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r12()
    {
        $sql = $this->db->get('MASTER_B3_R12');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r9a()
    {
        $sql = $this->db->get('MASTER_B3_R9A');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r9b()
    {
        $sql = $this->db->get('MASTER_B3_R9B');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb3_r10()
    {
        $sql = $this->db->get('MASTER_B3_R10');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1a()
    {
        $sql = $this->db->get('MASTER_B5_R1A');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1b()
    {
        $sql = $this->db->get('MASTER_B5_R1B');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1c()
    {
        $sql = $this->db->get('MASTER_B5_R1C');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1d()
    {
        $sql = $this->db->get('MASTER_B5_R1D');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1e()
    {
        $sql = $this->db->get('MASTER_B5_R1E');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1f()
    {
        $sql = $this->db->get('MASTER_B5_R1F');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1g()
    {
        $sql = $this->db->get('MASTER_B5_R1G');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1h()
    {
        $sql = $this->db->get('MASTER_B5_R1H');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1i()
    {
        $sql = $this->db->get('MASTER_B5_R1I');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1j()
    {
        $sql = $this->db->get('MASTER_B5_R1J');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1k()
    {
        $sql = $this->db->get('MASTER_B5_R1K');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1l()
    {
        $sql = $this->db->get('MASTER_B5_R1L');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1m()
    {
        $sql = $this->db->get('MASTER_B5_R1M');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r1n()
    {
        $sql = $this->db->get('MASTER_B5_R1N');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r10()
    {
        $sql = $this->db->get('MASTER_B5_R10');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r12()
    {
        $sql = $this->db->get('MASTER_B5_R12');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r13()
    {
        $sql = $this->db->get('MASTER_B5_R13');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getb5_r14()
    {
        $sql = $this->db->get('MASTER_B5_R14');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function gethasil_pencacahan()
    {
        $sql = $this->db->get('MASTER_HASIL_PENCACAHAN');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getstat_hbkel()
    {
        $sql = $this->db->get('MASTER_STAT_KELUARGA');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getstat_rtangga()
    {
        $sql = $this->db->get('MASTER_STAT_RUMAHTANGGA');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Opsi -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NO']] = ucwords($row['NO'].'. '.$row['DESCRIP']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }

    public function getno_urut()
    {
        $result = array('' => ' - Pilih Opsi - ',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10',
            '11' => '11',
            '12' => '12',
            '13' => '13',
            '14' => '14',
            '15' => '15',
            '16' => '16',
            '17' => '17',
            '18' => '18',
            '19' => '19',
            '20' => '20'
            );

        return $result;
    }

    public function getno_urutKK()
    {
        $result = array('' => ' - Pilih Opsi - ',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10'
            );

        return $result;
    }

    public function getomzet()
    {
        $result = array('' => ' - Pilih Opsi - ',
            '1' => '1) < 1 juta',
            '2' => '2) 1 - < 5 juta',
            '3' => '3) 5 - < 10 juta',
            '4' => '4) >= 10 juta'
            );

        return $result;
    }

    public function getkeberadaan()
    {
        $result = array('' => ' - Pilih Opsi - ',
            '1' => '1. Ada',
            '2' => '2. Tidak ada'
            );

        return $result;
    }

    public function getpencacah($no_kel, $no_kec)
    {
        $this->db->where('NO_KEC = '. $no_kec. ' AND (NO_KEL = ' .$no_kel. ' OR NO_KEL IS NULL)');
        $sql = $this->db->get('MASTER_PENCACAH');
        if ($sql->num_rows() > 0) {
            foreach ($sql->result_array() as $row) {
                $result['']= '- Pilih Nama Pencacah -';
                // $result[$row['NO']] = ucwords(strtoupper($row['NO'].'. '.$row['DESCRIP']));
                $result[$row['NIK'].'-'.$row['NAMA']] = ucwords($row['NAMA']);
            }
            // var_dump($this->db->last_query());die;
            return $result;
        }
    }
}
