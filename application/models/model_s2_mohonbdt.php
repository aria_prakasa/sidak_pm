<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_s2_mohonbdt extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function getBiodataMohon($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by(array('NO_KEC', 'NO_KEL', 'NO_KK', 'NAMA'));
		// $this->db->order_by('NO_KK', 'ASC');
		$query = $this->db->get('S2_VIEW_PENGAJUAN_IND');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getBiodataMohonTotal($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_LENGKAP', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_PENGAJUAN_IND');

		// var_dump($this->db->last_query());die;
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	public function getRumahTanggaMohon($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_RUMAHTANGGA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA_RUMAHTANGGA', 'ASC');
		$query = $this->db->get('S2_VIEW_PENGAJUAN_RT');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getRumahTanggaMohonTotal($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_RUMAHTANGGA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_PENGAJUAN_RT');

		// var_dump($this->db->last_query());die;
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	public function getRumahTanggaProses($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_PENGAJUAN_MAIN');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getRumahTanggaProsesTotal($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_PENGAJUAN_MAIN');

		// var_dump($this->db->last_query());die;
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	public function getIndividu($nik) {

		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('MHN_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getRumahTangga($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('MHN_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function getKepala($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('S2_GETDATAKELUARGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function insertIndividu($params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    $res = $this->db->insert('MHN_INDIVIDU', $data);
	    // die(var_dump($this->db->last_query()));

	    return $res;
	}

	public function insertRumahTangga($params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    $res = $this->db->insert('MHN_RUMAHTANGGA', $data);
	    // die(var_dump($this->db->last_query()));	

	    return $res;
	}

	public function insertRumahTanggaTgl($tgl_cacah,$tgl_periksa,$no_kk){
	    
	    $res = $this->db->query("UPDATE MHN_RUMAHTANGGA 
	    	SET TGL_CACAH = TO_DATE('".$tgl_cacah."', 'DD-MM-YYYY'), TGL_PERIKSA = TO_DATE('".$tgl_periksa."', 'DD-MM-YYYY') 
	    	where NO_KK = ".$no_kk);
	    // die(var_dump($this->db->last_query()));	
	    return $res;
	}

	public function updateIndividu($nik = NULL, $params = array()){
		
		if (count($params) <= 0 || $nik == NULL) {
			return false;
		}

		$individu = array_change_key_case($params, CASE_UPPER);
		// var_dump($individu);die;
		$this->db->where('NIK', $nik);
		$this->db->query("update MHN_INDIVIDU set TGL_RUBAH = SYSDATE where NIK = ".$nik);
		$res = $this->db->update('MHN_INDIVIDU', $individu);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function updateRumahTangga($no_kk = NULL, $params = array()){
		
		if (count($params) <= 0 || $no_kk == NULL) {
			return false;
		}

		$rumahtangga = array_change_key_case($params, CASE_UPPER);
		// var_dump($rumahtangga);die;
		$this->db->where('NO_KK_RUMAHTANGGA', $no_kk);
		$this->db->query("UPDATE MHN_RUMAHTANGGA SET TGL_RUBAH = SYSDATE WHERE NO_KK_RUMAHTANGGA = ".$no_kk);
		$res = $this->db->update('MHN_RUMAHTANGGA', $rumahtangga);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function penetapanIndividu($nik, $proses){
		
		$data = array(
               'PROSES' => $proses
            );

		$this->db->where('NIK', $nik);
		$res = $this->db->update('MHN_INDIVIDU', $data);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function penetapanRumahTangga($no_kk, $proses){
		
		$data = array(
               'PROSES' => $proses
            );

		$this->db->where('NO_KK_RUMAHTANGGA', $no_kk);
		$res = $this->db->update('MHN_RUMAHTANGGA', $data);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function deleteIndividu($nik) {
		$this->db->where('NIK', $nik);
		$res = $this->db->delete('MHN_INDIVIDU');
		// var_dump($this->db->last_query());die;

		return $res;
	}

	public function deleteRumahTangga($no_kk) {
		$this->db->where('NO_KK', $no_kk);
		$res = $this->db->delete('MHN_RUMAHTANGGA');
		// var_dump($this->db->last_query());die;

		return $res;
	}

	public function deleteRumahTangga_all($no_kk) {
		$this->db->where('NO_KK_RUMAHTANGGA', $no_kk);
		$res = $this->db->delete('MHN_RUMAHTANGGA');
		// var_dump($this->db->last_query());die;

		return $res;
	}

	public function deleteIndividuRumahTangga($no_kk) {
		$this->db->where('NO_KK', $no_kk);
		$res = $this->db->delete('MHN_INDIVIDU');
		// var_dump($this->db->last_query());die;

		return $res;
	}

	public function deleteIndividuRumahTangga_all($no_kk) {
		$this->db->where('NO_KK_RUMAHTANGGA', $no_kk);
		$res = $this->db->delete('MHN_INDIVIDU');
		// var_dump($this->db->last_query());die;

		return $res;
	}

	public function get_b4_k4($no_kk){
	    
	    /*if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    if ($data['KK']) {
			$this->db->select_max('B4_K4');
			$this->db->where(array('NO_KK_RUMAH_TANGGA' => $data['KK']));
	    }
	    if ($data['NO_KK']) {
			$this->db->select_max('B4_K4');
			$this->db->where(array('NO_KK' => $data['NO_KK']));
	    }
	    if ($data['ID']) {
			$this->db->select_max('B4_K4');
			$this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $data['ID']));
	    }*/

		$this->db->select_max('B4_K4');
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('MHN_INDIVIDU');
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function get_no_art($no_kk){
	    
	    /*if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    if ($data['KK']) {
			$this->db->select_max('NO_ART');
			$this->db->where(array('NO_KK' => $data['KK']));
	    }
	    if ($data['ID']) {
			$this->db->select_max('NO_ART');
			$this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $data['ID']));
	    }*/
	    $this->db->select_max('NO_ART');
		$this->db->where(array('NO_KK_RUMAHTANGGA' => $no_kk));
		$query = $this->db->get('MHN_INDIVIDU');
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function cekmohon($no_kk){
	   
		// $this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $id));
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('MHN_INDIVIDU');
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row());
		}

		return false;
	}

	public function getDetail($nik){
	   
		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('S2_MOHON_DETAIL');
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row());
		}

		return false;
	}

	public function insertPerubahanIndividu($nik = NULL, $params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    $res = $this->db->insert('MHN_INDIVIDU', $data);
	    // die(var_dump($this->db->last_query()));
	    $res = $this->db->query("UPDATE MHN_INDIVIDU set PROSES = 2 WHERE NIK = ".$nik);
	    // die(var_dump($this->db->last_query()));

	    return $res;
	}

	public function insertPerubahanRumahTangga($no_kk, $tgl_cacah, $tgl_periksa, $params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    $res = $this->db->insert('MHN_RUMAHTANGGA', $data);
	    // die(var_dump($this->db->last_query()));	
	    $this->db->query("UPDATE MHN_RUMAHTANGGA SET TGL_CACAH = TO_DATE('".$tgl_cacah."', 'DD-MM-YYYY'), TGL_PERIKSA = TO_DATE('".$tgl_periksa."', 'DD-MM-YYYY'), PROSES = 2  WHERE NO_KK = ".$no_kk);
	    // $this->db->query("UPDATE MHN_RUMAHTANGGA set PROSES = 2 WHERE NO_KK = ".$no_kk);
	    // die(var_dump($this->db->last_query()));	    	

	    return $res;
	}

	public function updatePerubahanIndividu($nik = NULL, $params = array()){
		
		if (count($params) <= 0 || $nik == NULL) {
			return false;
		}

		$individu = array_change_key_case($params, CASE_UPPER);
		// var_dump($individu);die;
		// $petugas = $this->cu->NAMA_LENGKAP;
		$this->db->where('NIK', $nik);
		$this->db->query("UPDATE MHN_INDIVIDU set TGL_RUBAH = SYSDATE where NIK = ".$nik);
		$res = $this->db->update('MHN_INDIVIDU', $individu);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function updatePerubahanRumahTangga($no_kk = NULL, $params = array()){
		
		if (count($params) <= 0 || $no_kk == NULL) {
			return false;
		}

		$rumahtangga = array_change_key_case($params, CASE_UPPER);
		// var_dump($rumahtangga);die;
		// $petugas = $this->cu->NAMA_LENGKAP;
		$this->db->query("UPDATE MHN_RUMAHTANGGA set TGL_RUBAH = SYSDATE where NO_KK_RUMAHTANGGA = ".$no_kk);
		// die(var_dump($this->db->last_query()));	
		$this->db->where('NO_KK_RUMAHTANGGA', $no_kk);
		$res = $this->db->update('MHN_RUMAHTANGGA', $rumahtangga);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function cek(){
	    
		// $this->db->select_max('NOMOR_URUT_RUMAH_TANGGA');
		// $this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $data['ID']));
		$query = $this->db->get('MHN_RUMAHTANGGA');
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function getno(){
	    
		$this->db->select_max('NOMOR_URUT_RUMAH_TANGGA');
		// $this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $data['ID']));
		$query = $this->db->get('MHN_RUMAHTANGGA');
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function getno_urutKK($no_kk_rumahtangga){
	    
		$this->db->select_max('NO_URUT_KK');
		$this->db->where(array('NO_KK_RUMAHTANGGA' => $no_kk_rumahtangga));
		$query = $this->db->get('MHN_RUMAHTANGGA');
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row_array());
		}

		return false;
	}
}
