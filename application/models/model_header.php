<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_header extends CI_model
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        //$this->db_pullout = $this->load->database('pullout', TRUE);
    }

    public function getWilayah($no_kec,$no_kel) {

        /*if (count($params) <= 0) {
            return false;
        }
        $data = array_change_key_case($params, CASE_UPPER);*/

        if ($no_kec) {
            if ($no_kel) {
                $this->db->select('KECAMATAN, KELURAHAN', false);
                $this->db->where('NO_KEL', $no_kel);
                
                $query = $this->db->get('SETUP_KEL');
            } 
            $this->db->select('KECAMATAN', false);
            $this->db->where('NO_KEC', $no_kec); 
            $this->db->group_by('KECAMATAN');                         
        }
        $this->db->where('NO_KAB', NO_KAB);
        $this->db->where('NO_PROP', NO_PROP);
        $query = $this->db->get('V_NAMA_WILAYAH');   
        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->row());
        }
    }

    public function getheaderAgama()
    {
        $this->db->order_by('NO');
        $query = $this->db->get('MASTER_AGAMA');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getheaderGoldrh()
    {
        $this->db->order_by('NO');
        $query = $this->db->get('MASTER_GOLONGAN_DARAH');

        // var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }

    public function getheaderPddkan()
    {
        $this->db->order_by('NO');
        $query = $this->db->get('MASTER_PENDIDIKAN');

        var_dump($this->db->last_query());die;

        if ($query->num_rows() > 0) {
            return keysToLower($query->result());
        }

        return false;
    }
}
