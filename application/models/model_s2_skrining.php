<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_s2_skrining extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function getRumahTanggaMohon($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK_INDUK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_KEP_RUMAHTANGGA', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
			if (stristr($key, 'proses')) {
				$this->db->where(array('PROSES' => $value));
			}
			if (stristr($key, 'pengajuan')) {
				$this->db->where(array('KD_PENGAJUAN' => $value));
			}
		}
		if ($data['TGL_AWAL'] && $data['TGL_AKHIR']) {
			$tgl_awal = $data['TGL_AWAL'];
			$tgl_akhir = $data['TGL_AKHIR'];
			$tgl_entri = "TGL_ENTRI BETWEEN TO_DATE('".$tgl_awal."','DD-MM-YYYY')
                                      AND TO_DATE('".$tgl_akhir."','DD-MM-YYYY') +1 ";
			$this->db->where($tgl_entri);
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA_RUMAHTANGGA', 'ASC');
		$query = $this->db->get('S2_GETDATA_PRELIST');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getRumahTanggaMohonTotal($where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK_INDUK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_KEP_RUMAHTANGGA', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
			if (stristr($key, 'proses')) {
				$this->db->where(array('PROSES' => $value));
			}
			if (stristr($key, 'pengajuan')) {
				$this->db->where(array('KD_PENGAJUAN' => $value));
			}
		}
		if ($data['TGL_AWAL'] && $data['TGL_AKHIR']) {
			$tgl_awal = $data['TGL_AWAL'];
			$tgl_akhir = $data['TGL_AKHIR'];
			$tgl_entri = "TGL_ENTRI BETWEEN TO_DATE('".$tgl_awal."','DD-MM-YYYY')
                                      AND TO_DATE('".$tgl_akhir."','DD-MM-YYYY') +1 ";
			$this->db->where($tgl_entri);
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_GETDATA_PRELIST');

		// var_dump($this->db->last_query());die;
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	public function getAnggotaMohon($no_kk) {

		// $this->db->select("NO_KK, NIK, NAMA_LGKP, JENIS_KLMIN, TO_CHAR (TGL_LHR, 'DD-MM-YYYY') TGL_LHR, STAT_HBKEL");
		$this->db->where(array('NO_KK_INDUK' => $no_kk));
		$this->db->order_by('B4_K4, NO_KK, STAT_HBKEL, UMUR DESC');
		$query = $this->db->get('S2_GETBIODATA_PRELIST');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result_array());
		}

		return false;
	}

	public function getIndividuMohon($nik) {

		// $this->db->select("NO_KK, NIK, NAMA_LGKP, JENIS_KLMIN, TO_CHAR (TGL_LHR, 'DD-MM-YYYY') TGL_LHR, STAT_HBKEL");
		$this->db->where(array('NIK' => $nik));
		// $this->db->order_by('B4_K4, NO_KK, STAT_HBKEL, UMUR DESC');
		$query = $this->db->get('S2_GETBIODATA_PRELIST');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function insert($no_kk, $params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    $res = $this->db->insert('SKRINING_RUMAHTANGGA', $data);
	    // die(var_dump($this->db->last_query()));	
	    $this->db->query("UPDATE SKRINING_RUMAHTANGGA SET TGL_ENTRI = SYSDATE WHERE NO_KK = ".$no_kk);
	    // die(var_dump($this->db->last_query()));	    	

	    return $res;
	}

	public function getNoUrutKK($no_kk){
	    
	    $this->db->select_max('B4_K4');
		$this->db->where(array('NO_KK_INDUK' => $no_kk));
		$query = $this->db->get('SKRINING_INDIVIDU');

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row_array());
		}
		return false;
	}

	public function insertAnggota($no_kk, $anggota = array(), $params = array()){
	    
	    if (count($anggota) <= 0) {
	        return false;
	    }
	    // var_dump($params);die;
	    $petugas_entri = strtoupper($this->cu->NAMA_LENGKAP);
	    $data = null;
	    foreach ($params as $key => $value) {
	    	$$key = $value;
	    }
	    // var_dump($no_kk_induk, $b4_k4);die;
	    foreach ($anggota as $keys => $values) {
	    	foreach ($values as $key => $value) {
	    		$key        = strtoupper($key);
	        	// $data[$key] = $value;
				if (stristr($key, 'TGL_LHR')) {
					$tgl_lhr = $value;
					continue;
				}
				else {
					$data[$key] = $value;	
					$this->db->set($key, $value);
				}
				// var_dump($this->db->set($key, $value));
			}
			$res = $this->db->insert('SKRINING_INDIVIDU', $data);
			$this->db->query("UPDATE SKRINING_INDIVIDU SET B4_K4 = ".$b4_k4.", NO_KK_INDUK = ".$no_kk_induk.", TGL_LHR = TO_DATE('".$tgl_lhr."', 'DD-MM-YYYY') ,TGL_ENTRI = SYSDATE, PETUGAS_ENTRI = '".$petugas_entri."' WHERE NIK = ".$data['NIK']. " AND NO_KK = ".$no_kk);
			// die(var_dump($this->db->last_query()));	
	    }
	    // var_dump($data);die;
	    // $res = $this->db->insert_batch('SKRINING_INDIVIDU', $data);
	    
	    // $this->db->query("UPDATE SKRINING_INDIVIDU SET TGL_ENTRI = SYSDATE WHERE NO_KK = ".$no_kk);
	    // die(var_dump($this->db->last_query()));	    	

	    return $res;
	}

	public function getData($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('SKRINING_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function getDataPrelist($no_kk) {

		$this->db->where(array('NO_KK_INDUK' => $no_kk));
		$query = $this->db->get('SKRINING_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function countKK($no_kk) {

		$this->db->where(array('NO_KK_INDUK' => $no_kk));
		$query = $this->db->get('SKRINING_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	public function countART($no_kk) {

		$this->db->where(array('NO_KK_INDUK' => $no_kk));
		$query = $this->db->get('S2_GETBIODATA_DETAIL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	/*public function getDataKK($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('SKRINING_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		}

		return false;
	}*/


	public function update($no_kk = NULL, $params = array()) {
		
		if (count($params) <= 0 || $no_kk == NULL) {
			return false;
		}

		$rumahtangga = array_change_key_case($params, CASE_UPPER);
		// var_dump($rumahtangga);die;
		$this->db->where('NO_KK', $no_kk);
		$this->db->query("UPDATE SKRINING_RUMAHTANGGA SET TGL_RUBAH = SYSDATE WHERE NO_KK = ".$no_kk);
		$res = $this->db->update('SKRINING_RUMAHTANGGA', $rumahtangga);

		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function delete($no_kk) {
		$this->db->where('NO_KK', $no_kk);
		$res = $this->db->delete('SKRINING_RUMAHTANGGA');
		$res = $this->db->delete('SKRINING_INDIVIDU');
		// var_dump($this->db->last_query());die;

		return $res;
	}

	public function deleteRuta($no_kk) {
		$this->db->where('NO_KK_INDUK', $no_kk);
		$res = $this->db->delete('SKRINING_RUMAHTANGGA');
		$res = $this->db->delete('SKRINING_INDIVIDU');
		// var_dump($this->db->last_query());die;

		return $res;
	}

	public function deleteKK($no_kk) {
		$this->db->where('NO_KK', $no_kk);
		$res = $this->db->delete('SKRINING_RUMAHTANGGA');
		$res = $this->db->delete('SKRINING_INDIVIDU');
		// var_dump($this->db->last_query());die;

		return $res;
	}

	public function prosesVerifikasi($no_kk,$verifikasi) {
		
		$petugas = $this->cu->NAMA_LENGKAP;
		$res = $this->db->query("UPDATE SKRINING_RUMAHTANGGA SET PROSES = 2, VERIFIKASI = ".$verifikasi." ,TGL_PROSES = SYSDATE, PETUGAS_PROSES = '".$petugas."' WHERE NO_KK_INDUK = ".$no_kk);
		if ($verifikasi == 3) {
			$this->db->query("UPDATE SKRINING_RUMAHTANGGA SET PROSES = 4, TGL_PROSES = SYSDATE, PETUGAS_PROSES = '".$petugas."' WHERE NO_KK_INDUK = ".$no_kk);
		}
		// die(var_dump($this->db->last_query()));

		return $res;
	}

	public function updateTambahKK($params = array()) {
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        // $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    // var_dump($data);die;
	    $petugas = strtoupper($this->cu->NAMA_LENGKAP);
	    $res = $this->db->query("UPDATE SKRINING_RUMAHTANGGA SET PROSES = 2, VERIFIKASI = 2, NO_KK_INDUK = ".$data['no_kk_induk'].", NIK_INDUK = ".$data['nik_induk'].", NAMA_INDUK = '".$data['nama_induk']."', HUBUNGAN_RUMAHTANGGA_INDUK = ".$data['hubungan_rumahtangga_induk'].", TGL_RUBAH = SYSDATE, PETUGAS_RUBAH = '".$petugas."' WHERE NO_KK = ".$data['no_kk_tambah']);
	    $res = $this->db->query("UPDATE SKRINING_INDIVIDU SET NO_KK_INDUK = ".$data['no_kk_induk'].", B4_K4 = ".$data['b4_k4']." WHERE NO_KK = ".$data['no_kk_tambah']);
	    // die(var_dump($this->db->last_query()));	    	

	    return $res;
	}

	/*public function tolakVerifikasi($no_kk) {
		
		$petugas = $this->cu->NAMA_LENGKAP;
		$res = $this->db->query("UPDATE SKRINING_RUMAHTANGGA SET PROSES = 2, VERIFIKASI = 1 , TGL_PROSES = SYSDATE, PETUGAS_PROSES = '".$petugas."' WHERE NO_KK = ".$no_kk);

		// die(var_dump($this->db->last_query()));

		return $res;
	}*/
}
