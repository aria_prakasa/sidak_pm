<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_s2_bdt extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function getListRumahTangga($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			/*if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}*/
			/*if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}*/
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('B1_R8', $value, 'both');
			}
		}
		// $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$this->db->order_by("NO_KEC,NO_KEL,B1_R8 ASC");
		$query = $this->db->get('S2_VIEW_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getListRumahTanggaTotal($where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			/*if (stristr($key, 'kk')) {
				$this->db->where(array('NO_KK' => $value));
			}*/
			/*if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}*/
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('B1_R8', $value, 'both');
			}
		}
		// $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$query = $this->db->get('S2_VIEW_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return 0;
	}

	public function getListIndividu($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			/*if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}*/
			/*if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}*/
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
			if (stristr($key, 'id_rt')) {
				$this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $value));
				$this->db->order_by('NO_ART', 'ASC');
			}
			if (stristr($key, 'b4_k4')) {
				$this->db->where(array('B4_K4' => $value));
			}
		}
		// $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getListIndividuTotal($where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			/*if (stristr($key, 'kk')) {
				$this->db->where(array('NO_KK' => $value));
			}*/
			/*if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}*/
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
		}
		// $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$query = $this->db->get('S2_VIEW_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return 0;
	}

	public function viewRumahTangga($id_rt) {

		$this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $id_rt));
		$query = $this->db->get('S2_VIEW_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function viewIndividu($id_rt,$no_art) {

		$this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $id_rt, 'NO_ART' => $no_art));
		$query = $this->db->get('S2_VIEW_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getRumahTangga($id_rt) {

		$this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $id_rt));
		$query = $this->db->get('BDT_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getRumahTanggaKK($rid_rumahtangga) {

		$this->db->where(array('RID_RUMAHTANGGA' => $rid_rumahtangga));
		$query = $this->db->get('BDT_RUMAHTANGGA');

		$this->db->where(array('RID_RUMAHTANGGA' => $rid_rumahtangga));
		$query2 = $this->db->get('PLUS_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		} else if ($query2->num_rows() > 0) {
			return keysToLower($query2->row_array());
		}

		return false;
	}

	public function getIndividu($nik) {

		// $this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $id_rt));
		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('BDT_INDIVIDU');

		$this->db->where(array('NIK' => $nik));
		$query2 = $this->db->get('PLUS_INDIVIDU');
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		} else if ($query2->num_rows() > 0) {
			return keysToLower($query2->row_array());
		}

		return false;
	}

	public function getRID_Rumahtangga_nik($nik) {

		$this->db->select('RID_RUMAHTANGGA');
		$this->db->where(array('NIK' => $nik, 'NO_ART' => 1));
		$query = $this->db->get('BDT_INDIVIDU');
		
		$this->db->select('RID_RUMAHTANGGA');
		$this->db->where(array('NIK' => $nik, 'NO_ART' => 1));
		$query2 = $this->db->get('PLUS_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		} else if ($query2->num_rows() > 0) {
			return keysToLower($query2->row_array());
		}

		return false;
	}

	public function getRID_Rumahtangga($no_kk) {

		$this->db->select('RID_RUMAHTANGGA');
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('BDT_RUMAHTANGGA');

		/*$this->db->select('RID_RUMAHTANGGA');
		$this->db->where(array('NO_KK' => $no_kk));
		$query2 = $this->db->get('PLUS_RUMAHTANGGA');*/

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		} /*else if ($query2->num_rows() > 0) {
			return keysToLower($query2->row_array());
		}*/

		return false;
	}

	public function getRID_Individu($nik) {

		$this->db->select('RID_INDIVIDU');
		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('BDT_INDIVIDU');

		$this->db->select('RID_INDIVIDU');
		$this->db->where(array('NIK' => $nik));
		$query2 = $this->db->get('PLUS_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		} else if ($query2->num_rows() > 0) {
			return keysToLower($query2->row_array());
		}

		return false;
	}

	public function get_b4_k4($id) {

		$this->db->select_max('B4_K4');
		$this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $id));
		$query = $this->db->get('BDT_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row());
		}

		return false;
	}

	public function get_no_art($id) {

		$this->db->select_max('NO_ART');
		$this->db->where(array('NOMOR_URUT_RUMAH_TANGGA' => $id));
		$query = $this->db->get('BDT_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row());
		}

		return false;
	}

	public function updateNik($id, $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}
			$key = strtoupper($key);
			$data[$key] = $value;
		}
		// var_dump($data);
		$ids = explode("|", $id);
        $id_rt = $ids[0];
        $no_art = $ids[1];
        // var_dump($ids);
		$where = array('NOMOR_URUT_RUMAH_TANGGA' => $id_rt, 'NO_ART' => $no_art);
		$ret = $this->db->update('BDT_INDIVIDU', $data, $where);
		// var_dump($this->db->last_query());
	}

	public function deleteNik($nik){
		$data = array(
	        'NIK' => NULL);

		$where = array('NIK' => $nik);
		$ret = $this->db->update('BDT_INDIVIDU', $data, $where);
	}
	
}
