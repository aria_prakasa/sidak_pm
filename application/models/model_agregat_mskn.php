<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_agregat_mskn extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function getagrMiskin($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
					}
					$this->db->select('NAMA_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
				} else {
					$this->db->select('NAMA_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
					$this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
				}
			}
		}

		$query = $this->db->get('V_AGR_MISKIN');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getagrBpjs($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
					}
					$this->db->select('NAMA_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
				} else {
					$this->db->select('NAMA_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
					$this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
				}
			}
		}

		$query = $this->db->get('V_AGR_BPJS');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getagrAgama_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
						/*$this->db->select('BLN,
							                    KODE,
							                    SUM (LK) AS LK,
							                    SUM (PR) AS PR,
							                    (SUM (LK) + SUM (PR)) AS JUMLAH', FALSE);
							                    $this->db->order_by('KODE');
							                    $this->db->group_by(array('BLN','KODE'));
							                    $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
						*/
					}
					/*else
						                    {
						                    $this->db->order_by('NO_KEL');
						                    $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
						                    $query = $this->db->get('STT_AGR_AGAMA');
					*/
					$this->db->order_by('NO_KEL');
					// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
					$query = $this->db->get('V_AGR_AGAMA');
					// var_dump($this->db->last_query());die;
				} else {
					$this->db->select('NO_KEC,
									NAMA_KEC,
									SUM(ISLAM) ISLAM,
									SUM(KRISTEN) KRISTEN,
									SUM(KATHOLIK) KATHOLIK,
									SUM(HINDU) HINDU,
									SUM(BUDHA) BUDHA,
									SUM(KONGHUCHU) KONGHUCHU,
									SUM(LAINNYA) LAINNYA,
                                    SUM(JUMLAH) AS JUMLAH', false);
					$this->db->order_by('NO_KEC');
					$this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
					// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
					$query = $this->db->get('V_AGR_AGAMA');
				}
			}
		}
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getagrGoldrh_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
						/*$this->db->select('BLN,
							                    KODE,
							                    SUM (LK) AS LK,
							                    SUM (PR) AS PR,
							                    (SUM (LK) + SUM (PR)) AS JUMLAH', FALSE);
							                    $this->db->order_by('KODE');
							                    $this->db->group_by(array('BLN','KODE'));
							                    $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
						*/
					}
					/*else
						                    {
						                    $this->db->order_by('NO_KEL');
						                    $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
						                    $query = $this->db->get('V_AGR_GOL_DRH');
					*/
					$this->db->order_by('NO_KEL');
					// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
					$query = $this->db->get('V_AGR_GOL_DARAH');
				} else {
					$this->db->select('NO_KEC,
							         NAMA_KEC,
							         SUM (A) A,
							         SUM (B) B,
							         SUM (AB) AB,
							         SUM (O) O,
							         SUM (A_PLUS) A_PLUS,
							         SUM (A_MIN) A_MIN,
							         SUM (B_PLUS) B_PLUS,
							         SUM (B_MIN) B_MIN,
							         SUM (AB_PLUS) AB_PLUS,
							         SUM (AB_MIN) AB_MIN,
							         SUM (O_PLUS) O_PLUS,
							         SUM (O_MIN) O_MIN,
							         SUM (TDK_TAHU) TDK_TAHU,
                                     SUM(JUMLAH) AS JUMLAH', false);
					$this->db->order_by('NO_KEC');
					$this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
					// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
					$query = $this->db->get('V_AGR_GOL_DARAH');
				}
			}
		}
		//var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getagrPddkan_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
						/*$this->db->select('BLN,
							                    KODE,
							                    SUM (LK) AS LK,
							                    SUM (PR) AS PR,
							                    (SUM (LK) + SUM (PR)) AS JUMLAH', FALSE);
							                    $this->db->order_by('KODE');
							                    $this->db->group_by(array('BLN','KODE'));
							                    $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
						*/
					}
					/*else
						                    {
						                    $this->db->order_by('NO_KEL');
						                    $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
						                    $query = $this->db->get('STT_AGR_PENDIDIKAN');
					*/
					$this->db->order_by('NO_KEL');
					// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
					$query = $this->db->get('V_AGR_PENDIDIKAN');
				} else {
					$this->db->select('NO_KEC,
							         NAMA_KEC,
							         SUM (TDK_SKLH) TDK_SKLH,
							         SUM (BLM_TMT_SD) BLM_TMT_SD,
							         SUM (TMT_SD) TMT_SD,
							         SUM (SLTP) SLTP,
							         SUM (SLTA) SLTA,
							         SUM (D_I) D_I,
							         SUM (D_III) D_III,
							         SUM (S_I) S_I,
							         SUM (S_II) S_II,
							         SUM (S_III) S_III,
                                     SUM(JUMLAH) AS JUMLAH', false);
					$this->db->order_by('NO_KEC');
					$this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
					// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
					$query = $this->db->get('V_AGR_PENDIDIKAN');
				}
			}
		}
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getagrSHDK_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
						/*$this->db->select('BLN,
							                    KODE,
							                    SUM (LK) AS LK,
							                    SUM (PR) AS PR,
							                    (SUM (LK) + SUM (PR)) AS JUMLAH', FALSE);
							                    $this->db->order_by('KODE');
							                    $this->db->group_by(array('BLN','KODE'));
							                    $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
						*/
					}
					/*else
						                    {
						                    $this->db->order_by('NO_KEL');
						                    $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
						                    $query = $this->db->get('STT_AGR_PENDIDIKAN');
					*/
					$this->db->order_by('NO_KEL');
					// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
					$query = $this->db->get('V_AGR_STAT_HBKEL');
				} else {
					$this->db->select('NO_KEC,
                                    NAMA_KEC,
                                    SUM(KEPALA_KELUARGA) AS  KEPALA_KELUARGA,
                                    SUM(SUAMI) AS  SUAMI,
                                    SUM(ISTRI) AS  ISTRI,
                                    SUM(ANAK) AS  ANAK,
                                    SUM(MENANTU) AS  MENANTU,
                                    SUM(CUCU) AS  CUCU,
                                    SUM(ORANG_TUA) AS  ORANG_TUA,
                                    SUM(MERTUA) AS  MERTUA,
                                    SUM(FAMILI_LAIN) AS  FAMILI_LAIN,
                                    SUM(PEMBANTU) AS  PEMBANTU,
                                    SUM(LAINNYA) AS  LAINNYA,
                                    SUM(JUMLAH) AS JUMLAH', false);
					$this->db->order_by('NO_KEC');
					$this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
					// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
					$query = $this->db->get('V_AGR_STAT_HBKEL');
				}
			}
		}
		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getagrUmur_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
					}
				}
			}
		}
		$this->db->select('KODE,
				         STRUKTUR_UMUR,
				         SUM (LK) AS LK,
				         SUM (PR) AS PR,
				         (SUM (LK) + SUM (PR)) AS JUMLAH', false);
		$this->db->order_by('KODE');
		$this->db->group_by(array('KODE', 'STRUKTUR_UMUR'));
		// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
		$query = $this->db->get('V_AGR_STRUKTUR_UMUR');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getagrUmurBppd_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
					}
				}
			}
		}
		$this->db->select('USIA,
                         STRUKTUR_UMUR,
                         SUM (LK) AS LK,
                         SUM (PR) AS PR,
                         (SUM (LK) + SUM (PR)) AS JUMLAH', false);
		$this->db->order_by('USIA');
		$this->db->group_by(array('USIA', 'STRUKTUR_UMUR'));
		// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
		$query = $this->db->get('V_AGR_UMUR_BPPD');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getagrWajibktp_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
					}
					$this->db->select('NAMA_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
				} else {
					$this->db->select('NAMA_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
					$this->db->group_by(array('NO_KEC', 'NAMA_KEC'));
				}
			}
		}
		// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
		$query = $this->db->get('V_AGR_WAJIB_KTP');

		//var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getagrPkrjn_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
					}
				}
			}
		}
		$this->db->select('KODE,
                         PEKERJAAN,
                         SUM (LK) AS LK,
                         SUM (PR) AS PR,
                         (SUM (LK) + SUM (PR)) AS JUMLAH', false);
		$this->db->order_by('KODE');
		$this->db->group_by(array('KODE', 'PEKERJAAN'));
		// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
		$query = $this->db->get('V_AGR_PEKERJAAN_SIDAK');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getagrPkrjn($kd_prop, $kd_kab, $kd_kec, $kd_kel) {
		if ($kd_prop) {
			$this->db->where('NO_PROP', $kd_prop);
			if ($kd_kab) {
				$this->db->where('NO_KAB', $kd_kab);
				if ($kd_kec) {
					$this->db->where('NO_KEC', $kd_kec);
					if ($kd_kel) {
						$this->db->where('NO_KEL', $kd_kel);
					}
				}
			}
		}
		$this->db->select('KODE,
                         PEKERJAAN,
                         SUM (LK) AS LK,
                         SUM (PR) AS PR,
                         (SUM (LK) + SUM (PR)) AS JUMLAH', false);
		$this->db->order_by('KODE');
		$this->db->group_by(array('KODE', 'PEKERJAAN'));
		// $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
		$query = $this->db->get('V_AGR_PEKERJAAN_SIAK');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	/*public function getagrKTP($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan)
		    {
		        if ($kd_prop) {
		            $this->db->where('NO_PROP', $kd_prop);
		            if ($kd_kab) {
		                $this->db->where('NO_KAB', $kd_kab);
		                if ($kd_kec) {
		                    $this->db->where('NO_KEC', $kd_kec);
		                    if ($kd_kel) {
		                        $this->db->where('NO_KEL', $kd_kel);
		                    }
		                    $this->db->select('NAMA_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
		                } else {
		                    $this->db->select('NAMA_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
		                    $this->db->group_by('NAMA_KEC');
		                }
		            }
		        }
		        $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
		        $query = $this->db->get('STT_AGR_KEPEM_KTP');

		        //var_dump($this->db->last_query());die;

		        if ($query->num_rows() > 0) {
		            return keysToLower($query->result());
		        }

		        return false;
		    }

		    public function getagrKTP17($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan)
		    {
		        if ($kd_prop) {
		            $this->db->where('NO_PROP', $kd_prop);
		            if ($kd_kab) {
		                $this->db->where('NO_KAB', $kd_kab);
		                if ($kd_kec) {
		                    $this->db->where('NO_KEC', $kd_kec);
		                    if ($kd_kel) {
		                        $this->db->where('NO_KEL', $kd_kel);
		                    }
		                    $this->db->select('NAMA_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
		                } else {
		                    $this->db->select('NAMA_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
		                    $this->db->group_by('NAMA_KEC');
		                }
		            }
		        }
		        $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
		        $query = $this->db->get('STT_AGR_KEPEM_KTP_17');

		        //var_dump($this->db->last_query());die;

		        if ($query->num_rows() > 0) {
		            return keysToLower($query->result());
		        }

		        return false;
		    }

		    public function getagrKwn17 ($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan)
		    {
		        if ($kd_prop) {
		            $this->db->where('NO_PROP', $kd_prop);
		            if ($kd_kab) {
		                $this->db->where('NO_KAB', $kd_kab);
		                if ($kd_kec) {
		                    $this->db->where('NO_KEC', $kd_kec);
		                    if ($kd_kel) {
		                        $this->db->where('NO_KEL', $kd_kel);
		                    }
		                    $this->db->select('NAMA_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
		                } else {
		                    $this->db->select('NAMA_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
		                    $this->db->group_by('NAMA_KEC');
		                }
		            }
		        }
		        $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
		        $query = $this->db->get('STT_AGR_17TH_MENIKAH');

		        //var_dump($this->db->last_query());die;

		        if ($query->num_rows() > 0) {
		            return keysToLower($query->result());
		        }

		        return false;
		    }

		    public function getagrKK ($kd_prop, $kd_kab, $kd_kec, $kd_kel, $bulan)
		    {
		        if ($kd_prop) {
		            $this->db->where('NO_PROP', $kd_prop);
		            if ($kd_kab) {
		                $this->db->where('NO_KAB', $kd_kab);
		                if ($kd_kec) {
		                    $this->db->where('NO_KEC', $kd_kec);
		                    if ($kd_kel) {
		                        $this->db->where('NO_KEL', $kd_kel);
		                    }
		                    $this->db->select('NAMA_KEL,LK,PR,(LK+PR) AS JUMLAH', false);
		                } else {
		                    $this->db->select('NAMA_KEC, SUM(LK) AS LK, SUM(PR) AS PR, (SUM(LK)+SUM(PR)) AS JUMLAH', false);
		                    $this->db->group_by('NAMA_KEC');
		                }
		            }
		        }
		        $this->db->where("BLN LIKE TO_DATE('$bulan','MM/YYYY')");
		        $query = $this->db->get('STT_AGR_KEPALA_KELUARGA');

		        //var_dump($this->db->last_query());die;

		        if ($query->num_rows() > 0) {
		            return keysToLower($query->result());
		        }

		        return false;
	*/
}
