<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_s2_list extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function getRumahTanggaRelease($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK_KEP_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('B1_R8', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
		}
		if ($data['TGL_AWAL'] && $data['TGL_AKHIR']) {
			$tgl_awal = $data['TGL_AWAL'];
			$tgl_akhir = $data['TGL_AKHIR'];
			$tgl_entri = "TGL_ENTRI BETWEEN TO_DATE('".$tgl_awal."','DD-MM-YYYY')
                                      AND TO_DATE('".$tgl_akhir."','DD-MM-YYYY') +1 ";
			$this->db->where($tgl_entri);
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA_RUMAHTANGGA', 'ASC');
		$query = $this->db->get('S2_VIEW_LIST_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getRumahTanggaReleaseTotal($where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK_KEP_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('B1_R8', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
		}
		if ($data['TGL_AWAL'] && $data['TGL_AKHIR']) {
			$tgl_awal = $data['TGL_AWAL'];
			$tgl_akhir = $data['TGL_AKHIR'];
			$tgl_entri = "TGL_ENTRI BETWEEN TO_DATE('".$tgl_awal."','DD-MM-YYYY')
                                      AND TO_DATE('".$tgl_akhir."','DD-MM-YYYY') +1 ";
			$this->db->where($tgl_entri);
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_LIST_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	public function getRumahTanggaList($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK_KEP_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('B1_K8', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
		}
		if ($data['TGL_AWAL'] && $data['TGL_AKHIR']) {
			$tgl_awal = $data['TGL_AWAL'];
			$tgl_akhir = $data['TGL_AKHIR'];
			$tgl_entri = "TGL_ENTRI BETWEEN TO_DATE('".$tgl_awal."','DD-MM-YYYY')
                                      AND TO_DATE('".$tgl_akhir."','DD-MM-YYYY') +1 ";
			$this->db->where($tgl_entri);
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA_RUMAHTANGGA', 'ASC');
		$query = $this->db->get('S2_VIEW_LIST_RUMAHTANGGA_FULL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getRumahTanggaVerifikasi($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK_KEP_RUMAHTANGGA' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('B1_R8', $value, 'both');
			}
			if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}
		}
		if ($data['TGL_AWAL'] && $data['TGL_AKHIR']) {
			$tgl_awal = $data['TGL_AWAL'];
			$tgl_akhir = $data['TGL_AKHIR'];
			$tgl_entri = "TGL_ENTRI BETWEEN TO_DATE('".$tgl_awal."','DD-MM-YYYY')
                                      AND TO_DATE('".$tgl_akhir."','DD-MM-YYYY') +1 ";
			$this->db->where($tgl_entri);
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA_RUMAHTANGGA', 'ASC');
		$query = $this->db->get('S2_VIEW_VERIFIKASI_RUTA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getIndividuRelease($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
			/*if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}*/
		}
		if ($data['TGL_AWAL'] && $data['TGL_AKHIR']) {
			$tgl_awal = $data['TGL_AWAL'];
			$tgl_akhir = $data['TGL_AKHIR'];
			$tgl_entri = "TGL_ENTRI BETWEEN TO_DATE('".$tgl_awal."','DD-MM-YYYY')
                                      AND TO_DATE('".$tgl_akhir."','DD-MM-YYYY') +1 ";
			$this->db->where($tgl_entri);
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA_RUMAHTANGGA', 'ASC');
		$query = $this->db->get('S2_VIEW_LIST_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getIndividuReleaseTotal($where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
			/*if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}*/
		}
		if ($data['TGL_AWAL'] && $data['TGL_AKHIR']) {
			$tgl_awal = $data['TGL_AWAL'];
			$tgl_akhir = $data['TGL_AKHIR'];
			$tgl_entri = "TGL_ENTRI BETWEEN TO_DATE('".$tgl_awal."','DD-MM-YYYY')
                                      AND TO_DATE('".$tgl_akhir."','DD-MM-YYYY') +1 ";
			$this->db->where($tgl_entri);
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_LIST_INDIVIDU');

		// var_dump($this->db->last_query());die;
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return false;
	}

	public function getIndividuList($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
			/*if (stristr($key, 'rt')) {
				$this->db->where(array('NO_RT' => $value));
			}
			if (stristr($key, 'rw')) {
				$this->db->where(array('NO_RW' => $value));
			}*/
		}
		if ($data['TGL_AWAL'] && $data['TGL_AKHIR']) {
			$tgl_awal = $data['TGL_AWAL'];
			$tgl_akhir = $data['TGL_AKHIR'];
			$tgl_entri = "TGL_ENTRI BETWEEN TO_DATE('".$tgl_awal."','DD-MM-YYYY')
                                      AND TO_DATE('".$tgl_akhir."','DD-MM-YYYY') +1 ";
			$this->db->where($tgl_entri);
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->where(array('FLAG_PROSES' => 1));
		// $this->db->order_by('NAMA_RUMAHTANGGA', 'ASC');
		$query = $this->db->get('S2_VIEW_LIST_INDIVIDU_FULL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function insertRumahTangga($no_kk,$tgl_cacah,$tgl_periksa,$params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    $res = $this->db->insert('LIST_RUMAHTANGGA', $data);
	    // die(var_dump($this->db->last_query()));	
	    $this->db->query("UPDATE LIST_RUMAHTANGGA SET TGL_ENTRI = SYSDATE, TGL_CACAH = TO_DATE('".$tgl_cacah."', 'DD-MM-YYYY'), TGL_PERIKSA = TO_DATE('".$tgl_periksa."', 'DD-MM-YYYY') WHERE NO_KK = ".$no_kk);
	    $petugas = strtoupper($this->cu->NAMA_LENGKAP);
	    $b3_r3 = $data['B3_R3'];
		$res = $this->db->query("UPDATE SKRINING_RUMAHTANGGA SET B3_R3 = ".$b3_r3.", PROSES = 3, TGL_PROSES = SYSDATE, PETUGAS_PROSES = '".$petugas."' WHERE NO_KK_INDUK = ".$no_kk);
	    // die(var_dump($this->db->last_query()));	    	

	    return $res;
	}

	public function insertIndividu($nik,$params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    $res = $this->db->insert('LIST_INDIVIDU', $data);
	    // die(var_dump($this->db->last_query()));	
	    // $petugas = $this->cu->NAMA_LENGKAP;
	    $this->db->query("UPDATE LIST_INDIVIDU SET TGL_ENTRI = SYSDATE WHERE NIK = ".$nik);
	    // die(var_dump($this->db->last_query()));	    	

	    return $res;
	}

	public function updateRumahTangga($no_kk,$tgl_cacah,$tgl_periksa,$params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $rumahtangga = array_change_key_case($params, CASE_UPPER);
	    // var_dump($rumahtangga);die;
	    $this->db->query("UPDATE LIST_RUMAHTANGGA SET TGL_RUBAH = SYSDATE, TGL_CACAH = TO_DATE('".$tgl_cacah."', 'DD-MM-YYYY'), TGL_PERIKSA = TO_DATE('".$tgl_periksa."', 'DD-MM-YYYY') WHERE NO_KK = ".$no_kk);
	    $this->db->where('NO_KK', $no_kk);
	    $res = $this->db->update('LIST_RUMAHTANGGA', $rumahtangga);
	    // die(var_dump($this->db->last_query()));	    	

	    return $res;
	}

	public function updateUsaha($no_kk,$b5_r5a){

		$petugas = strtoupper($this->cu->NAMA_LENGKAP);
	    $res = $this->db->query("UPDATE LIST_RUMAHTANGGA SET TGL_RUBAH = SYSDATE, PETUGAS_RUBAH = '".$petugas."', B5_R5A = ".$b5_r5a." WHERE NO_KK = ".$no_kk);
	    // die(var_dump($this->db->last_query()));		    	

	    return $res;
	}

	public function updateJmlArt($no_kk,$no_art){

		$petugas = strtoupper($this->cu->NAMA_LENGKAP);
		$res = $this->db->query("UPDATE LIST_RUMAHTANGGA SET TGL_RUBAH = SYSDATE, B1_R9 = ".$no_art.", PETUGAS_RUBAH = '".$petugas."' WHERE NO_KK = ".$no_kk);
		/*if ($no_art == 1) {
	    	$res = $this->db->query("UPDATE LIST_RUMAHTANGGA SET B1_R9 = 1, PETUGAS_RUBAH = '".$petugas."' WHERE NO_KK = ".$no_kk);
		} else {
			$res = $this->db->query("UPDATE LIST_RUMAHTANGGA SET B1_R9 = B1_R9 + 1, PETUGAS_RUBAH = '".$petugas."' WHERE NO_KK = ".$no_kk);
		}*/
	    // die(var_dump($this->db->last_query()));		    	

	    return $res;
	}

	public function updateJmlKK($no_kk,$b1_r10){

		$petugas = strtoupper($this->cu->NAMA_LENGKAP);
		$res = $this->db->query("UPDATE LIST_RUMAHTANGGA SET TGL_RUBAH = SYSDATE, B1_R10 = ".$b1_r10.", PETUGAS_RUBAH = '".$petugas."' WHERE NO_KK = ".$no_kk);
	    // die(var_dump($this->db->last_query()));		    	

	    return $res;
	}

	public function updateRID_Rumahtangga($no_kk,$rid_ruta){

		$petugas = strtoupper($this->cu->NAMA_LENGKAP);
		$res = $this->db->query("UPDATE LIST_RUMAHTANGGA SET TGL_RUBAH = SYSDATE, RID_RUMAHTANGGA = ".$rid_ruta.", PETUGAS_RUBAH = '".$petugas."' WHERE NO_KK = ".$no_kk);
	    // die(var_dump($this->db->last_query()));		    	

	    return $res;
	}

	public function updateKepalaRuta($no_kk,$nama,$nik){

		$petugas = strtoupper($this->cu->NAMA_LENGKAP);
		$res = $this->db->query("UPDATE LIST_RUMAHTANGGA SET TGL_RUBAH = SYSDATE, NIK_KEP_RUMAHTANGGA = ".$nik.", B1_R8 = '".$nama."', PETUGAS_RUBAH = '".$petugas."' WHERE NO_KK = ".$no_kk);
	    // die(var_dump($this->db->last_query()));		    	

	    return $res;
	}

	public function updateTambahKK($no_kk){

		$petugas = strtoupper($this->cu->NAMA_LENGKAP);
	    $res = $this->db->query("DELETE LIST_RUMAHTANGGA WHERE NO_KK = ".$no_kk);
	    $res = $this->db->query("DELETE LIST_INDIVIDU WHERE NO_KK = ".$no_kk);
	    // die(var_dump($this->db->last_query()));		    	

	    return $res;
	}

	public function deleteRumahtangga($no_kk) {
		// $this->db->where('NO_KK', $no_kk);
		$petugas = strtoupper($this->cu->NAMA_LENGKAP);
		$res = $this->db->query("DELETE LIST_RUMAHTANGGA WHERE NO_KK = ".$no_kk);
	    $res = $this->db->query("DELETE LIST_INDIVIDU WHERE NO_KK = ".$no_kk);
	    $this->db->query("UPDATE SKRINING_RUMAHTANGGA SET PROSES = 1, VERIFIKASI = 1, TGL_RUBAH = SYSDATE, PETUGAS_RUBAH = '".$petugas."' WHERE NO_KK_INDUK = ".$no_kk);
		// var_dump($this->db->last_query());die;

		return $res;
	}

	public function resetRumahtangga($no_kk) {
		// $this->db->where('NO_KK', $no_kk);
		$petugas = strtoupper($this->cu->NAMA_LENGKAP);
	    $res = $this->db->query("UPDATE SKRINING_RUMAHTANGGA SET PROSES = 1, VERIFIKASI = 1, TGL_RUBAH = SYSDATE, PETUGAS_RUBAH = '".$petugas."' WHERE NO_KK_INDUK = ".$no_kk);
		// var_dump($this->db->last_query());die;

		return $res;
	}

	public function updateIndividu($nik,$params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $individu = array_change_key_case($params, CASE_UPPER);
	    // var_dump($individu);die;
	    $petugas = strtoupper($this->cu->NAMA_LENGKAP);
	    $this->db->query("UPDATE LIST_INDIVIDU SET TGL_RUBAH = SYSDATE, PETUGAS_RUBAH = '".$petugas."' WHERE NIK = ".$nik);
	    $this->db->where('NIK', $nik);
	    $res = $this->db->update('LIST_INDIVIDU', $individu);
	    // die(var_dump($this->db->last_query()));		    	

	    return $res;
	}

	public function getData($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('LIST_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function getDataAnggota($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('S2_VIEW_LIST_INDIVIDU_FULL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result_array());
		}

		return false;
	}

	public function getDataIndividu($nik) {

		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('LIST_INDIVIDU');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function getNoArt($no_kk){
	    
	    $this->db->select_max('NO_ART');
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('LIST_INDIVIDU');

		if ($query->num_rows() > 0) {
			// return $data;
			return keysToLower($query->row_array());
		}
		return false;
	}

	public function getRID_Rumahtangga($no_kk){
	    
		$this->db->select('RID_RUMAHTANGGA');
		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('LIST_RUMAHTANGGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function deleteIndividu($nik) {
		$this->db->where('NIK', $nik);
		$query = $this->db->delete('LIST_INDIVIDU');
		// var_dump($this->db->last_query());die;

		return $query;
	}
}
