<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Omaster extends Otable
{

    public $table_name = "";
    public $row        = null;
    public $ID         = null;

    // $O = new Omaster(1);
    // $O->delete();

    public function __construct($ID = null, $type = "ID")
    {        
        if (!empty($ID)) {
            $arr = array("NO" => $ID);

            $res = $this->db->get_where($this->table_name, $arr);
            // var_dump($this->db->last_query());
            // $res = $this->db->where($arr)
            //                 ->get($this->table_name);
            if (!emptyres($res)) {
                $this->row = $res->row();
                $this->ID  = $this->row->NO;
            }
        }
    }

    public function setup($row = null)
    {
        if (is_object($row) || is_array($row)) {
            if (is_array($row)) {
                $row = (object) $row;
            }

            $this->row = $row;
            $this->ID  = $row->NO;
        }
        return false;
    }

    public function get_name()
    {
        if(!$this->row) return false;
        return $this->row->DESCRIP;
    }

    public function drop_down_select($name, $selval, $optional = "", $default = "", $readonly = "")
    {
        $list = $this->get_list(0, 0, "");

        $Otmp = new self();
        foreach ($list as $r) {
            $Otmp->setup($r);
            $arr[$Otmp->ID] = $Otmp->get_name();
        }
        unset($Otmp);
        return dropdown($name, $arr, $selval, $optional, $default, $readonly);
    }
}
