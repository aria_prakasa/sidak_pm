<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Oadmin extends Otable
{

    public $table_name = "ADMIN";
    public $row        = null;
    public $ID         = null;

    public function __construct($ID = null)
    {
        // // $this->db = $this->load->database('pullout', TRUE);

        if (!empty($ID)) {
            $arr = array("USERNAME" => $ID);
            $res = $this->db->get_where($this->table_name, $arr);
            // var_dump($this->db->last_query());
            if (!emptyres($res)) {
                $this->row = $res->row();
                $this->ID  = $this->row->USERNAME;
            }
        }
    }

    public function setup($row = null)
    {
        if (is_object($row)) {
            $this->row = $row;
            $this->ID  = $row->USERNAME;
        }
        return false;
    }

    public function edit($arr)
    {
        return $this->db->update($this->table_name, $arr, array("USERNAME" => $this->ID));
    }

}
