<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_s2_biodata extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function getBiodata($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_LGKP', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		// $this->db->order_by('NAMA_LGKP', 'ASC');
		$query = $this->db->get('S2_GETBIODATA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getBiodataTotal($where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			if (stristr($key, 'kk')) {
				$this->db->where(array('NO_KK' => $value));
			}
			if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA_LGKP', $value, 'both');
			}
		}
		$this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$query = $this->db->get('S2_GETBIODATA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return 0;
	}

	public function getKepala($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$query = $this->db->get('S2_GETDATAKELUARGA');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getAnggota($no_kk) {

		$this->db->where(array('NO_KK' => $no_kk));
		$this->db->order_by('STAT_HBKEL ASC, UMUR DESC');
		// $this->db->order_by('TGL_LAHIR', 'ASC');
		$query = $this->db->get('S2_GETBIODATA_DETAIL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getAnggotaInsert($no_kk) {

		$this->db->select("NO_KK, NIK, NAMA_LGKP, JENIS_KLMIN, TGL_LHR, STAT_HBKEL, STAT_KWN, JENIS_PKRJN, PDDK_AKH, AGAMA, NO_PROP, NO_KAB, NO_KEC, NO_KEL, FLAG_BDT, FLAG_PROSES, FLAG_BPJS, FLAG_KIP, FLAG_PBI, FLAG_SPM, FLAG_RTSPM, FLAG_PKH, FLAG_KKS");
		$this->db->where(array('NO_KK' => $no_kk));
		$this->db->order_by('STAT_HBKEL ASC, TGL_LHR ASC');
		// $this->db->order_by('TGL_LAHIR', 'ASC');
		$query = $this->db->get('S2_GETBIODATA_DETAIL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getAnggotaPrelist($no_kk) {

		$this->db->where(array('NO_KK_INDUK' => $no_kk));
		$this->db->order_by('NO_URUT_KK, NO_URUT_ART, NO_KK, STAT_HBKEL ASC, UMUR DESC');
		// $this->db->order_by('TGL_LAHIR', 'ASC');
		$query = $this->db->get('S2_GETBIODATA_DETAIL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getAnggotaList($no_kk) {

		$this->db->where(array('NO_KK_INDUK' => $no_kk,
								'PROSES_IND' => 2));
		$this->db->order_by('NO_URUT_KK, NO_URUT_ART, STAT_HBKEL ASC, UMUR DESC');
		// $this->db->order_by('TGL_LAHIR', 'ASC');
		$query = $this->db->get('S2_GETBIODATA_DETAIL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getIndividu($nik) {

		$this->db->where(array('NIK' => $nik));
		$query = $this->db->get('S2_GETBIODATA_DETAIL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

	public function getNikKK($no_kk) {

		$this->db->select('NIK');
		$this->db->where(array('NO_KK' => $no_kk, 'STAT_HBKEL' => 1));
		$query = $this->db->get('S2_GETBIODATA_DETAIL');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row_array());
		}

		return false;
	}

	public function updateIndividu($nik) {
		// set flag proses, 1 = mohon, 2 = accept.
		$this->db->set('FLAG_PROSES', '1');
		$this->db->where(array('NIK' => $nik));
		return $this->db->update('BIODATA_PENDUDUK');
	}

	public function deleteIndividu($nik) {
		// set flag proses, 1 = mohon, 2 = accept.
		$this->db->set('FLAG_PROSES', '0');
		$this->db->where(array('NIK' => $nik));
		return $this->db->update('BIODATA_PENDUDUK');
	}

	public function deleteIndividuRumahTangga($no_kk) {
		// set flag proses, 1 = mohon, 2 = accept.
		$this->db->set('FLAG_PROSES', '0');
		$this->db->where(array('NO_KK' => $no_kk, 'FLAG_PROSES' => '1'));
		return $this->db->update('BIODATA_PENDUDUK');
	}

	public function deleteIndividuRumahTangga_all($no_kk) {
		// set flag proses, 1 = mohon, 2 = accept.
		/*$this->db->set('FLAG_PROSES', '0');
		$this->db->where(array('NO_KK' => $no_kk);
		return $this->db->update('BIODATA_PENDUDUK');*/
		$query = $this->db->query("UPDATE BIODATA_PENDUDUK SET FLAG_PROSES = NULL WHERE NO_KK IN (SELECT NO_KK FROM MHN_RUMAHTANGGA WHERE NO_KK_RUMAHTANGGA = '".$no_kk."')");
		// var_dump($this->db->last_query());die;
		return $query;
	}

	public function proses($nik){
		
		$data = array(
               'FLAG_PROSES' => '2',
               // 'PENETAPAN' => $penetapan,
               // 'PETUGAS_PENETAPAN' => $this->cu->NAMA_LENGKAP
            );

		// $this->db->where('NO_KK', $no_kk);
		$this->db->where('NIK', $nik);
		$res = $this->db->update('BIODATA_PENDUDUK', $data);

		// die(var_dump($this->db->last_query()));

		return $query;
	}

	public function proses_rtangga($no_kk){
		
		/*$data = array(
               'FLAG_PROSES' => '2',
               // 'PENETAPAN' => $penetapan,
               // 'PETUGAS_PENETAPAN' => $this->cu->NAMA_LENGKAP
            );

		// $this->db->where('NO_KK', $no_kk);
		$this->db->where('NIK', $nik);
		$res = $this->db->update('BIODATA_PENDUDUK', $data);*/
		$query = $this->db->query("UPDATE BIODATA_PENDUDUK SET FLAG_PROSES = '2' WHERE NO_KK IN (SELECT NO_KK FROM PRS_RUMAHTANGGA WHERE NO_KK_RUMAHTANGGA = '".$no_kk."')");
		// die(var_dump($this->db->last_query()));
		return $query;
	}

	public function penetapan($nik, $proses){
		
		$data = array(
               'FLAG_PROSES' => $proses,
               // 'PENETAPAN' => $penetapan,
               // 'PETUGAS_PENETAPAN' => $this->cu->NAMA_LENGKAP
            );

		// $this->db->where('NO_KK', $no_kk);
		$this->db->where('NIK', $nik);
		$res = $this->db->update('BIODATA_PENDUDUK', $data);

		// die(var_dump($this->db->last_query()));

		return $query;
	}

	public function cekStatusPddk($nik){
		
		$this->db->select('FLAG_STATUS,FLAG_BDT,FLAG_MISKIN,FLAG_KIS,FLAG_KESEHATAN,FLAG_SOSIAL,FLAG_PENDIDIKAN,FLAG_PROSES,FLAG_BPJS,FLAG_KKS');
		$this->db->where('NIK', $nik);
		$query = $this->db->get('BIODATA_PENDUDUK');

		// die(var_dump($this->db->last_query()));

		return $query;
	}

	public function cekStatusKks($no_kk){
		
		$this->db->select('FLAG_KKS');
		$this->db->where('NO_KK_INDUK', $no_kk);
		$this->db->where('FLAG_KKS', 2);
		$query = $this->db->get('S2_GETBIODATA_DETAIL');

		// die(var_dump($this->db->last_query()));

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

}
