<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_s2_verifikasi extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function insertRumahTangga($no_kk, $tgl_cacah, $tgl_periksa, $params = array()){
	    
	    if (count($params) <= 0) {
	        return false;
	    }

	    $data = null;
	    foreach ($params as $key => $value) {
	        $key        = strtoupper($key);
	        $data[$key] = $value;
	    }
	    $petugas = $this->cu->NAMA_LENGKAP;
	    $res = $this->db->insert('PRS_RUMAHTANGGA', $data);
	    // die(var_dump($this->db->last_query()));	    	
	    $this->db->query("UPDATE PRS_RUMAHTANGGA SET TGL_CACAH = TO_DATE('".$tgl_cacah."', 'DD-MM-YYYY'), TGL_PERIKSA = TO_DATE('".$tgl_periksa."', 'DD-MM-YYYY'), PETUGAS_PROSES = '".$petugas."', PROSES = 2, TGL_PERMOHONAN = SYSDATE WHERE NO_KK = ".$no_kk);
	    // $this->db->query("UPDATE PRS_RUMAHTANGGA SET PETUGAS_PROSES = '".$petugas."', PROSES = 2, TGL_PERMOHONAN = SYSDATE WHERE NO_KK = ".$no_kk);

	    return $res;
	}
}
