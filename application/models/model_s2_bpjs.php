<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Model_s2_bpjs extends CI_model {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->db = $this->load->database('default', true);
		// $this->db_pullout = $this->load->database('pullout', true);
	}

	public function getList($offset = 0, $limit = 0, $orderby = "", $where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			// var_dump($data);die;
			/*if (stristr($key, 'no_kk')) {
				$this->db->where(array('NO_KK' => $value));
			}*/
			/*if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}*/
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
		}
		// $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (intval($limit) > 0) {
			$this->db->limit($limit, $offset);
		}
		if (trim($orderby) != "") {
			$this->db->order_by($orderby);
		}
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$this->db->order_by('NAMA', 'ASC');
		$query = $this->db->get('S2_VIEW_BPJS');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->result());
		}

		return false;
	}

	public function getListTotal($where = "", $params = array()) {
		$data = null;
		foreach ($params as $key => $value) {
			if (empty($value)) {
				continue;
			}

			$key = strtoupper($key);
			$data[$key] = $value;
			/*if (stristr($key, 'kk')) {
				$this->db->where(array('NO_KK' => $value));
			}*/
			/*if (stristr($key, 'nik')) {
				$this->db->where(array('NIK' => $value));
			}*/
			if (stristr($key, 'kec')) {
				$this->db->where(array('NO_KEC' => $value));
			}
			if (stristr($key, 'kel')) {
				$kelurahan_code = $value;
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				if (count($kelurahan_code_arr) == 2) {
					$entri['kd_kec'] = $kelurahan_code_arr[0];
					$value = $kelurahan_code_arr[1];
				}
				$this->db->where(array('NO_KEL' => $value));
			}
			if (stristr($key, 'nama')) {
				$this->db->like('NAMA', $value, 'both');
			}
		}
		// $this->db->where(array('NO_PROP' => NO_PROP, 'NO_KAB' => NO_KAB));
		if (trim($where) != "") {
			$this->db->where($where);
		}
		$query = $this->db->get('S2_VIEW_BPJS');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}

		return 0;
	}

	public function viewBpjs($no_bpjs) {

		$this->db->where(array('NO_BPJS' => $no_bpjs));
		$query = $this->db->get('S2_VIEW_BPJS');

		// var_dump($this->db->last_query());die;

		if ($query->num_rows() > 0) {
			return keysToLower($query->row());
		}

		return false;
	}

}
