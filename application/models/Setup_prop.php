<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup_prop extends Otable {

  public $table_name = "SETUP_PROP";
  public $row = NULL;
  public $ID = NULL;

	function __construct($ID=NULL, $type="ID")
  {
	  if(!empty($ID))
	  {
			$arr = array("NO_PROP" => $ID);
			if($type != "ID") $arr = array("URL_TITLE" => $ID);
			$res = $this->db->get_where($this->table_name,$arr);
			if(!emptyres($res))
			{
				$this->row = $res->row();
				$this->ID = $this->row->NO_PROP;
			}
	  }
  }

  function setup($row=NULL)
  {
    if(is_object($row) && intval($row->NO_PROP) > 0)
    {
      $this->row = $row;
      $this->ID = $row->NO_PROP;
    }
    return FALSE;
  }

  function get_name()
  {
  	return $this->row->NAMA_PROP;
  }

  function drop_down_select($name,$selval,$optional="",$default="",$readonly="")
  {
    $list = $this->get_list(0, 0, "NO_PROP ASC", "NO_PROP=".intval(NO_PROP));
    
    foreach($list as $r)
    {
      $arr[$r->NO_PROP] = $r->NAMA_PROP;
    }
    return dropdown($name,$arr,$selval,$optional,$default,$readonly);
  }

}