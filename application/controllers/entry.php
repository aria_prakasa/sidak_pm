<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Entry extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        $this->_init_wilayah();
        $this->load->model('model_s2_biodata');
        $this->load->model('model_s2_bdt');
        $this->load->model('model_s2_masterbdt');
        $this->load->model('model_s2_mohonbdt');
        $this->load->model('model_s2_skrining');
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        /*if ($this->cu->USER_LEVEL >= 2) {
            $isi['content']    = 'entry/closed';
        } else {
            $isi['content']    = 'entry/main_view';
        }*/
        $isi['content']    = 'entry/main_view';
        $isi['judul']      = 'Pemasukan Data';
        $isi['sub_judul']  = 'Entri';
        $isi['daftar_nav'] = 'active';
        $isi['entry_nav']  = 'active';

        if ($this->input->get())
        {
            if (($this->input->get('inputkk') != "") || ($this->input->get('inputnik') != "") || ($this->input->get('inputnama') != "")) 
            {

                $isi['show'] = null;
                $isi = array_merge($isi, $this->input->get());
                // var_dump($isi);die;
                if ($this->input->get('kecamatan_id') != "") {
                    $entri['kd_kec'] = $this->input->get('kecamatan_id');

                    if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                        $kelurahan_code = $this->input->get('kelurahan_id');
                        $kelurahan_code_arr = explode("-", $kelurahan_code);
                        $entri['kd_kec'] = $kelurahan_code_arr[0];
                        $entri['kd_kel'] = $kelurahan_code_arr[1];
                    }

                    if ($this->cu->USER_LEVEL == 2) {
                        $entri['kd_kec'] = $this->cu->NO_KEC;
                    }
                    if ($this->cu->USER_LEVEL == 3) {
                        $entri['kd_kec'] = $this->cu->NO_KEC;
                        $entri['kd_kel'] = $this->cu->NO_KEL;
                    }
                }

                if ($this->input->get('inputkk') != "") {

                    $no_kk = $this->input->get('inputkk', TRUE);
                    $entri['no_kk'] = $no_kk;
                    $isi['toggleKk'] = "checked";

                }
                if ($this->input->get('inputnik') != "") {

                    $nik = $this->input->get('inputnik', TRUE);
                    $entri['nik'] = $nik;
                    $isi['toggleNik'] = "checked";

                }
                if ($this->input->get('inputnama') != "") {

                    $n = $this->input->get('inputnama', TRUE);
                    $nama = strtoupper($n);
                    $entri['nama'] = $nama;
                    $isi['n'] = $n;
                    $isi['toggleNama'] = "checked";

                }

                $GET = $this->input->get();

                $offset = 0;
                if ($this->input->get('page') != '') {
                    $offset = $isi['page'] = $this->input->get('page');
                }

                $perpage = 10;

                if ($this->input->get('export')) {
                    $perpage = 0;
                }

                // TAMPIL ONLY
                // $where = 'STAT_HBKEL = 1';
                $orderby = 'NO_KEC, NO_KEL, NO_RW, NO_RT, NO_KK, STAT_HBKEL';
                $isi['data'] = $this->model_s2_biodata->getBiodata($offset, $perpage, $orderby, $where, $entri);
                // var_dump($isi['data']);die;
                /*if(
                    $this->input->get('inputkk') != "" ||
                    $this->input->get('inputnik') != "" ||
                    $this->input->get('inputnama') != "" ||
                    $entri['kd_kec'] != "" ||
                    $entri['kd_kel'] != ""
                    ) 
                {*/
                    $isi['total_data'] = $this->model_s2_biodata->getBiodataTotal($where, $entri);
                /*} else {
                    $isi['total_data'] = $this->db->count_all_results("S2_GETBIODATA");
                }*/
                if($isi['total_data'] > 0){
                    $isi['show']=1;
                }else{
                    $isi['show']=0;
                }
                // var_dump($isi['show']);die; 
                // var_dump($isi['biodata_list']);die;
                // $isi['total_data'] = $this->model_entry->getEntriTotal($where, $entri);
                $total_data = $isi['total_data'];
                // $isi['jml_data'] = 1;

                $this->load->library('pagination');

                $config = get_pagination_template();
                $config['base_url'] = site_url('entry/index') . "?" . get_params($GET, array('page'));
                $config['total_rows'] = $total_data;
                $config['per_page'] = $perpage;
                // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
                $config['num_links'] = 5;
                $config['page_query_string'] = TRUE;
                $config['query_string_segment'] = 'page';

                $this->pagination->initialize($config);

                $isi['pagination'] = $this->pagination->create_links();
                // $isi['pagination'] = "OK";
                // var_dump($isi['data']);die;
            } 
            else {
                $isi['show']=2;
                // redirect('entry', "refresh");
                // exit;
            }


        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('entry/result/result', $isi, TRUE);
            die;
        }
        
        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']   = 'entry/main_data';
        $isi['judul']     = 'Pemasukan Data';
        $isi['sub_judul'] = 'Entri';
        $isi['daftar_nav'] = 'active';
        $isi['entry_nav']  = 'active';

        $no_kk = $this->input->get('no_kk');
        $isi['data'] = $this->model_s2_biodata->getKepala($no_kk);
        
        $data = $isi['data'];

        if (!$data) {
            $this->session->set_flashdata('info', "MAAF!!... KEPALA KELUARGA tidak ada");
            redirect('entry', "refresh");
            exit;
        } else {
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }
        $isi['data'] = $this->model_s2_biodata->getAnggota($no_kk);

        if ($this->input->get('download') != "") {
            // var_dump($this->input->get());die;
            $this->load->helper('download');
            $file = file_get_contents(FCPATH."_assets/document/PanduanOperatorKelurahan.pdf");
            $name = "Panduan Operator Kelurahan.pdf";
            // var_dump($file);die;
            force_download($name, $file);
            exit;
        }

        $this->load->view('home_view', $isi);
    }

    public function daftar()
    {
        $isi['content']   = 'entry/view/form_RumahTangga';
        $isi['judul']     = 'Form Data Rumah Tangga';
        $isi['sub_judul'] = 'Penduduk Miskin';
        $isi['daftar_nav'] = 'active';
        $isi['entry_nav']  = 'active';

        // var_dump($this->input->get());die;
        $nik = $this->input->get('nik');
        // $id_rt = $this->input->get('id_rt');
        $no_kk = $this->input->get('no_kk');
        // $isi['stat_hbkel'] = $this->input->get('stat_hbkel');

        // var_dump($nik, $id_rt,$no_kk);die;
        /*if ($id_rt) {
            $rumahtangga = $this->model_s2_bdt->getRumahTangga($id_rt);
            foreach ($rumahtangga as $key => $value) {
                $isi[$key] = $value;
            }
        }*/
        if ($no_kk) {
            /*$rumahtangga = $this->model_s2_bdt->getRumahTangga($no_kk;
            foreach ($rumahtangga as $key => $value) {
                $isi[$key] = $value;
            }*/
            $nik_kk = $this->model_s2_biodata->getNikKK($no_kk);
            // var_dump($nik_kk['nik'], $no_kk);die;
            $data = $this->model_s2_bdt->getRumahTanggaKK($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }
        $isi['propinsi_lama'] = $isi['provinsi'];
        $isi['kabupaten_lama'] = $isi['kabupaten'];
        $isi['kecamatan_lama'] = $isi['kecamatan'];
        $isi['kelurahan_lama'] = $isi['desa'];
        // var_dump($isi);die;

        if ($nik) {
            $data = $this->model_s2_bdt->getIndividu($nik);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }

        $data = $this->model_s2_biodata->getKepala($no_kk);
        // die(var_dump($data));
        if ($data) {
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }
        
        $data = $this->model_s2_biodata->getIndividu($nik);
        // die(var_dump($data));
        if ($data) {
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }

        if ($nik_kk){
            $nik_kk = $nik_kk['nik'];
            // cek bdt
            $check = $this->model_s2_bdt->getRID_Rumahtangga_nik($nik_kk);
            if ($check) {
                $isi['rid_rumahtangga'] = $check['rid_rumahtangga'];
            } else {
                $isi['rid_rumahtangga'] = null;
            }   
            // var_dump($isi['rid_rumahtangga'], $nik_kk);die;
            /*if ($check && $check['no_art'] == 1) {
            }*/
        }
        // var_dump($isi);die;
        $isi['nik']             = $this->input->get('nik');
        $isi['no_kk']           = $this->input->get('no_kk');
        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_stat_hbkel']       = $this->model_s2_masterbdt->getstat_hbkel();
        $isi['opsi_stat_rtangga']       = $this->model_s2_masterbdt->getstat_rtangga();

        // var_dump($isi);die;
        $this->load->view('home_view', $isi);
    }

    public function submit()
    {
        if($this->input->post())
        {
            extract($this->input->post());
            // $isi = $this->input->post();  
            // var_dump($rid_rumahtangga);die;

            /*cek data pengajuan*/
            // $cek = $this->model_s2_bdt->getIndividu($nik_pendaftar);
            if ($rid_rumahtangga) {
                $kd_pengajuan = 2;
                $status_rt = '001';
            } else {
                $kd_pengajuan = 3;
                $status_rt = '000';
            }
            if ($anggota_rt != 1) {
                $no_kk_induk = $no_kk;
                $nik_rumahtangga_induk = $nik_rumahtangga;
                $nama_kep_rumahtangga_induk = $nama_kep_rumahtangga;
            }

            $skrining = array(
                    'petugas_entri'    =>  strtoupper($this->cu->NAMA_LENGKAP),
                    // 'kodewilayah'   => '3575'.$no_kec.$no_kel,
                    'provinsi'      => $provinsi,
                    'kabupaten'     => $kabupaten,
                    'kecamatan'     => $kecamatan,
                    'desa'          => $desa,
                    'no_prop'       => $no_prop,
                    'no_kab'        => $no_kab,
                    'no_kec'        => $no_kec,
                    'no_kel'        => $no_kel,
                    'no_kk'         => $no_kk,
                    'alamat'        => $alamat,
                    'no_rt'         => $no_rt,
                    'no_rw'         => $no_rw,
                    'nik_pendaftar'           => $nik_pendaftar,
                    'nama_pendaftar'          => $nama_pendaftar, 
                    'jenis_klmin_pendaftar' => $jenis_klmin_pendaftar,
                    'nik_rumahtangga' => $nik_rumahtangga,
                    'nama_kep_rumahtangga' => $nama_kep_rumahtangga,
                    'jenis_klmin_rumahtangga' => $jenis_klmin_pendaftar,
                    'hubungan_keluarga' => $hubungan_keluarga,
                    'hubungan_rumahtangga_induk' => $hubungan_rumahtangga_induk,
                    'bulan_lhr_rumahtangga'     => $bulan_lhr_rumahtangga,
                    'tahun_lhr_rumahtangga'     => $tahun_lhr_rumahtangga,
                    'status_pkrjn_rumahtangga' => $status_pkrjn_rumahtangga,
                    'b1_r9'         => $b1_r9,
                    'b3_r1a'        => $b3_r1a_bdt,
                    'b3_r3'         => $b3_r3_bdt,
                    'b3_r4a'        => $b3_r4a_bdt,
                    'b3_r5a'        => $b3_r5a_bdt,
                    'b3_r11a'       => $b3_r11a_bdt,
                    'b5_r1a'        => $b5_r1a_bdt,
                    'b5_r1c'        => $b5_r1c_bdt,
                    'b5_r1k'        => $b5_r1k_bdt,
                    'b4_k18'        => $b4_k18_bdt,
                    'kd_pengajuan'  => $kd_pengajuan,
                    'no_kk_induk'   => $no_kk_induk,
                    'nik_induk'     => $nik_rumahtangga_induk,
                    'nama_induk'    => $nama_kep_rumahtangga_induk,
                    'nomor_urut_rumah_tangga'   => $nomor_urut_rumah_tangga, 
                    'rid_rumahtangga'   => $rid_rumahtangga,
                    'status_rt'     => $status_rt,
                    'status_pengajuan'     => $status_pengajuan
                );
            $cek = $this->model_s2_skrining->getNoUrutKK($no_kk_induk);
            if (!$cek['b4_k4']) {
                $params['b4_k4'] = 1;
            } else {
                $params['b4_k4'] = $cek['b4_k4']+1;
            }
            $params['no_kk_induk'] = $no_kk_induk;
            $anggota = $this->model_s2_biodata->getAnggotaInsert($no_kk);
            // $anggota['no_kk_induk'] = $no_kk_induk;
            // var_dump($anggota);die;
            
            $this->model_s2_skrining->insert($no_kk,$skrining);
            $this->model_s2_skrining->insertAnggota($no_kk,$anggota,$params);
            
            $get_params = $this->input->post('get_params');
            redirect('entry/lihat?'.$get_params, array('nik'));
            // redirect('entry/lihat?'.get_params($this->input->post('get_params'), array('nik', 'id_rt')));
            exit;
        }
    }

    public function daftar_rtangga()
    {
        $isi['content']   = 'entry/view/form_RumahTangga';
        $isi['judul']     = 'Form Data Rumah Tangga';
        $isi['sub_judul'] = 'Penduduk Miskin';
        $isi['daftar_nav'] = 'active';
        $isi['entry_nav']  = 'active';

        // var_dump($this->input->get());die;
        $nik = $this->input->get('nik');
        $id_rt = $this->input->get('id_rt');
        $no_kk = $this->input->get('no_kk');
        // $isi['stat_hbkel'] = $this->input->get('stat_hbkel');

        // var_dump($nik, $id_rt,$no_kk);die;
        if ($id_rt) {
            $rumahtangga = $this->model_s2_bdt->getRumahTangga($id_rt);
            foreach ($rumahtangga as $key => $value) {
                $isi[$key] = $value;
            }
        }

        if ($nik) {
            $individu = $this->model_s2_bdt->getIndividu($nik);
            foreach ($individu as $key => $value) {
                $isi[$key] = $value;
            }
        }

        $data = $this->model_s2_biodata->getIndividu($nik);
        // die(var_dump($data));
        if (!$data) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('entry', "refresh");
            exit;
        } else {
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }
        $isi['nik']             = $this->input->get('nik');
        $isi['no_kk']           = $this->input->get('no_kk');
        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();

        // var_dump($isi);die;
        $this->load->view('home_view', $isi);
    }

    public function submit_rtangga()
    {
        if($this->input->post())
        {
            extract($this->input->post());
            // $isi = $this->input->post();  
            // var_dump($isi);die;
            if ($anggota_rt == 1) {
                $nomor_urut_rumah_tangga = $nomor_urut_rumah_tangga_bdt;
                $no_urut = $this->model_s2_mohonbdt->getno_urutKK($no_kk_rumahtangga_bdt);
                $no_urut_kk = $no_urut['no_urut_kk']+1;
            } else {
                $cekmohon = $this->model_s2_mohonbdt->cek();
                // var_dump($cekmohon);die;
                if (!$cekmohon) {
                    $nomor_urut_rumah_tangga = 14337;
                } else {
                    $no_rt = $this->model_s2_mohonbdt->getno();
                    $nomor_urut_rumah_tangga = $no_rt['nomor_urut_rumah_tangga']+1;
                }
                $no_urut_kk = 1;
            }

            // var_dump($nomor_urut_rumah_tangga);die;
            $rumahtangga = array(
                    'petugas_permohonan'    =>  $this->cu->NAMA_LENGKAP,
                    'kodewilayah'   => '3575'.$no_kec.$no_kel,
                    'provinsi'      => $provinsi,
                    'kabupaten'     => $kabupaten,
                    'kecamatan'     => $kecamatan,
                    'desa'          => $desa,
                    'no_prop'       => $no_prop,
                    'no_kab'        => $no_kab,
                    'no_kec'        => $no_kec,
                    'no_kel'        => $no_kel,
                    'no_kk_rumahtangga' => $no_kk_rumahtangga_bdt,
                    'nik_rumahtangga' => $nik_rumahtangga_bdt,
                    'no_kk'         => $no_kk,
                    'nik'           => $nik,
                    'nama_kk'       => $nama_kk, 
                    // 'tgl_cacah'     => "TO_DATE('".$tgl_cacah."', 'DD-MM-YYYY')",
                    'petugas_cacah' => strtoupper($nama_cacah),
                    'kd_cacah'      => $kd_cacah,
                    // 'tgl_periksa'   => "TO_DATE('".$tgl_periksa."', 'DD-MM-YYYY')",
                    'petugas_periksa'  => strtoupper($nama_periksa),
                    'kd_periksa'    => $kd_periksa,
                    'hasil_cacah'   => $hasil_cacah,
                    'nama_responden'=> strtoupper($nama_responden),    
                    'b1_r6'         => $b1_r6,
                    'b1_r8'         => $b1_r8_bdt,
                    'b1_r9'         => $b1_r9_bdt,
                    'b1_r10'        => $b1_r10_bdt,
                    'b3_r1a'        => $b3_r1a_bdt,
                    'b3_r1b'        => $b3_r1b_bdt,
                    'b3_r2'         => $b3_r2_bdt,
                    'b3_r3'         => $b3_r3_bdt,
                    'b3_r4a'        => $b3_r4a_bdt,
                    'b3_r4b'        => $b3_r4b_bdt,
                    'b3_r5a'        => $b3_r5a_bdt,
                    'b3_r5b'        => $b3_r5b_bdt,
                    'b3_r6'         => $b3_r6_bdt,
                    'b3_r7'         => $b3_r7_bdt,
                    'b3_r8'         => $b3_r8_bdt,
                    'b3_r9a'        => $b3_r9a_bdt,
                    'b3_r9b'        => $b3_r9b_bdt,
                    'b3_r10'        => $b3_r10_bdt,
                    'b3_r11a'       => $b3_r11a_bdt,
                    'b3_r11b'       => $b3_r11b_bdt,
                    'b3_r12'        => $b3_r12_bdt,
                    'b5_r1a'        => $b5_r1a_bdt,
                    'b5_r1b'        => $b5_r1b_bdt,
                    'b5_r1c'        => $b5_r1c_bdt,
                    'b5_r1d'        => $b5_r1d_bdt,
                    'b5_r1e'        => $b5_r1e_bdt,
                    'b5_r1f'        => $b5_r1f_bdt,
                    'b5_r1g'        => $b5_r1g_bdt,
                    'b5_r1h'        => $b5_r1h_bdt,
                    'b5_r1i'        => $b5_r1i_bdt,
                    'b5_r1j'        => $b5_r1j_bdt,
                    'b5_r1k'        => $b5_r1k_bdt,
                    'b5_r1l'        => $b5_r1l_bdt,
                    'b5_r1m'        => $b5_r1m_bdt,
                    'b5_r1n'        => $b5_r1n_bdt,
                    'b5_r1o'        => $b5_r1o_bdt,
                    'b5_r2a'        => $b5_r2a_bdt,
                    'b5_r2b'        => $b5_r2b_bdt,
                    'b5_r3a1'       => $b5_r3a1_bdt,
                    'b5_r3a2'       => $b5_r3a2_bdt,
                    'b5_r3b'        => $b5_r3b_bdt,
                    'b5_r4a'        => $b5_r4a_bdt,
                    'b5_r4b'        => $b5_r4b_bdt,
                    'b5_r4c'        => $b5_r4c_bdt,
                    'b5_r4d'        => $b5_r4d_bdt,
                    'b5_r4e'        => $b5_r4e_bdt,
                    'b5_r5a'        => $b5_r5a_bdt,
                    'b5_r6a'        => $b5_r6a_bdt,
                    'b5_r6b'        => $b5_r6b_bdt,
                    'b5_r6c'        => $b5_r6c_bdt,
                    'b5_r6d'        => $b5_r6d_bdt,
                    'b5_r6e'        => $b5_r6e_bdt,
                    'b5_r6f'        => $b5_r6f_bdt,
                    'b5_r6g'        => $b5_r6g_bdt,
                    'b5_r6h'        => $b5_r6h_bdt,
                    'b5_r6i'        => $b5_r6i_bdt,
                    // 'b5_r7'         => $b5_r7_bdt,
                    'b5_r8a'        => $b5_r8a_bdt,
                    'b5_r8b'        => $b5_r8b_bdt,
                    'b5_r9'         => $b5_r9_bdt,
                    'b5_r10'        => $b5_r10_bdt,
                    'b5_r11a'       => $b5_r11a_bdt,
                    'b5_r11b'       => $b5_r11b_bdt,
                    'b5_r12'        => $b5_r12_bdt,
                    'b5_r13'        => $b5_r13_bdt,
                    'b5_r14'        => $b5_r14_bdt,
                    // 'status_kesejahteraan' => $status_kesejahteraan_bdt,
                    'nomor_urut_rumah_tangga' => $nomor_urut_rumah_tangga,
                    'kd_pengajuan'  => 1,
                    'no_urut_kk'    => $no_urut_kk
                );
            // var_dump($rumahtangga);die;
            $this->model_s2_mohonbdt->insertRumahTangga($rumahtangga);
            $this->model_s2_mohonbdt->insertRumahTanggaTgl($tgl_cacah,$tgl_periksa,$no_kk);
            //insert data rumah tangga from exist
            // var_dump($no_kk);die;
            if ($anggota_rt == 1) {
                unset($rumahtangga['no_kk']);
                unset($rumahtangga['nik']);
                unset($rumahtangga['nama_kk']);
                unset($rumahtangga['no_urut_kk']);
                // var_dump($rumahtangga);die;
                $this->model_s2_mohonbdt->updateRumahTangga($no_kk_rumahtangga_bdt,$rumahtangga);
            }
            /*$cek = $this->model_s2_mohonbdt->getRumahTangga($id_rt);
            if (!$cek) {
                $rumahtangga = $this->model_s2_bdt->getRumahTanggaKK($id_rt);
                // var_dump($rumahtangga);die;
                $this->model_s2_mohonbdt->insertRumahTangga($rumahtangga);
            }else{
                // var_dump($rumahtangga);die;
                $this->model_s2_mohonbdt->updateRumahTangga($no_kk,$rumahtangga);
                // var_dump($update);die;
            }*/            
            
            // date('Y-m-d', strtotime($tanggal)); 
            $get_params = $this->input->post('get_params');
            redirect('entry/lihat?'.$get_params, array('nik', 'id_rt'));
            // redirect('entry/lihat?'.get_params($this->input->post('get_params'), array('nik', 'id_rt')));
            exit;
        }
    }

    public function daftar_individu()
    {
        $isi['content']   = 'entry/view/form_Individu';
        $isi['judul']     = 'Form Data Individu';
        $isi['sub_judul'] = 'Penduduk Miskin';
        $isi['daftar_nav'] = 'active';
        $isi['entry_nav']  = 'active';

        $nik = $this->input->get('nik');
        $individu = $this->model_s2_bdt->getIndividu($nik);
        if ($individu) {
            foreach ($individu as $key => $value) {
                $isi[$key] = $value;
            }
        }

        $data = $this->model_s2_biodata->getIndividu($nik);
        // die(var_dump($data));
        if ($data) {
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }

        /*get parameter rumahtangga*/
        $no_kk = $this->input->get('no_kk');
        $data_rt = $this->model_s2_mohonbdt->getRumahTangga($no_kk);
        if ($data_rt) {
            foreach ($data_rt as $key => $value) {
                $isi[$key] = $value;
            }
        }

        $no_kk_rumahtangga = $isi['no_kk_rumahtangga'];
        $cek = $this->model_s2_mohonbdt->cekmohon($no_kk_rumahtangga);
        if ($cek) {
            $no_art = $this->model_s2_mohonbdt->get_no_art($no_kk_rumahtangga);
            $b4_k4 = $this->model_s2_mohonbdt->get_b4_k4($no_kk);
            $isi['no_art'] = $no_art['no_art']+1;
            // $isi['b4_k4'] = $isi['no_urut_kk'];
            // var_dump($no_art,$b4_k4,$isi['b4_k4']);die;
        } else {
            $isi['no_art'] = 1;
            // $isi['b4_k4'] = 1;
        }

        // var_dump($isi);die;    

        $isi['nik']             = $this->input->get('nik');
        $isi['no_kk']           = $this->input->get('no_kk');
        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        // die(var_dump($rumahtangga));
        $this->load->view('home_view', $isi);
    }

    public function submit_individu()
    {
        
        if($this->input->post())
        {
            extract($this->input->post());
            // $isi = $this->input->post();  

            $individu = array(
                'petugas_permohonan' =>  $this->cu->NAMA_LENGKAP,
                'kodewilayah'   => $kodewilayah,
                'provinsi'      => $provinsi,
                'kabupaten'     => $kabupaten,
                'kecamatan'     => $kecamatan,
                'desa'          => $desa,
                'no_prop'       => $no_prop,
                'no_kab'        => $no_kab,
                'no_kec'        => $no_kec,
                'no_kel'        => $no_kel,
                'no_art'        => $no_art,
                'nama'          => $nama,
                'nik'           => $nik,
                'b4_k3'         => $b4_k3,
                'b4_k4'         => $b4_k4,
                'b4_k5'         => $b4_k5,
                'b4_k6'         => $b4_k6,
                'b4_k7'         => $b4_k7,
                'b4_k8'         => $b4_k8,
                'b4_k9'         => $b4_k9,
                'b4_k10'        => $b4_k10,
                'b4_k11'        => $b4_k11,
                'b4_k12'        => $b4_k12,
                'b4_k13'        => $b4_k13,
                'b4_k14'        => $b4_k14,
                'b4_k15'        => $b4_k15,
                'b4_k16'        => $b4_k16,
                'b4_k17'        => $b4_k17,
                'b4_k18'        => $b4_k18,
                'b4_k19a'       => $b4_k19a,
                'b4_k19b'       => $b4_k19b,
                'b4_k20'        => $b4_k20,
                'b4_k21'        => $b4_k21,
                'nomor_urut_rumah_tangga' =>  $nomor_urut_rumah_tangga,
                'no_kk'         =>  $no_kk,
                'no_kk_rumahtangga' => $no_kk_rumahtangga,
                'nik_rumahtangga' => $nik_rumahtangga,
                'nama_kep_rumahtangga' => $b1_r8
            );
            // var_dump($individu);die;
            $this->model_s2_mohonbdt->insertIndividu($individu);
            $this->model_s2_biodata->updateIndividu($nik);
            
            // date('Y-m-d', strtotime($tanggal)); 
            $get_params = $this->input->post('get_params'); 
            redirect('entry/lihat?'.$get_params, array('nik', 'id_rt'));
            // redirect('entry/lihat?'.get_params($this->input->post('get_params'), array('nik', 'id_rt')));
            exit;
        }
    }

    public function delete() 
    {  
        if ($this->input->get()) {
            $nik = $this->input->get('nik');
            $no_kk = $this->input->get('no_kk');
            $shdk = $this->input->get('stat_hbkel');
            
            if ($shdk == 1) {
                $this->model_s2_mohonbdt->deleteRumahTangga($no_kk);
                $this->model_s2_mohonbdt->deleteIndividuRumahTangga($no_kk);
                $this->model_s2_biodata->deleteIndividuRumahTangga($no_kk);
            }

            $this->model_s2_mohonbdt->deleteIndividu($nik);
            $this->model_s2_biodata->deleteIndividu($nik);
        }
        if($this->input->is_ajax_request()) {
            echo 'success';
            // echo 'error';
        }
        else {
            $get_params = get_params($this->input->get(), array('nik'));
            redirect('entry/lihat?'.$get_params);
        }
        exit;
    }
}
