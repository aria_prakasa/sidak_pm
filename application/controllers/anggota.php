<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anggota extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        // $this->curpage = $this->router->fetch_class();
        $this->load->model('model_entry');
        $this->load->model('model_pullout');
        $this->load->model('model_list');
        // $this->model_security->getsecurity();
    }

    public function index($a = "")
    {
        $isi['content']     = 'anggota/main_anggota';
        $isi['judul']       = 'Verifikasi';
        $isi['sub_judul']   = 'Data Anggota Keluarga Baru';
        $isi['verify_nav']  = 'active';
        $isi['anggota_nav'] = 'active';
        
        if ($this->input->get('kecamatan_id'))
        {
            $isi = array_merge($isi, $this->input->get());

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";

            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";

            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";

            }
            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }
            
            // TAMPIL ONLY
            $isi['biodata_list'] = $this->model_list->getbioAnggota($offset, $perpage, $orderby, $where, $entri);
            // var_dump($isi['biodata_list']);die;
            if(
                $this->input->get('inputkk') != "" ||
                $this->input->get('inputnik') != "" ||
                $this->input->get('inputnama') != "" ||
                $entri['kd_kec'] != "" ||
                $entri['kd_kel'] != ""
                ) {
                $isi['total_data'] = $this->model_list->getbioAnggotaTotal($where, $entri);
            } else {
                $isi['total_data'] = $this->db->count_all_results("VIEW_ANGGOTA_BARU");
            }
            // $isi['total_data'] = $this->model_list->getbioAnggotaTotal($where, $entri);
            $total_data = $isi['total_data'];
            $isi['jml_data'] = 1;

            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('anggota/index') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            // EXPORT
            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "data-anggota-detail--" . date("Ym") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('table/anggota', $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('anggota/result/result_anggota', $isi, TRUE);
            die;
        }
        /*$this->load->model('model_keterangan');
        $isi['opsi_pekerjaan']   = $this->model_keterangan->getpekerjaan();*/

        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']     = 'anggota/view/view_anggota';
        $isi['judul']       = 'Verifikasi';
        $isi['sub_judul']   = 'Data Anggota Keluarga Baru';
        $isi['verify_nav']  = 'active';
        $isi['anggota_nav'] = 'active';

        if ($this->session->flashdata('no_kk')) {
            $no_kk = $this->session->flashdata('no_kk');
        } else {
            $no_kk = $this->input->get('no_kk');
        }

        // var_dump($no_kk);die;
        if($this->cu->USER_LEVEL == 2)
        {
            $no_kec = $this->cu->NO_KEC;
        }
        if($this->cu->USER_LEVEL == 3)
        {
            $no_kec = $this->cu->NO_KEC;
            $no_kel = $this->cu->NO_KEL;
        }
        $check_kk   = $this->model_entry->getdatamohon($no_kk);
        $data_kk    = $this->model_entry->getdatakk($no_kk,$no_kec,$no_kel);
        
        $anggota_list = $this->model_entry->getanggotakk($no_kk);

        if (!$data_kk) {
            $this->session->set_flashdata('info', "ERROR");
            redirect('entry', "refresh");
            exit;
        } else {
            foreach ($data_kk as $key => $value) {
                $isi[$key] = $value;
            }
            $isi['anggota_list'] = $anggota_list;
        }
        // var_dump($isi);die;

        $this->load->view('home_view', $isi);
    }

    public function proses()
    {
        $submit = $this->input->post();
        $no_kk = $this->input->post('no_kk');
        // var_dump($nama_lgkp);die;
        if($this->input->post())
        {
            if($this->input->post('submit')){
                $nik = $this->input->post('submit');
                $nama_lgkp = $this->input->post('nama_lgkp')[$nik];
                // var_dump($this->input->post('jenis_pkrjn')[$nik]);die;
                $biodata = array(
                    'nik'            => $nik,
                    'no_kk'          => $no_kk,
                    'nama_lgkp'      => $nama_lgkp,
                    'jenis_pkrjn'    => $this->input->post('jenis_pkrjn')[$nik],
                    'nip_pet_entri'  => $this->cu->NIP,
                    'nama_pet_entri' => $this->cu->NAMA_LENGKAP
                     );
                
                $this->load->model('model_pullout');
                if($insert = $this->model_pullout->insert_biomohon($biodata))
                {
                    $this->load->model('model_entry');
                    $insert = $this->model_entry->insert_biomohon($biodata);
                } else {
                    die("Koneksi VPN gagal!");
                }
            }

            if($this->input->post('hapus')){
                $nik = $this->input->post('hapus');
                // var_dump($nik);die;
                $this->load->model('model_pullout');
                if($this->model_pullout->biodeleteMohon($nik))
                {
                    $this->model_entry->get_biodelete($nik);               
                } 
                else 
                {
                    die("Koneksi VPN gagal!");
                }
            }
        }
        
        $this->session->set_flashdata('no_kk', $no_kk);
        redirect('anggota/lihat?'.$this->input->post('get_params'));
        exit;
    }
    
}
