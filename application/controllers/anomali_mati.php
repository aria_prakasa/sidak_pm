<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anomali_mati extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_init_logged_in();
        $this->_init_wilayah();

        $this->load->model('model_s2_anomali');
        $this->load->model('model_s2_biodata');
    }

    public function index($a = "")
    {
        $isi['content']         = 'anomali/mati/main_view';
        $isi['judul']           = 'Data Penduduk Miskin Mati';
        $isi['sub_judul']       = 'List';
        $isi['kemiskinan_nav']  = 'active';
        // $isi['penetapan_nav']   = 'active';
        $isi['mati_nav']        = 'active';
                
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";

            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";

            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";

            }
            if ($this->input->get('inputrw') != "") {

                $no_rw = $this->input->get('inputrw', TRUE);
                $entri['no_rw'] = $no_rw;
                $isi['toggleRw'] = "checked";
            }
            if ($this->input->get('inputrt') != "") {

                $no_rt = $this->input->get('inputrt', TRUE);
                $entri['no_rt'] = $no_rt;
                $isi['toggleRt'] = "checked";
            }

            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            // $where = 'FLAG_STATUS IN (0,6,7)';
            $orderby = 'NO_KEC, NO_KEL, NAMA_LGKP';
            $isi['biodata_list'] = $this->model_s2_anomali->getBiodataMati($offset, $perpage, $orderby, $where, $entri);
            /*if(
                $this->input->get('inputkk') != "" ||
                $this->input->get('inputnik') != "" ||
                $this->input->get('inputnama') != "" ||
                $entri['kd_kec'] != "" ||
                $entri['kd_kel'] != ""
                ) {
                $isi['total_data'] = $this->model_s2_anomali->getBiodataMohonTotal($where, $entri);
            } else {
                $isi['total_data'] = $this->db->count_all_results("MHN_INDIVIDU");
            }*/
            // $isi['total_data'] = $this->db->count_all_results("VIEW_MOHON");
            $isi['total_data'] = $this->model_s2_anomali->getBiodataMatiTotal($where, $entri);

            if($isi['biodata_list']){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['biodata_list']);die;

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('anomali_mati/index') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            // EXPORT
            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-Induk-Kemiskinan-Mati--" . date("Ym") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('anomali/table', $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('anomali/mati/result', $isi, TRUE);
            die;
        }
        // var_dump($isi);die;
        $this->load->view('home_view', $isi);
    }

    public function view()
    {
        $isi['content']         = 'anomali/mati/data_keluarga';
        $isi['judul']           = 'Data Penduduk Miskin Mati';
        $isi['sub_judul']       = 'Data Keluarga';
        $isi['kemiskinan_nav']  = 'active';
        // $isi['penetapan_nav']   = 'active';
        $isi['mati_nav']        = 'active';

        if ($this->input->get()) {
            $isi = $this->input->get();

            $no_kk = $this->input->get('no_kk');
            // var_dump($no_kk);die;
            $isi['data'] = $this->model_s2_biodata->getKepala($no_kk);
            $data = $isi['data'];
            // var_dump($data);die;

            if (!$data) {
                $this->session->set_flashdata('info', "ERROR");
                redirect('entry', "refresh");
                exit;
            } else {
                foreach ($data as $key => $value) {
                    $isi[$key] = $value;
                }
            }
            $isi['data'] = $this->model_s2_biodata->getAnggota($no_kk);
                // var_dump($isi);die;
        }
        // var_dump($isi['data']);die;
        $this->load->view('home_view', $isi);
    }

}
