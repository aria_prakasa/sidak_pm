<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anomali_pindah extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_init_logged_in();
        $this->_init_wilayah();

        $this->load->model('model_s2_anomali');
        $this->load->model('model_s2_biodata');
    }

    public function dalam($a = "")
    {
        $isi['content']         = 'anomali/pindah/main_dalam';
        $isi['judul']           = 'Data Penduduk Miskin Pindah Alamat / Pisah KK';
        $isi['sub_judul']       = 'List';
        $isi['kemiskinan_nav']  = 'active';
        // $isi['penetapan_nav']   = 'active';
        $isi['pindah_nav']      = 'active';
        $isi['dalam_nav']       = 'active';
                
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";

            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";

            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";

            }
            if ($this->input->get('inputrw') != "") {

                $no_rw = $this->input->get('inputrw', TRUE);
                $entri['no_rw'] = $no_rw;
                $isi['toggleRw'] = "checked";
            }
            if ($this->input->get('inputrt') != "") {

                $no_rt = $this->input->get('inputrt', TRUE);
                $entri['no_rt'] = $no_rt;
                $isi['toggleRt'] = "checked";
            }


            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            // $where = 'FLAG_STATUS IN (0,6,7)';
            $orderby = 'NO_KEC, NO_KEL, NO_RW, NO_RT, NO_KK, STAT_HBKEL';
            $isi['biodata_list'] = $this->model_s2_anomali->getBiodataPindah($offset, $perpage, $orderby, $where, $entri);
            $isi['total_data'] = $this->model_s2_anomali->getBiodataPindahTotal($where, $entri);

            if($isi['biodata_list']){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['biodata_list']);die;

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('anomali_pindah/dalam') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            // EXPORT
            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-Induk-Kemiskinan-Pindah-Alamat--" . date("Ym") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('anomali/table', $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('anomali/pindah/result', $isi, TRUE);
            die;
        }
        // var_dump($isi);die;
        $this->load->view('home_view', $isi);
    }

    public function keluarga($a = "")
    {
        $isi['content']         = 'anomali/pindah/main_keluarga';
        $isi['judul']           = 'Data Penduduk Miskin Pindah Keluarga Lain';
        $isi['sub_judul']       = 'List';
        $isi['kemiskinan_nav']  = 'active';
        // $isi['penetapan_nav']   = 'active';
        $isi['pindah_nav']      = 'active';
        $isi['keluarga_nav']       = 'active';
                
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";

            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";

            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";

            }
            if ($this->input->get('inputrw') != "") {

                $no_rw = $this->input->get('inputrw', TRUE);
                $entri['no_rw'] = $no_rw;
                $isi['toggleRw'] = "checked";
            }
            if ($this->input->get('inputrt') != "") {

                $no_rt = $this->input->get('inputrt', TRUE);
                $entri['no_rt'] = $no_rt;
                $isi['toggleRt'] = "checked";
            }


            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            // $where = 'FLAG_STATUS IN (0,6,7)';
            $orderby = 'NO_KEC, NO_KEL, NO_RW, NO_RT, NO_KK, STAT_HBKEL';
            $isi['biodata_list'] = $this->model_s2_anomali->getBiodataPindahKeluarga($offset, $perpage, $orderby, $where, $entri);
            $isi['total_data'] = $this->model_s2_anomali->getBiodataPindahKeluargaTotal($where, $entri);

            if($isi['biodata_list']){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['biodata_list']);die;

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('anomali_pindah/keluarga') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-Induk-Kemiskinan-Pindah-Keluarga--" . date("Ym") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('anomali/table', $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('anomali/pindah/result', $isi, TRUE);
            die;
        }
        // var_dump($isi);die;
        $this->load->view('home_view', $isi);
    }

    public function luar($a = "")
    {
        $isi['content']         = 'anomali/pindah/main_luar';
        $isi['judul']           = 'Data Penduduk Miskin Pindah Keluar Kota';
        $isi['sub_judul']       = 'List';
        $isi['kemiskinan_nav']  = 'active';
        // $isi['penetapan_nav']   = 'active';
        $isi['pindah_nav']      = 'active';
        $isi['luar_nav']        = 'active';
                
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";

            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";

            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";

            }
            if ($this->input->get('inputrw') != "") {

                $no_rw = $this->input->get('inputrw', TRUE);
                $entri['no_rw'] = $no_rw;
                $isi['toggleRw'] = "checked";
            }
            if ($this->input->get('inputrt') != "") {

                $no_rt = $this->input->get('inputrt', TRUE);
                $entri['no_rt'] = $no_rt;
                $isi['toggleRt'] = "checked";
            }


            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            // $where = 'FLAG_STATUS IN (2,8)';
            $orderby = 'NO_KEC, NO_KEL, NO_RW, NO_RT, NO_KK, STAT_HBKEL';
            $isi['biodata_list'] = $this->model_s2_anomali->getBiodataKeluar($offset, $perpage, $orderby, $where, $entri);
            $isi['total_data'] = $this->model_s2_anomali->getBiodataKeluarTotal($where, $entri);

            if($isi['biodata_list']){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['biodata_list']);die;

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('anomali_pindah/luar') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-Induk-Kemiskinan-Pindah-Luar-Kota--" . date("Ym") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('anomali/table', $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('anomali/pindah/result', $isi, TRUE);
            die;
        }
        // var_dump($isi);die;
        $this->load->view('home_view', $isi);
    }

    public function view()
    {
        if ($this->input->get()) {
            $isi = $this->input->get();
            $uri = $this->input->get('uri');
            if ($uri == 'dalam') {     
                $isi['content']         = 'anomali/pindah/data_keluarga';
                $isi['judul']           = 'Data Penduduk Miskin Pindah Alamat / Pisah KK';
                $isi['sub_judul']       = 'Data Keluarga';
                $isi['kemiskinan_nav']  = 'active';
                // $isi['penetapan_nav']   = 'active';
                $isi['pindah_nav']      = 'active';
                $isi['dalam_nav']       = 'active';
            } else if ($uri == 'keluarga') {
                $isi['content']         = 'anomali/pindah/data_keluarga';
                $isi['judul']           = 'Data Penduduk Miskin Pindah Keluarga Lain';
                $isi['sub_judul']       = 'Data Keluarga';
                $isi['kemiskinan_nav']  = 'active';
                // $isi['penetapan_nav']   = 'active';
                $isi['pindah_nav']      = 'active';
                $isi['keluarga_nav']        = 'active';
            }
            else if ($uri == 'luar') {
                $isi['content']         = 'anomali/pindah/data_keluarga';
                $isi['judul']           = 'Data Penduduk Miskin Pindah Keluar Kota';
                $isi['sub_judul']       = 'Data Keluarga';
                $isi['kemiskinan_nav']  = 'active';
                // $isi['penetapan_nav']   = 'active';
                $isi['pindah_nav']      = 'active';
                $isi['luar_nav']        = 'active';
            }

            $no_kk = $this->input->get('no_kk');
            // var_dump($no_kk);die;
            $isi['data'] = $this->model_s2_biodata->getKepala($no_kk);
            $data = $isi['data'];
            // var_dump($data);die;

            if (!$data) {
                $this->session->set_flashdata('info', "ERROR");
                redirect('entry', "refresh");
                exit;
            } else {
                foreach ($data as $key => $value) {
                    $isi[$key] = $value;
                }
            }
            $isi['data'] = $this->model_s2_biodata->getAnggota($no_kk);
                // var_dump($isi);die;
        }
        // var_dump($isi['data']);die;
        $this->load->view('home_view', $isi);
    }

}
