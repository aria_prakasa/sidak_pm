<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Daftar extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        $this->_init_wilayah();
        // $this->curpage = $this->router->fetch_class();
        // $this->load->model('model_entry');
        // $this->load->model('model_pullout');
        $this->load->model('model_s2_biodata');
        $this->load->model('model_s2_bdt');
        $this->load->model('model_s2_masterbdt');
        $this->load->model('model_s2_mohonbdt');
        $this->load->model('model_s2_skrining');
        // $this->model_security->getsecurity();
    }

    public function index($a = "")
    {
        $isi['content']     = 'daftar/main_daftar';
        $isi['judul']       = 'Pengajuan Data Kemiskinan';
        $isi['sub_judul']   = 'List';
        // $isi['data_nav']    = 'active';
        $isi['daftar_nav']  = 'active';
        $isi['mohon_nav']   = 'active';
        
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";

            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";

            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";

            }
            if ($this->input->get('inputpengajuan') != "") {

                $pengajuan = $this->input->get('inputpengajuan', TRUE);
                $entri['pengajuan'] = $pengajuan;
                $isi['togglePengajuan'] = "checked";

            }
            if ($this->input->get('inputproses') != "") {

                $proses = $this->input->get('inputproses', TRUE);
                $entri['proses'] = $proses;
                $isi['toggleProses'] = "checked";
                
            }
            if (($this->input->get('tgl_awal') != "") && ($this->input->get('tgl_awal') != "")) {

                $tgl_awal = $this->input->get('tgl_awal', TRUE);
                $entri['tgl_awal'] = $tgl_awal;
                $tgl_akhir = $this->input->get('tgl_akhir', TRUE);
                $entri['tgl_akhir'] = $tgl_akhir;
                $isi['toggleEntri'] = "checked";
                // var_dump($tgl_awal,$tgl_akhir);die;
            }


            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            // $where = 'NO_URUT_KK = 1';
            $orderby = 'NO_KEC,NO_KEL,NO_RW,NO_RT,NAMA_KEP_RUMAHTANGGA';
            // $isi['data'] = $this->model_s2_mohonbdt->getRumahTanggaMohon($offset, $perpage, $orderby, $where, $entri);
            $isi['data'] = $this->model_s2_skrining->getRumahTanggaMohon($offset, $perpage, $orderby, $where, $entri);
            /*if(
                $this->input->get('inputkk') != "" ||
                $this->input->get('inputnik') != "" ||
                $this->input->get('inputnama') != "" ||
                $entri['kd_kec'] != "" ||
                $entri['kd_kel'] != ""
                ) {
                $isi['total_data'] = $this->model_s2_mohonbdt->getBiodataMohonTotal($where, $entri);
            } else {
                $isi['total_data'] = $this->db->count_all_results("MHN_INDIVIDU");
            }*/
            // $isi['total_data'] = $this->db->count_all_results("VIEW_MOHON");
            $isi['total_data'] = $this->model_s2_skrining->getRumahTanggaMohonTotal($where, $entri);

            if($isi['data']){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('daftar/index') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            // var_dump($isi);die;
            // EXPORT
            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-Prelist-Kemiskinan--" . date("dmY") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('daftar/result/table', $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('daftar/result/result', $isi, TRUE);
            die;
        }

        $this->load->view('home_view', $isi);
    }

    public function edit()
    {
        $isi['content']     = 'daftar/view/form_edit';
        $isi['judul']       = 'Edit Pengajuan Data Rumah Tangga ';
        $isi['sub_judul']   = 'Penduduk Miskin';
        // $isi['data_nav']    = 'active';
        $isi['daftar_nav']  = 'active';
        $isi['mohon_nav']   = 'active';

        $no_kk = $this->input->get('no_kk');
        
        if ($no_kk) {
            // var_dump($no_kk);die;
            // $rumahtangga = $this->model_s2_mohonbdt->getRumahTangga($no_kk);
            $data = $this->model_s2_skrining->getData($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

            // $isi['anggota'] = $data = $this->model_s2_skrining->getAnggotaMohon($no_kk);
        }

        $isi['anggota'] = $this->model_s2_skrining->getAnggotaMohon($no_kk);
        // var_dump($isi['anggota']);die;

        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_stat_hbkel']       = $this->model_s2_masterbdt->getstat_hbkel();
        $isi['opsi_stat_rtangga']       = $this->model_s2_masterbdt->getstat_rtangga();

        // PRINT
        if($this->input->get('print')) {

            // $data = $this->model_s2_mohonbdt->getDetail($nik);
            // var_dump($rumahtangga);die;
            foreach ($data as $key => $value) {
                if (empty($value)) {
                    $value = '0';
                }
                $isi[$key] = $value;
            }
            // var_dump($isi);die;

            // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
            $filename = "form-pengajuan-".$no_kk."-".$isi['b1_r8']."-".time();
            $pdfFilePath = FCPATH."_assets/downloads/reports/{$filename}.pdf";

            if(!is_dir(FCPATH."_assets/downloads/reports")) {
                mkdir(FCPATH."_assets/downloads/reports", 0777, TRUE);
            }
             
            if (is_file($pdfFilePath) == FALSE)
            {
                ini_set('memory_limit','512M');
                 
                $this->load->library('pdf_form');
                $pdf = $this->pdf_form->load();

                $isi['header'] = "MEKANISME PEMUTAKHIRAN MANDIRI (MPM)";
                $isi['sub_header'] = "FORMULIR PENDAFTARAN RUMAH TANGGA MISKIN DAN KURANG MAMPU";
                // $head = $this->load->view('daftar/cetak/header', $isi, TRUE);
                $html = $this->load->view('daftar/cetak/content_lampiran', $isi, TRUE);
                $html .= $this->load->view('daftar/cetak/content_anggota', $isi, TRUE);
                // $foot = $this->load->view('daftar/cetak/footer', $isi, TRUE);
                // $html .= $this->load->view('daftar/cetak/footer', $isi, TRUE);
                // echo "$html";die;
                // $pdf->SetHTMLHeader($head); // write the HTML header into the PDF
                $stylesheet = file_get_contents(FCPATH."_assets/css/screen.css");
                $stylesheet .= file_get_contents(FCPATH."_assets/css/print.css");
                // $pdf->SetHTMLFooter($foot); // write the HTML header into the PDF
                $pdf->WriteHTML($stylesheet,1);
                $pdf->WriteHTML($html,2); // write the HTML into the PDF
                // $pdf->WriteHTML($html,2); // write the HTML into the PDF_arc(    , x, y, r, alpha, beta)
                $pdf->Output($pdfFilePath, 'F'); // save to file because we can
            }
            // $this->output->set_content_type('application/pdf')->set_output(file_get_contents(FCPATH."_assets/downloads/reports/{$filename}.pdf"));
            // window.open(base_url("_assets/downloads/reports/{$filename}.pdf");
            redirect(base_url("_assets/downloads/reports/{$filename}.pdf")); 
            die;
        }

        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']     = 'daftar/view/form_lihat';
        $isi['judul']       = 'Lihat Pengajuan Data Rumah Tangga ';
        $isi['sub_judul']   = 'Penduduk Miskin';
        // $isi['data_nav']    = 'active';
        $isi['daftar_nav']  = 'active';
        $isi['mohon_nav']   = 'active';

        $no_kk = $this->input->get('no_kk');
        
        if ($no_kk) {
            // var_dump($no_kk);die;
            // $rumahtangga = $this->model_s2_mohonbdt->getRumahTangga($no_kk);
            $data = $this->model_s2_skrining->getData($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }

        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_stat_hbkel']       = $this->model_s2_masterbdt->getstat_hbkel();
        $isi['opsi_stat_rtangga']       = $this->model_s2_masterbdt->getstat_rtangga();
      
        $this->load->view('home_view', $isi);
    }

    public function submit()
    {
        // var_dump($this->input->post());die;

        if($this->input->post())
        {
            extract($this->input->post());
            // $isi = $this->input->post();    
            // var_dump($isi);die;
            $skrining = array(
                    'petugas_rubah'    =>  strtoupper($this->cu->NAMA_LENGKAP),
                    // 'kodewilayah'   => '3575'.$no_kec.$no_kel,
                    // 'provinsi'      => $provinsi,
                    // 'kabupaten'     => $kabupaten,
                    // 'kecamatan'     => $kecamatan,
                    // 'desa'          => $desa,
                    // 'no_prop'       => $no_prop,
                    // 'no_kab'        => $no_kab,
                    // 'no_kec'        => $no_kec,
                    // 'no_kel'        => $no_kel,
                    // 'no_kk'         => $no_kk,
                    // 'alamat'        => $alamat,
                    // 'no_rt'         => $no_rt,
                    // 'no_rw'         => $no_rw,
                    // 'nik_pendaftar'           => $nik_pendaftar,
                    // 'nama_pendaftar'          => $nama_pendaftar, 
                    // 'jenis_klmin_pendaftar' => $jenis_klmin_pendaftar,
                    // 'nik_rumahtangga' => $nik_rumahtangga,
                    // 'nama_kep_rumahtangga' => $nama_kep_rumahtangga,
                    // 'jenis_klmin_rumahtangga' => $jenis_klmin_pendaftar,
                    'hubungan_keluarga' => $hubungan_keluarga,
                    'hubungan_rumahtangga_induk' => $hubungan_rumahtangga_induk,
                    // 'bulan_lhr_rumahtangga'     => $bulan_lhr_rumahtangga,
                    // 'tahun_lhr_rumahtangga'     => $tahun_lhr_rumahtangga,
                    'status_pkrjn_rumahtangga' => $status_pkrjn_rumahtangga,
                    'b1_r9'         => $b1_r9,
                    'b3_r1a'        => $b3_r1a_bdt,
                    'b3_r3'         => $b3_r3_bdt,
                    'b3_r4a'        => $b3_r4a_bdt,
                    'b3_r5a'        => $b3_r5a_bdt,
                    'b3_r11a'       => $b3_r11a_bdt,
                    'b5_r1a'        => $b5_r1a_bdt,
                    'b5_r1c'        => $b5_r1c_bdt,
                    'b5_r1k'        => $b5_r1k_bdt,
                    'b4_k18'        => $b4_k18_bdt,
                    // 'kd_pengajuan'  => $kd_pengajuan,
                    // 'no_kk_induk'   => $no_kk_induk,
                    // 'nik_induk'     => $nik_rumahtangga_induk,
                    // 'nama_induk'    => $nama_kep_rumahtangga_induk,
                    // 'nomor_urut_rumah_tangga'   => $nomor_urut_rumah_tangga 
                );
            // var_dump($skrining);die;
            $no_kk = $this->input->post('no_kk');
            // var_dump($no_kk);die;
            $update = $this->model_s2_skrining->update($no_kk,$skrining);
            // var_dump($update);die;
            // date('Y-m-d', strtotime($tanggal)); 
            $get_params = $this->input->post('get_params');
            redirect('daftar/edit?'.$get_params);
            exit;
        }
    }

    public function delete() 
    {  
        if ($this->input->get('no_kk')) {
            $no_kk = $this->input->get('no_kk');

            $check = $this->model_s2_skrining->getData($no_kk);
            // var_dump($check['no_kk']);die;
            if ($check['no_kk_induk'] == $no_kk) {
                $this->model_s2_skrining->deleteRuta($no_kk);
                // echo "kepala rumahtangga";die;
            } else {
                $this->model_s2_skrining->deleteKK($no_kk);
                // echo "kepala keluarga";die;
            }
            
            
            /*$this->model_s2_mohonbdt->deleteIndividuRumahTangga($no_kk);
            $this->model_s2_biodata->deleteIndividuRumahTangga($no_kk);*/
        }
        if($this->input->is_ajax_request()) {
            echo 'success';
            // echo 'error';
        }
        else {
            $get_params = get_params($this->input->get(), array('no_kk'));
            redirect('daftar/index?'.$get_params);
        }
        exit;
    }

}
