<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Statistik extends MY_Controller {
	public function __construct() {
		parent::__construct();
		// check login user
		$this->_init_logged_in();
		$this->_init_wilayah();

		// $this->model_security->getsecurity();
		$this->load->model('model_s2_agr_stat', '', true);
	}

	public function index() {
		// $this->model_security->getsecurity();
		$isi['content'] = 'statistik/main/main_view';
		$isi['judul'] = 'Statistik';
		$isi['sub_judul'] = 'Agregat Data Kemiskinan';
		$isi['stat_nav'] = 'active';
		// $isi['stat_main_nav'] = 'active';
		
		if ($this->input->get()) {
			$isi = array_merge($isi, $this->input->get());
	
			$kd_prop = $this->input->get('provinsi_id');
			$kd_kab = $this->input->get('kabupaten_id');
			$kd_kec = $this->input->get('kecamatan_id');

			if ($this->input->get('kelurahan_id')) {
				$kelurahan_code = $this->input->get('kelurahan_id'); // 03|1001
				$kelurahan_code_arr = explode("-", $kelurahan_code);
				$kd_kec = $kelurahan_code_arr[0];
				$kd_kel = $kelurahan_code_arr[1];
				$isi['filter_level'] = 2;
				$isi['pane_nav'] = 'hide';
				$isi['kel_active'] = 'active';
				// var_dump($kd_kel);die;
			} else {
				$isi['kec_active'] = 'active';
			}
			// var_dump($kd_prop,$kd_kab,$kd_kec,$kd_kel);die;
			$kd_agr = $this->input->get('kd_agr');

			if ($kd_agr == 1) {
				$judul = '';
				$isi['agregat'] = $this->model_s2_agr_stat->getAgregat($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				//var_dump($isi['agregat']);die;
			}
			if ($kd_agr == 2) {
				$isi['agregatAgama'] = $this->model_s2_agr_stat->getagrAgama($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				// $isi['headers']      = $this->model_header->getheaderAgama();
				// var_dump($isi['agregatAgama']);die;
			}
			if ($kd_agr == 3) {
				$isi['agregatGoldrh'] = $this->model_s2_agr_stat->getagrGoldrh_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				//$isi['headers'] = $this->model_header->getheaderGoldrh();
			}
			if ($kd_agr == 4) {
				$isi['agregatPddkan'] = $this->model_s2_agr_stat->getagrPddkan($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				// $isi['headers'] = $this->model_header->getheaderPddkan();
				//var_dump($isi['agregatPddkan']);die;
			}
			if ($kd_agr == 5) {
				$isi['agregatSHDK'] = $this->model_s2_agr_stat->getagrSHDK_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				// $isi['headers'] = $this->model_header->getheaderPddkan();
				//var_dump($isi['agregatPddkan']);die;
			}
			if ($kd_agr == 6) {
				$isi['agregatUmur'] = $this->model_s2_agr_stat->getagrUmur_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				$isi['kec_active'] = 'active';
				unset($isi['pane_nav']);
				unset($isi['kel_active']);
			}
			if ($kd_agr == 7) {
				$isi['agregatUmur'] = $this->model_s2_agr_stat->getagrUmur($kd_prop, $kd_kab, $kd_kec, $kd_kel);
				$isi['kec_active'] = 'active';
				unset($isi['pane_nav']);
				unset($isi['kel_active']);
			}
			if ($kd_agr == 8) {
				$isi['agregat'] = $this->model_s2_agr_stat->getagrWajibktp_mskn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			if ($kd_agr == 9) {
				$judul = 'Pekerjaan';
				$isi['agregatPkrjn'] = $this->model_s2_agr_stat->getagrPkrjn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			if ($kd_agr == 10) {
				$judul = 'Pekerjaan';
				$isi['agregatPkrjn'] = $this->model_s2_agr_stat->getagrPkrjn($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			if ($kd_agr == 11) {
				$judul = 'Pusat';
				$isi['agregat'] = $this->model_s2_agr_stat->getagrSinkronisasi($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			if ($kd_agr == 12) {
				$judul = 'NonPusat';
				$isi['agregat'] = $this->model_s2_agr_stat->getagrNonpusat($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			if ($kd_agr == 13) {
				$judul = 'BPJS';
				$isi['agregat'] = $this->model_s2_agr_stat->getagrBpjs($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			if ($kd_agr == 14) {
				$judul = 'Prelist';
				$isi['agregat'] = $this->model_s2_agr_stat->getagrPrelist($kd_prop, $kd_kab, $kd_kec, $kd_kel);
			}
			// var_dump($isi);die;
			
			if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Agr--Miskin--".$judul."--" . date("Ym") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('statistik/main/table/table_main', $isi, TRUE);
                exit;
            }
		}

		$this->load->view('home_view', $isi);
	}

	public function lihat()
    {
        $isi['content'] 	= 'statistik/main/result/table_view';
		$isi['judul'] 		= 'Statistik';
		$isi['sub_judul'] 	= 'Data Agregat Data Kemiskinan';
		$isi['stat_nav'] 	= 'active';

		if ($this->input->get()) {
			if ($this->input->get('kecamatan_id') != "") {
                $filter['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $filter['kd_kec'] = $kelurahan_code_arr[0];
                    $filter['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $filter['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $filter['kd_kec'] = $this->cu->NO_KEC;
                    $filter['kd_kel'] = $this->cu->NO_KEL;
                }
            }
            
        	$GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            if ($this->input->get('kd_agr') == 1) {
				if ($this->input->get('no_kec')){
            		$no_kec = $this->input->get('no_kec');
					$where = 'NO_KEC = '.$no_kec;
					// var_dump($where);die;
            	}
            	if ($this->input->get('no_kel')){
            		$no_kel = $this->input->get('no_kel');
					$where = 'NO_KEL = '.$no_kel;
            	}if (!$this->input->get('no_kec') && !$this->input->get('no_kel')){
            		$where = "";
            	}
            	
			}
            if ($this->input->get('kd_agr') == 9) {
            	if ($this->input->get('kode')){
            		$kode = $this->input->get('kode');
					$where = 'JENIS_PKRJN = '.$kode.' AND FLAG_BDT = 2';
            	}
            	$judul = 'Pekerjaan';
			}
			if ($this->input->get('kd_agr') == 11) {
				if ($this->input->get('no_kec')){
            		$no_kec = $this->input->get('no_kec');
					$where = 'FLAG_BDT = 2 AND NO_KEC = '.$no_kec;
            	}
            	if ($this->input->get('no_kel')){
            		$no_kel = $this->input->get('no_kel');
					$where = 'FLAG_BDT = 2 AND NO_KEL = '.$no_kel;
            	}if (!$this->input->get('no_kec') && !$this->input->get('no_kel')){
            		$where = 'FLAG_BDT = 2';
            	}
            	$judul = 'Sinkronisasi';
			}
			if ($this->input->get('kd_agr') == 12) {
				if ($this->input->get('no_kec')){
            		$no_kec = $this->input->get('no_kec');
					$where = 'FLAG_BDT <> 2 AND FLAG_BPJS = 2 AND NO_KEC = '.$no_kec;
            	}
            	if ($this->input->get('no_kel')){
            		$no_kel = $this->input->get('no_kel');
					$where = 'FLAG_BDT <> 2 AND FLAG_BPJS = 2 AND NO_KEL = '.$no_kel;
            	}if (!$this->input->get('no_kec') && !$this->input->get('no_kel')){
            		$where = 'FLAG_BDT <> 2 AND FLAG_BPJS = 2';
            	}
            	$judul = 'NonPusat';
			}
			if ($this->input->get('kd_agr') == 13) {
				if ($this->input->get('no_kec')){
            		$no_kec = $this->input->get('no_kec');
					$where = 'FLAG_BPJS = 2 AND NO_KEC = '.$no_kec;
            	}
            	if ($this->input->get('no_kel')){
            		$no_kel = $this->input->get('no_kel');
					$where = 'FLAG_BPJS = 2 AND NO_KEL = '.$no_kel;
            	}if (!$this->input->get('no_kec') && !$this->input->get('no_kel')){
            		$where = 'FLAG_BPJS = 2';
            	}
            	$judul = 'BPJS';
			}
			if ($this->input->get('kd_agr') == 14) {
				if ($this->input->get('no_kec')){
            		$no_kec = $this->input->get('no_kec');
					$where = 'NO_KEC = '.$no_kec;
					// var_dump($where);die;
            	}
            	if ($this->input->get('no_kel')){
            		$no_kel = $this->input->get('no_kel');
					$where = 'NO_KEL = '.$no_kel;
            	}if (!$this->input->get('no_kec') && !$this->input->get('no_kel')){
            		$where = "";
            	}
            	$judul = 'Prelist';
			}


            // var_dump($where);die;
            $table = 'induk';
            if ($this->input->get('kd_agr') == 1) {
            	$orderby = 'NO_KEC,NO_KEL,NO_KK,NAMA_LGKP,STAT_HBKEL';
            	$isi['data'] = $this->model_s2_agr_stat->getTable($offset, $perpage, $orderby, $where, $filter);
            	$isi['total_data'] = $this->model_s2_agr_stat->getTableTotal($where, $filter);
            }
            if ($this->input->get('kd_agr') == 14) {
            	$orderby = 'NO_KEC,NO_KEL,NO_KK,NAMA_LGKP';
            	$isi['data'] = $this->model_s2_agr_stat->getTablePrelist($offset, $perpage, $orderby, $where, $filter);
            	$isi['total_data'] = $this->model_s2_agr_stat->getTablePrelistTotal($where, $filter);
            	$table = 'prelist';
            }
            else {
            	$orderby = 'NO_KEC,NO_KEL,NO_KK,NAMA_LGKP,STAT_HBKEL';
	            $isi['data'] = $this->model_s2_agr_stat->getTableMiskin($offset, $perpage, $orderby, $where, $filter);
	            $isi['total_data'] = $this->model_s2_agr_stat->getTableMiskinTotal($where, $filter);
            }
			// var_dump($isi['total_data']);die;

			if($isi['total_data'] > 0){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['show']);die;
            // var_dump($isi['data']);die;

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('statistik/lihat') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            // EXPORT
            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Induk--Kemiskinan--".$judul."--" . date("Ym") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('statistik/main/table/table_'.$table, $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('statistik/main/result/table_view', $isi, TRUE);
            die;
        }

    	$this->load->view('home_view', $isi);

    }
}
