<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Induk extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_init_logged_in();
        $this->_init_wilayah();
        $this->load->model('model_s2_induk');
        $this->load->model('model_s2_biodata');
    }

    public function index($a = "")
    {
        $isi['content']         = 'induk/individu/main_index';
        $isi['judul']           = 'Data Induk Kemiskinan';
        $isi['sub_judul']       = 'Daftar';
        $isi['kemiskinan_nav']  = 'active';
        $isi['induk_nav']       = 'active';
        $isi['induk_sink_nav']  = 'active';
                
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());
            // var_dump($isi);die;

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";
            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";
            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";
            }
            if ($this->input->get('inputrw') != "") {

                $no_rw = $this->input->get('inputrw', TRUE);
                $entri['no_rw'] = $no_rw;
                $isi['toggleRw'] = "checked";
            }
            if ($this->input->get('inputrt') != "") {

                $no_rt = $this->input->get('inputrt', TRUE);
                $entri['no_rt'] = $no_rt;
                $isi['toggleRt'] = "checked";
            }

            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            // $where = 'STAT_HBKEL = 1';
            $orderby = 'NO_KEC,NO_KEL,NO_RW,NO_RT,NO_KK,STAT_HBKEL';
            $isi['data'] = $this->model_s2_induk->getSinkronisasi($offset, $perpage, $orderby, $where, $entri);
            // var_dump($isi['data']);die;
            $isi['total_data'] = $this->model_s2_induk->getSinkronisasiTotal($where, $entri);
            
            if($isi['total_data'] > 0){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['show']);die;
            // var_dump($isi['data']);die;

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('induk/index') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            // EXPORT
            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-Kemiskinan-Individu-Sinkronisasi-" . date("Ym") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('induk/individu/table', $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('induk/individu/result', $isi, TRUE);
            die;
        }

        $this->load->view('home_view', $isi);
    }

    public function verifikasi()
    {
        $isi['content']         = 'induk/individu/main_verifikasi';
        $isi['judul']           = 'Data Induk Kemiskinan';
        $isi['sub_judul']       = 'Daftar';
        $isi['kemiskinan_nav']  = 'active';
        $isi['induk_nav']    = 'active';
        $isi['induk_ver_nav']   = 'active';
                
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());
            // var_dump($isi);die;

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";
            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";
            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";
            }
            if ($this->input->get('inputrw') != "") {

                $no_rw = $this->input->get('inputrw', TRUE);
                $entri['no_rw'] = $no_rw;
                $isi['toggleRw'] = "checked";
            }
            if ($this->input->get('inputrt') != "") {

                $no_rt = $this->input->get('inputrt', TRUE);
                $entri['no_rt'] = $no_rt;
                $isi['toggleRt'] = "checked";
            }


            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            // $where = 'STAT_HBKEL = 1';
            $entri['view'] = 'INDIVIDU';
            $orderby = 'NO_KEC,NO_KEL,NO_RW,NO_RT,NO_KK,STAT_HBKEL';
            $isi['data'] = $this->model_s2_induk->getList($offset, $perpage, $orderby, $where, $entri);
            // var_dump($isi['data']);die;
            $isi['total_data'] = $this->model_s2_induk->getListTotal($where, $entri);
            
            if($isi['total_data'] > 0){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['show']);die;
            // var_dump($isi['data']);die;

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('induk/prelist') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            // EXPORT
            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-Kemiskinan-Individu-Prelist-" . date("Ym") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('induk/individu/table', $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('induk/individu/result', $isi, TRUE);
            die;
        }

        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']         = 'induk/data_keluarga';
        $isi['judul']           = 'Data Induk Kemiskinan';
        $isi['sub_judul']       = 'Data Keluarga';
        $isi['kemiskinan_nav']  = 'active';
        $isi['induk_nav']       = 'active';
        // $isi['menu']             = $this->input->get('menu');

        $uri = $isi['uri'] = $this->input->get('uri');
        if ($uri == 'index') {     
            $isi['induk_sink_nav']   = 'active';
        } else if ($uri == 'verifikasi') {
            $isi['induk_ver_nav']   = 'active';
        }

        // $isi = $this->input->get();
        $no_kk = $this->input->get('no_kk');
        
        // var_dump($no_kk);die;
        $isi['data'] = $this->model_s2_biodata->getKepala($no_kk);
        $data = $isi['data'];
        // var_dump($data);die;

        if ($data) {
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }

        if ($uri == 'index') {     
            $isi['data'] = $this->model_s2_induk->getAnggotaSinkronisasi($no_kk);
        } else if ($uri == 'verifikasi') {
            $isi['data'] = $this->model_s2_induk->getAnggotaVerifikasi($no_kk);
        }

        $this->load->view('home_view', $isi);
    }

    public function lihat_biodata()
    {
        $isi['content']     = 'induk/individu/data_keluarga';
        $isi['judul']       = 'Data Induk Kemiskinan';
        $isi['sub_judul']   = 'Review';
        $isi['induk_nav']   = 'active';

        $nik = $this->input->get('nik');
        // var_dump($id_rt, $no_art);die;
        
        if ($nik) {
            $individu = $this->model_s2_induk->viewBpjs($nik);
            foreach ($individu as $key => $value) {
                if (empty($value)) {
                    $value = "-";
                }
                $isi[$key] = strtoupper($value);
                // $key        = strtoupper($key);
                // $data[$key] = $value;
            }
        }
        // var_dump($individu);die;
        $this->load->view('home_view', $isi);
    }

}
