<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home extends ADM_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->_init_logged_in();

        // var_dump($this->ca);
    }

    public function index()
    {
        $this->template->load_admin(ADM_VIEW . 'home', $this->global_data);
    }

    public function logout()
    {
        unset_login_session('admin');
        $this->session->sess_destroy();
        admin_login();
        exit;
    }
}

/* End of file Home.php */
