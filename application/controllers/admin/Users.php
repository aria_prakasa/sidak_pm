<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends ADM_Controller
{

    public $curpage = "";

    public function __construct()
    {
        // error_reporting(-1);
        parent::__construct();
        $this->_init_logged_in();
        $this->curpage = ADM_BASE . "users";
        $this->load->model(array('setup_prop', 'setup_kab', 'setup_kec', 'setup_kel', 'master_user'));
    }

    public function index()
    {
        $this->global_data['page_title']    = "Users";
        $this->global_data['page_subtitle'] = "Data";

        $this->global_data['list'] = $this->ouser->get_list(0, 0, "NAMA_LENGKAP ASC");
        // var_dump($this->global_data['list']);
        $this->template->load_admin(ADM_VIEW . 'users/list', $this->global_data);
    }

    public function add()
    {
        $this->global_data['page_title']    = "User Form";
        $this->global_data['page_subtitle'] = "Create New";

        $this->form_validation->set_rules('nip', 'N I P', 'trim|required|min_length[3]|is_unique[USERS.NIP]');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required|min_length[2]');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|is_unique[USERS.USERNAME]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|md5');
        $this->form_validation->set_rules('user_level', 'User Level', 'trim|required|min_length[1]');

        // $this->form_validation->set_rules('no_kec', 'Kecamatan', 'trim|required|min_length[2]');
        // $this->form_validation->set_rules('no_kel', 'Kelurahan', 'trim|required|min_length[2]');
        $this->form_validation->set_error_delimiters('', '<br />');

        if ($this->form_validation->run()) {
            $O              = new Ouser;
            $arr            = $this->input->post();
            $arr['no_prop'] = NO_PROP;
            $arr['no_kab']  = NO_KAB;
            if ($this->input->post('no_kec') != "") {
                $arr['no_kec'] = end(explode("-", $this->input->post('no_kec')));
            }

            if ($this->input->post('no_kel') != "") {
                $arr['no_kel'] = end(explode("-", $this->input->post('no_kel')));
            }

            $arr = array_change_key_case($arr, CASE_UPPER);
            if ($O->add($arr)) {
                warning('add', 'success');
            } else {
                warning('add', 'fail');
            }
            redirect($this->curpage);
            exit;
        }

        $this->template->load_admin(ADM_VIEW . 'users/form', $this->global_data);
    }

    public function nip_check($str, $id)
    {
        $check = $this->ouser->get_list(0, 1, "", "NIP = '" . trim($str) . "' AND NIP <> '" . trim($id) . "'");
        // var_dump($this->db->last_query()); die;
        if ($check) {
            $this->form_validation->set_message('nip_check', 'NIP tersebut sudah digunakan!');
            return false;
        } else {
            return true;
        }
    }

    public function username_check($str, $id)
    {
        $check = $this->ouser->get_list(0, 1, "", "USERNAME = '" . trim($str) . "' AND NIP <> '" . trim($id) . "'");
        // var_dump($this->db->last_query()); die;
        if ($check) {
            $this->form_validation->set_message('username_check', 'User tersebut sudah digunakan!');
            return false;
        } else {
            return true;
        }
    }

    public function edit($id = null)
    {
        $this->global_data['page_title']    = "User Form";
        $this->global_data['page_subtitle'] = "Edit";

        $O   = new Ouser($id, 'NIP');
        $row = $O->row;
        // var_dump($row); die;
        keysToLower($row);
        // var_dump($row);die;
        $this->global_data['row'] = $row;

        $this->form_validation->set_rules('nip', 'N I P', 'trim|required|min_length[3]|callback_nip_check[' . $O->ID . ']');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required|min_length[2]');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|callback_username_check[' . $O->ID . ']');
        if ($this->input->post('password') != "") {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|md5');
        }
        $this->form_validation->set_rules('user_level', 'User Level', 'trim|required|min_length[1]');

        // $this->form_validation->set_rules('no_kec', 'Kecamatan', 'trim|required|min_length[2]');
        // $this->form_validation->set_rules('no_kel', 'Kelurahan', 'trim|required|min_length[2]');
        $this->form_validation->set_error_delimiters('', '<br />');

        if ($this->form_validation->run()) {
            $arr = $this->input->post();
            if ($this->input->post('password') == "") {
                unset($arr['password']);
            }

            $arr['no_prop'] = NO_PROP;
            $arr['no_kab']  = NO_KAB;
            if ($this->input->post('no_kec') != "") {
                $arr['no_kec'] = end(explode("-", $this->input->post('no_kec')));
            }

            if ($this->input->post('no_kel') != "") {
                $arr['no_kel'] = end(explode("-", $this->input->post('no_kel')));
            }

            $arr = array_change_key_case($arr, CASE_UPPER);
            if ($O->edit($arr)) {
                warning('edit', 'success');
            } else {
                warning('edit', 'fail');
            }
            redirect($this->curpage);
            exit;
        }

        $this->template->load_admin(ADM_VIEW . 'users/form', $this->global_data);
    }

    public function delete($id = null)
    {
        $O = new Ouser($id, 'NIP');
        if ($O->delete()) {
            warning('delete', 'success');
        } else {
            warning('delete', 'fail');
        }
        redirect($this->curpage);
        exit;
    }

}
