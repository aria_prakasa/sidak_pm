<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ajax extends MY_Controller
{

    public function index()
    {

    }

    public function get_info()
    {
        $cu = $this->cu;
        extract(get_object_vars($cu));
        $OP        = new Setup_prop($no_prop);
        $OKb       = new Setup_kab($no_kab, $no_prop);
        $OKc       = new Setup_kec($no_kec, $no_kab, $no_prop);
        $OKl       = new Setup_kel($no_kel, $no_kec, $no_kab, $no_prop);
        $nama_prop = $OP->get_name();
        $nama_kab  = $OKb->get_name();
        $nama_kec  = $OKc->get_name();
        $nama_kel  = $OKl->get_name();

        $stat_arr = null;
        if (!$this->input->is_ajax_request()) {
            $stat_arr['status']  = "error";
            $stat_arr['message'] = "Restricted!";
            die(json_encode($stat_arr));
        }
        extract(get_object_vars($cu));

        extract($_POST);

        // get filter month and year
        $filter_bulan = $this->input->post('month');
        $filter_tahun = $this->input->post('year');
        $parse_month  = str_pad(intval($filter_bulan), 2, "0", STR_PAD_LEFT);

        // get table suffix
        $table_suffix = get_table_suffix($filter_bulan, $filter_tahun);

        // try caching
        // $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $this->load->driver('cache');

        if (!empty($nik)) {
            $cache_key = 'wni_' . $nik . $table_suffix;

            $wni_cache = $this->cache->file->get($cache_key);
            if (!CACHE) {
                $wni_cache = false;
            }

            if (!$wni_cache) {
                $O        = new Biodata_wni($nik, "ID", $table_suffix);
                $wni_data = json_encode($O->row);
                $wni_row  = $O->row;
                // Save into the cache for 1 minute
                $this->cache->file->save($cache_key, $wni_data, 60);
            } else {
                $wni_row = json_decode($wni_cache);
                $O       = new Biodata_wni;
                $O->setup($wni_row);
            }

            if (!$O->row) {
                $stat_arr['status']  = "error";
                $stat_arr['message'] = "Maaf data NIK tersebut tidak tersedia. Silahkan periksa dan ulangi kembali.";
                die(json_encode($stat_arr));
            }
            $no_prop = $O->row->NO_PROP;
            $no_kab  = $O->row->NO_KAB;
            $no_kec  = $O->row->NO_KEC;
            $no_kel  = $O->row->NO_KEL;
            if (empty($no_kk)) {
                $no_kk = $O->row->NO_KK;
            }

        }

        $ODk = null;
        if (!empty($no_kk)) {
            $cache_key = 'kel_' . $no_kk . $table_suffix;

            $kel_cache = $this->cache->file->get($cache_key);
            if (!CACHE) {
                $kel_cache = false;
            }

            if (!$kel_cache) {
                $ODk      = new Data_keluarga($no_kk, "ID", $table_suffix);
                $kel_data = json_encode($ODk->row);
                $kel_row  = $ODk->row;
                // Save into the cache for 1 minute
                $this->cache->file->save($cache_key, $kel_data, 60);
            } else {
                $kel_row = json_decode($kel_cache);
                $ODk     = new Data_keluarga;
                $ODk->setup($kel_row);
            }

            if (!$ODk->row) {
                $stat_arr['status']  = "error";
                $stat_arr['message'] = "Maaf data No KK tersebut tidak tersedia. Silahkan periksa dan ulangi kembali.";
                die(json_encode($stat_arr));
            }
            /*
            $nik_arr = $ODk->get_anggota_nik_arr();
            $nik_ddl = dropdown("pemohon_nik",$nik_arr,$nik,'required',"-- SELECT --");
            $ayah_nik_ddl = dropdown("ayah_nik",$nik_arr,$ayah_nik,'required',"-- SELECT --");
            $ibu_nik_ddl = dropdown("ibu_nik",$nik_arr,$ibu_nik,'required',"-- SELECT --");
            //*/
            /*
        $no_prop = $ODk->row->no_prop;
        $no_kab = $ODk->row->no_kab;
        $no_kec = $ODk->row->no_kec;
        $no_kel = $ODk->row->no_kel;
        //*/
        }

        if ($ODk->row) {
            // tambah wilayah name untuk kk
            extract(get_object_vars($ODk->row));
            $OP                  = new Setup_prop($no_prop);
            $OKb                 = new Setup_kab($no_kab, $no_prop);
            $OKc                 = new Setup_kec($no_kec, $no_kab, $no_prop);
            $OKl                 = new Setup_kel($no_kel, $no_kec, $no_kab, $no_prop);
            $ODk->row->NAMA_PROP = $OP->get_name();
            $ODk->row->NAMA_KAB  = $OKb->get_name();
            $ODk->row->NAMA_KEC  = $OKc->get_name();
            $ODk->row->NAMA_KEL  = $OKl->get_name();
        }

        /*if($local==1)
        {
        if($no_prop != $cu->no_prop ||
        $no_kab != $cu->no_kab ||
        $no_kec != $cu->no_kec ||
        $no_kel != $cu->no_kel
        )
        {
        $stat_arr['status'] = "error";
        $stat_arr['message'] = "Maaf data tersebut tidak tersedia. Silahkan periksa dan ulangi kembali.";
        //$stat_arr['data'] = json_encode((array) $ODk->row);
        die(json_encode($stat_arr));
        }
        }*/

        /*$O->row->KEBANGSAAN = "Indonesia";
        $O->row->KEWARGANEGARAAN = "wni";
        $umur_arr = datediff($O->row->TGL_LHR);
        $umur = $umur_arr['years'];
        $O->row->UMUR = $umur;

        if(intval($O->row->TGL_LHR) > 0)
        {
        $O->row->TGL_LHR = get_date_lang($O->row->TGL_LHR, "ID");
        //$O->row->TGL_LHR = date("d-m-Y", strtotime($O->row->TGL_LHR));
        }
        if(intval($O->row->TGL_KWN) > 0)
        {
        $O->row->TGL_KWN = get_date_lang($O->row->TGL_KWN, "ID");
        //$O->row->TGL_KWN = date("d-m-Y", strtotime($O->row->TGL_KWN));
        }

        $OTmp = new Master_pekerjaan($O->row->JENIS_PKRJN);
        $O->row->JENIS_PKRJN_DESCRIP = $OTmp->row->DESCRIP;
        unset($OTmp);

        $OTmp = new Master_kelamin($O->row->jenis_klmin);
        $O->row->jenis_klmin_DESCRIP = $OTmp->row->DESCRIP;
        unset($OTmp);

        $OTmp = new Master_agama($O->row->AGAMA);
        $O->row->AGAMA_DESCRIP = $OTmp->row->DESCRIP;
        unset($OTmp);

        if(intval($O->row->TGL_KWN) > 0)
        {
        $O->row->TGL_KWN = date("d-m-Y", strtotime($O->row->TGL_KWN));
        }
         */
        if (intval($O->row->TGL_LHR) > 0) {
            $O->row->TGL_LHR = date("d-m-Y", strtotime($O->row->TGL_LHR));
        }

        $stat_arr['value'] = array("wni" => (array) $O->row,
            "kk"                             => (array) $ODk->row, /*,
        "nik_ddl" => $nik_ddl,
        "ayah_nik_ddl" => $ayah_nik_ddl,
        "ibu_nik_ddl" => $ibu_nik_ddl*/
        );
        $stat_arr['status']  = "success";
        $stat_arr['message'] = "Successful";
        die(json_encode($stat_arr));
    }

    public function get_kelamin_ddl()
    {
        if (!$this->input->is_ajax_request()) {
            die('No Direct Access!');
        }

        extract($_POST);
        $O   = new Master_kelamin;
        $tmp = $O->drop_down_select($name, $value);
        die($tmp);
    }

    public function get_agama_ddl()
    {
        if (!$this->input->is_ajax_request()) {
            die('No Direct Access!');
        }

        extract($_POST);
        $O   = new Master_agama;
        $tmp = $O->drop_down_select($name, $value);
        die($tmp);
    }

    public function get_pekerjaan_ddl()
    {
        if (!$this->input->is_ajax_request()) {
            die('No Direct Access!');
        }

        extract($_POST);
        $O   = new Master_pekerjaan;
        $tmp = $O->drop_down_select($name, $value);
        die($tmp);
    }

    public function get_prop_ddl()
    {
        if (!$this->input->is_ajax_request()) {
            die('No Direct Access!');
        }

        $no_prop = "";
        extract($_POST);
        $O   = new Setup_prop;
        $tmp = $O->drop_down_select($name, $no_prop, "", "-- select --");
        die($tmp);
    }

    public function get_kab_ddl()
    {
        if (!$this->input->is_ajax_request()) {
            die('No Direct Access!');
        }

        $no_kab = "";
        extract($_POST);
        if (empty($no_prop)) {
            die("Select Propinsi");
        }

        $O         = new Setup_kab;
        $attr_addt = '';
        if ($disabled == 1) {
            $attr_addt = 'disabled="disabled"';
        }

        $tmp = $O->drop_down_select_by_prop($no_prop, $name, $no_kab, $attr_addt, "-- select --");
        die($tmp);
    }

    public function get_kec_ddl()
    {
        if (!$this->input->is_ajax_request()) {
            die('No Direct Access!');
        }

        $no_kec = "";
        extract($_POST);
        if (empty($no_kab)) {
            die("Select Kabupaten/Kota");
        }

        $no_kab_arr = explode("-", $no_kab);
        $no_prop    = $no_kab_arr[0];
        $no_kab     = $no_kab_arr[1];
        $O          = new Setup_kec;
        $attr_addt  = '';
        if ($disabled == 1) {
            $attr_addt = 'disabled="disabled"';
        }

        $tmp = $O->drop_down_select_by_kab($no_prop, $no_kab, $name, $no_kec, $attr_addt, "-- select --");
        die($tmp);
    }

    public function get_kel_ddl()
    {
        if (!$this->input->is_ajax_request()) {
            die('No Direct Access!');
        }

        $no_kel = "";
        extract($_POST);
        if (empty($no_kec)) {
            die("Select Kecamatan");
        }

        $no_kec_arr = explode("-", $no_kec);
        $no_prop    = $no_kec_arr[0];
        $no_kab     = $no_kec_arr[1];
        $no_kec     = $no_kec_arr[2];
        $O          = new Setup_kel;
        $attr_addt  = '';
        if ($disabled == 1) {
            $attr_addt = 'disabled="disabled"';
        }

        $tmp = $O->drop_down_select_by_kec($no_prop, $no_kab, $no_kec, $name, $no_kel, $attr_addt, "-- select --");
        die($tmp);
    }

}
