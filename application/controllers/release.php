<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Release extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        $this->_init_wilayah();
        // $this->curpage = $this->router->fetch_class();
        // $this->load->model('model_entry');
        // $this->load->model('model_pullout');
        $this->load->model('model_s2_biodata');
        $this->load->model('model_s2_bdt');
        $this->load->model('model_s2_masterbdt');
        $this->load->model('model_s2_mohonbdt');
        $this->load->model('model_s2_skrining');
        $this->load->model('model_s2_list');
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        $isi['content']     = 'release/main';
        $isi['judul']       = 'Daftar Pengajuan Terverifikasi';
        $isi['sub_judul']   = 'Data pendaftar miskin';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['release_nav']    = 'active';
        // $isi['release_lama_nav']   = 'active';
        
        // var_dump($isi);die;
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());
            // var_dump($isi);die;

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";
            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";
            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";
            }
            if ($this->input->get('inputrw') != "") {

                $no_rw = $this->input->get('inputrw', TRUE);
                $entri['no_rw'] = $no_rw;
                $isi['toggleRw'] = "checked";
            }
            if ($this->input->get('inputrt') != "") {

                $no_rt = $this->input->get('inputrt', TRUE);
                $entri['no_rt'] = $no_rt;
                $isi['toggleRt'] = "checked";
            }

            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            // $where = 'NO_URUT_KK = 1';
            $orderby = 'NO_KEC,NO_KEL, NO_RW, NO_RT, NO_KK, B1_R6';
            $isi['data'] = $this->model_s2_list->getRumahTanggaRelease($offset, $perpage, $orderby, $where, $entri);
            $isi['total_data'] = $this->model_s2_list->getRumahTanggaReleaseTotal($where, $entri);

            if($isi['data']){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['data']);die;

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('release/index') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            // EXPORT
            if ($this->input->get('export') == "1") {

                $orderby = 'NO_KEC, NO_KEL, NO_RW, NO_RT, NO_KK, B1_K6';
                $isi['data'] = $this->model_s2_list->getRumahTanggaList($offset, $perpage, $orderby, $where, $entri);
                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-List-Rumahtangga-" . date("dmY") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache

                echo $this->load->view('release/result/table_ruta', $isi, TRUE);
                exit;
            }

            if ($this->input->get('export') == "2") {

                $orderby = 'NO_KEC, NO_KEL, NO_KK, NO_ART';
                $isi['data'] = $this->model_s2_list->getIndividuList($offset, $perpage, $orderby, $where, $entri);
                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-List-Individu-" . date("dmY") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache

                echo $this->load->view('release/result/table_individu', $isi, TRUE);
                exit;
            }

            if ($this->input->get('export') == "3") {

                $isi['data'] = $this->model_s2_list->getRumahTanggaVerifikasi($offset, $perpage, $orderby, $where, $entri);
                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-verifikasi-" . date("dmY") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache

                echo $this->load->view('release/result/table_verifikasi_ruta', $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('release/result/result', $isi, TRUE);
            die;
        }

        $this->load->view('home_view', $isi);
    }

    public function cetak()
    {
        $no_kk = $this->input->get('no_kk');
        
        if ($no_kk) {
            // var_dump($no_kk);die;
            // $rumahtangga = $this->model_s2_mohonbdt->getRumahTangga($no_kk);
            $data = $this->model_s2_skrining->getData($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }

        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();

        // PRINT
        if($this->input->get('print')) {

            // $data = $this->model_s2_mohonbdt->getDetail($nik);
            // var_dump($rumahtangga);die;
            foreach ($data as $key => $value) {
                if (empty($value)) {
                    $value = '0';
                }
                $isi[$key] = $value;
            }
            // var_dump($isi);die;

            $isi['header'] = "MEKANISME PEMUTAKHIRAN MANDIRI (MPM)";
            $isi['sub_header'] = "FORMULIR PENDAFTARAN RUMAH TANGGA MISKIN DAN KURANG MAMPU";
            // $head = $this->load->view('release/cetak/header', $isi, TRUE);
            $html = $this->load->view('release/cetak/content_lampiran', $isi, TRUE);
            // $foot = $this->load->view('release/cetak/footer', $isi, TRUE);
            // $html .= $this->load->view('release/cetak/footer', $isi, TRUE);
            // echo "$html";die;
            // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
            $filename = "form-pengajuan-".$no_kk."-".$isi['b1_r8']."-".time();
            $pdfFilePath = FCPATH."_assets/downloads/reports/{$filename}.pdf";

            if(!is_dir(FCPATH."_assets/downloads/reports")) {
                mkdir(FCPATH."_assets/downloads/reports", 0777, TRUE);
            }
             
            if (is_file($pdfFilePath) == FALSE)
            {
                ini_set('memory_limit','128M');
                 
                $this->load->library('pdf_form');
                $pdf = $this->pdf_form->load();
                
                // $pdf->SetHTMLHeader($head); // write the HTML header into the PDF
                /*$stylesheets = array(
                    FCPATH."_assets/css/screen.css",
                    FCPATH."_assets/css/print.css"
                );*/
                /*$stylesheet = file_get_contents($stylesheets[0]);
                $stylesheet .= file_get_contents($stylesheets[1]);*/
                $stylesheet = file_get_contents(FCPATH."_assets/css/screen.css");
                $stylesheet .= file_get_contents(FCPATH."_assets/css/print.css");
                $pdf->SetHTMLFooter($foot); // write the HTML header into the PDF
                // $pdf->WriteHTML($stylesheet,1);
                $pdf->WriteHTML($html); // write the HTML into the PDF
                // $pdf->WriteHTML($html,2); // write the HTML into the PDF_arc(    , x, y, r, alpha, beta)
                $pdf->Output($pdfFilePath, 'F'); // save to file because we can
            }
             
            redirect(base_url("_assets/downloads/reports/{$filename}.pdf")); 
            die;
        }

        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']     = 'release/main_data';
        $isi['judul']       = 'Data Pengajuan';
        $isi['sub_judul']   = 'Pendaftaran Terpadu Program Penanganan Fakir Miskin';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['release_nav'] = 'active';
        // $isi['release_lama_nav']   = 'active';

        $no_kk = $this->input->get('no_kk');
        $isi['data'] = $this->model_s2_list->getData($no_kk);
        
        $data = $isi['data'];

        if ($data) {
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }
        // var_dump($data);die;
        // $isi['data'] = $this->model_s2_biodata->getAnggotaList($no_kk);
        $isi['data'] = $this->model_s2_list->getDataAnggota($no_kk);

        $this->load->view('home_view', $isi);
    }

    public function edit_rtangga()
    {
        $isi['content']     = 'release/view/form_Rumahtangga_edit';
        $isi['judul']       = 'Edit / Rubah Pengajuan';
        $isi['sub_judul']   = 'Data Rumah Tangga';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['release_nav'] = 'active';
        // $isi['release_lama_nav']   = 'active';

        $no_kk = $this->input->get('no_kk');
        
        if ($no_kk) {
            $data = $this->model_s2_list->getData($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }

        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_stat_hbkel']       = $this->model_s2_masterbdt->getstat_hbkel();
        $isi['opsi_stat_rtangga']       = $this->model_s2_masterbdt->getstat_rtangga();
        $isi['opsi_no_urut']       = $this->model_s2_masterbdt->getno_urut();
        $no_kec = $isi['no_kec'];
        $no_kel = $isi['no_kel'];
        $isi['opsi_pencacah']       = $this->model_s2_masterbdt->getpencacah($no_kel,$no_kec);


        $this->load->view('home_view', $isi);
    }

    public function lihat_rtangga()
    {
        $isi['content']     = 'release/view/form_Rumahtangga_lihat';
        $isi['judul']       = 'Lihat Pengajuan';
        $isi['sub_judul']   = 'Data Rumah Tangga';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['release_nav'] = 'active';
        // $isi['release_lama_nav']   = 'active';

        $no_kk = $this->input->get('no_kk');
        
        if ($no_kk) {
            $data = $this->model_s2_list->getData($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }

        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_stat_hbkel']       = $this->model_s2_masterbdt->getstat_hbkel();
        $isi['opsi_stat_rtangga']       = $this->model_s2_masterbdt->getstat_rtangga();
        $isi['opsi_no_urut']       = $this->model_s2_masterbdt->getno_urut();
        $no_kec = $isi['no_kec'];
        $no_kel = $isi['no_kel'];
        $isi['opsi_pencacah']       = $this->model_s2_masterbdt->getpencacah($no_kel,$no_kec);

        $this->load->view('home_view', $isi);
    }

    public function update_rtangga()
    {
        if($this->input->post())
        {
            extract($this->input->post());
            // var_dump($this->input->post());die;
            $pencacah_arr = explode("-", $pencacah);
            $kd_cacah     = $pencacah_arr[0];
            $nama_cacah   = $pencacah_arr[1];

            $rumahtangga = array(
                'petugas_rubah' =>  strtoupper($this->cu->NAMA_LENGKAP),
                // 'no_kk'         => $no_kk_rumahtangga_bdt,                    
                // 'nik_kep_rumahtangga' => $nik_rumahtangga_bdt,
                // 'kodewilayah'   => '3575'.$no_kec.$no_kel,
                // 'no_prop'       => $no_prop,
                // 'no_kab'        => $no_kab,
                // 'no_kec'        => $no_kec,
                // 'no_kel'        => $no_kel,
                // 'provinsi'      => $provinsi,
                // 'kabupaten'     => $kabupaten,
                // 'kecamatan'     => $kecamatan,
                // 'desa'          => $desa,
                // 'b1_r6'         => $b1_r6,
                // 'no_rt'         => $no_rt,
                // 'no_rw'         => $no_rw,
                // 'b1_r8'         => $b1_r8_bdt,
                'b1_r9'         => $b1_r9_bdt,
                'b1_r10'        => $b1_r10_bdt,
                'b3_r1a'        => $b3_r1a_bdt,
                'b3_r1b'        => $b3_r1b_bdt,
                'b3_r2'         => $b3_r2_bdt,
                'b3_r3'         => $b3_r3_bdt,
                'b3_r4a'        => $b3_r4a_bdt,
                'b3_r4b'        => $b3_r4b_bdt,
                'b3_r5a'        => $b3_r5a_bdt,
                'b3_r5b'        => $b3_r5b_bdt,
                'b3_r6'         => $b3_r6_bdt,
                'b3_r7'         => $b3_r7_bdt,
                'b3_r8'         => $b3_r8_bdt,
                'b3_r9a'        => $b3_r9a_bdt,
                'b3_r9b'        => $b3_r9b_bdt,
                'b3_r10'        => $b3_r10_bdt,
                'b3_r11a'       => $b3_r11a_bdt,
                // 'b3_r11b'       => $b3_r11b_bdt,
                'b3_r12'        => $b3_r12_bdt,
                'b5_r1a'        => $b5_r1a_bdt,
                'b5_r1b'        => $b5_r1b_bdt,
                'b5_r1c'        => $b5_r1c_bdt,
                'b5_r1d'        => $b5_r1d_bdt,
                'b5_r1e'        => $b5_r1e_bdt,
                'b5_r1f'        => $b5_r1f_bdt,
                'b5_r1g'        => $b5_r1g_bdt,
                'b5_r1h'        => $b5_r1h_bdt,
                'b5_r1i'        => $b5_r1i_bdt,
                'b5_r1j'        => $b5_r1j_bdt,
                'b5_r1k'        => $b5_r1k_bdt,
                'b5_r1l'        => $b5_r1l_bdt,
                'b5_r1m'        => $b5_r1m_bdt,
                'b5_r1n'        => $b5_r1n_bdt,
                'b5_r1o'        => $b5_r1o_bdt,
                // 'b5_r2a'        => $b5_r2a_bdt,
                // 'b5_r2b'        => $b5_r2b_bdt,
                'b5_r3a1'       => $b5_r3a1_bdt,
                'b5_r3a2'       => $b5_r3a2_bdt,
                'b5_r3b'        => $b5_r3b_bdt,
                'b5_r4a'        => $b5_r4a_bdt,
                'b5_r4b'        => $b5_r4b_bdt,
                'b5_r4c'        => $b5_r4c_bdt,
                'b5_r4d'        => $b5_r4d_bdt,
                'b5_r4e'        => $b5_r4e_bdt,
                'b5_r5a'        => $b5_r5a_bdt,
                'b5_r6a'        => $b5_r6a_bdt,
                'b5_r6b'        => $b5_r6b_bdt,
                'b5_r6c'        => $b5_r6c_bdt,
                'b5_r6d'        => $b5_r6d_bdt,
                'b5_r6e'        => $b5_r6e_bdt,
                'b5_r6f'        => $b5_r6f_bdt,
                'b5_r6g'        => $b5_r6g_bdt,
                'b5_r6h'        => $b5_r6h_bdt,
                'b5_r6i'        => $b5_r6i_bdt,
                'b5_r8a'        => $b5_r8a_bdt,
                'b5_r8b'        => $b5_r8b_bdt,
                'b5_r9'         => $b5_r9_bdt,
                'b5_r10'        => $b5_r10_bdt,
                'b5_r11a'       => $b5_r11a_bdt,
                'b5_r11b'       => $b5_r11b_bdt,
                'b5_r12'        => $b5_r12_bdt,
                'b5_r13'        => $b5_r13_bdt,
                'b5_r14'        => $b5_r14_bdt,
                // 'status_kesejahteraan'  => $status_kesejahteraan,
                // 'no_kk_induk'   => $no_kk_induk,                    
                // 'nik_induk'     => $nik_induk,
                // 'nama_induk'    => $nama_induk,
                'petugas_cacah' => strtoupper($nama_cacah),
                'kd_cacah'      => $kd_cacah,
                'petugas_periksa'  => strtoupper($nama_periksa),
                'kd_periksa'    => $kd_periksa,
                'hasil_cacah'   => $hasil_cacah,
                'nama_responden'=> strtoupper($nama_responden),    
                // 'nomor_urut_rumah_tangga' => $nomor_urut_rumah_tangga,
                // 'kd_pengajuan'  => $kd_pengajuan,
                // 'verifikasi'    => $verifikasi
            );
            // var_dump($rumahtangga);die;
            $no_kk = $no_kk_rumahtangga_bdt;
            $update = $this->model_s2_list->updateRumahtangga($no_kk,$tgl_cacah,$tgl_periksa,$rumahtangga);
            // $this->model_s2_biodata->updateIndividu($nik);
            
            // date('Y-m-d', strtotime($tanggal)); 
            $get_params = $this->input->post('get_params'); 
            redirect('release/edit_rtangga?'.$get_params);
            // redirect('entry/lihat?'.get_params($this->input->post('get_params'), array('nik', 'id_rt')));
            exit;
        }
    }

    public function edit_individu()
    {
        $isi['content']     = 'release/view/form_Individu_edit';
        $isi['judul']       = 'Edit / Rubah Pengajuan';
        $isi['sub_judul']   = 'Data Individu';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['release_nav'] = 'active';
        // $isi['release_lama_nav']   = 'active';

        $nik = $this->input->get('nik');
        $no_kk = $this->input->get('no_kk');
        
        if ($nik) {
            $data = $this->model_s2_list->getData($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

            $data = $this->model_s2_list->getDataIndividu($nik);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

            $data = $this->model_s2_biodata->getIndividu($nik);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

        }
        $isi['b1_r9']           = $this->model_s2_skrining->countART($no_kk);
        $isi['b1_r10']          = $this->model_s2_skrining->countKK($no_kk);
        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_stat_hbkel']       = $this->model_s2_masterbdt->getstat_hbkel();
        $isi['opsi_stat_rtangga']       = $this->model_s2_masterbdt->getstat_rtangga();
        $isi['opsi_no_urut']    = $this->model_s2_masterbdt->getno_urut();
        $isi['opsi_no_urutKK']  = $this->model_s2_masterbdt->getno_urutKK();
        $isi['opsi_omzet']      = $this->model_s2_masterbdt->getomzet();
        $isi['opsi_keberadaan'] = $this->model_s2_masterbdt->getkeberadaan();

        $this->load->view('home_view', $isi);
    }

    public function lihat_individu()
    {
        $isi['content']     = 'release/view/form_Individu_lihat';
        $isi['judul']       = 'Lihat Pengajuan';
        $isi['sub_judul']   = 'Data Individu';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['release_nav'] = 'active';
        // $isi['release_lama_nav']   = 'active';

        $nik = $this->input->get('nik');
        $no_kk = $this->input->get('no_kk');
        
        if ($nik) {
            $data = $this->model_s2_list->getData($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

            $data = $this->model_s2_list->getDataIndividu($nik);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

            $data = $this->model_s2_biodata->getIndividu($nik);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

        }
        $isi['b1_r9']           = $this->model_s2_skrining->countART($no_kk);
        $isi['b1_r10']          = $this->model_s2_skrining->countKK($no_kk);
        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_stat_hbkel']       = $this->model_s2_masterbdt->getstat_hbkel();
        $isi['opsi_stat_rtangga']       = $this->model_s2_masterbdt->getstat_rtangga();
        $isi['opsi_no_urut']    = $this->model_s2_masterbdt->getno_urut();
        $isi['opsi_no_urutKK']  = $this->model_s2_masterbdt->getno_urutKK();
        $isi['opsi_omzet']      = $this->model_s2_masterbdt->getomzet();
        $isi['opsi_keberadaan'] = $this->model_s2_masterbdt->getkeberadaan();

        $this->load->view('home_view', $isi);
    }

    public function update_individu()
    {
        if($this->input->post())
        {
            extract($this->input->post());
            // var_dump($this->input->post());die;
            $individu = array(
                'petugas_rubah' =>  strtoupper($this->cu->NAMA_LENGKAP),
                // 'kodewilayah'   => $kodewilayah,
                // 'provinsi'      => $provinsi,
                // 'kabupaten'     => $kabupaten,
                // 'kecamatan'     => $kecamatan,
                // 'desa'          => $desa,
                // 'no_prop'       => $no_prop,
                // 'no_kab'        => $no_kab,
                // 'no_kec'        => $no_kec,
                // 'no_kel'        => $no_kel,
                'no_art'        => $no_art,
                // 'nama'          => $nama,
                // 'nik'           => $nik,
                'b4_k3'         => $b4_k3,
                'b4_k4'         => $b4_k4,
                'b4_k5'         => $b4_k5,
                // 'b4_k6'         => $b4_k6,
                'b4_k7'         => $b4_k7,
                'b4_k8'         => $b4_k8,
                'b4_k9'         => $b4_k9,
                'b4_k10'        => $b4_k10,
                'b4_k11'        => $b4_k11,
                'b4_k12'        => $b4_k12,
                'b4_k13'        => $b4_k13,
                'b4_k14'        => $b4_k14,
                'b4_k15'        => $b4_k15,
                'b4_k16'        => $b4_k16,
                'b4_k17'        => $b4_k17,
                'b4_k18'        => $b4_k18,
                'b4_k19a'       => $b4_k19a,
                'b4_k19b'       => $b4_k19b,
                'b4_k20'        => $b4_k20,
                'b4_k21'        => $b4_k21,
                'b5_r5b2'       => strtoupper($b5_r5b2),
                'b5_r5b3'       => $b5_r5b3,
                'b5_r5b4'       => $b5_r5b4,
                'b5_r5b5'       => $b5_r5b5
                // 'nomor_urut_rumah_tangga' =>  $nomor_urut_rumah_tangga,
                // 'no_kk'         =>  $no_kk,
                // 'no_kk_rumahtangga' => $no_kk_rumahtangga,
                // 'nik_rumahtangga' => $nik_rumahtangga,
                // 'nama_kep_rumahtangga' => $nama_kep_rumahtangga
            );
            // var_dump($individu);die;
            $update = $this->model_s2_list->updateIndividu($nik,$individu);
            // $this->model_s2_biodata->updateIndividu($nik);
            
            // date('Y-m-d', strtotime($tanggal)); 
            $get_params = $this->input->post('get_params'); 
            redirect('release/edit_individu?'.$get_params);
            // redirect('entry/lihat?'.get_params($this->input->post('get_params'), array('nik', 'id_rt')));
            exit;
        }
    }
}
