<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        // $this->load->model('model_s2_dashboard', '', true);
        $this->load->model('model_s2_home', '', true);
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        /*$isi['content']     = 'home/dashboard';
        $isi['judul']        = 'Halaman Utama';
        $isi['sub_judul']    = '';
        $this->load->view('welcome',$isi);*/

        $isi['content']   = 'home/dashboard';
        $isi['judul']     = 'Halaman Utama';
        $isi['sub_judul'] = 'Dashboard';
        $isi['home_nav']  = 'active';
        $isi['dash_nav']  = 'active';
        $isi['user_level'] = $this->cu->USER_LEVEL;

        if ($this->input->get('download') != "") {
            // var_dump($this->input->get());die;
            $download = $this->input->get('download');
            $this->load->helper('download');
            if ($download == 3) {
                $file = file_get_contents(FCPATH."_assets/document/PanduanOperatorKelurahan.pdf");
                $name = "Panduan Operator Kelurahan.pdf";
            } elseif ($download == 2) {
                $file = file_get_contents(FCPATH."_assets/document/PanduanVerifikator.pdf");
                $name = "Panduan Verifikator.pdf";
            } else {
                // $file = base_url()."_assets/document/PanduanSidakMPM.pdf";
                $file = file_get_contents(FCPATH."_assets/document/PanduanSidakMPM.pdf");
                $name = "Panduan Sidak.pdf";
            }           
            
            // var_dump($file);die;
            force_download($name, $file);
            exit;
        }
        if ($this->cu->NO_KEC) {
            $kd_kec = $this->cu->NO_KEC;
        }
        if ($this->cu->NO_KEL) {
            $kd_kel = $this->cu->NO_KEL;
        }
        /*agregat penduduk*/
        $agregat = $this->model_s2_home->getAgregat($kd_kec, $kd_kel);
        $isi['agr_lk'] = $agregat['lk'];
        $isi['agr_pr'] = $agregat['pr'];
        $isi['agr_jml'] = $agregat['jumlah'];
        if ($this->cu->NO_KEC) {
            $isi['nama_kec'] = $agregat['nama_kec'];
            $isi['no_kec'] = $agregat['no_kec'];
        }
        if ($this->cu->NO_KEL) {
            $isi['nama_kel'] = $agregat['nama_kel'];
            $isi['no_kel'] = $agregat['no_kel'];
        }

        /*agregat kepala*/
        $agregat = $this->model_s2_home->getKepala($kd_kec, $kd_kel);
        $isi['kpl_lk'] = $agregat['lk'];
        $isi['kpl_pr'] = $agregat['pr'];
        $isi['kpl_jml'] = $agregat['jumlah'];

         /*agregat miskin*/
        $agregat = $this->model_s2_home->getMiskin($kd_kec, $kd_kel);
        $isi['miskin_lk'] = $agregat['lk'];
        $isi['miskin_pr'] = $agregat['pr'];
        $isi['miskin_jml'] = $agregat['jumlah'];

        /*agregat kepala miskin*/
        $agregat = $this->model_s2_home->getKepalaMiskin($kd_kec, $kd_kel);
        $isi['miskinkpl_lk'] = $agregat['lk'];
        $isi['miskinkpl_pr'] = $agregat['pr'];
        $isi['miskinkpl_jml'] = $agregat['jumlah'];

         /*agregat pengajuan*/
        $agregat = $this->model_s2_home->getMohon($kd_kec, $kd_kel);
        $isi['mohon_lk'] = $agregat['lk'];
        $isi['mohon_pr'] = $agregat['pr'];
        $isi['mohon_jml'] = $agregat['jumlah'];

         /*agregat proses*/
        $agregat = $this->model_s2_home->getProses($kd_kec, $kd_kel);
        $isi['proses_lk'] = $agregat['lk'];
        $isi['proses_pr'] = $agregat['pr'];
        $isi['proses_jml'] = $agregat['jumlah'];

         /*agregat verifikasi*/
        $agregat = $this->model_s2_home->getVerifikasi($kd_kec, $kd_kel);
        $isi['verifikasi_lk'] = $agregat['lk'];
        $isi['verifikasi_pr'] = $agregat['pr'];
        $isi['verifikasi_jml'] = $agregat['jumlah'];

         /*agregat penetapan*/
        $agregat = $this->model_s2_home->getPenetapan($kd_kec, $kd_kel);
        $isi['penetapan_lolos'] = $agregat['lolos'];
        $isi['penetapan_tidak_lolos'] = $agregat['tidak_lolos'];
        $isi['penetapan_jml'] = $agregat['jumlah'];

        // var_dump($isi);die;
        $this->load->view('home_view', $isi);

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}
