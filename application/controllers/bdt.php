<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bdt extends MY_Controller {
	public function __construct() {
		parent::__construct();
		// check login user
		$this->_init_logged_in();
		// $this->model_security->getsecurity();
		$this->load->model('model_s2_bdt', '', true);
		$this->load->model('model_s2_mohonbdt', '', true);
	}

	public function ajax($a = "get_rumahtangga") {
		$no_kk = $this->input->post('no_kk');
		// var_dump($no_kk);die;
		if (empty($no_kk)) {
			die('error');
		}

		$cek = $this->model_s2_mohonbdt->getRumahTangga($no_kk);
		if ($cek) {
			$data = $this->model_s2_mohonbdt->getRumahTangga($no_kk);
		} else {
			$data = $this->model_s2_bdt->getRumahTanggaKK($no_kk);
		}
		
		// var_dump($data);die
		if (!$data) {
			die('error');
		}

		if ($this->input->is_ajax_request()) {
			$json = json_encode($data);
			echo $json;
			die;
		}
	}

}
