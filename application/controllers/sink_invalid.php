<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sink_invalid extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_init_logged_in();
        $this->load->model('model_s2_biodata');
        $this->load->model('model_s2_bdt_siak');
        $this->load->model('model_s2_masterbdt');
        $this->load->model('model_s2_bdt');
    }

    public function index($a = "")
    {
        $isi['content']             = 'Sinkronisasi/invalid/main_invalid';
        $isi['judul']               = 'Data Individu NIK Invalid';
        $isi['sub_judul']           = 'List';
        // $isi['miskin_nav']          = 'active';
        $isi['sink_nav']            = 'active';
        $isi['sink_invalid_nav']    = 'active';
        
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";

            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";

            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";

            }

            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            $isi['data'] = $this->model_s2_bdt_siak->getListInvalid($offset, $perpage, $orderby, $where, $entri);
            $isi['total_data'] = $this->model_s2_bdt_siak->getListInvalidTotal($where, $entri);
            if($isi['data']){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['data']);die;

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('sink_invalid/index') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";
            
            /*// EXPORT
            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-Rumahtangga--" . date("Ym") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('kemiskinan/bdt/table/table_rumahtangga', $isi, TRUE);
                exit;
            }*/

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('sinkronisasi/invalid/result_invalid', $isi, TRUE);
            die;
        }

        $this->load->view('home_view', $isi);
    }

    public function lihat()
    {
        $isi['content']             = 'Sinkronisasi/invalid/data_keluarga';
        $isi['judul']               = 'Data Individu NIK Invalid';
        $isi['sub_judul']           = 'List';
        // $isi['miskin_nav']          = 'active';
        $isi['sink_nav']            = 'active';
        $isi['sink_invalid_nav']    = 'active';

        $entri['id_rt'] = $this->input->get('id_rt');
        $entri['b4_k4'] = $this->input->get('no_keluarga');
        
        // var_dump($no_kk);die;
        $isi['data'] = $this->model_s2_bdt->getListIndividu($offset, $perpage, $orderby, $where, $entri);
        // var_dump($isi['data'] );die;
        $this->load->view('home_view', $isi);
    }

    public function update() 
    {
        // var_dump($this->input->post());
        // echo "<hr>Result:<br><br>";
        if ($this->input->post()) {
            // var_dump($this->input->post('direct'));die;

            foreach ($this->input->post('new_nik') as $key => $value) {
                if($value == "") continue;
                $entri = array(
                    'nik' => $value
                    );
                // var_dump($entri);
                $update[$key] = $this->model_s2_bdt->updateNik($key, $entri);
                // $query[$key] = $this->db->last_query();
            }
           
        }
        $get_params = $this->input->post('get_params');
        // var_dump($get_params); die;
        if ($this->input->post('direct') == 'lihat'){
            redirect('sink_invalid/lihat?'.$get_params);
        }else{
            redirect('sink_invalid/index?'.$get_params);
        }
    
        exit;
    }

    public function delete() 
    {  
        // $isi=$this->input->get('id');
        // var_dump($isi);die;
        // $jenis = $this->input->get('jenis');
        if ($this->input->get('nik')) {   
            $nik = $this->input->get('nik');
            $this->model_s2_bdt->deleteNik($nik);
        }
        $get_params = get_params($this->input->get(), array('nik'));
        redirect('sink_invalid/lihat?'.$get_params);
        exit;
    }

}
