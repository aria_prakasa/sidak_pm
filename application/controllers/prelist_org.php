<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Prelist extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        $this->_init_wilayah();
        // $this->curpage = $this->router->fetch_class();
        // $this->load->model('model_entry');
        // $this->load->model('model_pullout');
        $this->load->model('model_s2_biodata');
        $this->load->model('model_s2_bdt');
        $this->load->model('model_s2_masterbdt');
        $this->load->model('model_s2_mohonbdt');
        $this->load->model('model_s2_skrining');
        $this->load->model('model_s2_list');
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        $isi['content']     = 'prelist/main';
        $isi['judul']       = 'Daftar Prelist';
        $isi['sub_judul']   = 'Data pendaftar miskin';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['prelist_nav'] = 'active';
        $isi['prelist_lama_nav']   = 'active';

        // var_dump($isi);die;
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());
            // var_dump($isi);die;

            if ($this->input->get('kecamatan_id') != "") {
                $entri['kd_kec'] = $this->input->get('kecamatan_id');

                if ($this->input->get('kelurahan_id') != "Pilih Kelurahan" && $this->input->get('kelurahan_id') != "") {
                    $kelurahan_code = $this->input->get('kelurahan_id');
                    $kelurahan_code_arr = explode("-", $kelurahan_code);
                    $entri['kd_kec'] = $kelurahan_code_arr[0];
                    $entri['kd_kel'] = $kelurahan_code_arr[1];
                }

                if ($this->cu->USER_LEVEL == 2) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                }
                if ($this->cu->USER_LEVEL == 3) {
                    $entri['kd_kec'] = $this->cu->NO_KEC;
                    $entri['kd_kel'] = $this->cu->NO_KEL;
                }
            }

            if ($this->input->get('inputkk') != "") {

                $no_kk = $this->input->get('inputkk', TRUE);
                $entri['no_kk'] = $no_kk;
                $isi['toggleKk'] = "checked";
            }
            if ($this->input->get('inputnik') != "") {

                $nik = $this->input->get('inputnik', TRUE);
                $entri['nik'] = $nik;
                $isi['toggleNik'] = "checked";
            }
            if ($this->input->get('inputnama') != "") {

                $n = $this->input->get('inputnama', TRUE);
                $nama = strtoupper($n);
                $entri['nama'] = $nama;
                $isi['n'] = $n;
                $isi['toggleNama'] = "checked";
            }
            if ($this->input->get('inputrw') != "") {

                $no_rw = $this->input->get('inputrw', TRUE);
                $entri['no_rw'] = $no_rw;
                $isi['toggleRw'] = "checked";
            }
            if ($this->input->get('inputrt') != "") {

                $no_rt = $this->input->get('inputrt', TRUE);
                $entri['no_rt'] = $no_rt;
                $isi['toggleRt'] = "checked";
            }
            if ($this->input->get('inputproses') != "") {

                $proses = $this->input->get('inputproses', TRUE);
                $entri['proses'] = $proses;
                $isi['toggleProses'] = "checked";
            }
            if (($this->input->get('tgl_awal') != "") && ($this->input->get('tgl_awal') != "")) {

                $tgl_awal = $this->input->get('tgl_awal', TRUE);
                $entri['tgl_awal'] = $tgl_awal;
                $tgl_akhir = $this->input->get('tgl_akhir', TRUE);
                $entri['tgl_akhir'] = $tgl_akhir;
                $isi['toggleEntri'] = "checked";
                // var_dump($tgl_awal,$tgl_akhir);die;
            }

            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('export')) {
                $perpage = 0;
            }

            // TAMPIL ONLY
            // $where = 'NO_URUT_KK = 1';
            $orderby = 'NO_KEC,NO_KEL, NO_RW, NO_RT, NO_KK, NAMA_KEP_RUMAHTANGGA';
            $isi['data'] = $this->model_s2_skrining->getRumahTanggaMohon($offset, $perpage, $orderby, $where, $entri);
            $isi['total_data'] = $this->model_s2_skrining->getRumahTanggaMohonTotal($where, $entri);

            if($isi['data']){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['data']);die;

            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('prelist/index') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";

            // EXPORT
            if ($this->input->get('export') != "") {

                // var_dump($this->input->get(), $entri);die;
                $filename = "Data-Prelist-" . date("dmY") . '.xls';
                // var_dump($this->input->post());die;
                // header('Content-Type: application/vnd.ms-excel');
                $this->output->set_content_type('xls');
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                echo $this->load->view('prelist/result/table', $isi, TRUE);
                exit;
            }

        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('prelist/result/result', $isi, TRUE);
            die;
        }

        $this->load->view('home_view', $isi);
    }

    // public function cetak()
    // {
    //     $no_kk = $this->input->get('no_kk');

    //     if ($no_kk) {
    //         // var_dump($no_kk);die;
    //         // $rumahtangga = $this->model_s2_mohonbdt->getRumahTangga($no_kk);
    //         $data = $this->model_s2_skrining->getData($no_kk);
    //         foreach ($data as $key => $value) {
    //             $isi[$key] = $value;
    //         }
    //     }

    //     $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
    //     $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
    //     $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
    //     $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
    //     $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
    //     $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
    //     $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
    //     $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
    //     $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
    //     $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
    //     $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
    //     $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
    //     $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
    //     $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
    //     $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
    //     $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
    //     $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
    //     $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
    //     $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
    //     $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
    //     $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
    //     $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
    //     $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
    //     $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
    //     $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
    //     $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
    //     $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
    //     $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
    //     $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
    //     $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
    //     $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
    //     $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
    //     $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
    //     $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
    //     $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
    //     $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
    //     $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();

    //     // PRINT
    //     if($this->input->get('print')) {

    //         // $data = $this->model_s2_mohonbdt->getDetail($nik);
    //         // var_dump($rumahtangga);die;
    //         foreach ($data as $key => $value) {
    //             if (empty($value)) {
    //                 $value = '0';
    //             }
    //             $isi[$key] = $value;
    //         }
    //         // var_dump($isi);die;

    //         $isi['header'] = "MEKANISME PEMUTAKHIRAN MANDIRI (MPM)";
    //         $isi['sub_header'] = "FORMULIR PENPrelistAN RUMAH TANGGA MISKIN DAN KURANG MAMPU";
    //         // $head = $this->load->view('Prelist/cetak/header', $isi, TRUE);
    //         $html = $this->load->view('Prelist/cetak/rumahtangga/content', $isi, TRUE);
    //         // $foot = $this->load->view('Prelist/cetak/footer', $isi, TRUE);
    //         // $html .= $this->load->view('Prelist/cetak/footer', $isi, TRUE);
    //         // echo "$html";die;
    //         // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
    //         $filename = "form-pengajuan-".$no_kk."-".$isi['nama_kep_rumahtangga']."-".time();
    //         $pdfFilePath = FCPATH."_assets/downloads/reports/{$filename}.pdf";

    //         if(!is_dir(FCPATH."_assets/downloads/reports")) {
    //             mkdir(FCPATH."_assets/downloads/reports", 0777, TRUE);
    //         }

    //         if (is_file($pdfFilePath) == FALSE)
    //         {
    //             ini_set('memory_limit','512M');

    //             $this->load->library('pdf_landscape');
    //             $pdf = $this->pdf_landscape->load();

    //             // $pdf->SetHTMLHeader($head); // write the HTML header into the PDF
    //             /*$stylesheets = array(
    //                 FCPATH."_assets/css/screen.css",
    //                 FCPATH."_assets/css/print.css"
    //             );*/
    //             /*$stylesheet = file_get_contents($stylesheets[0]);
    //             $stylesheet .= file_get_contents($stylesheets[1]);*/
    //             $stylesheet = file_get_contents(base_url("_assets/css/screen.css"));
    //             $stylesheet .= file_get_contents(base_url("_assets/css/print.css"));
    //             $pdf->SetHTMLFooter($foot); // write the HTML header into the PDF
    //             // $pdf->WriteHTML($stylesheet,1);
    //             $pdf->WriteHTML($html); // write the HTML into the PDF
    //             // $pdf->WriteHTML($html,2); // write the HTML into the PDF_arc(    , x, y, r, alpha, beta)
    //             $pdf->Output($pdfFilePath, 'F'); // save to file because we can
    //         }

    //         redirect(base_url("_assets/downloads/reports/{$filename}.pdf"));
    //         die;
    //     }


    //     $this->load->view('home_view', $isi);
    // }

    public function proses()
    {
        // var_dump($this->input->get());die;
        $isi = $this->input->get();
        $no_kk = $this->input->get('no_kk');
        $verifikasi = $this->input->get('verifikasi');
        // var_dump($no_kk);die;
        if ($no_kk) {
            $this->model_s2_skrining->prosesVerifikasi($no_kk,$verifikasi);
            /*if ($verifikasi == 2) {
                $this->model_s2_skrining->prosesVerifikasi($no_kk);
            } else if ($verifikasi == 3) {
                $this->model_s2_skrining->tolakVerifikasi($no_kk);
            }*/
            // var_dump($data);die;
        }
        $uri = $this->input->get('uri');
        $get_params = get_params($this->input->get());
        redirect('prelist/'.$uri.'?'.$get_params);
        exit;
    }

    public function lihat()
    {
        $isi['content']     = 'prelist/main_data';
        $isi['judul']       = 'Data Pengajuan';
        $isi['sub_judul']   = 'Pendaftaran Terpadu Program Penanganan Fakir Miskin';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['prelist_nav'] = 'active';
        // $isi['prelist_lama_nav']   = 'active';

        $no_kk = $this->input->get('no_kk');

        $isi['data'] = $this->model_s2_skrining->getData($no_kk);

        $data = $isi['data'];
        if ($data) {
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }



        // var_dump($data);die;
        $isi['data'] = $this->model_s2_biodata->getAnggotaPrelist($no_kk);
        // var_dump($data);die;
        // cek if exist
        $cek = $this->model_s2_list->getData($no_kk);
        if ($cek) {
            $isi['updated'] = 2;
        } else {
            $isi['updated'] = 1;
        }

        if ($this->input->get('download') != "") {
            // var_dump($this->input->get());die;
            $this->load->helper('download');
            $file = file_get_contents(FCPATH."_assets/document/PanduanVerifikator.pdf");
            $name = "Panduan Verifikator.pdf";
            // var_dump($file);die;
            force_download($name, $file);
            exit;
        }

        $this->load->view('home_view', $isi);
    }

    public function tambahKK()
    {
        $isi['content']     = 'prelist/view/tambah_keluarga';
        $isi['judul']       = 'Tambah Data Keluarga';
        $isi['sub_judul']   = 'Pendaftaran Terpadu Program Penanganan Fakir Miskin';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['prelist_nav'] = 'active';
        // $isi['prelist_lama_nav']   = 'active';
         
        $no_kk = $isi['no_kk_ruta'] = $this->input->get('no_kk');
        $isi['data_ruta'] = $this->model_s2_biodata->getAnggotaPrelist($no_kk);
        if ($this->session->flashdata('data_kk')) {
            $data_kk = $this->session->flashdata('data_kk');
            if ($data_kk) {
                foreach ($data_kk as $key => $value) {
                    $isi[$key] = $value;
                }
            }
        // var_dump($isi);die;
        }
        $isi['opsi_stat_rtangga'] = $this->model_s2_masterbdt->getstat_rtangga();
        $this->load->view('home_view', $isi);
    }

    public function cariKK()
    {
        if ($this->session->flashdata('data_tambah')) {
            $data = $this->session->flashdata('data_tambah');
            $nama_kep_rumahtangga = $data['nama_kep_rumahtangga'];
            // $no_kk = $data['no_kk_tambah'];
            $get_params = $data['get_params'];
            $this->session->set_flashdata('info', $nama_kep_rumahtangga);
            // var_dump($no_kk, $get_params);die;
        }

        if($this->input->post())
        {
            extract($this->input->post());

            // $no_kk_induk = $this->input->post('no_kk');
            // var_dump($no_kk_induk, $this->input->post());die;
            $no_kk = $this->input->post('no_kk_tambah');
            $get_params = $this->input->post('get_params');
            // var_dump($no_kk, $no_kk_induk, $get_params);die;
        }
        if ($no_kk) {
            $isi['data'] = $this->model_s2_skrining->getData($no_kk);

            $data = $isi['data'];

            if ($data) {
                foreach ($data as $key => $value) {
                    $isi[$key] = $value;
                }
            }
            // var_dump($data);die;
            
            $isi['data'] = $this->model_s2_biodata->getAnggota($no_kk);
            $this->session->set_flashdata('data_kk', $isi);
        }
        // var_dump($isi);die;
        
        redirect('prelist/tambahKK?'.$get_params);
        /*redirect('prelist/tambahKK?'.$get_params);*/
        // redirect('entry/lihat?'.get_params($this->input->post('get_params'), array('nik', 'id_rt')));
        exit;
    }

    public function proses_tambahKK()
    {
        /*if($this->input->get())
        {
            var_dump($this->input->get());die;    
            $data = $this->input->get();
            // $no_kk_induk = $data['no_kk'];
            // $no_kk_tambah = $data['no_kk_tambah'];
            // $hubungan_rumahtangga_induk = $data['hubungan_rumahtangga_induk'];
            // var_dump($no_kk_induk, $no_kk_tambah);die;
            $isi['data'] = $this->model_s2_skrining->updateTambahKK($data);

            $get_params = $this->input->get('get_params');
            redirect('prelist/tambahKK?'.$get_params);
            // redirect('entry/lihat?'.get_params($this->input->post('get_params'), array('nik', 'id_rt')));
            exit;
        }*/

        if($this->input->post())
        {
            // var_dump($this->input->post());die;    
            extract($this->input->post());

            $no_kk_induk = $this->input->post('no_kk_induk');
            $data = $this->model_s2_skrining->getDataPrelist($no_kk_induk);
            if ($data) {
                foreach ($data as $key => $value) {
                    $data[$key] = $value;
                }
            }
            $data['hubungan_rumahtangga_induk'] = $this->input->post('hubungan_rumahtangga_induk');
            $data['no_kk_tambah'] = $no_kk = $this->input->post('no_kk_tambah');
            $data['nama_kep_rumahtangga'] = $this->input->post('nama_kep_rumahtangga');
            // var_dump($data);die;
            $isi['data'] = $this->model_s2_skrining->updateTambahKK($data);
            $isi['data'] = $this->model_s2_list->updateTambahKK($no_kk);
            $get_params = $data['get_params'] = $this->input->post('get_params');
            // var_dump($data);die;
            $this->session->set_flashdata('data_tambah', $data);
            redirect('prelist/cariKK?'.$get_params);
            exit;
        }
    }

    public function daftar_rtangga()
    {
        $isi['content']     = 'prelist/view/form_RumahTangga';
        $isi['judul']       = 'Pengisian Variabel Rumahtangga';
        $isi['sub_judul']   = 'Pendaftaran Terpadu Program Penanganan Fakir Miskin';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['prelist_nav'] = 'active';
        // $isi['prelist_lama_nav']   = 'active';

        

        if ($this->input->get()) {

            $no_kk = $this->input->get('no_kk');
            $rid_rumahtangga = $this->input->get('rid_rumahtangga');

            $data = $this->model_s2_bdt->getRumahTanggaKK($rid_rumahtangga);
            if($data) {
                foreach ($data as $key => $value) {
                    if (stristr($key, '_k')) {
                        $key = str_replace('_k', '_r', $key);
                    }
                    $isi[$key] = $value;
                }
            } 
            // var_dump($isi);die;
            /*else {
                $data = $this->model_s2_bdt->getRumahTanggaKKPlus($no_kk);
            }*/

            $data = $this->model_s2_skrining->getData($no_kk);
            if($data) {
                foreach ($data as $key => $value) {
                    $isi[$key] = $value;
                }
            }
            //count jumlah KK
            $isi['b1_r9'] = $this->model_s2_skrining->countART($no_kk);
            $isi['b1_r10'] = $this->model_s2_skrining->countKK($no_kk);
            $kks = $this->model_s2_biodata->cekStatusKks($no_kk);
            if ($kks) {
                $isi['b5_r6a'] = 1;
            } else {
                $isi['b5_r6a'] = 2;
            }
            // var_dump($isi['b5_r6a']);die;
            // var_dump($isi);die;
            /*$cek_rid_rt = $this->model_s2_bdt->getRID_Rumahtangga($no_kk);
            if ($cek_rid_rt) {
                // $rid_rumahtangga = $cek_rid_rt;
                $isi['rid_rumahtangga'] = $cek_rid_rt['rid_rumahtangga'];
            } else {
                $isi['rid_rumahtangga'] = null;
            } */
            // var_dump($rid_rumahtangga);die;
        }

        $isi['nik']             = $this->input->get('nik');
        $isi['no_kk']           = $this->input->get('no_kk');
        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_no_urut']       = $this->model_s2_masterbdt->getno_urut();

        // PRINT
        if($this->input->get('print')) {
            $isi['anggota'] = $this->model_s2_biodata->getAnggota($no_kk);

            /*foreach ($data as $key => $value) {
                if (empty($value)) {
                    $value = '0';
                }
                $isi[$key] = $value;
            }*/
            // var_dump($isi);die;

            $isi['header'] = "MEKANISME PEMUTAKHIRAN MANDIRI (MPM)";
            $isi['sub_header'] = "FORMULIR PENPDAFTARAN RUMAH TANGGA MISKIN DAN KURANG MAMPU";
            // $head = $this->load->view('Prelist/cetak/header', $isi, TRUE);
            $html = $this->load->view('Prelist/cetak/content', $isi, TRUE);
            // $foot = $this->load->view('Prelist/cetak/footer', $isi, TRUE);
            // $html .= $this->load->view('Prelist/cetak/footer', $isi, TRUE);
            if($this->input->get('print_html')) {
                echo "$html";die;
            }
            // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
            $filename = "form-pengajuan-".$no_kk."-".$isi['nama_kep_rumahtangga']."-".time();
            $pdfFilePath = FCPATH."_assets/downloads/reports/{$filename}.pdf";

            if(!is_dir(FCPATH."_assets/downloads/reports")) {
                mkdir(FCPATH."_assets/downloads/reports", 0777, TRUE);
            }

            if (is_file($pdfFilePath) == FALSE)
            {
                ini_set('memory_limit','512M');

                $this->load->library('pdf_landscape');
                $pdf = $this->pdf_landscape->load();

                // $pdf->SetHTMLHeader($head); // write the HTML header into the PDF
                // $stylesheets = array(
                //     FCPATH."_assets/css/screen.css",
                //     FCPATH."_assets/css/rumahtangga.css"
                // );
                // $stylesheet = file_get_contents($stylesheets[0]);
                // $stylesheet .= file_get_contents($stylesheets[1]);
                $stylesheet = file_get_contents(base_url("_assets/css/screen.css"));
                $stylesheet .= file_get_contents(base_url("_assets/css/rumahtangga.css"));
                $pdf->SetHTMLFooter($foot); // write the HTML header into the PDF
                // $pdf->WriteHTML($stylesheet,1);
                $pdf->WriteHTML($html); // write the HTML into the PDF
                // $pdf->WriteHTML($html,2); // write the HTML into the PDF_arc(    , x, y, r, alpha, beta)
                $pdf->Output($pdfFilePath, 'F'); // save to file because we can
            }

            redirect(base_url("_assets/downloads/reports/{$filename}.pdf"));
            die;
        }

        $this->load->view('home_view', $isi);
    }

    public function submit_rtangga()
    {
        if($this->input->post())
        {
            extract($this->input->post());
            // var_dump($this->input->post());die;
            $no_kk = $no_kk_rumahtangga_bdt;

            /*$cek = $this->model_s2_bdt->getRumahTanggaKK($no_kk);
            if ($cek) {
                $kd_pengajuan = 2;
            } else {
                $kd_pengajuan = 3;
            }*/

            /*if ($nomor_urut_rumah_tangga) {
                $status_rt = '001';
            } else {
                $status_rt = '000';
            }*/

            $rumahtangga = array(
                    'petugas_entri' =>  strtoupper($this->cu->NAMA_LENGKAP),
                    'no_kk'         => $no_kk_induk,
                    'nik_kep_rumahtangga' => $nik_induk,
                    'kodewilayah'   => '3575'.$kd_kec.$kd_kel,
                    'no_prop'       => $no_prop,
                    'no_kab'        => $no_kab,
                    'no_kec'        => $no_kec,
                    'no_kel'        => $no_kel,
                    'provinsi'      => $provinsi,
                    'kabupaten'     => $kabupaten,
                    'kecamatan'     => $kecamatan,
                    'desa'          => $desa,
                    'b1_r6'         => $b1_r6,
                    'no_rt'         => $no_rt,
                    'no_rw'         => $no_rw,
                    'b1_r8'         => $b1_r8_bdt,
                    // 'b1_r9'         => $b1_r9_bdt,
                    'b1_r10'        => $b1_r10_bdt,
                    'b3_r1a'        => $b3_r1a_bdt,
                    'b3_r1b'        => $b3_r1b_bdt,
                    'b3_r2'         => $b3_r2_bdt,
                    'b3_r3'         => $b3_r3_bdt,
                    'b3_r4a'        => $b3_r4a_bdt,
                    'b3_r4b'        => $b3_r4b_bdt,
                    'b3_r5a'        => $b3_r5a_bdt,
                    'b3_r5b'        => $b3_r5b_bdt,
                    'b3_r6'         => $b3_r6_bdt,
                    'b3_r7'         => $b3_r7_bdt,
                    'b3_r8'         => $b3_r8_bdt,
                    'b3_r9a'        => $b3_r9a_bdt,
                    'b3_r9b'        => $b3_r9b_bdt,
                    'b3_r10'        => $b3_r10_bdt,
                    'b3_r11a'       => $b3_r11a_bdt,
                    'b3_r11b'       => $b3_r11b_bdt,
                    'b3_r12'        => $b3_r12_bdt,
                    'b5_r1a'        => $b5_r1a_bdt,
                    'b5_r1b'        => $b5_r1b_bdt,
                    'b5_r1c'        => $b5_r1c_bdt,
                    'b5_r1d'        => $b5_r1d_bdt,
                    'b5_r1e'        => $b5_r1e_bdt,
                    'b5_r1f'        => $b5_r1f_bdt,
                    'b5_r1g'        => $b5_r1g_bdt,
                    'b5_r1h'        => $b5_r1h_bdt,
                    'b5_r1i'        => $b5_r1i_bdt,
                    'b5_r1j'        => $b5_r1j_bdt,
                    'b5_r1k'        => $b5_r1k_bdt,
                    'b5_r1l'        => $b5_r1l_bdt,
                    'b5_r1m'        => $b5_r1m_bdt,
                    'b5_r1n'        => $b5_r1n_bdt,
                    'b5_r1o'        => $b5_r1o_bdt,
                    // 'b5_r2a'        => $b5_r2a_bdt,
                    // 'b5_r2b'        => $b5_r2b_bdt,
                    'b5_r3a1'       => $b5_r3a1_bdt,
                    'b5_r3a2'       => $b5_r3a2_bdt,
                    'b5_r3b'        => $b5_r3b_bdt,
                    'b5_r4a'        => $b5_r4a_bdt,
                    'b5_r4b'        => $b5_r4b_bdt,
                    'b5_r4c'        => $b5_r4c_bdt,
                    'b5_r4d'        => $b5_r4d_bdt,
                    'b5_r4e'        => $b5_r4e_bdt,
                    'b5_r5a'        => 2,
                    'b5_r6a'        => $b5_r6a_bdt,
                    'b5_r6b'        => $b5_r6b_bdt,
                    'b5_r6c'        => $b5_r6c_bdt,
                    'b5_r6d'        => $b5_r6d_bdt,
                    'b5_r6e'        => $b5_r6e_bdt,
                    'b5_r6f'        => $b5_r6f_bdt,
                    'b5_r6g'        => $b5_r6g_bdt,
                    'b5_r6h'        => $b5_r6h_bdt,
                    'b5_r6i'        => $b5_r6i_bdt,
                    'b5_r8a'        => $b5_r8a_bdt,
                    'b5_r8b'        => $b5_r8b_bdt,
                    'b5_r9'         => $b5_r9_bdt,
                    'b5_r10'        => $b5_r10_bdt,
                    'b5_r11a'       => $b5_r11a_bdt,
                    'b5_r11b'       => $b5_r11b_bdt,
                    'b5_r12'        => $b5_r12_bdt,
                    'b5_r13'        => $b5_r13_bdt,
                    'b5_r14'        => $b5_r14_bdt,
                    'status_kesejahteraan'  => $status_kesejahteraan,
                    // 'no_kk_induk'   => $no_kk_induk,
                    // 'nik_induk'     => $nik_induk,
                    // 'nama_induk'    => $nama_induk,
                    'petugas_cacah' => strtoupper($nama_cacah),
                    'kd_cacah'      => $kd_cacah,
                    'petugas_periksa'  => strtoupper($nama_periksa),
                    'kd_periksa'    => $kd_periksa,
                    'hasil_cacah'   => $hasil_cacah,
                    'nama_responden'=> strtoupper($nama_responden),
                    'nomor_urut_rumah_tangga' => $nomor_urut_rumah_tangga_bdt,
                    'kd_pengajuan'     => $kd_pengajuan_bdt,
                    'status_rt'     => $status_rt_bdt,
                    'rid_rumahtangga'     => $rid_rumahtangga_bdt,
                    // 'verifikasi'    => $verifikasi
                );
            // var_dump($rumahtangga);die;
            $this->model_s2_list->insertRumahTangga($no_kk,$tgl_cacah,$tgl_periksa,$rumahtangga);

            $get_params = $this->input->post('get_params');
            redirect('prelist/lihat?'.$get_params, array('no_kk'));
            exit;
        }
    }

    public function edit_rtangga()
    {
        $isi['content']     = 'prelist/view/form_Rumahtangga_edit';
        $isi['judul']       = 'Edit / Rubah Pengajuan';
        $isi['sub_judul']   = 'Data Rumah Tangga';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['prelist_nav'] = 'active';
        // $isi['prelist_lama_nav']   = 'active';

        $no_kk = $this->input->get('no_kk');
        
        if ($no_kk) {
            $data = $this->model_s2_list->getData($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }

        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_stat_hbkel']       = $this->model_s2_masterbdt->getstat_hbkel();
        $isi['opsi_stat_rtangga']       = $this->model_s2_masterbdt->getstat_rtangga();
        $isi['opsi_no_urut']       = $this->model_s2_masterbdt->getno_urut();

        $this->load->view('home_view', $isi);
    }

    public function update_rtangga()
    {
        if($this->input->post())
        {
            extract($this->input->post());
            // var_dump($this->input->post());die;
            $rumahtangga = array(
                'petugas_rubah' =>  strtoupper($this->cu->NAMA_LENGKAP),
                // 'no_kk'         => $no_kk_rumahtangga_bdt,                    
                // 'nik_kep_rumahtangga' => $nik_rumahtangga_bdt,
                // 'kodewilayah'   => '3575'.$no_kec.$no_kel,
                // 'no_prop'       => $no_prop,
                // 'no_kab'        => $no_kab,
                // 'no_kec'        => $no_kec,
                // 'no_kel'        => $no_kel,
                // 'provinsi'      => $provinsi,
                // 'kabupaten'     => $kabupaten,
                // 'kecamatan'     => $kecamatan,
                // 'desa'          => $desa,
                // 'b1_r6'         => $b1_r6,
                // 'no_rt'         => $no_rt,
                // 'no_rw'         => $no_rw,
                // 'b1_r8'         => $b1_r8_bdt,
                'b1_r9'         => $b1_r9_bdt,
                'b1_r10'        => $b1_r10_bdt,
                'b3_r1a'        => $b3_r1a_bdt,
                'b3_r1b'        => $b3_r1b_bdt,
                'b3_r2'         => $b3_r2_bdt,
                'b3_r3'         => $b3_r3_bdt,
                'b3_r4a'        => $b3_r4a_bdt,
                'b3_r4b'        => $b3_r4b_bdt,
                'b3_r5a'        => $b3_r5a_bdt,
                'b3_r5b'        => $b3_r5b_bdt,
                'b3_r6'         => $b3_r6_bdt,
                'b3_r7'         => $b3_r7_bdt,
                'b3_r8'         => $b3_r8_bdt,
                'b3_r9a'        => $b3_r9a_bdt,
                'b3_r9b'        => $b3_r9b_bdt,
                'b3_r10'        => $b3_r10_bdt,
                'b3_r11a'       => $b3_r11a_bdt,
                // 'b3_r11b'       => $b3_r11b_bdt,
                'b3_r12'        => $b3_r12_bdt,
                'b5_r1a'        => $b5_r1a_bdt,
                'b5_r1b'        => $b5_r1b_bdt,
                'b5_r1c'        => $b5_r1c_bdt,
                'b5_r1d'        => $b5_r1d_bdt,
                'b5_r1e'        => $b5_r1e_bdt,
                'b5_r1f'        => $b5_r1f_bdt,
                'b5_r1g'        => $b5_r1g_bdt,
                'b5_r1h'        => $b5_r1h_bdt,
                'b5_r1i'        => $b5_r1i_bdt,
                'b5_r1j'        => $b5_r1j_bdt,
                'b5_r1k'        => $b5_r1k_bdt,
                'b5_r1l'        => $b5_r1l_bdt,
                'b5_r1m'        => $b5_r1m_bdt,
                'b5_r1n'        => $b5_r1n_bdt,
                'b5_r1o'        => $b5_r1o_bdt,
                // 'b5_r2a'        => $b5_r2a_bdt,
                // 'b5_r2b'        => $b5_r2b_bdt,
                'b5_r3a1'       => $b5_r3a1_bdt,
                'b5_r3a2'       => $b5_r3a2_bdt,
                'b5_r3b'        => $b5_r3b_bdt,
                'b5_r4a'        => $b5_r4a_bdt,
                'b5_r4b'        => $b5_r4b_bdt,
                'b5_r4c'        => $b5_r4c_bdt,
                'b5_r4d'        => $b5_r4d_bdt,
                'b5_r4e'        => $b5_r4e_bdt,
                'b5_r5a'        => $b5_r5a_bdt,
                'b5_r6a'        => $b5_r6a_bdt,
                'b5_r6b'        => $b5_r6b_bdt,
                'b5_r6c'        => $b5_r6c_bdt,
                'b5_r6d'        => $b5_r6d_bdt,
                'b5_r6e'        => $b5_r6e_bdt,
                'b5_r6f'        => $b5_r6f_bdt,
                'b5_r6g'        => $b5_r6g_bdt,
                'b5_r6h'        => $b5_r6h_bdt,
                'b5_r6i'        => $b5_r6i_bdt,
                'b5_r8a'        => $b5_r8a_bdt,
                'b5_r8b'        => $b5_r8b_bdt,
                'b5_r9'         => $b5_r9_bdt,
                'b5_r10'        => $b5_r10_bdt,
                'b5_r11a'       => $b5_r11a_bdt,
                'b5_r11b'       => $b5_r11b_bdt,
                'b5_r12'        => $b5_r12_bdt,
                'b5_r13'        => $b5_r13_bdt,
                'b5_r14'        => $b5_r14_bdt,
                // 'status_kesejahteraan'  => $status_kesejahteraan,
                // 'no_kk_induk'   => $no_kk_induk,                    
                // 'nik_induk'     => $nik_induk,
                // 'nama_induk'    => $nama_induk,
                'petugas_cacah' => strtoupper($nama_cacah),
                'kd_cacah'      => $kd_cacah,
                'petugas_periksa'  => strtoupper($nama_periksa),
                'kd_periksa'    => $kd_periksa,
                'hasil_cacah'   => $hasil_cacah,
                'nama_responden'=> strtoupper($nama_responden),    
                // 'nomor_urut_rumah_tangga' => $nomor_urut_rumah_tangga,
                // 'kd_pengajuan'  => $kd_pengajuan,
                // 'verifikasi'    => $verifikasi
            );
            // var_dump($rumahtangga);die;
            $no_kk = $no_kk_rumahtangga_bdt;
            $update = $this->model_s2_list->updateRumahtangga($no_kk,$tgl_cacah,$tgl_periksa,$rumahtangga);
            // $this->model_s2_biodata->updateIndividu($nik);
            
            // date('Y-m-d', strtotime($tanggal)); 
            $get_params = $this->input->post('get_params'); 
            redirect('prelist/edit_rtangga?'.$get_params);
            // redirect('entry/lihat?'.get_params($this->input->post('get_params'), array('nik', 'id_rt')));
            exit;
        }
    }

    public function delete_rtangga() 
    {  
        if ($this->input->get()) {
            $no_kk = $this->input->get('no_kk');

            $this->model_s2_list->deleteRumahtangga($no_kk);
        }

        $get_params = get_params($this->input->get(), array('nik','no_kk_individu'));
        redirect('prelist/index?'.$get_params);
        exit;
    }

    public function reset_rtangga() 
    {  
        if ($this->input->get()) {
            $no_kk = $this->input->get('no_kk');

            $this->model_s2_list->resetRumahtangga($no_kk);
        }

        $get_params = get_params($this->input->get(), array('nik','no_kk_individu'));
        redirect('prelist/index?'.$get_params);
        exit;
    }

    public function daftar_individu()
    {
        $isi['content']     = 'prelist/view/form_Individu';
        $isi['judul']       = 'Pengisian Variabel Individu';
        $isi['sub_judul']   = 'Pendaftaran Terpadu Program Penanganan Fakir Miskin';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['prelist_nav'] = 'active';
        // $isi['prelist_lama_nav']   = 'active';

        $nik = $this->input->get('nik');
        $no_kk = $this->input->get('no_kk_individu');
        $no_kk_ruta = $this->input->get('no_kk');
        // var_dump($no_kk_ruta);die;

        if ($nik) {
            $data = $this->model_s2_bdt->getIndividu($nik);
            if ($data) {
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
                }
            }

            $data = $this->model_s2_biodata->getIndividu($nik);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

            $data = $this->model_s2_skrining->getData($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

            // var_dump($isi);die;
        }
        $isi['no_art_bdt']      = $isi['no_art'];   
        $isi['no_kk_ruta']      = $no_kk_ruta;
        $isi['nik']             = $this->input->get('nik');
        // $isi['b1_r9']           = $this->model_s2_skrining->countART($no_kk_ruta);
        $isi['b1_r10']          = $this->model_s2_skrining->countKK($no_kk_ruta);
        // $isi['no_kk']           = $this->input->get('no_kk');
        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_no_urut']    = $this->model_s2_masterbdt->getno_urut();
        $isi['opsi_no_urutKK']  = $this->model_s2_masterbdt->getno_urutKK();
        $isi['opsi_omzet']      = $this->model_s2_masterbdt->getomzet();
        $isi['opsi_keberadaan'] = $this->model_s2_masterbdt->getkeberadaan();

        $cek_rid_rt = $this->model_s2_list->getRID_Rumahtangga($no_kk_ruta);
        // var_dump($cek_rid_rt);die;
        if ($cek_rid_rt) {
            // $rid_rumahtangga = $cek_rid_rt;
            $isi['rid_rumahtangga'] = $cek_rid_rt['rid_rumahtangga'];
        } else {
            $isi['rid_rumahtangga'] = null;
        } 
        // var_dump($isi['rid_rumahtangga']);die;

        // set no art
        $cek = $this->model_s2_list->getNoArt($no_kk_ruta);
        if (!$cek['no_art']) {
            $isi['no_art'] = 1;
        } else {
            $isi['no_art'] = $cek['no_art']+1;
        }
        // var_dump($isi);die;

        $this->load->view('home_view', $isi);
    }

    public function submit_individu()
    {
        if($this->input->post())
        {
            extract($this->input->post());
            // var_dump($this->input->post());die;

            /*$cek = $this->model_s2_bdt->getRumahTanggaKK($no_kk_ruta);
            if ($cek) {
                $kd_pengajuan = 2;
            } else {
                $kd_pengajuan = 3;
            }*/
            // var_dump($rid_rumahtangga);die;
            if(!$rid_individu){
                /*$cek_rid_rt = $this->model_s2_bdt->getRID_Rumahtangga($nik);
                if ($cek_rid_rt) {
                    // $rid_rumahtangga = $cek_rid_rt;
                    $rid_rumahtangga = $cek_rid_rt['rid_rumahtangga'];
                } else {
                    $rid_rumahtangga = null;
                } */

                $cek_rid_id = $this->model_s2_bdt->getRID_Individu($nik);
                if ($cek_rid_id) {
                    // $rid_individu = $cek_rid_id;
                    $rid_individu = $cek_rid_id['rid_individu'];
                } else {
                    $rid_individu = null;
                }   
            }  

            if ($rid_individu) {
                $status_id = '001';
            } else {
                $status_id = '000';
            }          
            // var_dump($rid_rumahtangga);die;
            // var_dump($nomor_urut_rumah_tangga);die;
            $individu = array(
                'petugas_entri' =>  strtoupper($this->cu->NAMA_LENGKAP),
                'kodewilayah'   => $kodewilayah,
                'provinsi'      => $provinsi,
                'kabupaten'     => $kabupaten,
                'kecamatan'     => $kecamatan,
                'desa'          => $desa,
                'no_prop'       => $no_prop,
                'no_kab'        => $no_kab,
                'no_kec'        => $no_kec,
                'no_kel'        => $no_kel,
                'no_art'        => $no_art,
                'nama'          => $nama,
                'nik'           => $nik,
                'b4_k3'         => $b4_k3,
                'b4_k4'         => $b4_k4,
                'b4_k5'         => $b4_k5,
                'b4_k6'         => $b4_k6,
                'b4_k7'         => $b4_k7,
                'b4_k8'         => $b4_k8,
                'b4_k9'         => $b4_k9,
                'b4_k10'        => $b4_k10,
                'b4_k11'        => $b4_k11,
                'b4_k12'        => $b4_k12,
                'b4_k13'        => $b4_k13,
                'b4_k14'        => $b4_k14,
                'b4_k15'        => $b4_k15,
                'b4_k16'        => $b4_k16,
                'b4_k17'        => $b4_k17,
                'b4_k18'        => $b4_k18,
                'b4_k19a'       => $b4_k19a,
                'b4_k19b'       => $b4_k19b,
                'b4_k20'        => $b4_k20,
                'b4_k21'        => $b4_k21,
                'nomor_urut_rumah_tangga' =>  $nomor_urut_rumah_tangga,
                'no_kk'         => $no_kk_ruta,
                'no_kk_individu' => $no_kk,
                'b5_r5b2'        => strtoupper($b5_r5b2),
                'b5_r5b3'        => $b5_r5b3,
                'b5_r5b4'        => $b5_r5b4,
                'b5_r5b5'        => $b5_r5b5,
                'no_art_bdt'     => $no_art_bdt,   
                'no_art_plus'    => $no_art_bdt,
                'stat_hbkel'     => $shdk,
                'rid_rumahtangga'=> $rid_rumahtangga,
                'rid_individu'   => $rid_individu,
                'status_id'      => $status_id,
                // 'nik_rumahtangga' => $nik_rumahtangga,
                // 'nama_kep_rumahtangga' => $nama_kep_rumahtangga
            );
            // var_dump($individu);die;

            $no_kk = $no_kk_ruta;
            if ($b4_k21 == 1) {
                $b5_r5a = 1;
                $this->model_s2_list->updateUsaha($no_kk,$b5_r5a);
            }
            // var_dump($individu, $b5_r5a);die;
            $this->model_s2_list->insertIndividu($nik,$individu);
            $this->model_s2_list->updateJmlArt($no_kk,$no_art);
            $this->model_s2_list->updateJmlKK($no_kk,$b1_r10);
            // var_dump($rid_rumahtangga);die;
            if ($no_art == 1 && $rid_rumahtangga != null) {
                $this->model_s2_list->updateRID_Rumahtangga($no_kk,$rid_rumahtangga);
            }
            // $this->model_s2_biodata->updateIndividu($nik);

            // date('Y-m-d', strtotime($tanggal));
            $get_params = $this->input->post('get_params');
            redirect('prelist/lihat?'.$get_params, array('nik', 'no_kk_individu'));
            // redirect('entry/lihat?'.get_params($this->input->post('get_params'), array('nik', 'id_rt')));
            exit;
        }
    }

    public function edit_individu()
    {
        $isi['content']     = 'prelist/view/form_Individu_edit';
        $isi['judul']       = 'Edit / Rubah Pengajuan';
        $isi['sub_judul']   = 'Pendaftaran Terpadu Program Penanganan Fakir Miskin';
        // $isi['data_nav']    = 'active';
        $isi['verify_nav']  = 'active';
        $isi['prelist_nav'] = 'active';
        // $isi['prelist_lama_nav']   = 'active';

        $nik = $this->input->get('nik');
        $no_kk = $this->input->get('no_kk');

        // var_dump($no_kk);die;
        
        if ($nik) {
            $data = $this->model_s2_list->getData($no_kk);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

            $data = $this->model_s2_list->getDataIndividu($nik);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }

            $data = $this->model_s2_biodata->getIndividu($nik);
            foreach ($data as $key => $value) {
                $isi[$key] = $value;
            }
        }

        $isi['nik']             = $this->input->get('nik');
        $isi['no_kk']           = $this->input->get('no_kk');
        $isi['b1_r9']           = $this->model_s2_skrining->countART($no_kk);
        $isi['b1_r10']          = $this->model_s2_skrining->countKK($no_kk);
        $isi['opsi_binary']     = $this->model_s2_masterbdt->getbinary();
        $isi['opsi_binary_ii']  = $this->model_s2_masterbdt->getbinary_ii();
        $isi['opsi_b4_k3']      = $this->model_s2_masterbdt->getb4_k3();
        $isi['opsi_b4_k5']      = $this->model_s2_masterbdt->getb4_k5();
        $isi['opsi_b4_k6']      = $this->model_s2_masterbdt->getb4_k6();
        $isi['opsi_b4_k8']      = $this->model_s2_masterbdt->getb4_k8();
        $isi['opsi_b4_k9']      = $this->model_s2_masterbdt->getb4_k9();
        $isi['opsi_b4_k11']     = $this->model_s2_masterbdt->getb4_k11();
        $isi['opsi_b4_k12']     = $this->model_s2_masterbdt->getb4_k12();
        $isi['opsi_b4_k13']     = $this->model_s2_masterbdt->getb4_k13();
        $isi['opsi_b4_k14']     = $this->model_s2_masterbdt->getb4_k14();
        $isi['opsi_b4_k15']     = $this->model_s2_masterbdt->getb4_k15();
        $isi['opsi_b4_k16']     = $this->model_s2_masterbdt->getb4_k16();
        $isi['opsi_b4_k18']     = $this->model_s2_masterbdt->getb4_k18();
        $isi['opsi_b4_k20']     = $this->model_s2_masterbdt->getb4_k20();
        $isi['opsi_b4_k21']     = $this->model_s2_masterbdt->getb4_k21();
        $isi['opsi_b3_r1a']     = $this->model_s2_masterbdt->getb3_r1a();
        $isi['opsi_b3_r1b']     = $this->model_s2_masterbdt->getb3_r1b();
        $isi['opsi_b3_r3']      = $this->model_s2_masterbdt->getb3_r3();
        $isi['opsi_b3_r4a']     = $this->model_s2_masterbdt->getb3_r4a();
        $isi['opsi_b3_r5a']     = $this->model_s2_masterbdt->getb3_r5a();
        $isi['opsi_b3_r4b']     = $this->model_s2_masterbdt->getb3_r4b();
        $isi['opsi_b3_r5b']     = $this->model_s2_masterbdt->getb3_r5b();
        $isi['opsi_b3_r7']      = $this->model_s2_masterbdt->getb3_r7();
        $isi['opsi_b3_r8']      = $this->model_s2_masterbdt->getb3_r8();
        $isi['opsi_b3_r11a']    = $this->model_s2_masterbdt->getb3_r11a();
        $isi['opsi_b3_r11b']    = $this->model_s2_masterbdt->getb3_r11b();
        $isi['opsi_b3_r12']     = $this->model_s2_masterbdt->getb3_r12();
        $isi['opsi_b3_r9a']     = $this->model_s2_masterbdt->getb3_r9a();
        $isi['opsi_b3_r9b']     = $this->model_s2_masterbdt->getb3_r9b();
        $isi['opsi_b3_r10']     = $this->model_s2_masterbdt->getb3_r10();
        $isi['opsi_b5_r10']     = $this->model_s2_masterbdt->getb5_r10();
        $isi['opsi_b5_r12']     = $this->model_s2_masterbdt->getb5_r12();
        $isi['opsi_b5_r13']     = $this->model_s2_masterbdt->getb5_r13();
        $isi['opsi_b5_r14']     = $this->model_s2_masterbdt->getb5_r14();
        $isi['opsi_status_kesejahteraan']   = $this->model_s2_masterbdt->getstatus_kesejahteraan();
        $isi['opsi_hasil_pencacahan']       = $this->model_s2_masterbdt->gethasil_pencacahan();
        $isi['opsi_no_urut']    = $this->model_s2_masterbdt->getno_urut();
        $isi['opsi_no_urutKK']  = $this->model_s2_masterbdt->getno_urutKK();
        $isi['opsi_omzet']      = $this->model_s2_masterbdt->getomzet();
        $isi['opsi_keberadaan'] = $this->model_s2_masterbdt->getkeberadaan();

        $this->load->view('home_view', $isi);
    }

    public function update_individu()
    {
        if($this->input->post())
        {
            extract($this->input->post());
            // var_dump($this->input->post());die;
            $individu = array(
                'petugas_rubah' =>  strtoupper($this->cu->NAMA_LENGKAP),
                // 'kodewilayah'   => $kodewilayah,
                // 'provinsi'      => $provinsi,
                // 'kabupaten'     => $kabupaten,
                // 'kecamatan'     => $kecamatan,
                // 'desa'          => $desa,
                // 'no_prop'       => $no_prop,
                // 'no_kab'        => $no_kab,
                // 'no_kec'        => $no_kec,
                // 'no_kel'        => $no_kel,
                'no_art'        => $no_art,
                // 'nama'          => $nama,
                // 'nik'           => $nik,
                'b4_k3'         => $b4_k3,
                'b4_k4'         => $b4_k4,
                'b4_k5'         => $b4_k5,
                // 'b4_k6'         => $b4_k6,
                'b4_k7'         => $b4_k7,
                'b4_k8'         => $b4_k8,
                'b4_k9'         => $b4_k9,
                'b4_k10'        => $b4_k10,
                'b4_k11'        => $b4_k11,
                'b4_k12'        => $b4_k12,
                'b4_k13'        => $b4_k13,
                'b4_k14'        => $b4_k14,
                'b4_k15'        => $b4_k15,
                'b4_k16'        => $b4_k16,
                'b4_k17'        => $b4_k17,
                'b4_k18'        => $b4_k18,
                'b4_k19a'       => $b4_k19a,
                'b4_k19b'       => $b4_k19b,
                'b4_k20'        => $b4_k20,
                'b4_k21'        => $b4_k21,
                'b5_r5b2'        => strtoupper($b5_r5b2),
                'b5_r5b3'        => $b5_r5b3,
                'b5_r5b4'        => $b5_r5b4,
                'b5_r5b5'        => $b5_r5b5
                // 'nomor_urut_rumah_tangga' =>  $nomor_urut_rumah_tangga,
                // 'no_kk'         =>  $no_kk,
                // 'no_kk_rumahtangga' => $no_kk_rumahtangga,
                // 'nik_rumahtangga' => $nik_rumahtangga,
                // 'nama_kep_rumahtangga' => $nama_kep_rumahtangga
            );
            // var_dump($individu);die;
            $update = $this->model_s2_list->updateIndividu($nik,$individu);
            // $this->model_s2_biodata->updateIndividu($nik);
            
            // date('Y-m-d', strtotime($tanggal)); 
            $get_params = $this->input->post('get_params'); 
            redirect('prelist/edit_individu?'.$get_params, array('nik'));
            // redirect('entry/lihat?'.get_params($this->input->post('get_params'), array('nik', 'id_rt')));
            exit;
        }
    }

    public function delete_individu() 
    {  
        if ($this->input->get()) {
            $nik = $this->input->get('nik');

            $this->model_s2_list->deleteIndividu($nik);
        }

        $get_params = get_params($this->input->get(), array('nik'));
        redirect('prelist/lihat?'.$get_params);
        exit;
    }

}
