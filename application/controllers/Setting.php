<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(-1);
        // check login user
        $this->_init_logged_in();
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        if ($this->input->post('nip') || $this->input->post('nama_lengkap')) {
            $this->form_validation->set_rules('nama_lengkap', 'Nama', 'trim|required|min_length[2]|max_length[200]');
            $this->form_validation->set_rules('nip', 'N I P', 'trim|required|exact_length[18]');
        }
        if ($this->input->post('old_password') || $this->input->post('new_password') || $this->input->post('conf_new_password')) {
            $this->form_validation->set_rules('old_password', 'Password Sekarang', 'trim|required|min_length[6]|callback_password_check');
            $this->form_validation->set_rules('new_password', 'Password Baru', 'trim|required|min_length[6]');
            $this->form_validation->set_rules('conf_new_password', 'Ulangi Password Baru', 'trim|required|min_length[6]|matches[new_password]');
        }
        $this->form_validation->set_error_delimiters('', '<br>');

        if ($this->form_validation->run()) {
            // var_dump($this->OU->row);die;
            if ($this->input->post('old_password') || $this->input->post('new_password') || $this->input->post('conf_new_password')) {
                $arr = array('PASSWORD' => md5($this->input->post('new_password')));
                if ($this->OU->edit($arr)) {
                    $this->session->set_flashdata('success', 'Password anda berhasil dirubah!');
                    user_logout();
                } else {
                    $this->session->set_flashdata('error', 'Password anda gagal dirubah!');
                }
            } elseif ($this->input->post('nip') || $this->input->post('nama_lengkap')) {
                $arr        = array('NAMA_LENGKAP' => $this->input->post('nama_lengkap'));
                $arr['NIP'] = $this->input->post('nip');
                if ($this->OU->edit($arr)) {
                    $this->session->set_flashdata('success', 'Profil anda berhasil dirubah!');
                } else {
                    $this->session->set_flashdata('error', 'Password anda gagal dirubah!');
                }
            }

            redirect('setting');
            exit;
        }

        $isi['content']   = 'setting';
        $isi['judul']     = 'Pengaturan';
        $isi['sub_judul'] = 'Ubah Password';

        $isi['nama_lengkap'] = $this->cu->NAMA_LENGKAP;
        $isi['nip']          = $this->cu->NIP;

        $this->load->view('home_view', $isi);
    }

    public function password_check($str)
    {
        if (md5($str) != $this->cu->password) {
            $this->form_validation->set_message('password_check', 'Password sekarang tidak valid!"');
            return false;
        } else {
            return true;
        }
    }
}
