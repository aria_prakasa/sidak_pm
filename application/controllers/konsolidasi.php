<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Konsolidasi extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // error_reporting(-1);
        // $this->output->enable_profiler(TRUE);
        // check login user
        $this->_init_logged_in();
        $this->_init_wilayah();
        // $this->curpage = $this->router->fetch_class();
        // $this->load->model('model_entry');
        // $this->load->model('model_pullout');
        $this->load->model('model_s2_biodata');
        $this->load->model('model_s2_bdt');
        $this->load->model('model_s2_masterbdt');
        $this->load->model('model_s2_mohonbdt');
        $this->load->model('model_s2_skrining');
        $this->load->model('model_s2_list');
        // $this->model_security->getsecurity();
    }

    public function index()
    {
        $isi['content']     = 'konsolidasi/main';
        $isi['judul']       = 'Konsolidasi Data MPM';
        $isi['sub_judul']   = 'Data Prelist Terverifikasi';
        // $isi['data_nav']    = 'active';
        $isi['konsolidasi_nav']  = 'active';
        
        // var_dump($isi);die;
        if ($this->input->get())
        {
            $isi = array_merge($isi, $this->input->get());
            // var_dump($isi);die;

            if ($this->input->get('inputtable') != "") {

                $table_id =  $isi['table_id'] = $this->input->get('inputtable', TRUE);
            }
            if (($this->input->get('tgl_awal') != "") && ($this->input->get('tgl_awal') != "")) {

                $tgl_awal = $this->input->get('tgl_awal', TRUE);
                $entri['tgl_awal'] = $tgl_awal;
                $tgl_akhir = $this->input->get('tgl_akhir', TRUE);
                $entri['tgl_akhir'] = $tgl_akhir;
            }

            $GET = $this->input->get();

            $offset = 0;
            if ($this->input->get('page') != '') {
                $offset = $isi['page'] = $this->input->get('page');
            }

            $perpage = 10;

            if ($this->input->get('konsolidate')) {
                $perpage = 0;
            }

            if ($table_id == 1) {
                $orderby = 'NO_KEC, NO_KEL, NO_RW, NO_RT, NO_KK, B1_R6';
                $isi['data'] = $data = $this->model_s2_list->getRumahTanggaRelease($offset, $perpage, $orderby, $where, $entri);
                $isi['total_data'] = $this->model_s2_list->getRumahTanggaReleaseTotal($where, $entri);
            } else if ($table_id == 2) {
                $orderby = 'NO_KEC, NO_KEL, NO_KK, NO_ART';
                $isi['data'] = $data = $this->model_s2_list->getIndividuRelease($offset, $perpage, $orderby, $where, $entri);
                $isi['total_data'] = $this->model_s2_list->getIndividuReleaseTotal($where, $entri);
            }
          
            if($isi['data']){
                $isi['show']=1;
            }else{
                $isi['show']=0;
            }
            // var_dump($isi['data']);die;
            $total_data = $isi['total_data'];
            // $isi['jml_data'] = 1;
            $this->load->library('pagination');

            $config = get_pagination_template();
            $config['base_url'] = site_url('konsolidasi/index') . "?" . get_params($GET, array('page'));
            $config['total_rows'] = $total_data;
            $config['per_page'] = $perpage;
            // $config['uri_segment'] = 3; // iki gawe nek metod E URL bukan get
            $config['num_links'] = 5;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';

            $this->pagination->initialize($config);

            $isi['pagination'] = $this->pagination->create_links();
            // $isi['pagination'] = "OK";
            
            // KONSOLIDASI KIRIM
            if ($this->input->get('konsolidate') != "") {
                if ($table_id == 1) {
                    $type = 'HH';
                    foreach ($data as $row) {
                        $row = keysToLower($row);
                        extract((array) $row);
                        $posts[] = array(
                            'kodewilayah'   =>  $kodewilayah,
                            'provinsi'  =>  $provinsi,
                            'kabupaten' =>  $kabupaten,
                            'kecamatan' =>  $kecamatan,
                            'desa'  =>  $desa,
                            'b1_k5' =>  'NO.RT '. $no_rt .' NO.RW '. $no_rw,
                            'b1_k6' =>  $b1_r6,
                            'b1_k7' =>  $nomor_urut_rumah_tangga,
                            'b1_k8' =>  $b1_r8,
                            'b1_k9' =>  $b1_r9,
                            'b1_k10'    =>  $b1_r10,
                            'b3_k1a'    =>  $b3_r1a,
                            'b3_k1b'    =>  $b3_r1b,
                            'b3_k2' =>  $b3_r2,
                            'b3_k3' =>  $b3_r3,
                            'b3_k4a'    =>  $b3_r4a,
                            'b3_k4b'    =>  $b3_r4b,
                            'b3_k5a'    =>  $b3_r5a,
                            'b3_k5b'    =>  $b3_r5b,
                            'b3_k6' =>  $b3_r6,
                            'b3_k7' =>  $b3_r7,
                            'b3_k8' =>  $b3_r8,
                            'b3_k9a'    =>  $b3_r9a,
                            'b3_k9b'    =>  $b3_r9b,
                            'b3_k10'    =>  $b3_r10,
                            'b3_k11a'   =>  $b3_r11a,
                            'b3_k11b'   =>  $b3_r11b,
                            'b3_k12'    =>  $b3_r12,
                            'b5_k1a'    =>  $b5_r1a,
                            'b5_k1b'    =>  $b5_r1b,
                            'b5_k1c'    =>  $b5_r1c,
                            'b5_k1d'    =>  $b5_r1d,
                            'b5_k1e'    =>  $b5_r1e,
                            'b5_k1f'    =>  4,
                            'b5_k1g'    =>  $b5_r1g,
                            'b5_k1h'    =>  $b5_r1h,
                            'b5_k1i'    =>  $b5_r1i,
                            'b5_k1j'    =>  $b5_r1j,
                            'b5_k1k'    =>  $b5_r1k,
                            'b5_k1l'    =>  $b5_r1l,
                            'b5_k1m'    =>  $b5_r1m,
                            'b5_k1n'    =>  $b5_r1n,
                            'b5_k1o'    =>  $b5_r1o,
                            'b5_k3a1'   =>  $b5_r3a1,
                            'b5_k3a2'   =>  $b5_r3a2,
                            'b5_k3b'    =>  $b5_r3b,
                            'b5_k4a'    =>  $b5_r4a,
                            'b5_k4b'    =>  $b5_r4b,
                            'b5_k4c'    =>  $b5_r4c,
                            'b5_k4d'    =>  $b5_r4d,
                            'b5_k4e'    =>  $b5_r4e,
                            'b5_k5a'    =>  $b5_r5a,
                            'b5_k6a'    =>  $b5_r6a,
                            'b5_k6b'    =>  $b5_r6b,
                            'b5_k6c'    =>  $b5_r6c,
                            'b5_k6d'    =>  $b5_r6d,
                            'b5_k6e'    =>  $b5_r6e,
                            'b5_k6f'    =>  $b5_r6f,
                            'b5_k6g'    =>  $b5_r6g,
                            'b5_k6h'    =>  $b5_r6h,
                            'b5_k6i'    =>  $b5_r6i,
                            'no_urut_rt'    =>  $nomor_urut_rumah_tangga,
                            'status_rt' =>  $status_rt,
                            'no_kk' =>  $no_kk,
                            'nik_kep_rumahtangga'   =>  $nik_kep_rumahtangga,
                            'tgl_cacah' =>  $tgl_cacah,
                            'petugas_cacah' =>  $petugas_cacah,
                            'tgl_periksa'   =>  $tgl_periksa,
                            'petugas_periksa'   =>  $petugas_periksa,
                            'hasil_cacah'   =>  $hasil_cacah,
                            'nama_responden'    =>  $nama_responden,
                            'tgl_entri' =>  $tgl_entri,
                            'petugas_entri' =>  $petugas_entri,
                            'tgl_rubah' =>  $tgl_rubah,
                            'petugas_rubah' =>  $petugas_rubah,
                            'rid_rumahtangga' =>  $rid_rumahtangga,
                        );
                    }
                    // $data_string = json_encode(array('household'=>$posts));
                } else if ($table_id == 2) {
                    $type = 'ID';
                    foreach ($data as $row) {
                        $row = keysToLower($row);
                        extract((array) $row);
                        $posts[] = array(
                            'kodewilayah'   =>  $kodewilayah,
                            'provinsi'  =>  $provinsi,
                            'kabupaten' =>  $kabupaten,
                            'kecamatan' =>  $kecamatan,
                            'desa'  =>  $desa,
                            'no_art'    =>  $no_art,
                            'nama'  =>  $nama,
                            'nik'   =>  $nik,
                            'b4_k3' =>  $b4_k3,
                            'b4_k4' =>  $b4_k4,
                            'b4_k5' =>  $b4_k5,
                            'b4_k6' =>  $b4_k6,
                            'b4_k7' =>  $b4_k7,
                            'b4_k8' =>  $b4_k8,
                            'b4_k9' =>  $b4_k9,
                            'b4_k10'    =>  $b4_k10,
                            'b4_k11'    =>  $b4_k11,
                            'b4_k12'    =>  $b4_k12,
                            'b4_k13'    =>  $b4_k13,
                            'b4_k14'    =>  $b4_k14,
                            'b4_k15'    =>  $b4_k15,
                            'b4_k16'    =>  $b4_k16,
                            'b4_k17'    =>  $b4_k17,
                            'b4_k18'    =>  $b4_k18,
                            'b4_k19a'   =>  $b4_k19a,
                            'b4_k19b'   =>  $b4_k19b,
                            'b4_k20'    =>  $b4_k20,
                            'b4_k21'    =>  $b4_k21,
                            'b5_k5b2'   =>  $b5_k5b2,
                            'b5_k5b3'   =>  $b5_k5b3,
                            'b5_k5b4'   =>  $b5_k5b4,
                            'b5_k5b5'   =>  $b5_k5b5,
                            'no_urut_rt'    =>  $nomor_urut_rumah_tangga,
                            'no_kk'     =>  $no_kk,
                            'tgl_entri' =>  $tgl_entri,
                            'petugas_entri' =>  $petugas_entri,
                            'tgl_rubah' =>  $tgl_rubah,
                            'petugas_rubah' =>  $petugas_rubah,
                            'no_kk_individu'=>  $no_kk_individu,
                            'no_art_bdt'    =>  $no_art_bdt,
                            'rid_rumahtangga'    =>  $rid_rumahtangga,
                            'rid_individu'  =>  $rid_individu,
                        );
                    }
                    // $data_string = json_encode(array('individu'=>$posts));
                }
                $pass = 'bUyzBoZzmQa';
                $url = 'http://43.231.128.10/mpm/api/postto.php?id=3575&type='.$type.'&pass='.$pass;
                // $url = 'http://43.231.128.10/dev/api/postto.php?id=3175&type=HH&pass=12345654321';
                
                $ch=curl_init($url);
                // $data_string = json_encode(array('household'=>$posts));
                $data_string = json_encode($posts);
                // $data_string = urlencode(json_encode($data));
                // var_dump($url,$data_string);die;
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

                if ($table_id == 1) {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, array('household'=>$data_string));
                } else if ($table_id == 2) {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, array('individual'=>$data_string));
                } 
                $result = curl_exec($ch);
                curl_close($ch);
                exit;
            }
        } else {
            $isi['error'] = 1;
        }

        if ($this->input->is_ajax_request()) {
            echo $this->load->view('konsolidasi/result', $isi, TRUE);
            die;
        }

        $this->load->view('home_view', $isi);
    }

}
