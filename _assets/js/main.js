$(window).load(function(){
  "use strict";
});

$(function() {
  "use strict";
  
  form_validate_init();

  // add form-control class auto
  $('input:not(:checkbox):not(:radio), select, textarea').addClass('form-control');

  // setup select
  SELECT2.init();

    // Datemask mm/yyyy
  $(".input-mask").inputmask("mm/yyyy", {"placeholder": "mm/yyyy"});

});