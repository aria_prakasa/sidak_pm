var form_validate_init = function() {
    if( $("form.jvalidate").length > 0 ) {
        $( "form.jvalidate" ).each( function() {
            $( this ).validate();
        } );
    }
};

var SELECT2 = {
  'init' : function()
  {
    SELECT2.destroy('.select2me');
    $('select:not(.original)').addClass('select2me');
    $('.select2me').select2({
      allowClear: true
    });
  },
  'destroy' : function(sels)
  {
    $(sels).each(function() {
      var element = $(this);
      $(this).select2('destroy');
    });
  },
  'trigger' : function(sels)
  {
    $(sels).each(function() {
      $(this).trigger("change");
    });
  }
};